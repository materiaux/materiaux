{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "\\DeclareMathOperator\\tr{tr}\n",
    "\\newcommand{\\bF}{\\mathbf{F}}\n",
    "\\newcommand{\\bC}{\\mathbf{C}}\n",
    "\\newcommand{\\bc}{\\mathbf{c}}\n",
    "\\newcommand{\\bA}{\\mathbf{A}}\n",
    "\\newcommand{\\bS}{\\mathbf{S}}\n",
    "\\newcommand{\\Bsigma}{{\\boldsymbol{\\sigma}}}\n",
    "\\newcommand{\\pdiff}[2]{\\frac{\\partial#1}{\\partial#2}}\n",
    "$$\n",
    "\n",
    "\n",
    "# Viscoelasticity\n",
    "\n",
    "Here we present the viscoelasticity model of LeTallec et al. [1] formulated in the framework of \n",
    "Generalized Standard Materials (GSM; see, e.g., [3]) along the lines of [2].\n",
    "Other relevant publications are, among others, those of Reese and Govindjee [4] and Bergström and Boyce [5].\n",
    "For a more complete account of what follows we refer to the recent work [6].\n",
    "\n",
    "In the present context of finite-strain viscoelasticity we employ a multiplicative decomposition of $\\bF$\n",
    "\\begin{equation}\n",
    "\\label{eq:visco_multiplicative_split}\n",
    "\\bF = \\bF^\\text{e} \\cdot \\bF^\\text{v} \n",
    "\\tag{1}\n",
    "\\end{equation}\n",
    "where $\\bF$ is the deformation gradient, $\\bF^\\text{e}$ the *elastic* part and \n",
    "$\\bF^\\text{v}$ the *viscous* part of the non-equlibrium branch that governs the viscous\n",
    "part of the material response.\n",
    "\n",
    "Next we introduce a couple of kinematic quantities based on $\\bF$ and its two\n",
    "components $\\bF^\\text{e}$ and $\\bF^\\text{v}$:\n",
    "\\begin{align}\n",
    "\\bC &= \\bF^\\text{T} \\cdot \\bF \\tag{2}\\\\\n",
    "\\bC^\\text{v} &= \\left(\\bF^\\text{v}\\right)^\\text{T} \\cdot \\bF^\\text{v} \\tag{3}\\\\\n",
    "\\bc^\\text{e} &= \\bF^\\text{e} \\cdot \\left(\\bF^\\text{e}\\right)^\\text{T} = \\bF \\cdot \\left(\\bC^\\text{v}\\right)^{-1} \\bF^\\text{T} \\tag{4}\n",
    "\\end{align}\n",
    "\n",
    "Based on these, we define the equilibrium energy density $\\Psi^\\text{eq}(I_1(\\bC))$, \n",
    "the non-equilibrium energy $\\Psi^\\text{neq}(I_1(\\bc^\\text{e}))$ which are of the general form \n",
    "\\begin{equation}\n",
    "\\Psi(I_1) = \n",
    "\\sum_i \n",
    "\\frac{3^{1 - \\alpha_i}}{2 \\alpha_i} G_i\n",
    "\\left(I_1^{\\alpha_i} - 3^{\\alpha_i} \\right)\n",
    "\\tag{5}\n",
    "\\quad\\text{with}\\quad\n",
    "I_1(\\bA) = \\tr \\bA \\quad\n",
    "\\end{equation}\n",
    "under the constraint of *incompressibility*.\n",
    "\n",
    "## Stress response\n",
    "\n",
    "In this framework the first Piola-Kirchfoff stress tensors $\\bS$ is obtained as \n",
    "\\begin{align}\n",
    "\\bS = \\pdiff{\\Psi^\\text{eq} + \\Psi^\\text{neq}}{\\bF} - p  \\cdot \\bF^\\text{-T}\n",
    "\\end{align}\n",
    "$p$ is the hydrostatic pressure. \n",
    "\n",
    "Accordingly, the Cauchy stress tensors is \n",
    "\\begin{align}\n",
    "\\Bsigma = \\pdiff{\\Psi^\\text{eq} + \\Psi^\\text{neq}}{\\bF} \\cdot \\bF^\\text{T} - p \\mathbf{1}\n",
    "\\end{align}\n",
    "where $\\mathbf{1}$ is the second-order indentity tensor. \n",
    "\n",
    "## Evolution equation\n",
    "\n",
    "Next, we introduce the dissipation potential\n",
    "\\begin{align}\n",
    "\\Phi(\\dot{\\bC}^\\text{v}) = \\frac{\\eta}{8} \n",
    "[\\bC^\\text{v}]^{ik} \\, [\\bC^\\text{v}]^{jl} \\, \\dot{C}^\\text{v}_{ij} \\, \\dot{C}^\\text{v}_{kl}\n",
    "\\tag{6}\n",
    "\\quad \\text{with} \\quad\n",
    "[\\bC^\\text{v}]^{ik} = [(\\bC^\\text{v})^{-1}]^{ik} .\n",
    "\\end{align}\n",
    "\n",
    "The resulting evolution reads\n",
    "\\begin{align}\n",
    "&\\dot{\\bC}^\\text{v} = \n",
    "-\\frac{4}{\\eta} \\mathbb{C} \\cdot \n",
    "\\pdiff{\\Psi^\\text{neq}(I_1(\\hat{\\bc}^\\text{e}))}{\\bC^\\text{v}}\n",
    "\\cdot \\mathbb{C}\n",
    "=\n",
    "\\frac{4}{\\eta}\n",
    "\\pdiff{\\Psi^\\text{neq}(I_1(\\hat{\\bc}^\\text{e}))}{\\mathbb{C}}\n",
    "\\quad\\text{with}\\quad  \\mathbb{C}=(\\bC^\\text{v})^{-1}\n",
    "\\tag{7}\n",
    "\\end{align}\n",
    "under the constraint\n",
    "\\begin{align}\n",
    "&\\det \\bC^\\text{v} = \\det \\bC .\n",
    "\\tag{8}\n",
    "\\end{align}\n",
    "\n",
    "For the time discretization we employ a first-order discretization to \n",
    "$\\dot{\\bC}^\\text{v}$, i.e.\n",
    "\\begin{equation}\n",
    "\\dot{\\bC}^\\text{v} \\doteq \\frac{\\bC^\\text{v}_{n+1} - \\bC^\\text{v}_n}{t_{n+1} - t_n} .\n",
    "\\tag{9}\n",
    "\\end{equation}\n",
    "Next one has to decide whether $\\bC^\\text{v}$ in $\\Psi^\\text{neq}$ is supposed to be evaluated\n",
    "at time $t_{n+1}$ or $t_{n}$, whereby one is quite free to choose individually for each occurance\n",
    "of $\\bC^\\text{v}$. This will be specified in the code below.\n",
    "\n",
    "\n",
    "## The example under consideration\n",
    "\n",
    "Similar to the notebook on [hyperelasticity](./1-hyperelasticity.ipynb) we consider stretch-controlled uniaxial tension in 3-direction.\n",
    "What is added to the procedure for purely elastic case is that now we have to solve the evolution equation for a given stretch $\\lambda_3$\n",
    "before the evaluation of $\\bS$ and $\\Bsigma$, respectively. Thus, this example shows how to setup a function that provides the solution\n",
    "to the evolution equation as well as a general loop to run a stretch loading cycle.\n",
    "\n",
    "\n",
    "## References\n",
    "\n",
    "1. LeTallec, P., Rahier, C. & Kaiss, A. [Three-dimensional incompressible viscoelasticity in large strains: Formulation and numerical approximation. Computer Methods in Applied Mechanics and Engineering 109(3), 233–258 (1993)](http://www.sciencedirect.com/science/article/pii/004578259390080H).\n",
    "2. Kumar, A. & Lopez-Pamies, O. [On the two-potential constitutive modeling of rubber viscoelastic materials. Comptes Rendus Mécanique 344, 102–112 (2016)](http://www.sciencedirect.com/science/article/pii/S1631072115001448).\n",
    "3. Germain, P., Nguyen, Q. S. & Suquet, P. [Continuum Thermodynamics. *Journal of Applied Mechanics* 50, 1010–1020 (1983)](http://dx.doi.org/10.1115/1.3167184).\n",
    "4. Reese, S. & Govindjee, S. [A theory of finite viscoelasticity and numerical aspects. International Journal of Solids and Structures 35, 3455–3482 (1998)](http://www.sciencedirect.com/science/article/pii/S0020768397002175).\n",
    "5. Bergström, J. S. & Boyce, M. C. [Constitutive modeling of the large strain time-dependent behavior of elastomers. Journal of the Mechanics and Physics of Solids 46, 931–954 (1998)](http://www.sciencedirect.com/science/article/pii/S0022509697000756).\n",
    "6. Rambausek, M., Mukherjee, D. & Danas, K. A computational framework for ferromagnetism and viscoelasticity at finite strains, *in preparation* (2021).\n",
    "\n",
    "\n",
    "\n",
    "## Python imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib nbagg\n",
    "# FOR JUPYTER LAB: This needs the ipympl jupyter lab extension (https://github.com/matplotlib/ipympl)\n",
    "# %matplotlib widget"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import matplotlib as mpl\n",
    "import pathlib"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import materiaux.cpp\n",
    "import materiaux.modeling.gsm as gsm\n",
    "import materiaux.codegeneration.jit as jit\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implementation of viscoelasticity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### General definitions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The tangent map (aka deformation gradient)\n",
    "F = gsm.State(\"F\", shape=(3, 3))\n",
    "\n",
    "# The viscous right Cauchy-Green tensor (see Eq. (3))\n",
    "Cv = gsm.InternalState(\"Cv\", shape=(3, 3), symmetry=True)\n",
    "\n",
    "# Current time\n",
    "t = gsm.Time(\"t\")\n",
    "\n",
    "# Previous time\n",
    "t_n = gsm.HistoryTime(t)\n",
    "\n",
    "# The previous viscous right Cauchy-Green tensor (see Eq. (3))\n",
    "Cv_n = gsm.HistoryState(Cv)\n",
    "\n",
    "# Lagrange multiplier for the constraint Eq. (8)\n",
    "pv = gsm.InternalState(\"pv\")\n",
    "\n",
    "# The time rate of the viscous right Cauchy-Green tensor (see Eq. (9))\n",
    "Cv_dot = gsm.TimeRate(Cv, (Cv - Cv_n) / (t - t_n))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Model Parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# For the equilibrium energy contribution Psi^eq...\n",
    "G_list = [gsm.Parameter(\"G_1\"), gsm.Parameter(\"G_3\")]\n",
    "alpha_list = [1, 3]\n",
    "\n",
    "# For the non-equilibrium energy contribution Psi^neq\n",
    "m_list = [gsm.Parameter(\"m\")]\n",
    "a_list = [1]\n",
    "\n",
    "# For the dissipation potential\n",
    "eta = gsm.Parameter(\"eta\") # viscosity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### General energy densities"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The right Cauchy-Green tensor (see Eq. (3)) ...\n",
    "C = gsm.transpose(F) * F\n",
    "# ... and its isochoric component\n",
    "C_iso = C * gsm.det(C)**(-1/3)\n",
    "# First invariant, i.e. the trace of 'C_iso'\n",
    "I_1_iso = gsm.tr(C_iso)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Implementation of Eq. (5)\n",
    "def Psi_deviatoric(I_1_iso, G_list, alpha_list):\n",
    "    return sum([\n",
    "        3 ** (1 - alpha_i) / (2 * alpha_i) * G_i * (I_1_iso ** alpha_i - 3 ** alpha_i) if alpha_i != 1 else \n",
    "            1 / 2 * G_i * (I_1_iso - 3) \n",
    "        for G_i, alpha_i in zip(G_list, alpha_list)\n",
    "    ])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The equilibrium energy contribution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Psi_eq = Psi_deviatoric(gsm.tr(C), G_list, alpha_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variant 1: direct implementation of the first equality in Eq. (7)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The elastic left Cauchy-Green tensor (see Eq. (3))...\n",
    "ce = F * gsm.inv(Cv) * gsm.transpose(F)\n",
    "# ... and its isochoric part.\n",
    "ce_iso = ce * gsm.det(ce)**(-1/3)\n",
    "\n",
    "# The first invariant needed for the non-equilibrium energy\n",
    "I_1_iso_ce = gsm.tr(ce_iso)\n",
    "\n",
    "# The non-equilibrium energy density whereby 'm_list' corresponds \n",
    "# to 'G_list' for the equilibrium energy density\n",
    "Psi_neq = Psi_deviatoric(I_1_iso_ce, m_list, a_list)\n",
    "\n",
    "# The total energy density\n",
    "Psi = Psi_eq + Psi_neq"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The contraint Eq. (8)\n",
    "evolution_constraint = pv * (gsm.det(C) - gsm.det(Cv))\n",
    "\n",
    "# The evolution according to Eq. (7)\n",
    "evolution_equation = \\\n",
    "    Cv_dot.discretization() + 4 / eta * Cv * gsm.diff(Psi, Cv) * Cv + gsm.diff(evolution_constraint, Cv)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create the symbolic model representation\n",
    "visco_model_v1 = gsm.GSMBase(Psi, gsm.equations({Cv: evolution_equation, pv: gsm.diff(evolution_constraint, pv)}))\n",
    "# The argument to 'gsm.equations' is a dictionary of which the keys provide information about \n",
    "# shape and symmetry of the value expression. For example, 'Cv' tells that the (residual) \n",
    "# value of 'evolution_equation' is a symmetric second order tensor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate code and JIT-compile it. The code can be found in the subdirectory '.materiaux_jit/visco_model_v1'\n",
    "module_visco_model_v1 = jit.jit_module(gsm.create_module_data(visco_model_v1), module_name=\"visco_model_v1\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variant 2: direct implementation of the second equality in Eq. (7)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a veriable from an expression. This enables differentiation wrt .inv(Cv)\n",
    "Cv_inv = gsm.variable(gsm.inv(Cv))\n",
    "\n",
    "# The elastic left Cauchy-Green tensor (see Eq. (3))...\n",
    "ce = F * Cv_inv * gsm.transpose(F)\n",
    "# ... and its isochoric part.\n",
    "ce_iso = ce * gsm.det(ce)**(-1/3)\n",
    "\n",
    "# The first invariant needed for the non-equilibrium energy\n",
    "I_1_iso_ce = gsm.tr(ce_iso)\n",
    "\n",
    "# The non-equilibrium energy density whereby 'm_list' corresponds \n",
    "# to 'G_list' for the equilibrium energy density\n",
    "Psi_neq = Psi_deviatoric(I_1_iso_ce, m_list, a_list)\n",
    "\n",
    "# The total energy density\n",
    "Psi = Psi_eq + Psi_neq"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The contraint Eq. (8)\n",
    "evolution_constraint = pv * (gsm.det(C) - gsm.det(Cv))\n",
    "\n",
    "# The evolution according to Eq. (7). Note the differentiation wrt. 'Cv_inv'\n",
    "evolution_equation = \\\n",
    "    Cv_dot.discretization() - 4 / eta * gsm.diff(Psi, Cv_inv) + gsm.diff(evolution_constraint, Cv)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create the symbolic model representation\n",
    "visco_model_v2 = gsm.GSMBase(Psi, gsm.equations({Cv: evolution_equation, pv: gsm.diff(evolution_constraint, pv)}))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate code and JIT-compile it. The code can be found in the subdirectory '.materiaux_jit/visco_model_v2'\n",
    "module_visco_model_v2 = jit.jit_module(gsm.create_module_data(visco_model_v2), module_name=\"visco_model_v2\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ls .materiaux_jit/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variant 3: use `gsm.GeneralizedStandardMaterial`\n",
    "\n",
    "Define the material behavior in terms of the energy density $\\Psi$ and the dissipation potential $\\Phi$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# As above...\n",
    "ce = F * gsm.inv(Cv) * gsm.transpose(F)\n",
    "ce_iso = ce * gsm.det(ce)**(-1/3)\n",
    "I_1_iso_ce = gsm.tr(ce_iso)\n",
    "\n",
    "Psi = Psi_deviatoric(I_1_iso, G_list, alpha_list) + Psi_deviatoric(I_1_iso_ce, m_list, a_list)\n",
    "\n",
    "# The dissipation potential (see Eq. (6))\n",
    "Phi = eta / 8 * (gsm.tr(Cv_dot * gsm.inv(Cv) * Cv_dot * gsm.inv(Cv)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create the symbolic model representation based on the energy density, the dissipation potential and the\n",
    "# internal evolution constaint\n",
    "visco_model_v3 = gsm.GeneralizedStandardMaterial(Psi, Phi, internal_constraints=[gsm.det(Cv) - gsm.det(C)])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate code and JIT-compile it. The code can be found in the subdirectory '.materiaux_jit/visco_model_v3'\n",
    "module_visco_model_v3 = jit.jit_module(gsm.create_module_data(visco_model_v3), module_name=\"visco_model_v3\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variant 4: use `gsm.IncrementalMaterialPotential` \n",
    "\n",
    "Define an incremental variational principle based on the potential\n",
    "\\begin{equation}\n",
    "\\pi = \\Psi(\\bF, \\bC^\\text{v}) + (t - t_n)\\,\\Phi(\\dot{\\bC}^\\text{v}; \\bC^\\text{v}_n) + p_v (\\det \\bC^\\text{v} - \\det \\bC_n)\n",
    "\\end{equation}\n",
    "\n",
    "Note that for the case of an incremental variational principle, the dissipation potential \n",
    "must not depend on any *current* external state. That is the reason for \n",
    "giving the constraint in terms of $\\bC_n = \\bC(\\bF_n)$ instead of $\\bC(\\bF)$.\n",
    "Otherwise, the evolution constraint on $\\det(\\bC^\\text{v})$ would give spurious stress contribution!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# As above...\n",
    "ce = F * gsm.inv(Cv) * gsm.transpose(F)\n",
    "ce_iso = ce * gsm.det(ce)**(-1/3)\n",
    "I_1_iso_ce = gsm.tr(ce_iso)\n",
    "\n",
    "Psi = Psi_deviatoric(I_1_iso, G_list, alpha_list) + Psi_deviatoric(I_1_iso_ce, m_list, a_list)\n",
    "Phi = eta / 8 * (gsm.tr(Cv_dot * gsm.inv(Cv_n) * Cv_dot * gsm.inv(Cv_n)))\n",
    "\n",
    "# Define a history state corresponding to 'F'\n",
    "F_n = gsm.HistoryState(F)\n",
    "\n",
    "# Define the incremental potential 'pi'\n",
    "pi = Psi + (t - t_n) * Phi + pv * (gsm.det(Cv) - gsm.det(gsm.transpose(F_n) * F_n))\n",
    "\n",
    "# Create the symbolic model representation based on an incremental variational principle.\n",
    "visco_model_v4 = gsm.IncrementalMaterialPotential(pi)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate code and JIT-compile it. The code can be found in the subdirectory '.materiaux_jit/visco_model_v4'\n",
    "module_visco_model_v4 = jit.jit_module(gsm.create_module_data(visco_model_v4), module_name=\"visco_model_v4\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variant 5: explicit time stepping"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Similar to \"2\" but with a first-order time interpolation of F\n",
    "\n",
    "# Define time increment and a history state corresponding to 'F'\n",
    "F_n = gsm.HistoryState(F)\n",
    "Delta_t = gsm.TimeIncrement()\n",
    "F_t = F_n + (F - F_n)/(t - t_n) * Delta_t\n",
    "\n",
    "Cv_inv = gsm.variable(gsm.inv(Cv))\n",
    "ce = F_t * Cv_inv * gsm.transpose(F_t)\n",
    "ce_iso = ce * gsm.det(ce)**(-1/3)\n",
    "\n",
    "I_1_iso_ce = gsm.tr(ce_iso)\n",
    "Psi_neq = Psi_deviatoric(I_1_iso_ce, m_list, a_list)\n",
    "Psi = Psi_eq + Psi_neq"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rate = jit.jit(gsm.Function(\"Cv_dot\", 4 / eta * gsm.diff(Psi, Cv_inv), symmetry=True))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rk5 = materiaux.cpp.create_runge_kutta_5_stable(\"double\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "evolution_explicit = materiaux.cpp.create_evolution(rate, rk5, True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for cd in evolution_explicit.coefficient_data + evolution_explicit.constant_data:\n",
    "    print(cd)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "duals_func = jit.jit(gsm.Function(\"duals\", gsm.diff(Psi, F)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running the models\n",
    "\n",
    "Boilerplate to run some stretch-controlled loading cycle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# helper for the extraction of certain array entries\n",
    "diag = tuple(([0, 1, 2], [0, 1, 2]))\n",
    "fdiag = tuple(([0, 4, 8],))\n",
    "sdiag = tuple(([0, 3, 5],))\n",
    "\n",
    "\n",
    "# Compute the actual stress, ie. pressure correction. Same as in the hyperelasticity example.\n",
    "def true_stress(S, F):\n",
    "    p = S[0, :] * F[0, :]\n",
    "    return S * F - p\n",
    "\n",
    "\n",
    "# The tensor F for a prescribed stretch (3-direction). Same as in the hyperelasticity example.\n",
    "def set_F(F, stretch: float):\n",
    "    F[0, 0] = 1 / np.sqrt(stretch)\n",
    "    F[1, 1] = 1 / np.sqrt(stretch)\n",
    "    F[2, 2] = stretch\n",
    "\n",
    "# Initialize fields. This is important since some tensors are identity tensors at the beginning, not zeros.\n",
    "def init_fields(field_arrays, stretch):\n",
    "    field_arrays[\"t\"][0] = 0\n",
    "    field_arrays[\"t_n\"][0] = 0\n",
    "    set_F(field_arrays[\"F\"], stretch)\n",
    "    field_arrays[\"F_n\"] = field_arrays[\"F\"].copy()\n",
    "    field_arrays[\"Cv\"][:] = np.array([1/stretch, 0, 0, 1/stretch, 0, stretch**2], dtype=np.float_)\n",
    "    field_arrays[\"Cv_n\"] = field_arrays[\"Cv\"][:]\n",
    "\n",
    "    # Handle the internal multipliers depending on implementation variant\n",
    "    _pv = None\n",
    "    if \"pv\" in field_arrays.keys():\n",
    "        _pv = \"pv\"\n",
    "    elif \"internal_multipliers\" in field_arrays.keys():\n",
    "        _pv = \"internal_multipliers\"\n",
    "    if _pv is not None:\n",
    "        field_arrays[_pv][:] = 0\n",
    "        \n",
    "\n",
    "# Stored fields of which the history is needed somewhere\n",
    "def save_history(field_arrays):\n",
    "    field_arrays[\"F_n\"][:] = field_arrays[\"F\"][:]\n",
    "    field_arrays[\"Cv_n\"][:] = field_arrays[\"Cv\"][:]\n",
    "    field_arrays[\"t_n\"][:] = field_arrays[\"t\"][:]\n",
    "    \n",
    "\n",
    "# Save data for later plotting\n",
    "def save_data(field_arrays, res_duals, F_data, Cv_data, S_data, pv_data, step):\n",
    "    F_data[:, step] = field_arrays[\"F\"][diag]\n",
    "    Cv_data[:, step] = field_arrays[\"Cv\"][sdiag]\n",
    "    S_data[:, step] = res_duals[fdiag]\n",
    "    if \"pv\" in field_arrays.keys():\n",
    "        pv_data[:, step] = field_arrays[\"pv\"][:]\n",
    "    elif \"internal_multipliers\" in field_arrays.keys():\n",
    "        pv_data[:, step] = field_arrays[\"internal_multipliers\"][:]\n",
    "\n",
    "\n",
    "# Generic runner for the implementations under consideration\n",
    "def run_model(target_stretch, stretch_rate, n_steps, material_response, evolution=None, reverse=True, **parameters):\n",
    "    # If there is an evolution, then the parameter material_response is the full model, \n",
    "    # not only the response in the sense of stresses\n",
    "    if evolution is None:\n",
    "        model = material_response\n",
    "        material_response = model.material_response\n",
    "        evolution_equations = model.evolution_equations\n",
    "        # Setup a function the returns the new internal states\n",
    "        evolution = materiaux.cpp.create_evolution(evolution_equations.eqs,\n",
    "                                                          evolution_equations.d_int_eqs,\n",
    "                                                          materiaux.cpp.create_newton_raphson(\"double\"), \n",
    "                                                          True)\n",
    "        material_response = material_response.duals\n",
    "    \n",
    "    # Create all arrays for storing coefficients and constants (states and parameters)\n",
    "    field_arrays = materiaux.cpp.create_field_arrays(\"double\", material_response.coefficient_data +\n",
    "                                                     material_response.constant_data +\n",
    "                                                     evolution.coefficient_data + \n",
    "                                                     evolution.constant_data)\n",
    "    # Set the parameters\n",
    "    field_arrays.update(t=np.zeros(1), t_n=np.zeros(1), **parameters)\n",
    "    \n",
    "    # Handle internal states depeding on the implemenation\n",
    "    if \"pv\" in field_arrays.keys():\n",
    "        _pv = \"pv\"\n",
    "    elif \"internal_multipliers\" in field_arrays.keys():\n",
    "        _pv = \"internal_multipliers\"\n",
    "    else:\n",
    "        _pv = None\n",
    "    \n",
    "    # Initialization\n",
    "    stretch = 1.0\n",
    "    init_fields(field_arrays, stretch)\n",
    "    \n",
    "    # Time stepping setup\n",
    "    direction = 1 if target_stretch > 1 else -1\n",
    "    dt = np.abs(target_stretch - 1) / stretch_rate / n_steps\n",
    "    if reverse:\n",
    "        dt *= 2\n",
    "    print(dt)\n",
    "\n",
    "    # Storage for postprocessing\n",
    "    F_data = np.zeros((3, n_steps))\n",
    "    Cv_data = np.zeros((3, n_steps))\n",
    "    S_data = np.zeros((3, n_steps))\n",
    "    pv_data = np.zeros((1, n_steps))\n",
    "\n",
    "    # The time stepping\n",
    "    for step in range(n_steps):\n",
    "        if reverse and step == np.floor((n_steps + 1) / 2):\n",
    "            direction *= -1\n",
    "\n",
    "        # Increment the stretch for that time step\n",
    "        stretch += direction * stretch_rate * dt\n",
    "        set_F(field_arrays[\"F\"], stretch)\n",
    "        \n",
    "        # Increment time\n",
    "        field_arrays[\"t\"][0] += dt\n",
    "        field_arrays[\"Delta_t\"] = dt\n",
    "        \n",
    "        # Solve the evolution\n",
    "        res_evolution = materiaux.cpp.call(evolution, **field_arrays)\n",
    "        \n",
    "        # Store the updated states\n",
    "        field_arrays[\"Cv\"][:] = res_evolution[:6]\n",
    "        if _pv is not None:\n",
    "            field_arrays[_pv][:] = res_evolution[6]\n",
    "\n",
    "        # Compute the stresses\n",
    "        res_duals = materiaux.cpp.call(material_response, **field_arrays)\n",
    "\n",
    "        # Write to history fields\n",
    "        save_history(field_arrays)\n",
    "        \n",
    "        # Write to postprocessing fields\n",
    "        save_data(field_arrays, res_duals, F_data, Cv_data, S_data, pv_data, step)\n",
    "\n",
    "    return F_data, Cv_data, S_data, pv_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Material and solver parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters = dict(\n",
    "    G_1=2.5e-3,\n",
    "    G_2=0,\n",
    "    G_3=2.5e-3,\n",
    "    m  = 30e-3,\n",
    "    eta=  2e-3,\n",
    ")\n",
    "parameters[\"newton_tol\"] = 1e-6\n",
    "parameters[\"newton_max_iter\"] = 10"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run all implementation variants for different stress rates and target stretches"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stretch_rates = [0.1, 1, 10]\n",
    "n_steps = int(1e3)\n",
    "reverse = True\n",
    "\n",
    "# Case (a)\n",
    "plot_data_a = {}\n",
    "target_stretch = 2\n",
    "\n",
    "plot_data_a[\"v1\"] = [run_model(target_stretch, stretch_rate, n_steps, module_visco_model_v1, **parameters) \n",
    "                     for stretch_rate in stretch_rates]\n",
    "\n",
    "plot_data_a[\"v2\"] = [run_model(target_stretch, stretch_rate, n_steps, module_visco_model_v2, **parameters) \n",
    "                     for stretch_rate in stretch_rates]\n",
    "\n",
    "plot_data_a[\"v3\"] = [run_model(target_stretch, stretch_rate, n_steps, module_visco_model_v3, **parameters) \n",
    "                     for stretch_rate in stretch_rates]\n",
    "\n",
    "model = module_visco_model_v4\n",
    "plot_data_a[\"v4\"] = [run_model(target_stretch, stretch_rate, n_steps,\n",
    "                         model.d_ext_incremental_potential, \n",
    "                         evolution=materiaux.cpp.create_evolution(\n",
    "                             model.d_int_incremental_potential, \n",
    "                             model.d_int_d_int_incremental_potential,\n",
    "                             materiaux.cpp.create_newton_raphson(\"double\"), \n",
    "                             True),\n",
    "                         **parameters) \n",
    "             for stretch_rate in stretch_rates]\n",
    "\n",
    "plot_data_a[\"v5\"] = [run_model(target_stretch, stretch_rate, n_steps,\n",
    "                         duals_func, evolution=evolution_explicit,\n",
    "                         **parameters) \n",
    "             for stretch_rate in stretch_rates]\n",
    "\n",
    "# Case (b)\n",
    "plot_data_b = {}\n",
    "\n",
    "target_stretch = 1/4\n",
    "\n",
    "plot_data_b[\"v1\"] = [run_model(target_stretch, stretch_rate, n_steps, module_visco_model_v1, **parameters) \n",
    "                     for stretch_rate in stretch_rates]\n",
    "\n",
    "plot_data_b[\"v2\"] = [run_model(target_stretch, stretch_rate, n_steps, module_visco_model_v2, **parameters) \n",
    "                     for stretch_rate in stretch_rates]\n",
    "\n",
    "plot_data_b[\"v3\"] = [run_model(target_stretch, stretch_rate, n_steps, module_visco_model_v3, **parameters) \n",
    "                     for stretch_rate in stretch_rates]\n",
    "\n",
    "model = module_visco_model_v4\n",
    "plot_data_b[\"v4\"] = [run_model(target_stretch, stretch_rate, n_steps,\n",
    "                         model.d_ext_incremental_potential, \n",
    "                         evolution=materiaux.cpp.create_evolution(\n",
    "                             model.d_int_incremental_potential, \n",
    "                             model.d_int_d_int_incremental_potential,\n",
    "                             materiaux.cpp.create_newton_raphson(\"double\"),\n",
    "                             True),\n",
    "                         **parameters) \n",
    "             for stretch_rate in stretch_rates]\n",
    "\n",
    "plot_data_b[\"v5\"] = [run_model(target_stretch, stretch_rate, n_steps,\n",
    "                         duals_func, evolution=evolution_explicit,\n",
    "                         **parameters) \n",
    "             for stretch_rate in stretch_rates]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mpl.rcParams[\"text.usetex\"] = True\n",
    "mpl.rcParams[\"font.size\"] = 11\n",
    "mpl.rcParams[\"text.latex.preamble\"] = r\"\\usepackage{siunitx}\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(8,4))\n",
    "ax1 = fig.add_subplot(121)\n",
    "ax2 = fig.add_subplot(122)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax1.clear()\n",
    "ax2.clear()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colors = {variant: color for variant, color in zip(list(plot_data_a.keys()), [\"black\", \"blue\", \"orange\", \"red\", \"green\"])}\n",
    "linestyles = {vv: ls for vv, ls in zip(stretch_rates, [\"-\", \"--\", \"-.\"])}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note: all lines corresponding to a single stretch rate should be on top of each other"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for kk, plot_data in plot_data_a.items():\n",
    "    for vv, stretch_rate in zip(plot_data, stretch_rates):\n",
    "        label = r\"$\\dot{{\\lambda}}_3 = \\SI{{{:.1f}}}{{\\per\\second}}$\".format(stretch_rate) \\\n",
    "            if kk == list(plot_data_a.keys())[-1] else None\n",
    "        ax1.plot(vv[0][2, :], 1e3*true_stress(vv[2], vv[0])[2,:], \n",
    "                 color=colors[kk], linestyle=linestyles[stretch_rate], label=label)\n",
    "\n",
    "for kk, plot_data in plot_data_b.items():\n",
    "    for vv, stretch_rate in zip(plot_data, stretch_rates):\n",
    "        label = r\"variant {:s}\".format(kk) if abs(stretch_rate - stretch_rates[0]) < 1e-6 else None\n",
    "        ax2.plot(vv[0][2, :], 1e3*true_stress(vv[2], vv[0])[2,:], \n",
    "                 color=colors[kk], linestyle=linestyles[stretch_rate], label=label)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax1.legend()\n",
    "ax2.legend()\n",
    "ax1.set_xlim(1, 2.05)\n",
    "ax1.set_xlabel(r\"$\\lambda_3$\")\n",
    "ax1.set_ylabel(r\"$\\sigma_{33} / \\si{\\kilo\\pascal}$\")\n",
    "ax2.set_xlim(0.2, 1)\n",
    "ax2.set_xticks(np.linspace(0.25, 1, 4))\n",
    "ax2.set_xlabel(r\"$\\lambda_3$\")\n",
    "ax2.set_ylabel(r\"$\\sigma_{33} / \\si{\\kilo\\pascal}$\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig.savefig(\"viscoelastic_response.svg\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"viscoelastic_response.svg\" alt=\"viscoelastic_response.svg figure\" width=\"800\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
