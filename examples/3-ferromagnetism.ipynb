{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "\\DeclareMathOperator\\tr{tr}\n",
    "\\newcommand{\\bF}{\\mathbf{F}}\n",
    "\\newcommand{\\bC}{\\mathbf{C}}\n",
    "\\newcommand{\\bb}{\\mathbf{b}}\n",
    "\\newcommand{\\bh}{\\mathbf{h}}\n",
    "\\newcommand{\\bA}{\\mathbf{A}}\n",
    "\\newcommand{\\bS}{\\mathbf{S}}\n",
    "\\newcommand{\\Bsigma}{{\\boldsymbol{\\sigma}}}\n",
    "\\newcommand{\\Bbeta}{{\\boldsymbol{\\beta}}}\n",
    "\\newcommand{\\bbm}{\\mathbf{m}}\n",
    "\\newcommand{\\pdiff}[2]{\\frac{\\partial#1}{\\partial#2}}\n",
    "\\newcommand{\\norm}[1]{\\left|#1\\right|}\n",
    "\\newcommand{\\qaq}{\\quad \\text{and} \\quad}\n",
    "$$\n",
    "\n",
    "\n",
    "# Ferromagnetism\n",
    "\n",
    "In this example we present the implementation of the plasticity-inspired phenomenological model for rigid ferromagnetic materials by Mukherjee and Danas [1].\n",
    "The actual theoretical description as well as the implementation are based on [2] but without accounting for rotations.\n",
    "\n",
    "In what follows $\\Psi$ and $\\Phi$ represent energy density and dissipation potential similar to the example \n",
    "on [viscoelasticity](./2-viscoelasticity.ipynb).\n",
    "The internal ferromagnetic state will be denoted by $\\bbm$, i.e. magnetization. Note however, that this is a simplification compared to [1,2].\n",
    "In general, the internal ferromagnetic state cannot be identified with the magnetization. \n",
    "The \"external\" magnetic quantities are the magnetic field $\\bb$ and $h$-field $\\bh$. \n",
    "In this formulation of we use $\\bh$ as primary field such that $\\bb$ is the work-conjugate (dual) obtained as\n",
    "\\begin{equation}\n",
    "\\bb = -\\pdiff{\\Psi}{\\bh} .\n",
    "\\tag{1}\n",
    "\\end{equation}\n",
    "\n",
    "According to the abstract framework of Generalized Standard Materials (GSM; [3])\n",
    "the corresponding evolution equation reads\n",
    "\\begin{align}\n",
    "\\label{eq:ferro_GSM_evolution}\n",
    "\\partial_{\\bbm} \\Psi(\\bh, \\bbm) + \\partial_{\\dot{\\bbm}} \\Phi(\\dot{\\bbm}; \\bh, \\bbm) &= 0\n",
    "&\\Rightarrow&&\n",
    "\\partial_{\\dot{\\bbm}} \\Phi = - \\partial_{\\bbm} \\Psi &= \\Bbeta\n",
    "\\tag{2}\n",
    "\\end{align}\n",
    "where $\\Bbeta$ is the energetic dual to $\\bbm$.\n",
    "\n",
    "In the case of rate-independence, which we consider in this example, the dissipation potential\n",
    "$\\Phi$ can be expressed as [1]\n",
    "\\begin{align}\n",
    "\\Phi(\\dot{\\bbm}) = \\sup_{\\bbm}\\,\\inf_{\\lambda\\geq0} \\left\\{\n",
    "\\Bbeta \\cdot \\dot{\\bbm} - \\lambda f(\\Bbeta)\n",
    "\\right\\}\n",
    "\\tag{3}\n",
    "\\end{align}\n",
    "where $f$ is the magnetic switching surface and $\\lambda$ is the Lagrange multiplier that enforces $f(\\Bbeta) \\leq 0$.\n",
    "The stationary conditions in Eq. (2) are\n",
    "\\begin{equation}\n",
    "\\dot{\\bbm} = \\lambda \\partial_{\\bbm} f \\qaq  f(\\Bbeta) \\leq 0\n",
    "\\tag{4a}\n",
    "\\end{equation}\n",
    "whereby\n",
    "\\begin{equation}\n",
    "\\lambda \\geq 0 \\qaq \\lambda f(\\Bbeta) = 0.\n",
    "\\tag{4b}\n",
    "\\end{equation}\n",
    "\n",
    "For simplicity we employ a switching surface $f$\n",
    "\\begin{align}\n",
    "f(\\Bbeta) = \\Bbeta \\cdot \\Bbeta - (\\beta^\\text{c})^2\n",
    "\\tag{5}\n",
    "\\end{align}\n",
    "where ${\\beta^\\text{c}}$ is a constant parameter.\n",
    "\n",
    "The energy density employed is\n",
    "\\begin{align}\n",
    "\\Psi(I_5^\\text{r}, I_5, I_5^\\text{er}) =\n",
    "- \\left(\\frac{\\mu_0}{2} I_5\n",
    "+ \\mu_0 I_5^\\text{er} \\right)\n",
    "+ \\mu_0 \\frac{\\left(m^\\text{sat}\\right)^2}{\\chi^r} \\, s\\left(\n",
    "\\sqrt{I_5^\\text{r}} \\,/\\, m^\\text{sat}\n",
    "\\right)\n",
    "\\tag{6}\n",
    "\\end{align}\n",
    "{with}\n",
    "\\begin{align}\n",
    "I_5^\\text{r} = \\bbm \\cdot \\bbm = \\norm{\\bbm}^2, \\quad\n",
    "I_5 = \\bh \\cdot \\bh) = \\norm{\\bh}^2 \\quad \\text{and} \\quad\n",
    "I_5^\\text{er} = \\bh \\cdot \\bbm .\n",
    "\\tag{7}\n",
    "\\end{align}\n",
    "The parameter $m^\\text{sat}$ is the saturation value of $\\norm{\\bbm}$ and \n",
    "$\\chi_r$ is magnetization slope. The function $s$ is given as \n",
    "\\begin{equation}\n",
    "\\label{eq:saturation_function}\n",
    "s(x) = -\\ln(1 - x) - x .\n",
    "\\tag{8}\n",
    "\\end{equation}\n",
    "and has a first derivative of inverse sigmoidal type which is responsible for magnetic saturation.\n",
    "\n",
    "\n",
    "## References\n",
    "\n",
    " 1. Mukherjee, D. & Danas, K. [An evolving switching surface model for ferromagnetic hysteresis. *Journal of Applied Physics* 125, 033902 (2019)](http://aip.scitation.org/doi/10.1063/1.5051483).\n",
    " 2. Rambausek, M., Mukherjee, D. & Danas, K. A computational framework for magnetically hard and soft viscoelastic magnetorheological elastomers, *in preparation* (2021).\n",
    " 3. Germain, P., Nguyen, Q. S. & Suquet, P. [Continuum Thermodynamics. *Journal of Applied Mechanics* 50, 1010–1020 (1983)](http://dx.doi.org/10.1115/1.3167184)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Python imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib nbagg\n",
    "# FOR JUPYTER LAB: This needs the ipympl jupyter lab extension (https://github.com/matplotlib/ipympl)\n",
    "# %matplotlib widget"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import matplotlib as mpl\n",
    "import pathlib"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import materiaux.cpp\n",
    "import materiaux.modeling.gsm as gsm\n",
    "import materiaux.codegeneration.jit as jit\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## General definitions of states and parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The only external state\n",
    "h = gsm.State(\"h\", (3,))\n",
    "\n",
    "# The two internals states\n",
    "m = gsm.InternalState(\"m\", (3,))\n",
    "lmbda = gsm.InternalState(\"lmbda\") # note: 'lambda' is a Python keyword\n",
    "\n",
    "# Time...\n",
    "t = gsm.Time(\"t\")\n",
    "# ...and previous time\n",
    "t_n = gsm.HistoryTime(t)\n",
    "\n",
    "# History of 'm'. No need for the history of 'lambda'!\n",
    "m_n = gsm.HistoryState(m)\n",
    "# Time rate. First order time discretization\n",
    "m_dot = gsm.TimeRate(m, (m - m_n) / (t - t_n))\n",
    "\n",
    "# Vacuum permeability\n",
    "mu_0 = gsm.Parameter(\"mu_0\")\n",
    "\n",
    "# Magnetization slope\n",
    "chi_r = gsm.Parameter(\"chi_r\")\n",
    "\n",
    "# Saturation magnetization\n",
    "m_sat = gsm.Parameter(\"m_sat\")\n",
    "\n",
    "# Switching surface radius\n",
    "beta_c = gsm.Parameter(\"beta_c\")\n",
    "\n",
    "# Tolerance for the solution of the evolution equation\n",
    "evol_tol = gsm.Parameter(\"evol_tol\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Invariants (Eq. (7))\n",
    "I_5 = gsm.inner(h, h)\n",
    "I_5_er = gsm.inner(h,m)\n",
    "I_5_r = gsm.inner(m, m)\n",
    "xx = I_5_r / m_sat**2\n",
    "x = gsm.sqrt(xx)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Saturation function (Eq. (8))\n",
    "s = gsm.conditional(x > 1e-8, -(gsm.ln(1 - x) + x), 1/2 * xx)\n",
    "\n",
    "# Magnetic energy density (Eq. (6))\n",
    "Psi_mag = (- mu_0/2 * I_5 + mu_0 * I_5_er) + mu_0 * (m_sat**2 / chi_r) * s\n",
    "\n",
    "# Switching surface (Eq. (5))\n",
    "def f(beta):\n",
    "    return gsm.inner(beta, beta) - beta_c ** 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variant 1: use `gsm.GeneralizedStandardMaterial`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define 'beta' as an independent internal state\n",
    "beta = gsm.InternalState(\"beta\", (3,))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next the trial state: do as if there were no magnetic evolution.\n",
    "Handling the trial step explicitly as done below is not super nice since it could be done by\n",
    "an active set algorithm used instead of a standard Newton-Raphson scheme for evolution.\n",
    "However, such an algorithm is not yet provided by 'materiaux'."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a Psi that depends on the past 'm', i.e. 'm_n' and derive 'beta_trial'\n",
    "Psi_mag_trial = gsm.replace(Psi_mag, {m: m_n})\n",
    "beta_trial = -gsm.diff(Psi_mag_trial, m_n)\n",
    "\n",
    "# The condition for magnetic evolution\n",
    "trial_condition = f(beta_trial) > evol_tol"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the trial state to determine whether there should be magnetic evolution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The dissipation potential (Eq. (3)): if the evolution (trial) condition is true, use the term 'lmbda * f(beta)',\n",
    "# otherwise use 'lmbda**2' which ensure that no evolution happens.\n",
    "Phi = gsm.inner(beta, m_dot) - gsm.conditional(trial_condition, lmbda * f(beta), lmbda**2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the GSM model and compile the module.\n",
    "ferro_model_v1 = gsm.GeneralizedStandardMaterial(Psi_mag, Phi)\n",
    "module_ferro_v1 = jit.jit_module(gsm.create_module_data(ferro_model_v1), module_name=\"ferro_model_v1\")\n",
    "print(module_ferro_v1.lib.shared_library_path)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variant 2: direct implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# beta according to Eq. (2)\n",
    "beta = -gsm.diff(Psi_mag, m)\n",
    "\n",
    "# Make 'beta' a variable to enable differentiation wrt. 'beta'\n",
    "beta = gsm.variable(beta)\n",
    "\n",
    "# The trial 'beta' to determine whether magnetization evolves\n",
    "beta_trial = gsm.replace(beta, {m: m_n})\n",
    "trial_condition = f(beta_trial) > evol_tol\n",
    "\n",
    "# The dissipation potential (essentially the same as above)\n",
    "Phi = gsm.inner(beta, m_dot) - gsm.conditional(trial_condition, lmbda * f(beta), lmbda**2)\n",
    "\n",
    "# The (partial) stationary conditions (Eq. (4))\n",
    "eq_m_dot = gsm.diff(Phi, beta)\n",
    "eq_lmbda = gsm.diff(Phi, lmbda)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the model in terms of the energy and the evolution equations and compile the module. \n",
    "# The generated code and the module can be found in the subdirectory 'ferro_model_v2'\n",
    "ferro_model_v2 = gsm.GSMBase(Psi_mag, gsm.equations({m_dot: eq_m_dot, lmbda: eq_lmbda}))\n",
    "module_ferro_v2 = jit.jit_module(gsm.create_module_data(ferro_model_v2), module_name=\"ferro_model_v2\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running the models\n",
    "\n",
    "### Code for running the models: uniaxial magnetic loading"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Initialization\n",
    "def init_fields(field_arrays):\n",
    "    field_arrays[\"t\"][0] = 0\n",
    "    field_arrays[\"t_n\"][0] = 0\n",
    "    \n",
    "        \n",
    "# Save history values\n",
    "def save_history(field_arrays):\n",
    "    for kk in field_arrays.keys():\n",
    "        if kk[-2:] == \"_n\":\n",
    "            field_arrays[kk][:] = field_arrays[kk[:-2]][:]\n",
    "    \n",
    "    \n",
    "# Save data for postprocessing\n",
    "def save_data(field_arrays, res_duals, h_data, m_data, b_data, step):\n",
    "    h_data[:, step] = field_arrays[\"h\"]\n",
    "    m_data[:, step] = field_arrays[\"m\"]\n",
    "    b_data[:, step] = -res_duals\n",
    "    \n",
    "\n",
    "# Generic runner for the implemenations defined above\n",
    "def run_model(target_h, h_rate, n_steps, material_response, evolution=None, reverse=True, **parameters):\n",
    "    # If there is an evolution, then the parameter material_response is the full model, \n",
    "    # not only the response in the sense of stresses\n",
    "    if evolution is None:\n",
    "        model = material_response\n",
    "        material_response = model.material_response\n",
    "        evolution_equations = model.evolution_equations\n",
    "        evolution = materiaux.cpp.create_evolution(evolution_equations.eqs,\n",
    "                                                   evolution_equations.d_int_eqs,\n",
    "                                                   materiaux.cpp.create_newton_raphson(\"double\"),\n",
    "                                                   True)\n",
    "        material_response = material_response.duals\n",
    "    \n",
    "    # Gather internal states\n",
    "    internal_states = [coeff for coeff in evolution.coefficient_data if coeff.type == \"InternalState\"]\n",
    "    \n",
    "    # Create all arrays for storing coefficients and constants (states and parameters)\n",
    "    field_arrays = materiaux.cpp.create_field_arrays(\"double\", \n",
    "                                                     material_response.coefficient_data +\n",
    "                                                     material_response.constant_data +\n",
    "                                                     evolution.coefficient_data + \n",
    "                                                     evolution.constant_data)\n",
    "    # Set parameters\n",
    "    field_arrays.update(**parameters)\n",
    "    \n",
    "    # Intialization\n",
    "    init_fields(field_arrays)\n",
    "    \n",
    "    # Set up the loading cycle\n",
    "    h_0 = 0.0\n",
    "    direction = 1 if target_h > h_0 else -1\n",
    "    dt = np.abs(target_h - h_0) / h_rate / (n_steps / 5)\n",
    "    \n",
    "    # Fields for postprocessing/plotting\n",
    "    h_data = np.zeros((3, n_steps))\n",
    "    m_data = np.zeros((3, n_steps))\n",
    "    b_data = np.zeros((3, n_steps))\n",
    "\n",
    "    # The time loop\n",
    "    for step in range(n_steps):\n",
    "        if direction > 0 and step >= n_steps / 5 and step < n_steps * 3 / 5:\n",
    "            direction = -1\n",
    "        elif direction < 0 and step >= n_steps * 3 / 5:\n",
    "            direction = 1\n",
    "            \n",
    "        # set external state and time\n",
    "        field_arrays[\"h\"][0] += direction * h_rate * dt\n",
    "        field_arrays[\"t\"][0] += dt\n",
    "        \n",
    "        # Solve the evolution equations\n",
    "        res_evolution = materiaux.cpp.call(evolution, **field_arrays)\n",
    "        \n",
    "        # Update internal states\n",
    "        offset = 0\n",
    "        for istate in internal_states:\n",
    "            field_arrays[istate.name][:] = res_evolution[offset:offset + istate.size]\n",
    "            offset += istate.size\n",
    "        \n",
    "        # Compute the work-conjugate to the external state 'h', i.e. the magnetic field 'b'\n",
    "        res_duals = materiaux.cpp.call(material_response, **field_arrays)\n",
    "\n",
    "        # save history and postprocessing data\n",
    "        save_history(field_arrays)\n",
    "        save_data(field_arrays, res_duals, h_data, m_data, b_data, step)\n",
    "\n",
    "    return h_data, m_data, b_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters = dict(\n",
    "    chi_r=8.76,\n",
    "    m_sat=0.67,\n",
    "    beta_c=0.766,\n",
    "    evol_tol=1e-6,\n",
    "    mu_0=4*np.pi*1e-1,\n",
    ")\n",
    "parameters[\"newton_tol\"] = 1e-6\n",
    "parameters[\"newton_max_iter\"] = 10"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run the two implementations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "target_h = 3.0\n",
    "h_rate = 0.01\n",
    "n_steps = int(1e4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_data_v1 = run_model(target_h, h_rate, n_steps, module_ferro_v1, **parameters)\n",
    "plot_data_v2 = run_model(target_h, h_rate, n_steps, module_ferro_v2, **parameters)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mpl.rcParams[\"text.usetex\"] = True\n",
    "mpl.rcParams[\"font.size\"] = 11\n",
    "mpl.rcParams[\"text.latex.preamble\"] = r\"\\usepackage{siunitx}\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(8,4))\n",
    "ax1 = fig.add_subplot(121)\n",
    "ax2 = fig.add_subplot(122)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax1.clear()\n",
    "ax2.clear()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the graphs corresponding to the two variants are expected to be on top of each other."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax1.plot(plot_data_v1[0][0,:], plot_data_v1[1][0,:]/parameters[\"m_sat\"], \"r-\", label=\"v1\")\n",
    "ax1.plot(plot_data_v2[0][0,:], plot_data_v2[1][0,:]/parameters[\"m_sat\"], \"b-\", label=\"v2\")\n",
    "ax2.plot(plot_data_v1[0][0,:], plot_data_v1[-1][0,:]/parameters[\"mu_0\"] - plot_data_v1[0][0,:], \"r-\")\n",
    "ax2.plot(plot_data_v2[0][0,:], plot_data_v2[-1][0,:]/parameters[\"mu_0\"] - plot_data_v2[0][0,:], \"b-\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax1.set_xlabel(r\"$h_1 /\\si{\\mega\\ampere\\per\\meter}$\")\n",
    "ax1.set_ylabel(r\"$\\alpha_1 / \\alpha^\\text{sat}$\")\n",
    "ax2.set_xlabel(r\"$h_1 / \\si{\\mega\\ampere\\per\\meter}$\")\n",
    "ax2.set_ylabel(r\"$m_1 / \\si{\\mega\\ampere\\per\\meter}$\")\n",
    "\n",
    "ax1.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig.savefig(\"ferromagnetic_response.svg\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"ferromagnetic_response.svg\" alt=\"ferromagnetic_response.svg figure\" width=\"800\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
