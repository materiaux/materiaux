{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Hyperelasticity\n",
    "\n",
    "Without going into the details of finite elasticity [1,2] we briefly introduce the model that is to be implemented below.\n",
    "\n",
    "Let us start with the deformation map $\\varphi$ which maps initial positions $X$ to current positions $\\varphi(X)$.\n",
    "The local change of this map is represented by the tangent map \n",
    "\\begin{equation}\n",
    "\\mathbf{F} = \\frac{\\partial \\varphi(X)}{\\partial X} \\tag{1}\n",
    "\\end{equation}\n",
    "also known as <i>deformation gradient</i>. This quantity is the basic ingredient of deformation measures.\n",
    "\n",
    "In the scope of hyperelasticity one works with stored elastic energy density functions $\\Psi$ that in some form\n",
    "depend of $\\mathbf{F}$. A common choice is to employ the right Cauchy-Green tensor $\\mathbf{C} = \\mathbf{F}^\\text{T} \\cdot \\mathbf{F}$ such\n",
    "that $\\Psi = \\Psi(\\mathbf{C})$. \n",
    "The tensor is invariant with respect to mere spatial rotations of a body which is a desried property for $\\Psi$. However, \n",
    "$\\Psi$ shall be invariant to any changes of coordinate systems. When $\\Psi$ only depends on $\\mathbf{C}$ this is achieved\n",
    "by letting it depend only on invariants $\\mathbf{C}$. Since the latter is a second order tensor it has a matrix representation\n",
    "and we can use the well-known matrix invariants $I_1 = \\text{tr}\\,{\\mathbf{C}}$ and $I_3 = \\text{det}\\,{\\mathbf{C}}$\n",
    "for the actual representation of $\\Psi(\\mathbf{C})$.\n",
    "\n",
    "Following [3], we finally use the generic form for *incompressible* materials\n",
    "\\begin{equation}\n",
    "\\Psi(\\mathbf{C}) = \\begin{cases}\n",
    "\\sum_{i=1}^2 \\frac{3^{1-\\alpha_i}}{2\\alpha_i} \\, G_i \\left[\n",
    "\\left(\\text{tr}\\,\\hat{\\mathbf{C}}\\right)^{\\alpha_i} - 3^{\\alpha_i}\n",
    "\\right] & \\text{if} \\quad I_3 = 1 \\\\\n",
    "\\infty & \\text{if} \\quad I_3 \\neq 1\n",
    "\\end{cases} \n",
    "\\tag{2}\n",
    "\\end{equation}\n",
    "with $\\hat{\\mathbf{C}} = (I_3)^{-1/3}\\,{\\mathbf{C}}$ being the *isochoric* component of $\\mathbf{C}$.\n",
    "\n",
    "In a computational context, one ensures that $I_3 = 1$ such that $\\Psi(\\mathbf{C})$ is evaluated only for this case.\n",
    "\n",
    "The Cauchy stress tensor is in general obtained as\n",
    "\\begin{equation}\n",
    "\\boldsymbol{\\sigma} = \\frac{1}{\\text{det}\\,\\mathbf{F}} \\,  \\frac{\\partial\\Psi}{\\partial\\mathbf{F}} \\cdot \\mathbf{F}^\\text{T}\n",
    "\\tag{3}\n",
    "\\end{equation}\n",
    "which in case of incompresible media where $\\text{det}\\,\\mathbf{F}=1$ becomes\n",
    "\\begin{equation}\n",
    "\\boldsymbol{\\sigma} = \\frac{\\partial\\Psi}{\\partial\\mathbf{F}} \\cdot \\mathbf{F}^\\text{T} - p \\mathbf{1}\n",
    "\\tag{4}\n",
    "\\end{equation}\n",
    "where $\\mathbf{1}$ is the second-order identity tensor and $p$ is the hydrostatic pressure which in this case \n",
    "cannot be obtained from $\\Psi$ but has to be computed from a boundary value problem.\n",
    "\n",
    "Below we want to compute the stress response of a hyperelastic material under uniaxial tension in 3-direction, that is along the $z$-axis.\n",
    "In that case we have \n",
    "\\begin{equation}\n",
    "\\mathbf{F} = \n",
    "\\begin{pmatrix}\n",
    "\\sqrt{\\lambda_3} & 0 & 0 \\\\\n",
    "0 & \\sqrt{\\lambda_3} & 0 \\\\\n",
    "0 & 0 & \\lambda_3\n",
    "\\end{pmatrix}\n",
    "\\tag{5}\n",
    "\\end{equation}\n",
    "where $\\lambda_3$ is the stretch in 3-direction.\n",
    "The component $\\sigma_{33}$ is the only one non-zero compontent of Cauchy stress $\\boldsymbol{\\sigma}$ in response to this loading.\n",
    "This knowledge lets us compute the pressure $p$ for this unixial loading case from the equation\n",
    "\\begin{equation}\n",
    "\\sigma_{11} = \\left[\\frac{\\partial\\Psi}{\\partial\\mathbf{F}} \\cdot \\mathbf{F}^\\text{T}\\right]_{11} - p = 0 .\n",
    "\\tag{6}\n",
    "\\end{equation}\n",
    "\n",
    "The remainder of this notebook first shows how to formulate the given form of $\\Psi$ and how to obtain the expression \n",
    "$\\frac{\\partial\\Psi}{\\partial\\mathbf{F}} \\cdot \\mathbf{F}^\\text{T}$. Then it demonstrates just-in-time compilation such that the \n",
    "one is in possession of numerically evaluatable objects corresponding to these symbolic expressions. In a final step, \n",
    "the unixial tension response is plotted for two sets of parameters.\n",
    "\n",
    "Implementation hints will mainly be provided as comments alongside the code in the remainder of this notebook.\n",
    "\n",
    "## References\n",
    "\n",
    " 1. Bonet, J. & Wood, R. D. [Nonlinear continuum mechanics for finite element analysis](http://www.academia.edu/download/58768730/nonlinear_continuum_mechanics_for_finite_element_analysis_-_bonet__wood.pdf). (Cambridge University Press, 2008).\n",
    " 2. Marsden, J. E. & Hughes, T. J. R. [Mathematical foundations of elasticity](https://authors.library.caltech.edu/25074/1/Mathematical_Foundations_of_Elasticity.pdf). (Dover, 1994).\n",
    " 3. Lopez-Pamies, O. [A new I1-based hyperelastic model for rubber elastic materials. *Comptes Rendus Mécanique* 338, 3–11 (2010)](http://www.sciencedirect.com/science/article/pii/S1631072109002113)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Python imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib nbagg\n",
    "# FOR JUPYTER LAB: This needs the ipympl jupyter lab extension (https://github.com/matplotlib/ipympl)\n",
    "# %matplotlib widget"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import matplotlib as mpl\n",
    "import pathlib\n",
    "import numpy as np\n",
    "import typing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import main modeling module (gsm -> Generalized Standard Materials)\n",
    "import materiaux.modeling.gsm as gsm\n",
    "\n",
    "# import jit module (jit -> Just-in-Time)\n",
    "import materiaux.codegeneration.jit as jit"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Symbolic formulation\n",
    "\n",
    "Note that since we are only concerned with the material model itself, there is no need to derive $\\mathbf{F}$ from a deformation $\\varphi$.\n",
    "It is simply defined as a second-order tensors, i.e. a state variable with matrix shape (3, 3)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "F = gsm.State(\"F\", shape=(3, 3),\n",
    "              description=\"tangent map of the deformation (aka deformation gradient)\")\n",
    "\n",
    "# model parameters\n",
    "G_list = [gsm.Parameter(\"G_1\"), gsm.Parameter(\"G_2\")]\n",
    "alpha_list = [gsm.Parameter(\"alpha_1\"), gsm.Parameter(\"alpha_2\")]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The right Cauchy-Green tensor ...\n",
    "C = gsm.transpose(F) * F\n",
    "# ... and its deviatoric (isochoric) component\n",
    "C_hat = C * gsm.det(C)**(-1/3)\n",
    "\n",
    "# The first invariant, i.e. the trace of 'C_hat'\n",
    "I_1_iso = gsm.tr(C_hat)\n",
    "\n",
    "# The actual Helmholtz free energy density (Eq. 2)\n",
    "Psi = sum([3 ** (1 - alpha_i) / (2 * alpha_i) * G_i * (I_1_iso ** alpha_i - 3 ** alpha_i) if alpha_i != 1 else\n",
    "                1 / 2 * G_i * (I_1_iso - 3)\n",
    "           for G_i, alpha_i in zip(G_list, alpha_list)])\n",
    "\n",
    "# The Cauchy stress (Eq. 3): 'gsm.diff' does partial differentiation;\n",
    "sigma = gsm.diff(Psi, F) * gsm.transpose(F) / gsm.det(F) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compilation and evaluation\n",
    "\n",
    "Create the `materiauXcpp.double.Function` object on-the-fly. This is either done by \n",
    "`jit.create_function_cffi` or by `jit.create_function_pybind` where the former uses \n",
    "the plain C interface whereas the latter compiles C++ bindings which has a slightly longer \n",
    "build time but otherwise performs equally.\n",
    "\n",
    "Special note: The Cauchy stress $\\boldsymbol{\\sigma}$ is symmetric. While this is not directly expressed by Eq. (3), \n",
    "this property can be explicitly injected into the `gsm.Function`.\n",
    "\n",
    "The generated codes can be found in the hidden subdirectory `.materiaux_jit`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Psi_jit = jit.jit_function(gsm.Function(\"Psi\", Psi), module_name=\"Psi_jit\")\n",
    "sigma_jit = jit.jit_function(gsm.Function(\"sigma\", sigma, symmetry=True), module_name=\"sigma_jit\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ls .materiaux_jit"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we need some helpers for plotting. Note that `jit.call` allows to call 'jitted' functions\n",
    "to be called with a python dictionary in which the state/coefficient/constant/parameter names\n",
    "are the keys. The values are either scalar numbers or numpy arrays of appropriate shape."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# array holding values for 'F'\n",
    "_F = np.eye(3, dtype=np.float_)\n",
    "\n",
    "\n",
    "# prepare the python dictionary of arguments\n",
    "def set_args(lambda_3: float, _G_list: typing.Iterable[float], _alpha_list: typing.Iterable[float]):\n",
    "    # see Eq. (5)\n",
    "    _F[0, 0] = 1 / np.sqrt(lambda_3)\n",
    "    _F[1, 1] = 1 / np.sqrt(lambda_3)\n",
    "    _F[2, 2] = lambda_3\n",
    "    args = {F.name: _F}\n",
    "    \n",
    "    for ii, G in enumerate(_G_list):\n",
    "        args[G_list[ii].name] = G\n",
    "    for ii, alpha in enumerate(_alpha_list):\n",
    "        args[alpha_list[ii].name] = alpha\n",
    "    return args\n",
    "\n",
    "\n",
    "# convenience wrapper for calling 'Psi_jit'\n",
    "def call_Psi(lambda_3: float, _G_list: typing.Iterable[float], _alpha_list: typing.Iterable[float]):\n",
    "    return jit.call(Psi_jit, **set_args(lambda_3, _G_list, _alpha_list))\n",
    "\n",
    "\n",
    "# convenience wrapper for calling 'sigma_jit' whereby only the 33 \n",
    "# component corrected by the pressure is returned\n",
    "def call_sigma_33(lambda_3: float, _G_list: typing.Iterable[float], _alpha_list: typing.Iterable[float]):\n",
    "    tmp = jit.call(sigma_jit, **set_args(lambda_3, _G_list, _alpha_list))\n",
    "    # since sigma_jit exploits symmetry, we may wish to reconstruct the full tensor as done below\n",
    "    tmp = np.array([[tmp[0], tmp[1], tmp[2]], \n",
    "                    [tmp[1], tmp[3], tmp[4]],\n",
    "                    [tmp[2], tmp[4], tmp[5]]])\n",
    "    # note that we here do now strictly require the full tensor since we only return the (3,3)-component\n",
    "    # that has indices (2, 2) since Python indices start at 0.\n",
    "    return (tmp - tmp[0,0] * np.eye(3))[2, 2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sigma_jit.result_data.symmetry"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# some matplotlib settings\n",
    "mpl.rcParams[\"text.usetex\"] = True\n",
    "mpl.rcParams[\"font.size\"] = 11\n",
    "mpl.rcParams[\"text.latex.preamble\"] = r\"\\usepackage{siunitx}\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(8, 4))\n",
    "ax1 = fig.add_subplot(121)\n",
    "ax2 = fig.add_subplot(122)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define the stretches in 3-direction\n",
    "lambda_3_data = np.linspace(0.25, 3, 200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a plot for a certain set of parameters\n",
    "def plot(_G_list, _alpha_list):\n",
    "    Psi_data = np.array([call_Psi(lambda_3, _G_list, _alpha_list) for lambda_3 in lambda_3_data])\n",
    "    sigma_data = np.array([call_sigma_33(lambda_3, _G_list, _alpha_list) for lambda_3 in lambda_3_data])\n",
    "    label = r\"$\\{{G_1, G_2\\}}=\\{{{:.1f}, {:.1f}\\}}, \\{{\\alpha_1, \\alpha_2\\}}=\\{{{:d}, {:d}\\}}$\".format(*_G_list, *_alpha_list)\n",
    "    ax1.plot(lambda_3_data, Psi_data, label=label)\n",
    "    ax2.plot(lambda_3_data, sigma_data, label=label)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot([1.0, 0.0], [1, 3])\n",
    "plot([0.5, 0.5], [1, 3])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax1.legend()\n",
    "ax1.set_xlim(0, 3)\n",
    "ax1.set_xlabel(r\"$\\lambda_3$\")\n",
    "ax1.set_ylabel(r\"$\\Psi$\")\n",
    "ax2.set_xlim(0, 3)\n",
    "ax2.set_xlabel(r\"$\\lambda_3$\")\n",
    "ax2.set_ylabel(r\"$\\sigma_{33}$\")\n",
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig.savefig(\"hyperelastic_responses.svg\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"hyperelastic_responses.svg\" alt=\"hyperelastic_responses.svg figure\" width=\"800\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
