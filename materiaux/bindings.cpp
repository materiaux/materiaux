// Copyright (C) 2019-2021 Matthias Rambausek
//
// This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
//
// SPDX-License-Identifier:    AGPL-3.0-or-later

#include "materiaux.h"
#include <functional>
#include <pybind11/complex.h>
#include <pybind11/numpy.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

using FieldData = materiaux::FieldData;
using CodeData = materiaux::CodeData;

namespace {

    materiaux::FieldDataPtr create_scalar_field_data(std::string name, std::string type,
                                                     std::string description) {
        return materiaux::create_scalar_field_data(name, type, description, 0);
    }

    template <typename T> std::string append_type_name(const std::string &base) {
        return base + typeid(T).name();
    }

    template <typename scalar_t> std::vector<std::string> get_type_names();

    template <> std::vector<std::string> get_type_names<std::complex<double>>() {
        return {"std::complex<double>", "double complex", "complex128"};
    }

    template <> std::vector<std::string> get_type_names<double>() { return {"double", "float64"}; }

    // function scalar type information
    template <typename scalar_t, std::enable_if_t<std::is_same_v<scalar_t, double>, bool> = true>
    std::string scalar_type_to_name() {
        return "double";
    }

    // function scalar type information
    template <typename scalar_t,
              std::enable_if_t<std::is_same_v<scalar_t, std::complex<double>>, bool> = true>
    std::string scalar_type_to_name() {
        return "std::complex<double>";
    }

    template <typename scalar_t> void add_scalar_dependent_types(py::module &submodule) {

        using Function = materiaux::Function<scalar_t>;
        using NonlinearSolver = materiaux::evolution::NonlinearSolver<scalar_t>;
        using ExplicitTSAlgo = materiaux::evolution::ExplicitTSAlgo<scalar_t>;
        using FunctionPtr = materiaux::FunctionPtr<scalar_t>;
        using NonlinearSolverPtr = materiaux::evolution::NonlinearSolverPtr<scalar_t>;
        using ExplicitTSAlgoPtr = materiaux::evolution::ExplicitTSAlgoPtr<scalar_t>;

        py::class_<Function, FunctionPtr>(
            submodule, "Function",
            R"pbdoc(Numeric function class bundling a callable and metadata.)pbdoc")
            .def_property_readonly(
                "scalar_type", [](const Function &self) { return scalar_type_to_name<scalar_t>(); })
            .def_readonly("coefficient_data", &Function::coefficient_data)
            .def_readonly("active_coefficient_mask", &Function::active_coefficient_mask)
            .def_readonly("constant_data", &Function::constant_data)
            .def_readonly("result_data", &Function::result_data)
            .def_readonly("code_data", &Function::code_data)
            .def(
                "__call__",
                [](const Function &self, py::array_t<scalar_t, py::array::c_style> res,
                   py::array_t<scalar_t, py::array::c_style> coefficients,
                   py::array_t<scalar_t, py::array::c_style> constants) {
                    materiaux::call<scalar_t>(self, res.template mutable_data(),
                                              coefficients.template data(),
                                              constants.template data());
                },
                py::arg("result"), py::arg("coefficients"), py::arg("constants"));

        py::class_<NonlinearSolver, NonlinearSolverPtr>(submodule, "NonlinearSolver",
                                                        R"pbdoc(
                Python wrapper around the NonlinearSolver C++ type in materiaux::evolution.
            )pbdoc")
            .def_readonly("parameters", &NonlinearSolver::parameters)
            .def_readonly("description", &NonlinearSolver::description)
            .def_readonly("code_data", &NonlinearSolver::code_data);

        py::class_<ExplicitTSAlgo, ExplicitTSAlgoPtr>(submodule, "ExplicitTSAlgo",
                                                      R"pbdoc(
                Python wrapper around the ExplicitTSAlgo C++ type in materiaux::evolution.
            )pbdoc")
            .def_readonly("parameters", &ExplicitTSAlgo::parameters)
            .def_readonly("description", &ExplicitTSAlgo::description)
            .def_readonly("code_data", &ExplicitTSAlgo::code_data);
    }

    template <typename scalar_t> void add_scalar_dependent_overloads(py::module module) {
        using Function = materiaux::Function<scalar_t>;
        using FunctionPtr = materiaux::FunctionPtr<scalar_t>;
        using NonlinearSolverPtr = materiaux::evolution::NonlinearSolverPtr<scalar_t>;
        using ExplicitTSAlgoPtr = materiaux::evolution::ExplicitTSAlgoPtr<scalar_t>;

        for (const auto &name : get_type_names<scalar_t>())
            module.attr("scalar_type_names").attr("append")(name.c_str());

        module.def(append_type_name<scalar_t>("_create_newton_raphson_").c_str(),
                   &materiaux::evolution::create_newton_raphson<scalar_t>);
        for (const auto &name : get_type_names<scalar_t>())
            module.attr("create_newton_raphson")[name.c_str()] =
                module.attr(append_type_name<scalar_t>("_create_newton_raphson_").c_str());

        module.def(append_type_name<scalar_t>("_create_runge_kutta_5_stable_").c_str(),
                   &materiaux::evolution::create_runge_kutta_5_stable<scalar_t>);
        for (const auto &name : get_type_names<scalar_t>())
            module.attr("create_runge_kutta_5_stable")[name.c_str()] =
                module.attr(append_type_name<scalar_t>("_create_runge_kutta_5_stable_").c_str());

        module.def(
            append_type_name<scalar_t>("_create_function_").c_str(),
            [](std::uintptr_t addr, materiaux::FieldDataPtr result,
               std::vector<materiaux::FieldDataPtr> coefficients,
               std::vector<bool> active_coefficients,
               std::vector<materiaux::FieldDataPtr> constants, materiaux::CodeDataPtr code_data) {
                return Function{typename Function::func_t{
                                    reinterpret_cast<typename Function::func_ptr_t>(addr)},
                                std::move(result),
                                std::move(coefficients),
                                std::move(active_coefficients),
                                std::move(constants),
                                std::move(code_data)};
            },
            py::arg("ptr_address"), py::arg("result"), py::arg("coefficients"),
            py::arg("active_coefficients"), py::arg("constants"), py::arg("code_data"));
        for (const auto &name : get_type_names<scalar_t>())
            module.attr("create_function")[name.c_str()] =
                module.attr(append_type_name<scalar_t>("_create_function_").c_str());

        module.def("create_evolution",
                   py::overload_cast<FunctionPtr, FunctionPtr, NonlinearSolverPtr, bool>(
                       &materiaux::evolution::create_evolution<scalar_t>),
                   py::arg("equations"), py::arg("linearized_equations"),
                   py::arg("nonlinear_solver"), py::arg("throw_on_error") = true,
                   R"pbdoc(
                       Create a :cpp:class:`materiaux::Function` encapsulating an evolution
                       whereby the internal states are detected automatically based on the
                       coefficient field data.
                   )pbdoc");

        module.def("create_evolution",
                   py::overload_cast<FunctionPtr, FunctionPtr, std::vector<materiaux::FieldDataPtr>,
                                     NonlinearSolverPtr, bool>(
                       &materiaux::evolution::create_evolution<scalar_t>),
                   py::arg("equations"), py::arg("linearized_equations"),
                   py::arg("internal_states"), py::arg("nonlinear_solver"),
                   py::arg("throw_on_error") = true,
                   R"pbdoc(
                       Create a :cpp:class:`materiaux::Function` encapsulating an evolution.
                   )pbdoc");

        module.def("create_evolution",
                   py::overload_cast<FunctionPtr, FunctionPtr, std::vector<std::string>,
                                     NonlinearSolverPtr, bool>(
                       &materiaux::evolution::create_evolution<scalar_t, std::string>),
                   py::arg("equations"), py::arg("linearized_equations"),
                   py::arg("internal_states"), py::arg("nonlinear_solver"),
                   py::arg("throw_on_error") = true,
                   R"pbdoc(
                       Create a :cpp:class:`materiaux::Function` encapsulating an evolution.
                   )pbdoc");

        module.def("create_evolution",
                   py::overload_cast<FunctionPtr, ExplicitTSAlgoPtr, bool>(
                       &materiaux::evolution::create_evolution<scalar_t>),
                   py::arg("rate"), py::arg("explicit_time_step_algo"),
                   py::arg("throw_on_error") = true,
                   R"pbdoc(
                       Create a :cpp:class:`materiaux::Function` encapsulating an evolution
                       whereby the internal states are detected automatically based on the
                       coefficient field data.
                   )pbdoc");

        module.def("create_evolution",
                   py::overload_cast<FunctionPtr, std::vector<materiaux::FieldDataPtr>,
                                     materiaux::FieldDataPtr, ExplicitTSAlgoPtr, bool>(
                       &materiaux::evolution::create_evolution<scalar_t>),
                   py::arg("rate"), py::arg("internal_states"), py::arg("time_increment"),
                   py::arg("explicit_time_step_algo"), py::arg("throw_on_error") = true,
                   R"pbdoc(
                       Create a :cpp:class:`materiaux::Function` encapsulating an evolution.
                   )pbdoc");

        module.def(
            "create_evolution",
            py::overload_cast<FunctionPtr, std::vector<std::string>, std::string, ExplicitTSAlgoPtr,
                              bool>(&materiaux::evolution::create_evolution<scalar_t, std::string>),
            py::arg("rate"), py::arg("internal_states"), py::arg("time_increment"),
            py::arg("explicit_time_step_algo"), py::arg("throw_on_error") = true,
            R"pbdoc(
                       Create a :cpp:class:`materiaux::Function` encapsulating an evolution.
                   )pbdoc");

        module.def("create_consistent_linearization",
                   py::overload_cast<FunctionPtr, FunctionPtr, FunctionPtr, FunctionPtr>(
                       &materiaux::evolution::create_consistent_linearization<scalar_t>),
                   py::arg("lin_ext_duals"), py::arg("lin_int_duals"), py::arg("lin_ext_equations"),
                   py::arg("lin_int_equations"),
                   R"pbdoc(
                       Create a :cpp:class:`materiaux::Function` encapsulating the consistent
                       linearization of a material response that depends on internal states
                       determined through evolution equations.
                   )pbdoc");
    }

    template <typename T> void define_get_symbol(py::module &module) {
        module.def((std::string("get_symbol_") + typeid(T).name()).c_str(),
                   [](const materiaux::LibHandle &lib_handle, std::string symbol_name) {
                       return std::any_cast<T>(lib_handle.contents().at(symbol_name));
                   });
        module.attr("get_symbol")[typeid(T).name()] =
            module.attr((std::string("get_symbol_") + typeid(T).name()).c_str());
    }

} // namespace

PYBIND11_MODULE(materiauXcpp, m) {
    m.doc() = "pybind11-generated python bindings for the C++ header materiaux.h";

    py::class_<CodeData, std::shared_ptr<CodeData>>(m, "CodeData")
        .def(py::init<std::string, std::string, std::set<std::string>, std::set<std::string>,
                      std::set<std::string>, std::set<std::string>, std::set<std::string>>(),
             py::arg("symbol_name"), py::arg("code"), py::arg("include_directives"),
             py::arg("include_directories"), py::arg("link_libraries"), py::arg("link_directories"),
             py::arg("compile_flags"))
        .def_readonly("symbol_name", &CodeData::symbol_name)
        .def_readonly("code", &CodeData::code)
        .def_readonly("include_directives", &CodeData::include_directives)
        .def_readonly("include_directories", &CodeData::include_directories)
        .def_readonly("link_libraries", &CodeData::link_libraries)
        .def_readonly("link_directories", &CodeData::link_directories)
        .def_readonly("compile_flags", &CodeData::compile_flags);

    py::class_<materiaux::LibHandle, std::shared_ptr<materiaux::LibHandle>>(m, "LibHandle")
        .def(py::init([](std::string shared_library) {
            return std::make_shared<materiaux::LibHandle>(std::filesystem::path(shared_library));
        }))
        .def_property_readonly(
            "contents",
            [](const materiaux::LibHandle &self) {
                return std::accumulate(
                    begin(self.contents()), end(self.contents()),
                    std::map<std::string, std::string>{},
                    [](std::map<std::string, std::string> symbols, const auto &pair) {
                        symbols[pair.first] = pair.second.type().name();
                        return std::move(symbols);
                    });
            })
        .def_property_readonly(
            "shared_library_path",
            [](const materiaux::LibHandle &self) { return self.shared_library_path().string(); })
        .def_property_readonly("module_name", &materiaux::LibHandle::module_name);

    py::class_<FieldData, std::shared_ptr<FieldData>>(m, "FieldData")
        .def(py::init<
                 std::string, std::string, std::string, int, size_t, std::vector<size_t>,
                 std::vector<size_t>,
                 std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>>>(),
             py::arg("name"), py::arg("type"), py::arg("description"), py::arg("position"),
             py::arg("size"), py::arg("rank_sizes"), py::arg("shape"), py::arg("symmetry"))
        // Note: C++ can use a vector as map/dict key, but pybind11 cannot translate this to
        // materiaux.
        .def(py::init([](std::string name, std::string type, std::string description, int position,
                         size_t size, std::vector<size_t> rank_sizes, std::vector<size_t> shape,
                         std::vector<std::map<std::vector<size_t>, std::vector<size_t>>> symmetry) {
                 using symm_type =
                     std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>>;
                 return FieldData(
                     std::move(name), std::move(type), std::move(description), position, size,
                     std::move(rank_sizes), std::move(shape),
                     std::accumulate(
                         begin(symmetry), end(symmetry), symm_type{},
                         [](symm_type symm,
                            const std::map<std::vector<size_t>, std::vector<size_t>> &map) {
                             using pair_vec_t =
                                 std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>;
                             symm.push_back(std::accumulate(
                                 begin(map), end(map), pair_vec_t{},
                                 [](pair_vec_t target, const std::pair<std::vector<size_t>,
                                                                       std::vector<size_t>> &pair) {
                                     target.push_back(pair);
                                     return std::move(target);
                                 }));
                             return std::move(symm);
                         }));
             }),
             py::arg("name"), py::arg("type"), py::arg("description"), py::arg("position"),
             py::arg("size"), py::arg("rank_sizes"), py::arg("shape"), py::arg("symmetry"))
        .def_readonly("name", &FieldData::name)
        .def_readonly("type", &FieldData::type)
        .def_readonly("description", &FieldData::description)
        .def_readonly("size", &FieldData::size)
        .def_readonly("rank_sizes", &FieldData::rank_sizes)
        .def_readonly("shape", &FieldData::shape)
        .def_readonly("symmetry", &FieldData::symmetry)
        .def_readonly("position", &FieldData::position)
        .def("__eq__", [](const FieldData &self,
                          const FieldData &other) { return materiaux::operator==(self, other); })
        .def("__str__", [](const FieldData &self) {
            std::stringstream sstr;
            sstr << self;
            return sstr.str();
        });

    m.def("create_scalar_field_data", py::overload_cast<std::string, std::string, std::string, int>(
                                          &materiaux::create_scalar_field_data));
    m.def("create_scalar_field_data",
          py::overload_cast<std::string, std::string, std::string>(&create_scalar_field_data));
    m.def("create_vector_field_data",
          py::overload_cast<std::string, std::string, std::string, size_t>(
              &materiaux::create_vector_field_data));
    m.def("create_vector_field_data",
          py::overload_cast<std::string, std::string, std::string, int, size_t>(
              &materiaux::create_vector_field_data));

    py::module mdouble = m.def_submodule("double", "submodule for scalar type double");
    add_scalar_dependent_types<double>(mdouble);

    py::module mcomplex =
        m.def_submodule("double_complex", "submodule for scalar type std::complex<double>");
    add_scalar_dependent_types<std::complex<double>>(mcomplex);

    m.attr("scalar_type_names") = py::list{};
    m.attr("create_newton_raphson") = py::dict{};
    m.attr("create_runge_kutta_5_stable") = py::dict{};
    m.attr("create_function") = py::dict{};
    add_scalar_dependent_overloads<double>(m);
    add_scalar_dependent_overloads<std::complex<double>>(m);

    py::module any = m.def_submodule("any", "Handling of std:any objects");
    any.attr("get_symbol") = py::dict{};
    define_get_symbol<const char *>(any);
    define_get_symbol<materiaux::FunctionPtr<double>>(any);
    define_get_symbol<materiaux::FunctionPtr<std::complex<double>>>(any);
}