# Copyright (C) 2019-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


import materiaux.modeling.core as core
import ufl
import ufl.indexed
from ufl.core.expr import Expr
from ufl.core.operator import Operator
import typing
import itertools

# Operators and other useful things for writing a material model
from materiaux.modeling.core import (derivative, Function, equations, derivatives)

# Split function
from ufl.split_functions import split

# Literal constants
from ufl.constantvalue import PermutationSymbol, Identity, Zero, zero, as_ufl

# Indexing of tensor expressions
from ufl.core.multiindex import Index, indices

# Containers for expressions with value rank > 0
from ufl.tensors import as_tensor, as_vector, as_matrix, relabel
from ufl.tensors import unit_vector, unit_vectors, unit_matrix, unit_matrices

# Misc operators
from ufl.operators import \
    conj, real, imag, \
    outer, inner, dot, cross, perp, \
    det, inv, cofac, \
    transpose, tr, diag, diag_vector, \
    dev, skew, sym, \
    sqrt, exp, ln, erf, \
    cos, sin, tan, \
    acos, asin, atan, atan_2, \
    cosh, sinh, tanh, \
    bessel_J, bessel_Y, bessel_I, bessel_K, \
    eq, ne, le, ge, lt, gt, And, Or, Not, \
    conditional, sign, max_value, min_value, Max, Min, \
    variable, diff

# More manipulation
from ufl.formoperators import replace, extract_blocks


enable_experimental = False


class ExperimentNotEnabled(Exception):
    def __init__(self):
        super().__init__(
            self,
            "This is an experimental and possibly untested feature. "
            "If you really want to use it, set 'materiaux.gsm.enable_experimental' to 'True'."
        )


def _requires_experimental():
    if not enable_experimental:
        raise ExperimentNotEnabled()


class State(core.Coefficient):
    """Just providing another name for Coefficient. Generally used to represent a state variable."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class Parameter(core.Constant):
    """Just providing another name for Constant. Generally used to represent a material parameter."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class TimeRate(core.Coefficient):
    """A class describing a discrete time rate. Useful for numerical time integration algorithms."""

    def __init__(self, coefficient: typing.Union[core.Coefficient, ufl.indexed.Indexed], discretization: Expr):
        """Constructor taking the coefficient of which the rate is computed and the actual expression of the rate"""
        if not (isinstance(coefficient, core.Coefficient) or not (
                isinstance(coefficient.ufl_operands, ufl.indexed.Indexed)
                and isinstance(coefficient.ufl_operands[0], core.Coefficient))):
            raise ValueError('Coefficient must be of type ufl_extensions.Coefficient '
                             'or ufl.Indexed(ufl_extensions.Coefficient)')
        if isinstance(coefficient, TimeRate) or isinstance(coefficient.ufl_operands, TimeRate):
            raise ValueError('Time rate of time rate is not allowed.')
        super().__init__("{:s}_dot".format(coefficient.name),
                         description="time rate of {:s}".format(coefficient.name),
                         function_space=coefficient.ufl_function_space() if isinstance(coefficient, core.Coefficient)
                         else core._mk_element(core.CoeffShape(), coefficient.ufl_domain()))
        self._base_coefficient = coefficient if isinstance(coefficient, core.Coefficient) else coefficient.ufl_operands[
            0]
        self._base_obj = coefficient
        self._discretization = variable(discretization)

    def discretization(self) -> Operator:
        """The rate expression"""
        return self._discretization

    def base_coefficient(self) -> core.Coefficient:
        """The coefficient"""
        return self._base_coefficient

    def base_object(self) -> typing.Union[core.Coefficient, ufl.indexed.Indexed]:
        """The actual object to which the rate applies. Either the coefficient or some part of it."""
        return self._base_obj


class LieRate(core.Coefficient):
    """
    [Experimental, WIP] Representing a Lie rate in the sense of the push-forward of a referential rate. This is useful, e.g., for
    dissipation potentials which are expressed in terms of Lie rates but the actual internal variable is referential in
    the sense defined by actual form of the Lie rate.
    """

    def __init__(self, referential_rate: TimeRate, transform: typing.List[Expr]):
        """
        Create a Lie rate based on the referential time rate and its push-forward.

        :param referential_rate: the referential time rate
        :param transform: the push-forward of the referential rate given in terms of
        the "volume transform" followed by the maps for each tensorial dimension. If F is the tangent map,
        one has to provide [1, F] for a vector, [1/det(F), F] for a flux-type object and [1, inv(F.T)] for a one-form.
        """
        _requires_experimental()
        if not isinstance(referential_rate, TimeRate):
            raise ValueError('"referential_rate" has to be of type TimeRate')
        if referential_rate.base_coefficient() != referential_rate.base_object():
            raise ValueError('The referential time rate must be for the full coefficient, not only some component.')
        super().__init__("LL_{:s}".format(referential_rate.name),
                         description="Push forward of {:s}, i.e. the Lie rate of the push forward of {:s}".format(
                             referential_rate.name, referential_rate.base_coefficient()),
                         function_space=referential_rate.ufl_function_space())
        self._referential_rate = referential_rate
        self._transform = transform

    def referential_rate(self):
        """Access the referential rate"""
        return self._referential_rate

    def transform(self):
        """Access the transform rate"""
        return self._transform


class Time(core.Constant):
    """
    Representing time. While just a constant, by being a subclass it can be distinguished from other constants by
    specialized symbolic algorithms.
    """

    def __init__(self, name: str = "t"):
        super().__init__(name)


class HistoryTime(core.Constant):
    """
    Representing past time. While just a constant, by being a subclass it can be distinguished from other constants by
    specialized symbolic algorithms.
    """

    def __init__(self, base_time):
        if not isinstance(base_time, (Time, HistoryTime)):
            raise ValueError("Base object must be of type 'Time' or 'HistoryTime'")
        super().__init__("{:s}_n".format(base_time.name),
                         description="previous value of {:s}".format(base_time.name),)
        self._base_time = base_time

    def base_time(self):
        return self._base_time


class TimeIncrement(core.Constant):
    """
    Representing a time increment. While just a constant, by being a subclass it can be distinguished from other
    constants by specialized algorithms.
    """
    def __init__(self, name: str = "Delta_t"):
        super().__init__(name)


class HistoryState(core.Coefficient):
    """
    A history field. A special type of coefficient since it cannot exist without a "master" coefficient of which it is
    the history.
    """

    def __init__(self, coefficient: typing.Union[core.Coefficient, ufl.indexed.Indexed]):
        """
        Constructor of which one wants to store the history. The name of the history field is the name of the
        coefficient plus "_n".
        """
        if not (isinstance(coefficient, core.Coefficient) or not (
                isinstance(coefficient.ufl_operands, ufl.indexed.Indexed)
                and isinstance(coefficient.ufl_operands[0], core.Coefficient))):
            raise ValueError('Coefficient must be of type ufl_extensions.Coefficient '
                             'or ufl.Indexed(ufl_extensions.Coefficient)')
        super().__init__("{:s}_n".format(coefficient.name),
                         description="previous value of {:s}".format(coefficient.name),
                         function_space=coefficient.ufl_function_space() if isinstance(coefficient, core.Coefficient)
                         else core._mk_element(core.CoeffShape(), coefficient.ufl_domain()))
        self._base_coefficient = coefficient if isinstance(coefficient, core.Coefficient) else coefficient.ufl_operands[
            0]
        self._base_obj = coefficient

    def base_coefficient(self) -> core.Coefficient:
        """The base coefficient of which the history is represented by the current object"""
        return self._base_coefficient

    def base_object(self) -> typing.Union[core.Coefficient, ufl.indexed.Indexed]:
        """The base object -- either the base coefficient or some part of it"""
        return self._base_obj


class InternalState(core.Coefficient):
    """A class distinguishing an internal state"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


def replace_time_rates(form_or_expression: Expr, time_rates) -> Expr:
    """Replace time rates by their actual expressions"""
    return ufl.replace(form_or_expression, dict([(time_rate, time_rate.discretization()) for time_rate in time_rates]))


def find_internal_time_rates(expr: typing.Union[Expr, core.Function]) -> typing.List[TimeRate]:
    """Find time rates of internal states"""
    coeffs = expr.coefficients() if isinstance(expr, core.Function) else ufl.algorithms.extract_coefficients(expr)
    time_rates = [coeff for coeff in coeffs if isinstance(coeff, TimeRate)]
    time_rates += [coeff.referential_rate() for coeff in coeffs if isinstance(coeff, LieRate)]
    return core.sorted_by_count(filter(lambda item: isinstance(item.base_coefficient(), InternalState),
                                       time_rates))


def replace_internal_time_rates(expr: Expr) -> Expr:
    """Replace time rates of internal states by their expressions"""
    time_rates = find_internal_time_rates(expr)
    if len(time_rates) > 0:
        return ufl.replace(expr, dict([(time_rate, time_rate.discretization()) for time_rate in time_rates]))
    else:
        return expr


class EvolutionEquations(core.Function):
    """An Function representing the evolution of internal states"""

    def internal_states(self) -> typing.List[InternalState]:
        return core.sorted_by_count([state for state in self.coefficients() if isinstance(state, InternalState)])


class GSMBase:
    """
    Class holding the essential Function objects for a material model within the framework of Generalized Standard
    Materials. This class only holds data but does not provide symbolic manipulations. Such can be implemented by
    subclasses.
    """

    def __init__(self, energy_density: typing.Union[core.Function, Expr],
                 evolution_equations: typing.Union[core.Function, Expr] = core.Zero):
        self._energy_density = energy_density if isinstance(energy_density, core.Function) \
            else core.Function("energy_density", energy_density)
        evolution_equations = evolution_equations if isinstance(evolution_equations, core.Function) \
            else core.Function("evolution_equations", evolution_equations)
        self._evolution_equations = EvolutionEquations(
            evolution_equations.name(), replace_internal_time_rates(evolution_equations.expression())
        )

    def energy_density(self) -> core.Function:
        return self._energy_density

    def evolution_equations(self) -> core.Function:
        return self._evolution_equations

    def all_coefficients(self) -> typing.List[core.Coefficient]:
        return core.sorted_by_count(
            set(self.energy_density().coefficients() + self.evolution_equations().coefficients()))

    def all_constants(self) -> typing.List[core.Constant]:
        return core.sorted_by_count(set(self.energy_density().constants() + self.evolution_equations().constants()))

    def internal_states(self) -> typing.List[InternalState]:
        return core.sorted_by_count([state for state in self.all_coefficients() if isinstance(state, InternalState)])

    def history_states(self) -> typing.List[HistoryState]:
        return core.sorted_by_count([state for state in self.all_coefficients() if isinstance(state, HistoryState)])

    def internal_time_rates(self) -> typing.List[TimeRate]:
        return find_internal_time_rates(self.evolution_equations())

    # TODO: is this semantically okay?
    def time_states(self) -> typing.List[typing.Union[Time, HistoryTime]]:
        return core.sorted_by_count(
            set([coeff for coeff in set(self.all_coefficients() + self.all_constants())
                 if isinstance(coeff, (Time, HistoryTime))])
        )

    def external_states(self) -> typing.List[core.Coefficient]:
        others = set(self.internal_states() + self.history_states() + self.internal_time_rates() + self.time_states())
        return core.sorted_by_count(
            [item for item in list(set(self.all_coefficients()) - others) if not isinstance(item, core.Constant)])


def _make_internal_constraint_eqs(internal_constraints: typing.Iterable = None) \
        -> typing.Tuple[typing.Optional[core.Function], typing.Optional[InternalState]]:
    internal_multipliers = None
    internal_constraint_eqs = None
    if internal_constraints is not None:
        _constraints = list(constraint.expression() if isinstance(constraint, Function) else constraint
                            for constraint in internal_constraints)
        _elements = []
        for constraint in _constraints:
            _arguments = ufl.algorithms.extract_arguments(constraint)
            if len(_arguments) == 0:
                _elements.append(core.Coefficient("___").ufl_element())
            elif len(_arguments) == 1:
                _requires_experimental()
                _elements.append(_arguments.ufl_function_space().ufl_element())
            else:
                raise ValueError("Expressions with more than one ufl.Argument are not supported.")

        multiplier_element = ufl.MixedElement(_elements)
        internal_multipliers = InternalState("internal_multipliers",
                                             shape=multiplier_element.value_shape(),
                                             symmetry=multiplier_element.symmetry())
        internal_constraint_eqs = core.Function(
            "internal_constraints",
            sum([ufl.inner(-internal_multipliers[ii], _constraint) for ii, _constraint in enumerate(_constraints)])
        )
    return internal_constraint_eqs, internal_multipliers


class GeneralizedStandardMaterial(GSMBase):
    """
    Class for the classical definition of a Generalized Standard Material model
    """

    def __init__(self, energy_density: typing.Union[core.Function, Expr],
                 dissipation_potential: typing.Union[core.Function, Expr] = core.Zero,
                 internal_constraints: typing.Iterable[typing.Union[core.Function, Expr]] = None):
        energy_density = energy_density if isinstance(energy_density, core.Function) \
            else core.Function("energy_density", energy_density)
        dissipation_potential = dissipation_potential if isinstance(dissipation_potential, core.Function) \
            else core.Function("dissipation_potential", dissipation_potential)

        internal_constraints, internal_multipliers = _make_internal_constraint_eqs(internal_constraints)
        internal_states = self._detect_internal_states_for_evolution_eqs(dissipation_potential, internal_constraints)
        if dissipation_potential == core.Zero or len(internal_states) == 0:
            super(energy_density, core.Zero)
            return

        evolution_eqs = {}
        for internal_state in internal_states:
            evolution_terms = [diff(energy_density.expression(), internal_state)] \
                if internal_state in energy_density.coefficients() else []
            time_rate = self._find_time_rate(dissipation_potential, internal_state)
            if time_rate is None and internal_state in dissipation_potential.coefficients():
                # This case handles multipliers and Legendre transforms
                evolution_terms.append(diff(dissipation_potential.expression(), internal_state))
            elif time_rate is not None:
                evolution_terms.append(diff(dissipation_potential.expression(), time_rate))
            if internal_constraints is not None and internal_state in internal_constraints.coefficients():
                evolution_terms.append(diff(internal_constraints.expression(), internal_state))
            evolution_eqs[internal_state] = sum(evolution_terms, zero(internal_state.ufl_shape))
            if time_rate is not None:
                evolution_eqs[internal_state] = replace_time_rates(evolution_eqs[internal_state], [time_rate])
        super().__init__(energy_density, core.Function("evolution_eqs", equations(evolution_eqs)))
        self._dissipation_potential = dissipation_potential
        self._internal_constraints = internal_constraints
        self._internal_multipliers = internal_multipliers

    def dissipation_potential(self) -> core.Function:
        """The dissipation potential used in this model"""
        return self._dissipation_potential

    @staticmethod
    def _find_time_rate(dissipation_potential: core.Function, state: core.Coefficient) -> typing.Union[
        TimeRate, LieRate, None]:
        def _filter_func(coeff: core.Coefficient):
            return (isinstance(coeff, TimeRate) and coeff.base_coefficient() == state) \
                   or (isinstance(coeff, LieRate) and coeff.referential_rate().base_coefficient() == state)

        found = list(set(filter(_filter_func, dissipation_potential.coefficients())))
        if len(found) == 0:
            return None
        elif len(found) == 1:
            return found[0]
        else:
            raise ValueError("Only one time rate per coefficient is allowed.")

    @staticmethod
    def _detect_internal_states_for_evolution_eqs(dissipation_potential: core.Function,
                                                  internal_constraints: typing.Iterable = None) -> typing.List[
        InternalState]:
        time_rates = find_internal_time_rates(dissipation_potential)
        internal_states = [trate.base_coefficient() for trate in time_rates]
        internal_states += list(
            filter(lambda item: isinstance(item, InternalState),
                   dissipation_potential.coefficients() + (internal_constraints.coefficients()
                                                           if internal_constraints is not None else [])
                   )
        )
        return core.sorted_by_count(set(internal_states))


class IncrementalMaterialPotential(GSMBase):
    """
    Class for the definition of a Generalized Standard Material in terms of an incremental variational principle
    """

    def __init__(self, energy_density: typing.Union[core.Function, Expr],
                 dissipation_potential: typing.Union[core.Function, Expr] = None,
                 time_integrator: typing.Callable = None,
                 internal_constraints: typing.Iterable = None):
        energy_density = energy_density if isinstance(energy_density, Function) \
            else core.Function("energy_density", energy_density)
        if dissipation_potential is None:
            if time_integrator is not None or internal_constraints is not None:
                raise ValueError("When dissipation potential is not given, the energy_density is the only allowed "
                                 "argument and understood and the incremental potential.")
            self._incremental_potential = core.Function("incremental_potential",
                                                        replace_internal_time_rates(energy_density.expression()))
            self._dissipation_potential = None
            internal_states = [coeff for coeff in self.incremental_potential().coefficients()
                               if isinstance(coeff, InternalState)]
            super().__init__(
                self.incremental_potential(),
                core.derivative(self.incremental_potential(), internal_states, dname="evolution_eqs")
            )
        else:
            dissipation_potential = dissipation_potential if isinstance(dissipation_potential, Function) \
                else core.Function("dissipation_potential", dissipation_potential)
            _internal_constraint_eqs = _make_internal_constraint_eqs(internal_constraints)
            self._internal_constraints = _internal_constraint_eqs[0]
            self._internal_multipliers = _internal_constraint_eqs[1]

            self._time_integrator = time_integrator
            dissipation_pot_integral = core.Function("dissipation_pot_integral",
                                                     self._time_integrator(dissipation_potential.expression()))
            incremental_integrand = energy_density.expression() + dissipation_pot_integral.expression()
            if self._internal_constraints is not None:
                incremental_integrand += self._internal_constraints.expression()

            for time_rate in find_internal_time_rates(dissipation_potential):
                if time_rate.base_coefficient() in dissipation_potential.coefficients():
                    raise ValueError("For an incremental variational principle, the dissipation potential must not "
                                     "depend on the current internal state but only on its time rate.")

            # the real one
            self._incremental_potential = core.Function("incremental_potential",
                                                        replace_internal_time_rates(incremental_integrand))
            internal_states = [coeff.base_coefficient() for coeff in find_internal_time_rates(dissipation_potential)]
            if self._internal_multipliers is not None:
                internal_states.append(self._internal_multipliers)

            super().__init__(energy_density, core.derivative(self.incremental_potential(), internal_states,
                                                             dname="evolution_eqs"))
            self._check_dissipation_potential(dissipation_potential)

    def _check_dissipation_potential(self, dissipation_potential: core.Function):
        dissipation_coeffs = set(dissipation_potential.coefficients())
        external_coeffs = set(self.external_states())
        if not dissipation_coeffs.isdisjoint(external_coeffs):
            raise ValueError(
                "The given dissipation potential depends on external states. This is not allowed."
                "Problematic states: {:s}".format(
                    str([state for state in dissipation_coeffs.intersection(external_coeffs)])
                )
            )

    def incremental_potential(self) -> core.Function:
        """The incremental potential describing this model"""
        return self._incremental_potential


def _create_module_data_gsm(material_model: GSMBase) -> core.Namespace:
    """
    A helper creating module data for a GSM model

    :return: Data than can be fed into codegeneration.module_generation.generate_module
    """
    ext_states = tuple(material_model.external_states())
    all_coefficients = material_model.all_coefficients()
    all_constants = material_model.all_constants()

    stored_energy = core.Function("stored_energy", material_model.energy_density().expression(),
                                  additional_coefficients=all_coefficients, additional_constants=all_constants)

    duals = core.derivative(stored_energy, ext_states, dname="duals", dindex=0)
    d_ext_duals = core.derivative(duals, ext_states, dname="d_ext_duals", dindex=0)

    data = core.Namespace(**{"material_response::{:s}".format(expr.name()): expr
                                 for expr in [stored_energy, duals, d_ext_duals]})

    int_states = tuple(material_model.internal_states())
    if material_model.evolution_equations().expression() != Zero() and len(int_states) != 0:
        d_int_duals = core.derivative(duals, int_states, dname="d_int_duals", dindex=1)
        data.material_response.set_item(d_int_duals.name(), d_int_duals)

        evolution_eqs = core.Function("eqs", material_model.evolution_equations().expression(),
                                      additional_coefficients=all_coefficients, additional_constants=all_constants)
        eqs_lin_ext = core.derivative(evolution_eqs, ext_states, dindex=0, dname="d_ext_eqs")
        eqs_lin_int = core.derivative(evolution_eqs, int_states, dindex=1, dname="d_int_eqs")
        for item in evolution_eqs, eqs_lin_ext, eqs_lin_int:
            data.set_item("evolution_equations::{:s}".format(item.name()), item)
    return data


def _create_module_data_incremental_potential(material_model: IncrementalMaterialPotential) -> core.Namespace:
    """
    A helper creating module data for an IncrementalMaterialPotential model

    :return: Data than can be fed into codegeneration.module_generation.generate_module
    """
    ext = tuple(material_model.external_states())
    int = tuple(material_model.internal_states())
    all_coefficients = material_model.all_coefficients()
    all_constants = material_model.all_constants()

    incremental_potential = core.Function("incremental_potential", material_model.incremental_potential().expression(),
                                          additional_coefficients=all_coefficients,
                                          additional_constants=all_constants)

    states = {"ext": (0, ext), "int": (1, int)}

    derivatives = {tuple(): incremental_potential}

    for order in range(1, 3):
        for si in itertools.product(states.keys(), repeat=order):
            ti = tuple(si)
            index, state = states[ti[-1]]
            base = derivatives[ti[:-1]]
            derivatives[tuple(si)] = core.derivative(base, state,
                                                     dname="d_{:s}_{:s}".format(ti[-1], base.name()),
                                                     dindex=index)

    return core.Namespace(**{derivative.name(): derivative for _, derivative in sorted(derivatives.items())})


def create_module_data(material_model: GSMBase) -> core.Namespace:
    """
    A helper creating module data for GSM model with special handling for incremental potentials

    :return: Data than can be fed into codegeneration.module_generation.generate_module
    """
    if isinstance(material_model, IncrementalMaterialPotential):
        return _create_module_data_incremental_potential(material_model)
    elif isinstance(material_model, GSMBase):
        return _create_module_data_gsm(material_model)
    else:
        raise ValueError("Unsupported material model of type '{:s}'".format(type(material_model)))
