# Copyright (C) 2019-2020 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


import typing
from typing import Iterable
import re

import numpy as np
import ufl
import ufl.formoperators as _form_operators
from ufl.core.expr import Expr

from materiaux import FieldData, Namespace

_ufl_cell = ufl.interval
_ufl_domain = ufl.Mesh(ufl.VectorElement("P", _ufl_cell, 1))


def check_name(name: str):
    """A helper checking the validity of a given name"""
    if re.search(r"\W", name) is not None:
        raise ValueError("'name' must only contain letters, digits and '_'!")
    return name


def sorted_by_count(coeffs_or_constants: typing.Union[typing.Iterable[ufl.Coefficient], typing.Iterable[ufl.Constant]]) \
        -> typing.Union[typing.List[ufl.Coefficient], typing.List[ufl.Constant]]:
    """Sort given objects (either Coefficients or Constants; but not mixed) by their ufl count"""
    return sorted(coeffs_or_constants, key=lambda item: item.count())


class CoeffShape:
    """A helper class holding shape and symmetry data."""

    def __init__(self, shape: Iterable[int] = None, symmetry: typing.Union[bool, dict] = None):
        shape = tuple(shape) if shape is not None else tuple()
        if len(shape) < 1 and symmetry is not None:
            raise ValueError("'symmetry' can only be given in combination with non-scalar shapes or larger, "
                             "i.e. for 2nd- or higher-order tensors.")
        self.shape = shape
        self.symmetry = symmetry


def _mk_element(coeff_shape: CoeffShape, cell) -> ufl.FiniteElementBase:
    shape = coeff_shape.shape
    symmetry = coeff_shape.symmetry
    if shape is None or len(shape) == 0:
        return ufl.FiniteElement("R", cell, degree=0)
    elif len(shape) == 1:
        return ufl.VectorElement("R", cell, degree=0, dim=shape[0])
    else:
        return ufl.TensorElement("R", cell, degree=0, shape=shape, symmetry=symmetry)


class Constant(ufl.Constant):
    """A spatially constant value."""

    def __init__(self, name: str, shape: typing.Tuple[int] = None,
                 description: str = ""):
        if name == "res":
            raise RuntimeError("name '{:s}' is reserved.".format(name))
        shape = shape if shape is not None else tuple()
        super().__init__(_ufl_domain, shape=shape)
        self.name = check_name(name)
        self.rank = 0 if len(self.ufl_shape) == 0 else 1
        self.description = description
        self._repr = "Constant({!s}, {!s})".format(repr(self._count), repr(self.name))

    def field_data(self) -> FieldData:
        size = 1
        for dim in self.ufl_shape:
            size *= dim
        return FieldData(self.name, self.__class__.__name__, self.description, -1, size,
                         (size,) if self.rank != 0 else tuple(), self.ufl_shape, ({},))

    def __str__(self):
        count = str(self._count)
        if len(count) == 1:
            return "w_{:s}[{:s}]".format(count, self.name)
        else:
            return "w_{{{:s}}}[{:s}]".format(count, self.name)


class Coefficient(ufl.Coefficient):
    """A coefficient, ie. spatially non-constant."""

    def __init__(self, name: str, shape: Iterable[int] = None,
                 symmetry: typing.Union[bool, dict] = None, description: str = "",
                 function_space: ufl.FunctionSpace = None):
        if name in ("res", "lambda"):
            raise RuntimeError("name '{:s}' is reserved.".format(name))
        if function_space is None:
            function_space = ufl.FunctionSpace(_ufl_domain, _mk_element(CoeffShape(shape, symmetry), _ufl_cell))

        super().__init__(function_space)
        self.name = check_name(name)
        self.description = description
        self.rank = 0 if len(self.ufl_shape) == 0 else 1
        self._repr = "Coefficient({!s}, {!s}, {!s})".format(
            repr(self._ufl_function_space), repr(self._count), repr(self.name))

    def field_data(self) -> FieldData:
        return FieldData(self.name, self.__class__.__name__, self.description, -1,
                         self.ufl_element().reference_value_size(),
                         (self.ufl_element().reference_value_size(),) if self.rank != 0 else tuple(),
                         self.ufl_shape, (self.ufl_element().symmetry(),))

    def __str__(self):
        count = str(self._count)
        if len(count) == 1:
            return "w_{:s}[{:s}]".format(count, self.name)
        else:
            return "w_{{{:s}}}[{:s}]".format(count, self.name)


def create_mixed(name: str, objects: typing.Union[typing.Iterable[Constant], typing.Iterable[Coefficient]],
                 description: str = "") -> typing.Union[Constant, Coefficient]:
    """
    Create a Coefficient on a "mixed" space or a Constant of shape dervied from the given objects.

    :param name: name of the mixed coefficient or constant
    :param objects: the objects from which the "mixed"/combined object is created from
    :param description: description of the new object
    :return: the mixed coefficient or constant
    """
    _objects = list(objects)
    if len(_objects) > 1:
        for _object in _objects[1:]:
            if not isinstance(_object, type(_objects[0])):
                raise ValueError("All obejcts must be of same type. This is not the case.")
    elif len(_objects) == 0:
        raise ValueError("Please provide at least one object.")

    _class = type(_objects[0])
    if issubclass(_class, Constant):
        tuple_size = sum([np.zeros(_object.ufl_shape).size for _object in _objects])
        return _class(name, (tuple_size,), description=description)
    elif issubclass(_class, Coefficient):
        element = ufl.MixedElement(
            [_mk_element(CoeffShape(_object.ufl_shape, symmetry=_object.ufl_element().symmetry()), _ufl_cell) for
             _object in _objects])
        return _class(name, function_space=ufl.FunctionSpace(_ufl_domain, element), description=description)


class _Form(ufl.Form):
    """
    An internal helper class adding some functionality (mainly adding forms coefficients/constants) to ufl Forms.
    """
    def __init__(self, ufl_form: typing.Union[ufl.form.Form, Expr],
                 additional_coefficients: typing.Iterable[Coefficient] = None,
                 additional_constants: typing.Iterable[Constant] = None):
        if not isinstance(ufl_form, ufl.Form):
            ufl_form = ufl_form * ufl.dx(domain=_ufl_cell, metadata={"quadrature_degree": 0})
        integrals = ufl_form.integrals()
        if len(integrals) != 1:
            raise RuntimeError("Form must have exactly *one* integral.")
        super().__init__(integrals)

        self._constant_numbering = dict((c, i) for i, c in enumerate(self._constants))
        self._analyze_form_arguments()
        if additional_coefficients is not None:
            self.add_coefficients(additional_coefficients)
        if additional_constants is not None:
            self.add_constants(additional_constants)
        self._hash = None

    def add_constants(self, constants: typing.Iterable[Constant]):
        self._constants = list(sorted_by_count(set(self._constants) | set(constants)))
        self._constant_numbering = dict((c, i) for i, c in enumerate(self._constants))

    def add_coefficients(self, coefficients: typing.Iterable[Coefficient]):
        self._coefficients = list(sorted_by_count(set(self._coefficients) | set(coefficients)))
        self._coefficient_numbering = dict((c, i) for i, c in enumerate(self._coefficients))

    def _compute_renumbering(self):
        # Include integration domains, coefficients and constants in renumbering
        dn = self.domain_numbering()
        coefficient_numbering = self.coefficient_numbering()
        constant_numbering = self._constant_numbering
        renumbering = {}
        renumbering.update(dn)
        renumbering.update(coefficient_numbering)
        renumbering.update(constant_numbering)

        # Add domains of coefficients and constants, these may include domains not
        # among integration domains
        k = len(dn)
        for c in list(coefficient_numbering.keys()) + list(constant_numbering.keys()):
            d = c.ufl_domain()
            if d is not None and d not in renumbering:
                renumbering[d] = k
                k += 1

        # Add domains of arguments, these may include domains not
        # among integration domains
        for a in self._arguments:
            d = a.ufl_function_space().ufl_domain()
            if d is not None and d not in renumbering:
                renumbering[d] = k
                k += 1

        return renumbering

    def integrand(self):
        return self.integrals()[0]._integrand

    def derivative(self, coefficients: typing.Union[Coefficient, typing.Iterable[Coefficient]]):
        if isinstance(coefficients, typing.Iterable):
            # workaraound for FFCX-issue #246
            _coeffs, _args = _form_operators._handle_derivative_arguments(self, coefficients, None)
            der = sum([ufl.inner(ufl.diff(self.integrand(), coefficient), argument)
                       for coefficient, argument in zip(_coeffs, _args)])
        else:
            der = ufl.derivative(self, coefficients)
        return _Form(der, additional_coefficients=self.coefficients(), additional_constants=self.constants())


# TODO: work directly with ufl.core.Expr and avoid ufl.form.Form once FFCX's support for Expressions and their UFC
#  interface is considered stable. Handling of symmetries and "additional" coefficients might be an issue though.
#  This is a feature of coefficients but not expressions.

# TODO: extend docs

class Function:
    """
    The fundamental type for representing models. It wraps a UFL expression and provides additional metadata similar to
    a UFL form.
    """

    def __init__(self, name: str, expression: typing.Union[Expr, ufl.Form],
                 additional_coefficients: typing.Iterable[Coefficient] = None,
                 additional_constants: typing.Iterable[Constant] = None,
                 derivative_state: typing.List = None, base: object = None,
                 scope: str = None, symmetry: typing.Union[dict, bool, None] = None):
        self._name = check_name(name)
        self._scope = scope
        if base is None and derivative_state is not None or \
                base is not None and derivative_state is None:
            raise RuntimeError("Either both or none of 'base' and 'derivative_state' have to be given. "
                               "Intended to be used by 'derivative()' only.")
        self._derivative_state = [] if derivative_state is None else derivative_state
        self._base = base
        additional_coefficients = [] if additional_coefficients is None else list(additional_coefficients)
        additional_constants = [] if additional_constants is None else list(additional_constants)

        if isinstance(base, Function):
            additional_coefficients = list(set(additional_coefficients) | set(base.coefficients()))
            additional_constants = list(set(additional_constants) | set(base.constants()))

        if isinstance(expression, ufl.form.Form):
            if len(expression.integrals()) != 1:
                raise RuntimeError("Form must have exactly *one* integral.")
            all_coefficients = list(set(list(expression.coefficients()) + list(additional_coefficients)))
            all_constants = list(
                set(list(expression.constants()) + list(additional_constants)))
            self._form = _Form(expression,
                               additional_coefficients=all_coefficients,
                               additional_constants=all_constants)
        elif isinstance(expression, Expr):
            # hack around the misuse of ufl.form.Form
            if np.prod(expression.ufl_shape) != 1:
                n_arguments = len(ufl.algorithms.extract_arguments(expression))
                coeff_shape = CoeffShape(expression.ufl_shape, symmetry)
                TF = ufl.Argument(ufl.FunctionSpace(_ufl_domain, _mk_element(coeff_shape, _ufl_cell)), n_arguments)
                expression = ufl.inner(expression, TF)
            self._form = _Form(expression * ufl.dx(domain=_ufl_cell, metadata={"quadrature_degree": 0}),
                               additional_coefficients=additional_coefficients,
                               additional_constants=additional_constants)
        else:
            raise ValueError("Unexpected type of argument 'expression': type(expression) = {:s}"
                             .format(type(expression)))

    def coefficients(self) -> typing.List[Coefficient]:
        return list(self._form.coefficients())

    def constants(self) -> typing.List[Constant]:
        return list(self._form.constants())

    def arguments(self) -> typing.List[ufl.Argument]:
        return self._form.arguments()

    def scoped_name(self) -> str:
        if self._scope is not None:
            return "_".join([self._scope, self._name])
        else:
            return self._name

    def base_obj(self) -> typing.Optional:
        return self._base

    def derivative_state(self) -> typing.List[int]:
        return self._derivative_state

    def name(self) -> str:
        return self._name

    def expression(self) -> Expr:
        return self._form.integrand()

    def __hash__(self) -> int:
        return hash(self._form)

    def signature(self) -> str:
        """
        Signature for "jitting"
        """
        return self._form.signature()

    def derivative(self, coefficients: typing.Union[Coefficient, typing.Iterable[Coefficient]], dname=None,
                   dindex=None):
        """
        Derivative 'obj' wrt. given coefficients.

        :param func: the object to derive from
        :param coefficients: the coefficients with which to derive
        :param dname: [expert/internal] name of the new Expression
        :param dindex: [expert/internal] index of the new Expression within the derivation hierarchy
        :return: the derivative
        """
        if not isinstance(coefficients, typing.Iterable) or isinstance(coefficients, (ufl.Coefficient, Constant)):
            coefficients = [coefficients]
        if any([isinstance(coeff, Constant) or not isinstance(coeff, ufl.Coefficient) for coeff in coefficients]):
            raise ValueError("Currenty, only derivatives wrt to coefficients are supported.")
        self._form.add_coefficients(coefficients)
        indices = [self.coefficients().index(coeff) for coeff in coefficients]

        if dindex is None:
            dstate = self.derivative_state() + [indices[0]] if len(indices) == 1 else None
        else:
            dstate = self.derivative_state() + [dindex]

        if dname is None:
            if dstate is not None:
                dname = "d{:d}_{:s}".format(dstate[-1], self.name())
            else:
                dname = "d{:s}_{:s}".format("_".join([str(index) for index in indices]), self.name())

        return Function(dname, self._form.derivative(coefficients), derivative_state=dstate,
                        base=None if dstate is None else self)


Zero = Function("Zero", ufl.constantvalue.Zero())


def derivative(func: Function, coefficients: typing.Union[Coefficient, typing.Iterable[Coefficient]], dname=None,
               dindex=None) -> Function:
    """
    Derive 'obj' wrt. given coefficients. This function only works on core.Function but not on UFL
    types. Its purpose is to create derivatives that carry on information about the derived from object and symmetry.

    :param func: the object to derive from
    :param coefficients: the coefficients with which to derive
    :param dname: [expert/internal] name of the new Expression
    :param dindex: [expert/internal] index of the new Expression within the derivation hierarchy
    :return: the derivative
    """
    if isinstance(func, Function):
        return func.derivative(coefficients, dname=dname, dindex=dindex)
    else:
        raise ValueError("Object is of unsupported type '{:s}'".format(str(type(func))))


def derivatives(func: Function, coeffs: typing.Iterable[Coefficient] = None, max_degree=1) \
        -> typing.List[Function]:
    """
    Create derivatives wrt. given (or all) coefficients up to degree 'max_degree'.
    """
    coeffs = coeffs if coeffs is not None else list(
        filter(lambda item: not isinstance(item, Constant), func.coefficients()))
    derivatives = [func]
    new_derivatives = derivatives
    for ii in range(max_degree):
        print([vv.coefficients() for vv in new_derivatives])
        print("\n")
        new_derivatives = sum(
            [[derivative(vv, cc) for cc in coeffs if cc in vv.coefficients()] for vv in new_derivatives],
            []
        )
        derivatives += new_derivatives
    return derivatives


def _extract_arguments(func: typing.Union[Function, Expr]) -> typing.List[ufl.Argument]:
    """Extract ufl form arguments."""
    return func.arguments() if isinstance(func, Function) else ufl.algorithms.extract_arguments(func)


def equations(equation_data: typing.Dict[typing.Union[Coefficient, CoeffShape],
                                         typing.Union[Function, Expr]]) -> Expr:
    """
    Create proper equation expressions and, if more than one, merge them into a "single" (multi-valued) one.
    The 'Coefficients' used as keys simply provide a hint on the shape and symmetry of the actual equation.
    """
    for expr in equation_data.values():
        if isinstance(expr, Function):
            if len(_extract_arguments(expr)) > 1:
                raise NotImplementedError("Expressions represented by multilinear (>=2) ufl forms are not supported.")
        elif isinstance(expr, Expr):
            if len(_extract_arguments(expr)) > 0:
                raise NotImplementedError("UFL Expressions with ufl 'form arguments' are not supported.")
    coeffs = list(equation_data.keys())
    elmts = [_mk_element(coeff, _ufl_cell) if isinstance(coeff, CoeffShape) else coeff.ufl_element()
             for coeff in coeffs]
    elmt = ufl.MixedElement(elmts) if len(elmts) > 1 else elmts[0]
    test_functions = ufl.TestFunctions(elmt) if len(elmts) > 1 else [ufl.TestFunction(elmt)]
    return sum(
        [Function("___", ufl.replace(expr._form, {expr.arguments()[0]: v})).expression()
         if isinstance(expr, Function) and len(expr.arguments()) == 1
         else ufl.inner(expr, v)
         for expr, v in zip(equation_data.values(), test_functions)]
    )
