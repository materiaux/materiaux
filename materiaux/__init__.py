# Copyright (C) 2019-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


from materiaux.version import __version__

# include directory of cpp header
import pathlib
include_directory = pathlib.Path(__file__).parent.resolve()

from .cpp import CodeData, FieldData, call, field_data_to_dict, field_data_from_dict, load_library
from .namespace import Namespace
from .common import get_working_directory, set_working_directory, reset_working_directory


