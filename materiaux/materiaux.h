// Copyright (C) 2019-2021 Matthias Rambausek
//
// This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
//
// SPDX-License-Identifier:    AGPL-3.0-or-later

#pragma once

#include <algorithm>
#include <any>
#include <complex>
#include <dlfcn.h>
#include <filesystem>
#include <ios>
#include <iostream>
#include <map>
#include <memory>
#include <numeric>
#include <regex>
#include <set>
#include <type_traits>
#include <vector>

#include <Eigen/Core>
#include <Eigen/LU>

#include "materiaux_module.h"

/// \mainpage materiaux C++ interface documentation
/// Welcome to the C++ internals of materiaux.
/// The project consists of two header files with a top-level namespace ::materiaux.
/// The file materiaux_module.h contains minimal code for compiled modules whereas materiaux.h
/// represents the completely library header.
/// At the heart of the project are materiaux::Function and materiaux::FieldData. These are the two
/// data structures that represent the Python/C++ interface. The latter aggregates the callable
/// function (as <a
/// href="https://en.cppreference.com/w/cpp/utility/functional/function">std::function</a> or plain
/// C function pointer of signature ``void(*call)(scalar_t *res, const scalar_t
/// *active_coefficients, const scalar_t *constants)``) and its metadata represented by (vectors of)
/// materiaux::FieldData. They are accompanied by the members of the namespace materiaux::evolution
/// that provides basic facilities for the solution of evolution equations.

/// Top level namespace for the C++ backend library for numerical functions.

/// The namespaces ::materiaux and materiaux::evolution are (partially)
/// exposed to Python. The namespace materiaux::utils is used for internals.
namespace materiaux {

    /// A namespace containing string constants naming field types for extendability.

    /// Note that these names mirror Python class names of materiaux.modeling.core and
    /// materiaux.modeling.gsm. Go to materiaux.h for the list of names.
    namespace FieldTypes {

        /// Mirrors materiaux.modeling.core.Coefficient
        static constexpr auto Coefficient = "Coefficient";

        /// Mirrors materiaux.modeling.gsm.State
        static constexpr auto State = "State";

        /// Mirrors materiaux.modeling.gsm.InternalStateState
        static constexpr auto InternalState = "InternalState";

        /// Mirrors materiaux.modeling.gsm.HistoryState
        static constexpr auto HistoryState = "HistoryState";

        /// Mirrors materiaux.modeling.gsm.TimeRate
        static constexpr auto TimeRate = "TimeRate";

        /// Mirrors materiaux.modeling.core.Constant
        static constexpr auto Constant = "Constant";

        /// Mirrors materiaux.modeling.gsm.Parameter
        static constexpr auto Parameter = "Parameter";

        /// Mirrors materiaux.modeling.gsm.Time
        static constexpr auto Time = "Time";

        /// Mirrors materiaux.modeling.gsm.HistoryTime
        static constexpr auto HistoryTime = "HistoryTime";

        /// Mirrors materiaux.modeling.gsm.TimeIncrement
        static constexpr auto TimeIncrement = "TimeIncrement";

        /// Indicating result fields
        static constexpr auto Result = "Result";

    } // namespace FieldTypes

    /// A struct representing information for code generation.
    struct CodeData {
        const std::string symbol_name;
        const std::string code;
        const std::set<std::string> include_directives;
        const std::set<std::string> include_directories;
        const std::set<std::string> link_libraries;
        const std::set<std::string> link_directories;
        const std::set<std::string> compile_flags;
    };

    using CodeDataPtr = std::shared_ptr<CodeData>;

    namespace {
        std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>>
        sanitize_symmetry(
            std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>>
                symmetry) {
            if (symmetry.empty())
                symmetry = {{{{}}}};

            for (auto &symm : symmetry)
                if (symm.empty())
                    symm.push_back({{}, {}});
            return std::move(symmetry);
        }
    } // namespace

    /// A struct representing metadata for variable coefficients and constants
    struct MATERIAUX_DLL_EXPORT FieldData {

        /// Constructor
        ///
        /// @param[in] name Name of the field
        /// @param[in] type Preferably one materiaux::FieldTypes, though any string is possible in
        /// principle.
        /// @param[in] description Longer description of the Field. Useful, if 'name' is just a
        /// letter/symbol.
        /// @param[in] position Position of the field in some argument list (group of fields)
        /// @param[in] size Number of numerical entries (size of a container) holding data
        /// representing the field
        /// @param[in] rank_sizes In case that it makes sense to distinguish an additional internal
        /// structure as it may
        ///     be the case of fields representing a second derivatives.
        /// @param[in] shape The shape of the array representing the numerical field data
        /// @param[in] symmetry A symmetry map with pairs of governing and governed indices. One
        /// such map for each rank.
        FieldData(
            std::string name, std::string type, std::string description, int position, size_t size,
            std::vector<size_t> rank_sizes, std::vector<size_t> shape,
            std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>> symmetry)
            : name{name}, type{type}, description{description}, position{position}, size{size},
              rank_sizes{rank_sizes}, shape{shape}, symmetry{sanitize_symmetry(symmetry)} {}

        /// Name of the field
        const std::string name;

        /// Type of the field. For extendability this is simply a string. Predefined constant values
        /// can be found in the namespace materiaux::FieldTypes.
        const std::string type;

        /// A longer description of what this field represents.
        const std::string description;

        /// Position of the field in some argument list (group of fields)
        const int position;

        /// Number of numerical entries (size of a container) holding data representing the field
        const size_t size;

        /// In case that it makes sense to distinguish an additional internal structure as it may
        /// be the case of fields representing a second derivatives.
        const std::vector<size_t> rank_sizes;

        /// The shape of the array representing the numerical field data
        const std::vector<size_t> shape;

        /// A symmetry map with pairs of governing and governed indices. One such map for each rank.
        const std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>>
            symmetry;
    };

    /// Comparison: this does not compare position, which actually is connected to the holder
    bool operator==(const FieldData &lhs, const FieldData &rhs) noexcept;

    /// Output operator
    template <class CharT, class Traits>
    std::basic_ostream<CharT, Traits> &operator<<(std::basic_ostream<CharT, Traits> &os,
                                                  const FieldData &field_data) {
        os << "FieldData {\n"
           << "\t"
           << "name: " << field_data.name << "\n"
           << "\t"
           << "type: " << field_data.type << "\n"
           << "\t"
           << "description: " << field_data.description << "\n"
           << "\t"
           << "size: " << field_data.size << "\n"
           << "\t"
           << "rank_sizes: (";
        for (const auto rsize : field_data.rank_sizes) {
            os << rsize << ", ";
        }
        os << ")"
           << "\n"
           << "\t"
           << "shape: (";
        for (const auto dim : field_data.shape) {
            os << dim << ", ";
        }
        os << ")"
           << "\n"
           << "\t"
           << "symmetry:\n";
        for (const auto &rank_data : field_data.symmetry) {
            os << "\t\t(";
            for (const auto &entry : rank_data) {
                os << "[";
                for (const auto &from_index : entry.first)
                    os << from_index << ", ";
                os << "] -> [";
                for (const auto &to_index : entry.second)
                    os << to_index << ", ";
                os << "], ";
            }
            os << "), "
               << "\n";
        }
        os << "\t"
           << "position: " << field_data.position;
        os << "\n"
           << "}";
        return os;
    }

    /// Frequently used type alias
    using FieldDataPtr = std::shared_ptr<FieldData>;

    /// Convenience factory for scalar field descriptions
    MATERIAUX_DLL_EXPORT FieldDataPtr create_scalar_field_data(std::string name, std::string type,
                                                               std::string description,
                                                               int position = 0);

    /// Convenience factory for vector field descriptions
    MATERIAUX_DLL_EXPORT FieldDataPtr create_vector_field_data(std::string name, std::string type,
                                                               std::string description,
                                                               int position, size_t size);

    /// Convenience factory for vector field descriptions
    MATERIAUX_DLL_EXPORT FieldDataPtr create_vector_field_data(std::string name, std::string type,
                                                               std::string description,
                                                               size_t size);

    /// Comparison of FieldDataPtr
    MATERIAUX_DLL_EXPORT bool operator==(const FieldDataPtr &lhs, const FieldDataPtr &rhs) noexcept;

    /// Type alias for vectors in linear algebra sense
    template <typename scalar_t> using Vector = Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>;
    template <typename scalar_t> using VectorMap = Eigen::Map<Vector<scalar_t>>;
    template <typename scalar_t> using CVectorMap = Eigen::Map<const Vector<scalar_t>>;

    /// Type alias for 1d Eigen arrays
    template <typename scalar_t> using Array = Eigen::Array<scalar_t, Eigen::Dynamic, 1>;
    template <typename scalar_t> using ArrayMap = Eigen::Map<Array<scalar_t>>;
    template <typename scalar_t> using CArrayMap = Eigen::Map<const Array<scalar_t>>;

    /// Type alias for matrices in linear algebra sense
    template <typename scalar_t>
    using Matrix = Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
    template <typename scalar_t> using MatrixMap = Eigen::Map<Matrix<scalar_t>>;
    template <typename scalar_t> using CMatrixMap = Eigen::Map<const Matrix<scalar_t>>;

    template <typename T> struct Span {
        using item_type = T;
        T *ptr;
        size_t N;
        T *data() const { return ptr; }
    };

    /// This namespace contains small utilities and helpers for library internals
    namespace utils {

        /// std-compliant begin() for Span
        template <typename T> T *begin(Span<T> &span) { return span.data(); }

        /// std-compliant end() for Span
        template <typename T> T *end(Span<T> &span) { return span.data() + span.N; }

        /// std-compliant begin() for const Span
        template <typename T> T *cbegin(const Span<T> &span) { return span.data(); }

        /// std-compliant end() for const Span
        template <typename T> T *cend(const Span<T> &span) { return span.data() + span.N; }

        /// std-compliant begin() for Eigen3 class
        template <typename Derived>
        decltype(std::declval<Eigen::PlainObjectBase<Derived>>().data())
        begin(Eigen::PlainObjectBase<Derived> &obj) {
            return obj.data();
        }

        /// std-compliant begin() for Eigen3 class
        template <typename Derived>
        decltype(std::declval<Eigen::PlainObjectBase<Derived>>().data())
        end(Eigen::PlainObjectBase<Derived> &obj) {
            return obj.data() + obj.size();
        }

        /// std-compliant begin() for Eigen3 class
        template <typename Derived>
        decltype(std::declval<const Eigen::PlainObjectBase<Derived>>().data())
        begin(const Eigen::PlainObjectBase<Derived> &obj) {
            return obj.data();
        }

        /// std-compliant end() for Eigen3 class
        template <typename Derived>
        decltype(std::declval<const Eigen::PlainObjectBase<Derived>>().data())
        end(const Eigen::PlainObjectBase<Derived> &obj) {
            return obj.data() + obj.size();
        }

        /// std-compliant end() for Eigen3 class
        template <typename EigenObj>
        decltype(std::declval<const Eigen::Ref<EigenObj>>().data())
        begin(const Eigen::Ref<EigenObj> &obj) {
            return obj.data();
        }

        /// std-compliant end() for Eigen3 class
        template <typename EigenObj>
        decltype(std::declval<const Eigen::Ref<EigenObj>>().data())
        end(const Eigen::Ref<EigenObj> &obj) {
            return obj.data() + obj.size();
        }

        /// std-compliant begin() for Eigen3 class
        template <typename EigenObj>
        decltype(std::declval<Eigen::Map<EigenObj>>().data()) begin(Eigen::Map<EigenObj> &obj) {
            return obj.data();
        }

        /// std-compliant end() for Eigen3 class
        template <typename EigenObj>
        decltype(std::declval<Eigen::Map<EigenObj>>().data()) end(Eigen::Map<EigenObj> &obj) {
            return obj.data() + obj.size();
        }

        /// std-compliant begin() for Eigen3 class
        template <typename EigenObj>
        decltype(std::declval<const Eigen::Map<EigenObj>>().data())
        begin(const Eigen::Map<EigenObj> &obj) {
            return obj.data();
        }

        /// std-compliant end() for Eigen3 class
        template <typename EigenObj>
        decltype(std::declval<const Eigen::Map<EigenObj>>().data())
        end(const Eigen::Map<EigenObj> &obj) {
            return obj.data() + obj.size();
        }

        /// std-compliant size() as for scalar values treated as one-element arrays
        constexpr inline std::size_t size(const double &val) { return 1; }

        /// std-compliant size() as for scalar values treated as one-element arrays
        constexpr inline std::size_t size(const std::complex<double> &val) { return 1; }

        /// std-compliant size() for Eigen3 class
        template <typename Derived>
        constexpr std::size_t size(Eigen::PlainObjectBase<Derived> &obj) {
            return obj.size();
        }

        /// std-compliant size() for Eigen3 class
        template <typename Derived>
        constexpr std::size_t size(const Eigen::PlainObjectBase<Derived> &obj) {
            return obj.size();
        }

        /// std-compliant size() for Eigen3 class
        template <typename EigenObj> constexpr std::size_t size(const Eigen::Ref<EigenObj> &obj) {
            return obj.size();
        }

        /// std-compliant size() for Eigen3 class
        template <typename EigenObj> constexpr std::size_t size(const Eigen::Map<EigenObj> &obj) {
            return obj.size();
        }

        /// Template for extracting items from a container of FieldDataPtr by names of fields
        template <template <typename> typename container_t, typename string_t>
        container_t<FieldDataPtr> extract_fields(const container_t<FieldDataPtr> &fields,
                                                 const std::vector<string_t> &names) {
            container_t<FieldDataPtr> selected{};
            for (const FieldDataPtr &field : fields) {
                if (std::find(begin(names), end(names), field->name) != end(names))
                    selected.push_back(field);
            }
            return selected;
        }

        /// Template for filtering a container of FieldData for a specific value of the member
        /// FieldData::type. Here, type is template parameter.
        template <auto type, template <typename> typename container_t>
        container_t<FieldDataPtr> filter_by_type(const container_t<FieldDataPtr> &fields) {
            container_t<FieldDataPtr> filtered{};
            for (const auto &field : fields) {
                if (field->type == *type) // deref since type has to be an address
                    filtered.push_back(field);
            }
            return filtered;
        }

        /// Return the fields corresponding to position of 'true' in 'mask'.
        MATERIAUX_DLL_EXPORT std::vector<FieldDataPtr>
        active_fields(const std::vector<FieldDataPtr> &fields, const std::vector<bool> &mask);

        /// Template for filtering a container of FieldData for a specific value of the member
        /// FieldData::type, whereby type is specified as argument.
        template <typename type_t, template <typename> typename container_t>
        container_t<FieldDataPtr> filter_by_type(const container_t<FieldDataPtr> &fields,
                                                 type_t type) {
            container_t<FieldDataPtr> filtered{};
            for (const auto &field : fields) {
                if (field->type == type)
                    filtered.push_back(field);
            }
            return filtered;
        }

        /// Create a FieldData object from a data object as found in the (generated) interface.h
        /// files
        template <typename data_obj_t> FieldDataPtr to_field_data(int position = 0) {
            return std::make_shared<FieldData>(
                data_obj_t::name, data_obj_t::type, data_obj_t::description, position,
                data_obj_t::size,
                std::vector<int>(begin(data_obj_t::rank_sizes), end(data_obj_t::rank_sizes)),
                std::vector<int>(begin(data_obj_t::shape), end(data_obj_t::shape)),
                data_obj_t::symmetry());
        }

        /// Adds FieldData from coefficient/constant-tuples as found in the (generated) interface.h
        /// files to a vector
#ifndef DOXYGEN_SHOULD_SKIP_THIS
        template <typename container_t, typename coeff_tuple_t, int index = 0,
                  std::enable_if_t<index == std::tuple_size_v<coeff_tuple_t>, int> = 0>
        void fill_field_data(container_t &data) {}

        /// Adds FieldData from coefficient/constant-tuples as found in the (generated) interface.h
        /// files to a vector
        template <typename container_t, typename coeff_tuple_t, int index = 0,
                  std::enable_if_t<index<std::tuple_size_v<coeff_tuple_t>, int> = 0> void
                      fill_field_data(container_t &data) {
            if (index < std::tuple_size_v<coeff_tuple_t>) {
                data.emplace_back(to_field_data<std::tuple_element_t<index, coeff_tuple_t>>(index));
                fill_field_data<container_t, coeff_tuple_t, index + 1>(data);
            }
        }
#else
        template <typename container_t, typename coeff_tuple_t, int index = 0>
        void fill_field_data(container_t &data) {}
#endif // DOXYGEN_SHOULD_SKIP_THIS

        /// Create field data from a coefficient/constant-tuple as found in the (generated)
        /// interface.h files
        template <typename coeff_tuple_t> std::vector<FieldDataPtr> field_data() {
            std::vector<FieldDataPtr> data;
            data.reserve(std::tuple_size_v<coeff_tuple_t>);
            fill_field_data<std::vector<FieldDataPtr>, coeff_tuple_t>(data);
            return data;
        }

        /// Determine the offsets (starting positions) of the individual fields gathered in a common
        /// array
        MATERIAUX_DLL_EXPORT std::vector<size_t>
        field_offsets(const std::vector<FieldDataPtr> &fields);

        /// Determine the total size of the common array gathering a set of fields from a pair of
        /// interators
        template <typename InputIt> size_t fields_size(InputIt first, InputIt last) {
            int size = 0;
            return std::accumulate(first, last, size, [](int size, const FieldDataPtr &item) {
                return size + field_size(item);
            });
        }

        /// Determine offset (starting position) of an individual field gathered in a common array
        /// (FieldPtr version)
        MATERIAUX_DLL_EXPORT size_t field_offset(const FieldDataPtr &field,
                                                 const std::vector<FieldDataPtr> &fields);

        /// Determine offset (starting position) of an individual field gathered in a common array
        /// (FieldData version)
        MATERIAUX_DLL_EXPORT size_t field_offset(const FieldData &field,
                                                 const std::vector<FieldDataPtr> &fields);

        /// Append a vector of FileDataPtr to another
        MATERIAUX_DLL_EXPORT void append_unique(std::vector<FieldDataPtr> &target,
                                                const std::vector<FieldDataPtr> &source);

        /// Merge vector of vectors of FieldDataPtrs
        MATERIAUX_DLL_EXPORT std::vector<FieldDataPtr>
        merge(const std::vector<std::vector<FieldDataPtr>> &sources);

        /// Merge vectors of FieldDataPtrs
        template <typename... Args> inline std::vector<FieldDataPtr> merge(const Args &...vecs) {
            return merge(std::vector<std::vector<FieldDataPtr>>{vecs...});
        }

        /// Copy data which field_ptrs point to into dest. Plain pointer version.
        template <typename T, typename sizes_t>
        void copy_array_of_pointers_into_one(T *dest, const T *const *field_ptrs,
                                             const sizes_t &sizes, std::size_t offset = 0) {
            for (size_t ii = 0; ii < size(sizes); ++ii) {
                std::copy(field_ptrs[ii], field_ptrs[ii] + sizes[ii], dest + offset);
                offset += sizes[ii];
            }
        }

        /// Copy data which field_ptrs point to into dest. Container version.
        template <typename dest_array_t, typename array_t>
        void copy_array_of_pointers_into_one(dest_array_t &dest,
                                             const std::vector<const array_t *> &field_ptrs,
                                             std::size_t offset = 0) {
            for (const auto coefficient_data : field_ptrs) {
                std::copy(begin(*coefficient_data), end(*coefficient_data), begin(dest) + offset);
                offset += size(*coefficient_data);
            }
        }

        /// Create copying operation of data which field_ptrs point to into dest. Plain pointer
        /// version.
        template <typename T>
        auto create_copy_array_of_pointers_into_one(const std::vector<FieldDataPtr> &active_fields,
                                                    std::size_t offset = 0) -> auto {
            auto sizes =
                std::accumulate(begin(active_fields), end(active_fields), std::vector<size_t>{},
                                [](std::vector<size_t> sizes, const FieldDataPtr &field_data) {
                                    sizes.push_back(field_data->size);
                                    return std::move(sizes);
                                });
            return [=](T *dest, const T *const *field_ptrs) {
                copy_array_of_pointers_into_one<T>(dest, field_ptrs, sizes, offset);
            };
        }

        /// Copy data which field_ptrs point to into dest. Container version.
        template <typename dest_array_t, typename array_t>
        void copy_array_of_pointers_into_one(dest_array_t &dest,
                                             const std::vector<const array_t *> &fields,
                                             const std::vector<FieldDataPtr> &active_fields,
                                             std::size_t offset = 0) {
            if (size(fields) == size(active_fields)) {
                copy_array_of_pointers_into_one(dest, fields, offset);
                return;
            }
            assert(size(dest) >= fields_size(active_fields));
            for (const auto &field_data : active_fields) {
                std::copy(begin(*fields[field_data->position]), end(*fields[field_data->position]),
                          begin(dest) + offset);
                offset += field_data->size;
            }
        }

        /// Check whether the given 'subset' is a true subset
        inline bool is_field_subset(const std::vector<FieldDataPtr> &all_field_data,
                                    const std::vector<FieldDataPtr> &subset_field_data) {
            return std::all_of(begin(subset_field_data), end(subset_field_data),
                               [&](const auto &subset_data_ptr) {
                                   return std::find(begin(all_field_data), end(all_field_data),
                                                    subset_data_ptr) != end(all_field_data);
                               });
        }

        /// Create an extraction operation from arrays of pointers or ptr-to-ptr-like objects based
        /// on indices
        template <typename target_array_t, typename source_array_t = target_array_t,
                  typename source_offsets_t>
        std::function<void(target_array_t &, const source_array_t &)>
        create_extraction(source_offsets_t source_offsets) {
            return [=](target_array_t &target, const source_array_t &source) {
                for (std::size_t ii = 0; ii < size(source_offsets); ++ii) {
                    target[ii] = source[source_offsets[ii]];
                }
            };
        }

        /// Create an extraction operation from arrays of pointers or ptr-to-ptr-like objects based
        /// on vectors of FieldDataPtr. The extraction is based on the ordering of the
        /// FieldDataPtrs, not on their "position" member.
        template <typename target_array_t, typename source_array_t = target_array_t>
        std::function<void(target_array_t &target, const source_array_t &source)>
        create_extraction(const std::vector<FieldDataPtr> &all_field_data,
                          const std::vector<FieldDataPtr> &subset_field_data,
                          bool ignore_disjoint = true, bool strict_subset = false) {
            if (!ignore_disjoint and !is_field_subset(all_field_data, subset_field_data))
                throw std::invalid_argument("Given 'subset_field_data' is disjoint from "
                                            "'all_field_data'.");
            if (subset_field_data.empty())
                return [=](target_array_t &target, const source_array_t &source) {};

            std::vector<std::remove_const_t<decltype(FieldData::position)>> indices;
            for (auto &data : subset_field_data) {
                const auto found = std::find(begin(all_field_data), end(all_field_data), data);
                if (found != end(all_field_data)) {
                    indices.push_back(
                        static_cast<decltype(indices)::value_type>(found - begin(all_field_data)));
                } else if (strict_subset)
                    throw std::invalid_argument(
                        "Given 'subset_field_data' is not fully contained in "
                        "'all_field_data'.");
            }
            return create_extraction<target_array_t, source_array_t>(indices);
        }

        /// Create a copy operation from subsets on array into another array based on the given
        /// target and source offsets of the subsets and the source subset sizes
        template <typename target_array_t, typename source_array_t, typename target_offsets_t,
                  typename source_offsets_t, typename sizes_t>
        std::function<void(target_array_t &, const source_array_t &)>
        create_field_update(target_offsets_t target_offsets, source_offsets_t source_offsets,
                            sizes_t sizes) {
            using utils::begin;
            using utils::end;
            const auto num_coeffs = sizes.size();
            return [=](target_array_t &target, const source_array_t &source) {
                for (std::size_t ii = 0; ii < num_coeffs; ++ii) {
                    std::copy(begin(source) + source_offsets[ii],
                              begin(source) + source_offsets[ii] + sizes[ii],
                              begin(target) + target_offsets[ii]);
                }
            };
        }

        /// Create a copy operation from subsets on array into another array based on the offset map
        /// target->source and source offset sizes
        template <typename target_array_t, typename source_array_t, typename target_to_source_t,
                  typename offsets_t>
        std::function<void(target_array_t &, const source_array_t &)>
        create_field_update(const std::map<std::size_t, std::size_t> &target_to_source,
                            offsets_t offsets) {
            std::vector<std::size_t> sizes(offsets.size() - 1);
            for (std::size_t ii = 0; ii < sizes.size(); ++ii)
                sizes[ii] = offsets[ii + 1] - offsets[ii];

            std::vector<std::size_t> target_offsets;
            std::vector<std::size_t> source_offsets;
            for (const auto &pair : target_to_source) {
                target_offsets.push_back(pair.first);
                source_offsets.push_back(pair.second);
            }
            return create_field_update<target_array_t, source_array_t>(target_offsets,
                                                                       source_offsets, sizes);
        }

        /// Create a copy operation from subsets on array into another array based on the given
        /// target and source offsets. The target offsets are used to compute the subset sizes, the
        /// last target_offset marks the end of the target array.
        template <typename target_array_t, typename source_array_t, typename target_offsets_t,
                  typename source_offsets_t>
        std::function<void(target_array_t &, const source_array_t &)>
        create_field_update(target_offsets_t target_offsets, source_offsets_t source_offsets) {
            std::vector<std::size_t> sizes(target_offsets.size() - 1);
            for (std::size_t ii = 0; ii < sizes.size(); ++ii) {
                sizes[ii] = target_offsets[ii + 1] - target_offsets[ii];
            }
            return create_field_update<target_array_t, source_array_t>(target_offsets,
                                                                       source_offsets, sizes);
        }

        /// Create a copy operation into a subset based on vectors of FieldDataPtr representing
        /// source and target data.
        template <typename target_eigen_t, typename source_eigen_t = target_eigen_t>
        std::function<void(target_eigen_t &int_coeffs, const source_eigen_t &all_fields)>
        create_update_from_all(const std::vector<FieldDataPtr> &all_field_data,
                               const std::vector<FieldDataPtr> &subset_field_data,
                               bool ignore_disjoint = true, bool strict_subset = false) {
            if (!ignore_disjoint and !is_field_subset(all_field_data, subset_field_data))
                throw std::invalid_argument("Given 'subset_field_data' is disjoint from "
                                            "'all_field_data'.");
            if (subset_field_data.empty())
                return [=](target_eigen_t &target, const source_eigen_t &source) {};
            if (all_field_data == subset_field_data)
                return [](target_eigen_t &active_states_lin_eqs,
                          const source_eigen_t &active_states_eqs) {
                    std::copy(begin(active_states_eqs), end(active_states_eqs),
                              begin(active_states_lin_eqs));
                };

            auto all_offsets = field_offsets(all_field_data);
            auto subset_offsets = field_offsets(subset_field_data);
            decltype(subset_offsets) from_offsets;
            decltype(subset_offsets) sizes;
            for (auto &data : subset_field_data) {
                const auto found = std::find(begin(all_field_data), end(all_field_data), data);
                if (found != end(all_field_data)) {
                    from_offsets.push_back(all_offsets[found - begin(all_field_data)]);
                    sizes.push_back(data->size);
                } else if (strict_subset)
                    throw std::invalid_argument(
                        "Given 'subset_field_data' is not fully contained in "
                        "'all_field_data'.");
            }
            if (!subset_field_data.empty())
                from_offsets.push_back(all_offsets[subset_field_data.back()->position + 1]);

            return create_field_update<target_eigen_t, source_eigen_t>(subset_offsets, from_offsets,
                                                                       sizes);
        }

        /// Create a copy operation from a subset based on vectors of FieldDataPtr representing
        /// source and target data.
        template <typename target_eigen_t, typename source_eigen_t = target_eigen_t>
        std::function<void(target_eigen_t &all_coeffs, const source_eigen_t &int_coeffs)>
        create_update_from_subset(const std::vector<FieldDataPtr> &all_field_data,
                                  const std::vector<FieldDataPtr> &subset_field_data,
                                  bool ignore_disjoint = false) {
            if (!ignore_disjoint and !is_field_subset(all_field_data, subset_field_data))
                throw std::invalid_argument("Given 'subset_field_data' actually is not a subset of "
                                            "'all_field_data'.");

            auto all_offsets = field_offsets(all_field_data);
            auto subset_offsets = field_offsets(subset_field_data);
            decltype(subset_offsets) to_offsets;
            decltype(subset_offsets) sizes;
            for (auto &data : subset_field_data) {
                const auto found = std::find(begin(all_field_data), end(all_field_data), data);
                if (found != end(all_field_data)) {
                    to_offsets.push_back(all_offsets[found - begin(all_field_data)]);
                    sizes.push_back(data->size);
                }
            }
            if (!subset_field_data.empty())
                to_offsets.push_back(all_offsets[subset_field_data.back()->position + 1]);

            return create_field_update<target_eigen_t, source_eigen_t>(to_offsets, subset_offsets,
                                                                       sizes);
        }

        /// Create a vector of pointers to numeric data whereby the pointers are given in terms of a
        /// map and the ordering is given specified by a vector of FieldDataPtr.
        template <typename array_t>
        std::vector<array_t *>
        ordered_array_pointers(const std::map<std::string, array_t *> &pointers,
                               std::vector<FieldDataPtr> field_data) {
            assert(size(pointers) >= size(field_data));
            return std::accumulate(
                begin(field_data), end(field_data), std::vector<array_t *>{},
                [&](std::vector<array_t *> target, const FieldDataPtr &field_data_ptr) {
                    target.push_back(pointers.at(field_data_ptr->name));
                    return std::move(target);
                });
        }

        /// Create an instance of a field with a new position index
        MATERIAUX_DLL_EXPORT FieldData renumber_field(const FieldData &other, size_t new_position);

        /// Create a new vector with FieldDataPtr-entries numbered by position.
        MATERIAUX_DLL_EXPORT std::vector<FieldDataPtr>
        renumber_fields(const std::vector<FieldDataPtr> &original);

        /// Substitutes all occurrences of a substring "${key}" with "value" in a template
        ///
        /// @param template_string The string template (similar to Python)
        /// @param key the key to be replaced (substring replaced is "${key}")
        /// @param new_value the substring to be inserted
        /// @return The string containing the substitutions
        MATERIAUX_DLL_EXPORT std::string substitute(const std::string &template_string,
                                                    const std::string &key,
                                                    const std::string &new_value);

        /// Substitutes substrings "${key}" with "value" in a template
        ///
        /// @param template_string The string template (similar to Python)
        /// @param substitutes A map with keys to be substituted in the template with values
        /// @return The string containing the substitutions
        MATERIAUX_DLL_EXPORT std::string substitute(const std::string &template_string,
                                                    std::map<std::string, std::string> substitutes);

        /// Join strings provided in a container using a given "glue" string
        template <template <typename> typename container_t>
        std::string join(const std::string &glue, const container_t<std::string> &container) {
            if (container.empty())
                return {};

            std::stringstream str;
            std::for_each(begin(container), end(container) - 1,
                          [&](const std::string &item) { str << item << glue; });
            str << container.back();
            return str.str();
        }

        /// Create a new set from a given set of sets by combining their content
        template <typename T> std::set<T> merge(const std::set<std::set<T>> &sets) {
            if (sets.size() == 0)
                return {};
            std::set<T> res{};
            std::for_each(begin(sets), end(sets),
                          [&](const auto &_set) { res.insert(begin(_set), end(_set)); });
            return res;
        }

    } // namespace utils

    namespace {
        template <typename T> T *load_symbol(void *handle, std::string symbol_name) {
            char *error;
            dlerror();
            void *sym = dlsym(handle, symbol_name.c_str());

            error = dlerror();

            if (error == nullptr && sym != nullptr) {
                std::cout << "Successfully loaded \"" << symbol_name << "\"" << std::endl;
                return reinterpret_cast<T *>(sym);
            } else {
                std::string msg = "An error occurred when trying to load \"" + symbol_name +
                                  "\":\n\t" + std::string(error);
                throw std::runtime_error(msg);
            }
        }
    } // namespace

    /// A simple shared library handle
    class MATERIAUX_DLL_EXPORT LibHandle {
      public:
        explicit LibHandle(const std::filesystem::path &shared_library);

        LibHandle(const LibHandle &) = delete;

        LibHandle &operator=(const LibHandle &) = delete;

        LibHandle(LibHandle &&) = delete;

        LibHandle &operator=(LibHandle &&) = delete;

        ~LibHandle();

        [[nodiscard]] const std::map<std::string, std::any> &contents() const;

        std::string module_name() const;

        std::filesystem::path shared_library_path() const;

      private:
        std::filesystem::path shared_library;
        std::map<std::string, std::any> lib_contents;
        void *handle;
    };

#ifndef DOXYGEN_SHOULD_SKIP_THIS
    template <typename scalar_t>
    Function<scalar_t>::Function(func_t func, FieldDataPtr result,
                                 std::vector<FieldDataPtr> coefficients,
                                 std::vector<bool> active_coefficient_mask,
                                 std::vector<FieldDataPtr> constants, CodeDataPtr code_data)
        : func{func}, result_data{std::move(result)}, coefficient_data{utils::renumber_fields(
                                                          std::move(coefficients))},
          active_coefficient_mask{std::move(active_coefficient_mask)},
          constant_data{utils::renumber_fields(std::move(constants))}, code_data{
                                                                           std::move(code_data)} {}

    template <typename scalar_t>
    FunctionPtr<scalar_t>
    create_function(typename Function<scalar_t>::func_t func, FieldDataPtr result,
                    std::vector<FieldDataPtr> coefficients,
                    std::vector<bool> active_coefficient_mask, std::vector<FieldDataPtr> constants,
                    CodeDataPtr code_data) {
        return std::make_shared<materiaux::Function<scalar_t>>(
            func, result, coefficients, active_coefficient_mask, constants, code_data);
    }

    extern template struct Function<double>;
    extern template struct Function<std::complex<double>>;

    extern template FunctionPtr<double> create_function(typename Function<double>::func_t func,
                                                        FieldDataPtr result,
                                                        std::vector<FieldDataPtr> coefficients,
                                                        std::vector<bool> active_coefficient_mask,
                                                        std::vector<FieldDataPtr> constants,
                                                        CodeDataPtr code_data);

    extern template FunctionPtr<std::complex<double>>
    create_function(typename Function<std::complex<double>>::func_t func, FieldDataPtr result,
                    std::vector<FieldDataPtr> coefficients,
                    std::vector<bool> active_coefficient_mask, std::vector<FieldDataPtr> constants,
                    CodeDataPtr code_data);

#endif // DOXYGEN_SHOULD_SKIP_THIS

    /// Vector of active coefficients of the given Function
    template <typename scalar_t>
    std::vector<FieldDataPtr> active_coefficients(const Function<scalar_t> &function) {
        return utils::active_fields(function.coefficient_data, function.active_coefficient_mask);
    }

    /// Vector of active coefficients of the given Function
    template <typename scalar_t>
    std::vector<FieldDataPtr> active_coefficients(FunctionPtr<scalar_t> function) {
        return active_coefficients(*function);
    }

    /// Total size (length) required to store the numeric data of a function's (active) coefficients
    template <typename scalar_t> int active_coefficients_size(FunctionPtr<scalar_t> function) {
        return active_coefficients_size(*function);
    }

    /// Total size (length) required to store the numeric data of a function's constants
    template <typename scalar_t> int constants_size(FunctionPtr<scalar_t> function) {
        return constants_size(*function);
    }

    /// Total size (length) required to store the numeric data of a function's result
    template <typename scalar_t> int result_size(FunctionPtr<scalar_t> function) {
        return result_size(*function);
    }

#ifndef DOXYGEN_SHOULD_SKIP_THIS
    template <typename scalar_t> int result_size(const Function<scalar_t> &function) {
        return utils::field_size(function.result_data);
    }

    template <typename scalar_t> int active_coefficients_size(const Function<scalar_t> &function) {
        return utils::fields_size(active_coefficients(function));
    }

    template <typename scalar_t> int constants_size(const Function<scalar_t> &function) {
        return utils::fields_size(function.constant_data);
    }
#endif // DOXYGEN_SHOULD_SKIP_THIS

    /// Adapt the calling signature of the given Function. This also makes the given coefficients
    /// active such that the this wrapper factory helps to unify call signatures.
    template <typename scalar_t>
    MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t>
    wrap_function(FunctionPtr<scalar_t> function, std::vector<FieldDataPtr> new_coefficients,
                  std::vector<FieldDataPtr> new_constants) {
        using array_t = Eigen::Array<scalar_t, Eigen::Dynamic, 1>;
        using vec_t = std::vector<scalar_t>;
        const auto func = function->func;
        const bool same_coefficients = new_coefficients == active_coefficients(function);
        const bool same_constants = new_constants == function->constant_data;
        if (same_coefficients && same_constants)
            return function;

        for (auto fdp : active_coefficients(function)) {
            if (std::find(begin(new_coefficients), end(new_coefficients), fdp) ==
                end(new_coefficients))
                throw std::invalid_argument("Coefficient " + fdp->name +
                                            " not present in new coefficients.");
        }
        for (auto fdp : function->constant_data) {
            if (std::find(begin(new_constants), end(new_constants), fdp) == end(new_constants))
                throw std::invalid_argument("Constant " + fdp->name +
                                            " not present in new constants.");
        }

        const auto coeffs_size = utils::fields_size(new_coefficients);
        const auto active_coeffs_size = utils::fields_size(active_coefficients(function));
        const auto constants_size = utils::fields_size(new_constants);
        const auto active_constants_size = utils::fields_size(function->constant_data);

        const auto set_coefficients =
            utils::create_update_from_all<vec_t, Eigen::Map<const array_t>>(
                new_coefficients, active_coefficients(function));
        const auto set_constants = utils::create_update_from_all<vec_t, Eigen::Map<const array_t>>(
            new_constants, function->constant_data);

        const std::vector<bool> active_coefficient_mask(size(new_coefficients), true);

        const std::string code_template{
            "namespace original_function {\n"
            "\t${func_code}\n"
            "}\n"
            "static const auto ${symbol_name} = materiaux::wrap_function<scalar_t>(\n"
            "\toriginal_function::${func_symbol_name}, \n"
            "\tstd::vector<materiaux::FieldDataPtr>{${new_coefficients}}, \n"
            "\tstd::vector<materiaux::FieldDataPtr>{${new_constants}}\n"
            ");"};

        std::string symbol_name = "wrapped_function";

        const std::vector<std::string> new_coefficients_refs = std::accumulate(
            begin(new_coefficients), end(new_coefficients), std::vector<std::string>{},
            [](std::vector<std::string> names, const FieldDataPtr &state) {
                names.push_back(std::string{"field_data::"} + state->name);
                return std::move(names);
            });

        const std::vector<std::string> new_constants_refs =
            std::accumulate(begin(new_constants), end(new_constants), std::vector<std::string>{},
                            [](std::vector<std::string> names, const FieldDataPtr &state) {
                                names.push_back(std::string{"field_data::"} + state->name);
                                return std::move(names);
                            });

        const std::string code = utils::substitute(
            code_template, {
                               {"func_code", function->code_data->code},
                               {"symbol_name", symbol_name},
                               {"func_symbol_name", function->code_data->symbol_name},
                               {"new_coefficients", utils::join(", ", new_coefficients_refs)},
                               {"new_constants", utils::join(", ", new_constants_refs)},
                           });

        const auto code_data = std::make_shared<CodeData>(
            CodeData{symbol_name, code, function->code_data->include_directives,
                     function->code_data->include_directories, function->code_data->link_libraries,
                     function->code_data->link_directories, function->code_data->compile_flags});

        return std::make_shared<Function<scalar_t>>(
            [=](scalar_t *res, const scalar_t *coefficients, const scalar_t *constants) {
                // optimization applied here: if same_*, then no need to do a copy.
                vec_t active_coefficients(same_coefficients ? 0 : active_coeffs_size);
                if (!same_coefficients) {
                    set_coefficients(active_coefficients,
                                     Eigen::Map<const array_t>(coefficients, coeffs_size));
                }
                vec_t active_constants(same_constants ? 0 : active_constants_size);
                if (!same_constants)
                    set_constants(active_constants,
                                  Eigen::Map<const array_t>(constants, constants_size));
                func(res, same_coefficients ? coefficients : active_coefficients.data(),
                     same_constants ? constants : active_constants.data());
            },
            function->result_data, function->coefficient_data, active_coefficient_mask,
            function->constant_data, code_data);
    }

#ifndef DOXYGEN_SHOULD_SKIP_THIS
    extern template FunctionPtr<double>
    wrap_function<double>(FunctionPtr<double> function, std::vector<FieldDataPtr> new_coefficients,
                          std::vector<FieldDataPtr> new_constants);

    extern template FunctionPtr<std::complex<double>>
    wrap_function<std::complex<double>>(FunctionPtr<std::complex<double>> function,
                                        std::vector<FieldDataPtr> new_coefficients,
                                        std::vector<FieldDataPtr> new_constants);
#endif // DOXYGEN_SHOULD_SKIP_THIS

    /// A shorthand for simply making all coefficients of a Function active
    template <typename scalar_t>
    FunctionPtr<scalar_t> make_all_coeffs_active(FunctionPtr<scalar_t> function) {
        return wrap_function(function, function->coefficient_data, function->constant_data);
    }

    template <typename scalar_t> using Array = Eigen::Array<scalar_t, Eigen::Dynamic, 1>;

    /// Call a function object with plain pointers
    ///
    /// @tparam scalar_t The scalar type of Function and fields
    /// @param[in] function The function object to be called
    /// @param[out] res Pointer to the array/vector to which the result should be written
    /// @param[in] coefficients Pointer to the array/vector holding the numerical data for
    ///     all *active* coefficients of the function
    /// @param[in] constants Pointer to the array/vector holding the numerical data for
    ///     all constants of the function
    template <typename scalar_t>
    void call(const Function<scalar_t> &function, scalar_t *res, const scalar_t *coefficients,
              const scalar_t *constants) {
        function.func(res, coefficients, constants);
    }

#ifndef DOXYGEN_SHOULD_SKIP_THIS
    extern template void call(const Function<double> &function, double *res,
                              const double *coefficients, const double *constants);

    extern template void call(const Function<std::complex<double>> &function,
                              std::complex<double> *res, const std::complex<double> *coefficients,
                              const std::complex<double> *constants);

#endif // DOXYGEN_SHOULD_SKIP_THIS

    /// Call a function object with arrays
    ///
    /// @param[in] function The function object to be called
    /// @param[out] res The array to which the result should be written
    /// @param[in] coefficients The array holding the numerical data for
    ///     all *active* coefficients of the function
    /// @param[in] constants The array holding the numerical data for
    ///     all constants of the function

    template <typename scalar_t>
    void call(const Function<scalar_t> &function, Eigen::Ref<Array<scalar_t>> res,
              const Eigen::Ref<const Array<scalar_t>> &coefficients,
              const Eigen::Ref<const Array<scalar_t>> &constants) {
        function.func(res.data(), coefficients.data(), constants.data());
    }

    /// Call a function object with arrays
    ///
    /// @tparam scalar_t The scalar type of function and fields
    /// @tparam array_t The array type holding numerical data
    /// @param[in] function The function object to be called
    /// @param[out] res The array to which the result should be written
    /// @param[in] coefficients The array holding the numerical data for
    ///     all *active* coefficients of the function
    /// @param[in] constants The array holding the numerical data for
    ///     all constants of the function
    ///
    /// This function is mainly for debugging. It performs some checks. Less safe but faster
    /// is calling the Function via materiaux::call.
    /// Given arrays must hold contiguous data! This is not tested here.
    template <typename scalar_t>
    void safe_call(const Function<scalar_t> &function, Eigen::Ref<Array<scalar_t>> res,
                   const Eigen::Ref<const Array<scalar_t>> &coefficients,
                   const Eigen::Ref<const Array<scalar_t>> &constants) {
        Array<scalar_t> _res(utils::size(res));
        _res = res;
        Array<scalar_t> _coefficients(utils::size(coefficients));
        _coefficients = coefficients;
        Array<scalar_t> _constants(utils::size(constants));
        _constants = constants;

        if (std::size_t res_size = function.result_data->size; res_size != utils::size(_res))
            throw std::invalid_argument("Size of res does not match required size " +
                                        std::to_string(res_size) + ".");
        if (std::size_t active_size = utils::fields_size(
                utils::active_fields(function.coefficient_data, function.active_coefficient_mask));
            active_size != utils::size(_coefficients))
            throw std::invalid_argument("Size of coefficients does not match required size " +
                                        std::to_string(active_size) + ".");
        if (std::size_t constants_size = utils::fields_size(function.constant_data);
            constants_size != utils::size(_constants))
            throw std::invalid_argument("Size of res does not match required size " +
                                        std::to_string(constants_size) + ".");
        function.func(_res.data(), _coefficients.data(), _constants.data());
        res = _res;
    }

    /// Call a function object with arrays
    ///
    /// @tparam res_t Type for the function results
    /// @tparam array_t Type for the function coefficients and constants
    /// @tparam scalar_t The scalar type for function and fields
    /// @param[in] function The function object to be called
    /// @param[out] res The array/vector to which the result should be written. Needs to provide
    ///     a data() method that returns a pointer to the underlying memory.
    /// @param[in] coefficients A vector holding pointers to the arrays for the
    ///     numerical data for each individual *active* coefficient
    /// @param[in] constants A vector holding  pointers to the arrays for the
    ///     numerical data for each individual constant
    template <typename res_t, typename array_t, typename scalar_t>
    void call(const Function<scalar_t> &function, res_t &res,
              const std::vector<const array_t *> &coefficients,
              const std::vector<const array_t *> &constants) {
        using Array = Eigen::Array<scalar_t, Eigen::Dynamic, 1>;
        std::vector<FieldDataPtr> _active_coefficients{active_coefficients(function)};
        Array coefficient_array(utils::fields_size(_active_coefficients));
        utils::copy_array_of_pointers_into_one(coefficient_array, coefficients,
                                               _active_coefficients);
        Array constant_array(utils::fields_size(function.constant_data));
        utils::copy_array_of_pointers_into_one(constant_array, constants);
        function.func(res.data(), coefficient_array.data(), constant_array.data());
    }

    /// Call a function object with arrays
    ///
    /// @tparam res_t Type for the function results
    /// @tparam array_t Type for the function coefficients and constants
    /// @tparam scalar_t The scalar type for function and fields
    /// @param[in] function The function object to be called
    /// @param[out] res The array/vector to which the result should be written. Needs to provide
    ///     a data() method that returns a pointer to the underlying memory.
    /// @param[in] coefficients A map (coefficient name -> data) holding pointers to the arrays for
    /// the
    ///     numerical data for individual coefficients. Only active coefficients are required.
    /// @param[in] constants A map (constant name -> data) holding pointers to the arrays for the
    ///     numerical data for each individual constant
    template <typename res_t, typename array_t, typename scalar_t>
    void call(const Function<scalar_t> &function, res_t &res,
              const std::map<std::string, array_t *> &coefficients,
              const std::map<std::string, array_t *> &constants) {
        call(function, res,
             utils::ordered_array_pointers(coefficients, materiaux::active_coefficients(function)),
             utils::ordered_array_pointers(constants, function.constant_data));
    }

    /// Call a function object with arrays
    ///
    /// @tparam res_t Type for the function results
    /// @tparam array_t Type for the function coefficients and constants
    /// @tparam scalar_t The scalar type for function and fields
    /// @param[in] function The function object to be called
    /// @param[out] res The array/vector to which the result should be written. Needs to provide
    ///     a data() method that returns a pointer to the underlying memory.
    /// @param[in] fields A map (coefficient/constant name -> data) holding pointers to the arrays
    /// for the
    ///     numerical data for individual coefficients and constants. Only constants and active
    ///     coefficients are required.
    template <typename res_t, typename array_t, typename scalar_t>
    void call(const Function<scalar_t> &function, res_t &res,
              const std::map<std::string, array_t *> &fields) {
        call(function, res,
             utils::ordered_array_pointers(fields, materiaux::active_coefficients(function)),
             utils::ordered_array_pointers(fields, function.constant_data));
    }

    /// Namespace containing tools associated to evolution equations and linearization in this
    /// context
    namespace evolution {

#ifndef DOXYGEN_SHOULD_SKIP_THIS
        namespace {
            /// Parameter conversion to double
            template <typename scalar_t> inline double to_double(scalar_t val) {
                return std::real(val);
            }

            /// Parameter conversion to int
            template <typename scalar_t> inline int to_int(scalar_t val) {
                return std::round(to_double(val));
            }
        } // namespace
#endif    // DOXYGEN_SHOULD_SKIP_THIS

        /// Generic interface for a nonlinear solver
        template <typename scalar_t> struct MATERIAUX_DLL_EXPORT NonlinearSolver {

            /// Runtime parameter description
            const std::vector<FieldDataPtr> parameters;

            /// Solver description
            const std::string description;

            /// Solve function:
            ///
            /// The signature of this function is as follows:
            ///
            /// @param[in] res_func The first argument of the callable is the residual (out), the
            /// second the internal state
            ///     (in)
            /// @param[in] lin_res_func The first argument of the callable is the linearization of
            /// the residual with respect
            ///     to the internal state (out), the second the internal state (in)
            /// @param[in,out] x The internal state. [in]: initial guess, [out]: the solution
            /// @param[in] size The length (array size) of the internal state
            /// @param[in] params Solver parameters
            const std::function<void(const std::function<void(scalar_t *, const scalar_t *)> &,
                                     const std::function<void(scalar_t *, const scalar_t *)> &,
                                     scalar_t *, size_t, const scalar_t *)>
                solve;

            /// Code generation information
            const CodeDataPtr code_data;
        };

        template <typename scalar_t>
        using NonlinearSolverPtr = std::shared_ptr<NonlinearSolver<scalar_t>>;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
        extern template struct NonlinearSolver<double>;
        extern template struct NonlinearSolver<std::complex<double>>;
#endif // DOXYGEN_SHOULD_SKIP_THIS

        /// Generic interface for a an explicit time integration algorithm
        template <typename scalar_t> struct MATERIAUX_DLL_EXPORT ExplicitTSAlgo {

            /// Runtime parameter description
            const std::vector<FieldDataPtr> parameters;

            /// Solver description
            const std::string description;

            /// Step function: computes (internal) state for next time step
            ///
            /// The signature of this function is as follows:
            ///
            /// @param[in] rate_func The first argument of the callable is a function that
            /// represents the rate (first arguments; out) based on a time increment (second
            /// argument; in) and the (internal) state (third argument; in)
            /// @param[in,out] x The internal state. [in]: initial state, [out]: new state
            /// @param[in] size The length (array size) of the internal state
            /// @param[in] time_increment The full time increment (the time step size)
            /// @param[in] params Solver parameters
            const std::function<void(
                const std::function<void(scalar_t *, scalar_t, const scalar_t *)> &, scalar_t *,
                size_t, scalar_t, const scalar_t *)>
                step;

            /// Code generation information
            const CodeDataPtr code_data;
        };

        template <typename scalar_t>
        using ExplicitTSAlgoPtr = std::shared_ptr<ExplicitTSAlgo<scalar_t>>;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
        extern template struct ExplicitTSAlgo<double>;
        extern template struct ExplicitTSAlgo<std::complex<double>>;
#endif // DOXYGEN_SHOULD_SKIP_THIS

        /// Newton-Raphson solver for nonlinear systems of equations.
        template <typename scalar_t> struct MATERIAUX_DLL_EXPORT NewtonRaphson {

            /// Runtime parameters
            inline static const std::vector<FieldDataPtr> parameters{{
                create_scalar_field_data(
                    "newton_max_iter", FieldTypes::Parameter,
                    "[int] maximum number of iteration. Algorithm throws an exception if this "
                    "value is exceeded before a solution is found."),
                create_scalar_field_data("newton_tol", FieldTypes::Parameter,
                                         "[double] Absolute_tolerance for residual norm"),
            }};

            /// Solver description
            inline static const std::string description{
                "Standard Newton-Raphson solver provided by materiaux."};

            /// Call operator: solves the system
            ///
            /// @param[in] res_func The first argument of the callable is the residual (out), the
            /// second the internal state (in)
            /// @param[in] lin_res_func The first argument of the callable is the linearization of
            /// the residual with respect to the internal state (out), the second the internal state
            /// (in)
            /// @param[in,out] x The internal state. [in]: initial guess, [out]: the solution
            /// @param[in] size The length (array size) of the internal state
            /// @param[in] params Solver parameters: in case of complex version, these values will
            /// be processed appropriately
            ///  * maximum iterations (rounded to integer) -- algorithm throws an exception if this
            ///  value is
            ///    exceeded before a solution is found.
            ///  * tolerance (double) -- absolute tolerance for the residual norm
            static void solve(const std::function<void(scalar_t *, const scalar_t *)> &res_func,
                              const std::function<void(scalar_t *, const scalar_t *)> &lin_res_func,
                              scalar_t *x, size_t size, const scalar_t *params) {
                using utils::begin;
                using utils::end;

                CArrayMap<scalar_t> _params(params, parameters.size());
                const int max_iter = to_int(_params(0));
                const double tol = to_double(_params(1));

                VectorMap<scalar_t> _x(x, size);

                // TODO: can this be made less clunky? Provide overloads with scratch storage?
                // #define ALLOC(type, size) ....
                const auto vsize = size < 20 ? 0 : size;
                const auto asize = size < 20 ? size : 0;
                scalar_t mem[asize + asize * asize];
                Array<scalar_t> mvec(vsize + vsize * vsize);

                VectorMap<scalar_t> res(asize > 0 ? mem : mvec.data(), size);
                MatrixMap<scalar_t> lin_res(asize > 0 ? mem + size : mvec.data() + size, size,
                                            size);

                auto check_convergence = [&]() { return res.norm() < tol; };

                res_func(res.data(), x);
                if (check_convergence())
                    return;

                for (int ii = 1; ii <= max_iter; ++ii) {
                    lin_res_func(lin_res.data(), _x.data());
                    _x -= lin_res.fullPivLu().solve(res);
                    res_func(res.data(), _x.data());
                    if (check_convergence()) {
                        return;
                    }
                }
                std::stringstream str;
                str << std::setprecision(10) << std::scientific << res.norm();
                throw std::runtime_error("Failed to solve evolution equations within " +
                                         std::to_string(max_iter) + " iterations. Residual is " +
                                         str.str());
            }

            inline static const CodeDataPtr code_data = std::make_shared<CodeData>(
                CodeData{"newton_raphson",
                         "static const auto newton_raphson = "
                         "materiaux::evolution::create_newton_raphson<${scalar_type}>();",
                         {},
                         {},
                         {},
                         {}});
        };

        /// 5th-order Runge-Kutte solver for nonlinear systems of equations.
        template <typename scalar_t> struct MATERIAUX_DLL_EXPORT RungeKutta5Stable {

            /// Runtime parameters
            inline static const std::vector<FieldDataPtr> parameters{};

            /// Solver description
            inline static const std::string description{
                "5th-order Runge-Kutta time stepping scheme with extended region of stability. See "
                "Lawson, J. D. An Order Five Runge-Kutta Process with Extended Region of "
                "Stability. "
                "SIAM J. Numer. Anal. 3, 593–597 (1966). IMPORTANT: In the original reference is a "
                "typo. The implementation uses the coefficients reported in "
                "Kumar, A. & Lopez-Pamies, O. On the two-potential constitutive modeling of rubber "
                "viscoelastic materials. Comptes Rendus Mécanique 344, 102–112 (2016)."};

            /// Step function: computes (internal) state for next time step
            ///
            /// The signature of this function is as follows:
            ///
            /// @param[in] rate_func The first argument of the callable is a function that
            /// represents the rate (first arguments; out) based on a time increment (second
            /// argument; in) and the (internal) state (third argument; in)
            /// @param[in,out] x The internal state. [in]: initial state, [out]: new state
            /// @param[in] size The length (array size) of the internal state
            /// @param[in] time_increment The full time increment (step size)
            /// @param[in] params Solver parameters (unused for this implementation)
            static void
            step(const std::function<void(scalar_t *, scalar_t, const scalar_t *)> &rate_func,
                 scalar_t *x, size_t size, scalar_t time_increment,
                 __attribute__((unused)) const scalar_t *params) {
                using utils::begin;
                using utils::end;

                //                const auto Delta_t = time_increment;

                VectorMap<scalar_t> _x(x, size);

                // TODO: can this be made less clunky? Provide overloads with scratch storage?
                // #define ALLOC(type, size) ....
                const auto vsize = size < 20 ? 0 : size;
                const auto asize = size < 20 ? size : 0;
                const auto N = 7;
                scalar_t mem[asize * N];
                Array<scalar_t> mvec(vsize * N);

                VectorMap<scalar_t> g1((asize > 0 ? mem : mvec.data()) + 0 * size, size);
                VectorMap<scalar_t> g2((asize > 0 ? mem : mvec.data()) + 1 * size, size);
                VectorMap<scalar_t> g3((asize > 0 ? mem : mvec.data()) + 2 * size, size);
                VectorMap<scalar_t> g4((asize > 0 ? mem : mvec.data()) + 3 * size, size);
                VectorMap<scalar_t> g5((asize > 0 ? mem : mvec.data()) + 4 * size, size);
                VectorMap<scalar_t> g6((asize > 0 ? mem : mvec.data()) + 5 * size, size);
                VectorMap<scalar_t> tmp((asize > 0 ? mem : mvec.data()) + 6 * size, size);

                auto f = [&tmp, &rate_func](auto &res, auto Delta_tau,
                                            const auto &x) mutable -> void {
                    tmp = x;
                    rate_func(res.data(), Delta_tau, tmp.data());
                };

                f(g1, 0, _x);
                f(g2, time_increment / 2.0, _x + (time_increment / 2.0) * g1);
                f(g3, time_increment / 4.0, _x + (time_increment / 16.0) * (3.0 * g1 + g2));
                f(g4, time_increment / 2.0, _x + (time_increment / 2.0) * (g3));
                f(g5, 3.0 * time_increment / 4.0,
                  _x + (time_increment / 16.0) * 3.0 * (-g2 + 2.0 * g3 + 3.0 * g4));
                f(g6, time_increment,
                  _x + (time_increment / 7.0) * (g1 + 4.0 * g2 + 6.0 * g3 - 12.0 * g4 + 8.0 * g5));
                _x = _x + time_increment / 90.0 * (7.0 * (g1 + g6) + 32.0 * (g3 + g5) + 12.0 * g4);
            }

            inline static const CodeDataPtr code_data = std::make_shared<CodeData>(
                CodeData{"runge_kutta_5_stable",
                         "static const auto runge_kutta_5_stable = "
                         "materiaux::evolution::create_runge_kutta_5_stable<${scalar_type}>();",
                         {},
                         {},
                         {},
                         {}});
        };

#ifndef DOXYGEN_SHOULD_SKIP_THIS
        extern template struct NewtonRaphson<double>;
        extern template struct NewtonRaphson<std::complex<double>>;

        extern template struct RungeKutta5Stable<double>;
        extern template struct RungeKutta5Stable<std::complex<double>>;

        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT NonlinearSolverPtr<scalar_t> create_newton_raphson() {
            using NR = NewtonRaphson<scalar_t>;
            return std::make_shared<NonlinearSolver<scalar_t>>(NonlinearSolver<scalar_t>{
                NR::parameters, NR::description, NR::solve, NR::code_data});
        }

        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT ExplicitTSAlgoPtr<scalar_t> create_runge_kutta_5_stable() {
            using RK5s = RungeKutta5Stable<scalar_t>;
            return std::make_shared<ExplicitTSAlgo<scalar_t>>(ExplicitTSAlgo<scalar_t>{
                RK5s::parameters, RK5s::description, RK5s::step, RK5s::code_data});
        }

        extern template NonlinearSolverPtr<double> create_newton_raphson<double>();
        extern template NonlinearSolverPtr<std::complex<double>>
        create_newton_raphson<std::complex<double>>();

        extern template ExplicitTSAlgoPtr<double> create_runge_kutta_5_stable<double>();
        extern template ExplicitTSAlgoPtr<std::complex<double>>
        create_runge_kutta_5_stable<std::complex<double>>();

        namespace {
            template <typename scalar_t>
            CodeDataPtr
            create_evolution_code_data(FunctionPtr<scalar_t> eqs, FunctionPtr<scalar_t> lin_eqs,
                                       std::vector<FieldDataPtr> internal_states,
                                       NonlinearSolverPtr<scalar_t> algo, bool throw_on_error) {
                const std::string code_template{
                    "namespace eqs {\n"
                    "\t${eqs_code}\n"
                    "}\n"
                    "namespace lin_eqs {\n"
                    "\t${lin_eqs_code}\n"
                    "}\n"
                    "namespace algo {\n"
                    "\t${algo_code}\n"
                    "}\n"
                    "static const auto ${symbol_name} = "
                    "materiaux::evolution::create_evolution(\n"
                    "\teqs::${eqs_symbol_name}, \n"
                    "\tlin_eqs::${lin_eqs_symbol_name}, \n"
                    "\tstd::vector<materiaux::FieldDataPtr>{{${internal_states}}}, \n"
                    "\talgo::${algo_symbol_name},\n"
                    "\t${throw_on_error}\n"
                    ");"};

                std::string symbol_name = "evolution";

                const std::vector<std::string> internal_state_names = std::accumulate(
                    begin(internal_states), end(internal_states), std::vector<std::string>{},
                    [](std::vector<std::string> names, const FieldDataPtr &state) {
                        names.push_back(std::string{"field_data::"} + state->name + "");
                        return std::move(names);
                    });

                const std::string code = utils::substitute(
                    code_template,
                    {{"eqs_code", eqs->code_data->code},
                     {"lin_eqs_code", lin_eqs->code_data->code},
                     {"algo_code", algo->code_data->code},
                     {"symbol_name", symbol_name},
                     {"eqs_symbol_name", eqs->code_data->symbol_name},
                     {"lin_eqs_symbol_name", lin_eqs->code_data->symbol_name},
                     {"algo_symbol_name", algo->code_data->symbol_name},
                     {"internal_states", utils::join(", ", internal_state_names)},
                     {"throw_on_error", std::string{throw_on_error ? "true" : "false"}}});

                std::set<std::string> include_directives = utils::merge<std::string>(
                    {eqs->code_data->include_directives, lin_eqs->code_data->include_directives,
                     algo->code_data->include_directives});

                std::set<std::string> include_directories = utils::merge<std::string>(
                    {eqs->code_data->include_directories, lin_eqs->code_data->include_directories,
                     algo->code_data->include_directories});

                std::set<std::string> link_libraries = utils::merge<std::string>(
                    {eqs->code_data->link_libraries, lin_eqs->code_data->link_libraries,
                     algo->code_data->link_libraries});

                std::set<std::string> link_directories = utils::merge<std::string>(
                    {eqs->code_data->link_directories, lin_eqs->code_data->link_directories,
                     algo->code_data->link_directories});

                std::set<std::string> compile_flags = utils::merge<std::string>(
                    {eqs->code_data->compile_flags, lin_eqs->code_data->compile_flags,
                     algo->code_data->compile_flags});

                return std::make_shared<CodeData>(CodeData{symbol_name, code, include_directives,
                                                           include_directories, link_libraries,
                                                           link_directories, compile_flags});
            }

            template <typename scalar_t>
            CodeDataPtr create_evolution_code_data(FunctionPtr<scalar_t> rate,
                                                   std::vector<FieldDataPtr> internal_states,
                                                   FieldDataPtr time_increment,
                                                   ExplicitTSAlgoPtr<scalar_t> algo,
                                                   bool throw_on_error) {
                const std::string code_template{
                    "namespace rate {\n"
                    "\t${rate_code}\n"
                    "}\n"
                    "namespace algo {\n"
                    "\t${algo_code}\n"
                    "}\n"
                    "static const auto ${symbol_name} = "
                    "materiaux::evolution::create_evolution(\n"
                    "\trate::${rate_symbol_name}, \n"
                    "\tstd::vector<materiaux::FieldDataPtr>{{${internal_states}}}, \n"
                    "\tfield_data::${time_increment}, \n"
                    "\talgo::${algo_symbol_name},\n"
                    "\t${throw_on_error}\n"
                    ");"};

                std::string symbol_name = "evolution";

                const std::vector<std::string> internal_state_names = std::accumulate(
                    begin(internal_states), end(internal_states), std::vector<std::string>{},
                    [](std::vector<std::string> names, const FieldDataPtr &state) {
                        names.push_back(std::string{"field_data::"} + state->name + "");
                        return std::move(names);
                    });

                const std::string code = utils::substitute(
                    code_template,
                    {{"rate_code", rate->code_data->code},
                     {"algo_code", algo->code_data->code},
                     {"symbol_name", symbol_name},
                     {"rate_symbol_name", rate->code_data->symbol_name},
                     {"algo_symbol_name", algo->code_data->symbol_name},
                     {"internal_states", utils::join(", ", internal_state_names)},
                     {"time_increment", time_increment ? time_increment->name : ""},
                     {"throw_on_error", std::string{throw_on_error ? "true" : "false"}}});

                std::set<std::string> include_directives = utils::merge<std::string>(
                    {rate->code_data->include_directives, algo->code_data->include_directives});

                std::set<std::string> include_directories = utils::merge<std::string>(
                    {rate->code_data->include_directories, algo->code_data->include_directories});

                std::set<std::string> link_libraries = utils::merge<std::string>(
                    {rate->code_data->link_libraries, algo->code_data->link_libraries});

                std::set<std::string> link_directories = utils::merge<std::string>(
                    {rate->code_data->link_directories, algo->code_data->link_directories});

                std::set<std::string> compile_flags = utils::merge<std::string>(
                    {rate->code_data->compile_flags, algo->code_data->compile_flags});

                return std::make_shared<CodeData>(CodeData{symbol_name, code, include_directives,
                                                           include_directories, link_libraries,
                                                           link_directories, compile_flags});
            }
        } // namespace

        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t>
        create_evolution(FunctionPtr<scalar_t> eqs, FunctionPtr<scalar_t> lin_eqs,
                         std::vector<FieldDataPtr> internal_states,
                         NonlinearSolverPtr<scalar_t> algo, bool throw_on_error) {
            using utils::begin;
            using utils::end;
            using index_t = Eigen::Index;

            // Gather information about eqs and lin eqs
            std::vector<FieldDataPtr> all_coefficients =
                utils::merge(active_coefficients(eqs), active_coefficients(lin_eqs));

            std::vector<FieldDataPtr> all_constants =
                utils::merge(eqs->constant_data, lin_eqs->constant_data);
            const std::vector<bool> active_coefficient_mask(size(all_coefficients), true);

            // Modify their interface such that they can be called with the full set of fields
            const auto _eqs = wrap_function(eqs, all_coefficients, all_constants);
            const auto _eqs_func = _eqs->func;
            const auto _lin_eqs = wrap_function(lin_eqs, all_coefficients, all_constants);
            const auto _lin_eqs_func = _lin_eqs->func;

            // Size information
            const auto coeffs_size = utils::fields_size(all_coefficients);
            const auto constants_size = utils::fields_size(all_constants);

            // Create update function that writes from new internal state to the full state vector
            const auto update_coeffs =
                utils::create_update_from_subset<ArrayMap<scalar_t>, CArrayMap<scalar_t>>(
                    all_coefficients, internal_states);

            // Create update function that writes to the internal state from the full state vector
            const auto extract_istate = utils::create_update_from_all<ArrayMap<scalar_t>>(
                all_coefficients, internal_states);

            // Result data with some checks
            const auto result_size = _eqs->result_data->size;
            if (result_size >= std::numeric_limits<index_t>::max())
                throw std::invalid_argument("The \"size\" of results is too large.");

            if (result_size != _lin_eqs->result_data->rank_sizes[0] ||
                result_size != _lin_eqs->result_data->rank_sizes[1])
                throw std::invalid_argument(
                    "The linearized equations are of inappropriate shape: "
                    "(" +
                    std::to_string(_lin_eqs->result_data->rank_sizes[0]) + ", " +
                    std::to_string(_lin_eqs->result_data->rank_sizes[1]) + ")");

            if (result_size != utils::fields_size(internal_states))
                throw std::invalid_argument("The result size of the equations is not the same as "
                                            "the cumulated size of internal states.");

            // The actual data object
            const auto rd = _eqs->result_data;
            const FieldDataPtr result = std::make_shared<FieldData>(
                "New internal state", "Result", "Internal state that solves the evolution equation",
                0, result_size, rd->rank_sizes, rd->shape, rd->symmetry);

            // Prepare total set of constants including solver parameters
            std::vector<FieldDataPtr> all_constants_for_evolution =
                utils::merge(all_constants, algo->parameters);

            const auto algo_solve = algo->solve;

            // The function that computes the new internal states
            auto evolution = [=](scalar_t *internal_state, const scalar_t *coefficients,
                                 const scalar_t *constants) {
                using utils::begin;
                using utils::end;

                // TODO: can this be made less clunky? Provide overloads with scratch storage?
                scalar_t mem[coeffs_size < 100 ? coeffs_size : 0];
                Array<scalar_t> mvec(coeffs_size < 100 ? 0 : coeffs_size);
                ArrayMap<scalar_t> coeffs(coeffs_size < 100 ? mem : mvec.data(), coeffs_size);
                std::copy(coefficients, coefficients + coeffs_size, coeffs.data());
                ArrayMap<scalar_t> istate(internal_state, result_size);
                extract_istate(istate, coeffs);

                auto res_func = [&](scalar_t *res, const scalar_t *x) {
                    update_coeffs(coeffs, CArrayMap<scalar_t>(x, result_size));
                    std::fill(res, res + result_size, 0);
                    _eqs_func(res, coeffs.data(), constants);
                };

                auto lin_res_func = [&](scalar_t *lin_res, const scalar_t *x) {
                    update_coeffs(coeffs, CArrayMap<scalar_t>(x, result_size));
                    std::fill(lin_res, lin_res + result_size * result_size, 0);
                    _lin_eqs_func(lin_res, coeffs.data(), constants);
                };

                try {
                    algo_solve(res_func, lin_res_func, istate.data(), istate.size(),
                               constants + constants_size);
                    return;
                } catch (const std::runtime_error &e) {
                    if (!throw_on_error) {
                        istate.setConstant(std::nan(""));
                        return;
                    } else {
                        std::stringstream str;
                        str << e.what() << "\nError occurred for input state\n";
                        Eigen::IOFormat CommaInitFmt(Eigen::FullPrecision, Eigen::DontAlignCols,
                                                     ", ", ", ", "", "", "{", "};");
                        str << Eigen::Map<const Vector<scalar_t>>(coefficients, coeffs_size)
                                   .format(CommaInitFmt)
                            << "\n";
                        str << "and constants\n";
                        str << Eigen::Map<const Vector<scalar_t>>(constants, constants_size)
                                   .format(CommaInitFmt)
                            << std::endl;
                        throw std::runtime_error(str.str());
                    }
                }
            };

            const auto code_data =
                create_evolution_code_data(eqs, lin_eqs, internal_states, algo, throw_on_error);

            return std::make_shared<Function<scalar_t>>(evolution, result, all_coefficients,
                                                        active_coefficient_mask,
                                                        all_constants_for_evolution, code_data);
        }

        template <typename scalar_t, typename string_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t>
        create_evolution(FunctionPtr<scalar_t> eqs, FunctionPtr<scalar_t> lin_eqs,
                         std::vector<string_t> internal_states, NonlinearSolverPtr<scalar_t> algo,
                         bool throw_on_error) {
            return create_evolution(eqs, lin_eqs,
                                    utils::extract_fields(eqs->coefficient_data, internal_states),
                                    algo, throw_on_error);
        }

        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t>
        create_evolution(FunctionPtr<scalar_t> eqs, FunctionPtr<scalar_t> lin_eqs,
                         NonlinearSolverPtr<scalar_t> algo, bool throw_on_error) {
            std::vector<FieldDataPtr> internal_states =
                utils::filter_by_type(eqs->coefficient_data, FieldTypes::InternalState);
            return create_evolution(eqs, lin_eqs, internal_states, algo, throw_on_error);
        }

        extern template FunctionPtr<double>
        create_evolution<double>(FunctionPtr<double> eqs, FunctionPtr<double> lin_eqs,
                                 std::vector<FieldDataPtr> internal_states,
                                 NonlinearSolverPtr<double> algo, bool throw_on_error);

        extern template FunctionPtr<std::complex<double>> create_evolution<std::complex<double>>(
            FunctionPtr<std::complex<double>> eqs, FunctionPtr<std::complex<double>> lin_eqs,
            std::vector<FieldDataPtr> internal_states,
            NonlinearSolverPtr<std::complex<double>> algo, bool throw_on_error);

        extern template FunctionPtr<double>
        create_evolution<double>(FunctionPtr<double> eqs, FunctionPtr<double> lin_eqs,
                                 NonlinearSolverPtr<double> algo, bool throw_on_error);

        extern template FunctionPtr<std::complex<double>> create_evolution<std::complex<double>>(
            FunctionPtr<std::complex<double>> eqs, FunctionPtr<std::complex<double>> lin_eqs,
            NonlinearSolverPtr<std::complex<double>> algo, bool throw_on_error);

        extern template FunctionPtr<double>
        create_evolution<double, const char *>(FunctionPtr<double> eqs, FunctionPtr<double> lin_eqs,
                                               std::vector<const char *> internal_states,
                                               NonlinearSolverPtr<double> algo,
                                               bool throw_on_error);

        extern template FunctionPtr<std::complex<double>>
        create_evolution<std::complex<double>, const char *>(
            FunctionPtr<std::complex<double>> eqs, FunctionPtr<std::complex<double>> lin_eqs,
            std::vector<const char *> internal_states,
            NonlinearSolverPtr<std::complex<double>> algo, bool throw_on_error);

        extern template FunctionPtr<double>
        create_evolution<double, std::string>(FunctionPtr<double> eqs, FunctionPtr<double> lin_eqs,
                                              std::vector<std::string> internal_states,
                                              NonlinearSolverPtr<double> algo, bool throw_on_error);

        extern template FunctionPtr<std::complex<double>>
        create_evolution<std::complex<double>, std::string>(
            FunctionPtr<std::complex<double>> eqs, FunctionPtr<std::complex<double>> lin_eqs,
            std::vector<std::string> internal_states, NonlinearSolverPtr<std::complex<double>> algo,
            bool throw_on_error);

        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t>
        create_evolution(FunctionPtr<scalar_t> rate, std::vector<FieldDataPtr> internal_states,
                         FieldDataPtr time_increment, ExplicitTSAlgoPtr<scalar_t> algo,
                         bool throw_on_error) {
            using utils::begin;
            using utils::end;
            using index_t = Eigen::Index;

            const auto _rate_func = rate->func;

            if (std::find(begin(rate->coefficient_data), end(rate->coefficient_data),
                          time_increment) != end(rate->coefficient_data))
                throw std::invalid_argument(
                    "Time increment must be in constants, not coefficients!");

            // Result data with some checks
            const auto result_size = rate->result_data->size;
            if (result_size >= std::numeric_limits<index_t>::max())
                throw std::invalid_argument("The \"size\" of results is too large.");

            if (result_size != utils::fields_size(internal_states))
                throw std::invalid_argument("The result size of the equations is not the same as "
                                            "the cumulated size of internal states.");

            // Gather information about rate
            std::vector<FieldDataPtr> all_coefficients =
                utils::merge(active_coefficients(rate), internal_states);
            const std::vector<bool> active_coefficient_mask(size(all_coefficients), true);

            std::vector<FieldDataPtr> all_constants =
                utils::merge(rate->constant_data, std::vector<FieldDataPtr>{{time_increment}});

            // Size information
            const auto coeffs_size = utils::fields_size(all_coefficients);
            const auto constants_size = utils::fields_size(all_constants);

            // Create update function that writes from new state subset to the full state vector
            const auto update_coeffs =
                utils::create_update_from_subset<ArrayMap<scalar_t>, CArrayMap<scalar_t>>(
                    all_coefficients, internal_states, true);

            // Create update function that writes to the internal state from the full state vector
            const auto extract_istate = utils::create_update_from_all<ArrayMap<scalar_t>>(
                all_coefficients, internal_states, true);

            // Create update function that writes to the time increment reading from constants
            const auto extract_time_incr = utils::create_update_from_all<ArrayMap<scalar_t>>(
                all_constants, {{time_increment}}, true);

            // Create update of time_increment
            const auto time_incr_update =
                utils::create_update_from_subset<ArrayMap<scalar_t>, CArrayMap<scalar_t>>(
                    all_constants, {{time_increment}}, true);

            // The actual data object
            const auto rd = rate->result_data;
            const FieldDataPtr result = std::make_shared<FieldData>(
                "New internal state", "Result", "Internal state that solves the evolution equation",
                0, result_size, rd->rank_sizes, rd->shape, rd->symmetry);

            // Prepare total set of constants including solver parameters
            std::vector<FieldDataPtr> all_constants_for_evolution =
                utils::merge(all_constants, algo->parameters);

            const auto algo_step = algo->step;

            // The function that computes the new internal states
            auto evolution = [=](scalar_t *internal_state, const scalar_t *coefficients,
                                 const scalar_t *constants) {
                using utils::begin;
                using utils::end;

                // TODO: can this be made less clunky? Provide overloads with scratch storage?
                const auto all_size = coeffs_size + constants_size;
                scalar_t mem[all_size < 100 ? all_size : 0];
                Array<scalar_t> mvec(all_size < 100 ? 0 : all_size);
                auto _data = all_size < 100 ? mem : mvec.data();
                ArrayMap<scalar_t> coeffs(_data, coeffs_size);
                ArrayMap<scalar_t> consts(_data + coeffs_size, constants_size);

                std::copy(coefficients, coefficients + coeffs_size, coeffs.data());
                std::copy(constants, constants + constants_size, consts.data());

                scalar_t Delta_t{0};
                ArrayMap<scalar_t> Delta_t_map(&Delta_t, 1);
                extract_time_incr(Delta_t_map, consts);

                ArrayMap<scalar_t> istate(internal_state, result_size);
                extract_istate(istate, coeffs);

                auto rate_func = [&](scalar_t *res, scalar_t Delta_tau, const scalar_t *x) {
                    update_coeffs(coeffs, CArrayMap<scalar_t>(x, result_size));
                    time_incr_update(consts, CArrayMap<scalar_t>(&Delta_tau, 1));
                    std::fill(res, res + result_size, 0);
                    _rate_func(res, coeffs.data(), consts.data());
                };

                try {
                    algo_step(rate_func, istate.data(), istate.size(), Delta_t,
                              constants + constants_size);
                    return;
                } catch (const std::runtime_error &e) {
                    if (!throw_on_error) {
                        istate.setConstant(std::nan(""));
                        return;
                    } else {
                        std::stringstream str;
                        str << e.what() << "\nError occurred for input state\n";
                        Eigen::IOFormat CommaInitFmt(Eigen::FullPrecision, Eigen::DontAlignCols,
                                                     ", ", ", ", "", "", "{", "};");
                        str << Eigen::Map<const Vector<scalar_t>>(coefficients, coeffs_size)
                                   .format(CommaInitFmt)
                            << "\n";
                        str << "and constants\n";
                        str << Eigen::Map<const Vector<scalar_t>>(constants, constants_size)
                                   .format(CommaInitFmt)
                            << std::endl;
                        throw std::runtime_error(str.str());
                    }
                }
            };

            const auto code_data = create_evolution_code_data(rate, internal_states, time_increment,
                                                              algo, throw_on_error);

            return std::make_shared<Function<scalar_t>>(evolution, result, all_coefficients,
                                                        active_coefficient_mask,
                                                        all_constants_for_evolution, code_data);
        }

        template <typename scalar_t, typename string_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t>
        create_evolution(FunctionPtr<scalar_t> rate, std::vector<string_t> internal_states,
                         string_t time_increment_name, ExplicitTSAlgoPtr<scalar_t> algo,
                         bool throw_on_error) {

            if constexpr (std::is_pointer_v<string_t>)
                if (time_increment_name == nullptr)
                    throw std::invalid_argument("Argument time_increment must not be null.");

            if (std::string(time_increment_name) == std::string(""))
                throw std::invalid_argument("Argument time_increment must not be empty string.");

            auto time_increment_fd = utils::extract_fields(
                rate->constant_data, std::vector<string_t>{{time_increment_name}});

            if (time_increment_fd.empty())
                time_increment_fd.push_back(
                    create_scalar_field_data(time_increment_name, FieldTypes::TimeIncrement,
                                             "Automatically created time increment parameter"));

            if (time_increment_fd.size() > 1)
                throw std::invalid_argument(std::string("More than one parameter with name ") +
                                            time_increment_name + " detected");

            const auto time_increment = time_increment_fd.front();
            if (time_increment->size > 1)
                throw std::invalid_argument("Detected time increment is not a scalar.");

            const auto internal_states_fd =
                utils::extract_fields(rate->coefficient_data, internal_states);
            if (std::size(internal_states_fd) != std::size(internal_states))
                throw std::invalid_argument(
                    "Not all given internal states name coefficients of the rate function.");

            return create_evolution(rate, internal_states_fd, time_increment, algo, throw_on_error);
        }

        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t>
        create_evolution(FunctionPtr<scalar_t> rate, ExplicitTSAlgoPtr<scalar_t> algo,
                         bool throw_on_error) {
            std::vector<FieldDataPtr> internal_states =
                utils::filter_by_type(rate->coefficient_data, FieldTypes::InternalState);

            std::vector<FieldDataPtr> time_increments =
                utils::filter_by_type(rate->constant_data, FieldTypes::TimeIncrement);

            if (time_increments.size() > 1)
                throw std::invalid_argument("More than one field of type TimeIncrement detected.");

            FieldDataPtr time_increment =
                (!time_increments.empty())
                    ? time_increments.front()
                    : create_scalar_field_data("__automatically_create_time_increment__",
                                               FieldTypes::TimeIncrement,
                                               "Automatically created time rate");

            if (time_increments.empty() &&
                !utils::extract_fields(rate->constant_data,
                                       std::vector<std::string>{{time_increment->name}})
                     .empty())
                throw std::runtime_error("Failed to create unique time increment.");

            return create_evolution(rate, internal_states, time_increment, algo, throw_on_error);
        }

        extern template FunctionPtr<double> create_evolution<double>(
            FunctionPtr<double> rate, std::vector<FieldDataPtr> internal_states,
            FieldDataPtr time_increment, ExplicitTSAlgoPtr<double> algo, bool throw_on_error);

        extern template FunctionPtr<std::complex<double>> create_evolution<std::complex<double>>(
            FunctionPtr<std::complex<double>> rate, std::vector<FieldDataPtr> internal_states,
            FieldDataPtr time_increment, ExplicitTSAlgoPtr<std::complex<double>> algo,
            bool throw_on_error);

        extern template FunctionPtr<double> create_evolution<double>(FunctionPtr<double> rate,
                                                                     ExplicitTSAlgoPtr<double> algo,
                                                                     bool throw_on_error);

        extern template FunctionPtr<std::complex<double>>
        create_evolution<std::complex<double>>(FunctionPtr<std::complex<double>> rate,
                                               ExplicitTSAlgoPtr<std::complex<double>> algo,
                                               bool throw_on_error);

        extern template FunctionPtr<double> create_evolution<double, const char *>(
            FunctionPtr<double> rate, std::vector<const char *> internal_states,
            const char *time_increment, ExplicitTSAlgoPtr<double> algo, bool throw_on_error);

        extern template FunctionPtr<std::complex<double>>
        create_evolution<std::complex<double>, const char *>(
            FunctionPtr<std::complex<double>> rate, std::vector<const char *> internal_states,
            const char *time_increment, ExplicitTSAlgoPtr<std::complex<double>> algo,
            bool throw_on_error);

        extern template FunctionPtr<double> create_evolution<double, std::string>(
            FunctionPtr<double> rate, std::vector<std::string> internal_states,
            std::string time_increment, ExplicitTSAlgoPtr<double> algo, bool throw_on_error);

        extern template FunctionPtr<std::complex<double>>
        create_evolution<std::complex<double>, std::string>(
            FunctionPtr<std::complex<double>> rate, std::vector<std::string> internal_states,
            std::string time_increment, ExplicitTSAlgoPtr<std::complex<double>> algo,
            bool throw_on_error);

        namespace {
            template <typename scalar_t>
            CodeDataPtr create_consistent_linearization_code_data(
                FunctionPtr<scalar_t> lin_ext_duals, FunctionPtr<scalar_t> lin_int_duals,
                FunctionPtr<scalar_t> lin_ext_eqs, FunctionPtr<scalar_t> lin_int_eqs) {
                const std::string code_template{
                    "namespace lin_ext_duals {\n"
                    "\t${lin_ext_duals_code}\n"
                    "}\n"
                    "namespace lin_int_duals {\n"
                    "\t${lin_int_duals_code}\n"
                    "}\n"
                    "namespace lin_ext_eqs {\n"
                    "\t${lin_ext_eqs_code}\n"
                    "}\n"
                    "namespace lin_int_eqs {\n"
                    "\t${lin_int_eqs_code}\n"
                    "}\n"
                    "static const auto ${symbol_name} = "
                    "materiaux::evolution::create_consistent_linearization(\n"
                    "\tlin_ext_duals::${lin_ext_duals_symbol_name}, \n"
                    "\tlin_int_duals::${lin_int_duals_symbol_name}, \n"
                    "\tlin_ext_eqs::${lin_ext_eqs_symbol_name}, \n"
                    "\tlin_int_eqs::${lin_int_eqs_symbol_name} \n"
                    ");"};

                std::string symbol_name = "consistent_linearization";

                const std::string code = utils::substitute(
                    code_template,
                    {{"symbol_name", symbol_name},
                     {"lin_ext_duals_code", lin_ext_duals->code_data->code},
                     {"lin_int_duals_code", lin_int_duals->code_data->code},
                     {"lin_ext_eqs_code", lin_ext_eqs->code_data->code},
                     {"lin_int_eqs_code", lin_int_eqs->code_data->code},
                     {"lin_ext_duals_symbol_name", lin_ext_duals->code_data->symbol_name},
                     {"lin_int_duals_symbol_name", lin_int_duals->code_data->symbol_name},
                     {"lin_ext_eqs_symbol_name", lin_ext_eqs->code_data->symbol_name},
                     {"lin_int_eqs_symbol_name", lin_int_eqs->code_data->symbol_name}});

                std::set<std::string> include_directives =
                    utils::merge<std::string>({lin_ext_duals->code_data->include_directives,
                                               lin_int_duals->code_data->include_directives,
                                               lin_ext_eqs->code_data->include_directives,
                                               lin_int_eqs->code_data->include_directives});

                std::set<std::string> include_directories =
                    utils::merge<std::string>({lin_ext_duals->code_data->include_directories,
                                               lin_int_duals->code_data->include_directories,
                                               lin_ext_eqs->code_data->include_directories,
                                               lin_int_eqs->code_data->include_directories});

                std::set<std::string> link_libraries =
                    utils::merge<std::string>({lin_ext_duals->code_data->link_libraries,
                                               lin_int_duals->code_data->link_libraries,
                                               lin_ext_eqs->code_data->link_libraries,
                                               lin_int_eqs->code_data->link_libraries});

                std::set<std::string> link_directories =
                    utils::merge<std::string>({lin_ext_duals->code_data->link_directories,
                                               lin_int_duals->code_data->link_directories,
                                               lin_ext_eqs->code_data->link_directories,
                                               lin_int_eqs->code_data->link_directories});

                std::set<std::string> compile_flags = utils::merge<std::string>(
                    {lin_ext_duals->code_data->compile_flags,
                     lin_int_duals->code_data->compile_flags, lin_ext_eqs->code_data->compile_flags,
                     lin_int_eqs->code_data->compile_flags});

                return std::make_shared<CodeData>(CodeData{symbol_name, code, include_directives,
                                                           include_directories, link_libraries,
                                                           link_directories, compile_flags});
            }
        } // namespace

        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t> create_consistent_linearization(
            FunctionPtr<scalar_t> lin_ext_duals, FunctionPtr<scalar_t> lin_int_duals,
            FunctionPtr<scalar_t> lin_ext_eqs, FunctionPtr<scalar_t> lin_int_eqs) {

            using Matrix = Eigen::Matrix<scalar_t, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
            //            using Array = Eigen::Array<scalar_t, Eigen::Dynamic, 1>;

            if (lin_ext_duals->result_data->rank_sizes[0] !=
                    lin_int_duals->result_data->rank_sizes[0] ||
                lin_ext_duals->result_data->rank_sizes[1] !=
                    lin_ext_eqs->result_data->rank_sizes[1] ||
                lin_int_duals->result_data->rank_sizes[1] !=
                    lin_int_eqs->result_data->rank_sizes[0] ||
                lin_int_eqs->result_data->rank_sizes[1] != lin_ext_eqs->result_data->rank_sizes[0])
                throw std::invalid_argument("Inconsistent result data.");

            const auto n_duals(lin_ext_duals->result_data->rank_sizes[0]);
            const auto n_ext(lin_ext_duals->result_data->rank_sizes[1]);
            const auto n_eqs(lin_ext_eqs->result_data->rank_sizes[0]);
            const auto n_int(lin_int_eqs->result_data->rank_sizes[1]);

            const auto all_coefficients =
                utils::merge(active_coefficients(lin_ext_duals), active_coefficients(lin_int_duals),
                             active_coefficients(lin_ext_eqs), active_coefficients(lin_int_eqs));

            const std::vector<bool> active_coefficient_mask(size(all_coefficients), true);

            const auto all_constants =
                utils::merge(lin_ext_duals->constant_data, lin_int_duals->constant_data,
                             lin_ext_eqs->constant_data, lin_int_eqs->constant_data);

            const FunctionPtr<scalar_t> _lin_ext_duals{
                wrap_function(lin_ext_duals, all_coefficients, all_constants)};
            const auto _lin_ext_duals_func = _lin_ext_duals->func;
            const FunctionPtr<scalar_t> _lin_int_duals{
                wrap_function(lin_int_duals, all_coefficients, all_constants)};
            const auto _lin_int_duals_func = _lin_int_duals->func;
            const FunctionPtr<scalar_t> _lin_ext_eqs{
                wrap_function(lin_ext_eqs, all_coefficients, all_constants)};
            const auto _lin_ext_eqs_func = _lin_ext_eqs->func;
            const FunctionPtr<scalar_t> _lin_int_eqs{
                wrap_function(lin_int_eqs, all_coefficients, all_constants)};
            const auto _lin_int_eqs_func = _lin_int_eqs->func;

            auto consistent_linearization = [=](scalar_t *linearization,
                                                const scalar_t *coefficients,
                                                const scalar_t *constants) {
                // one large storage
                std::vector<scalar_t> scratch(n_duals * n_int + n_eqs * n_ext + n_eqs * n_int);

                // Views into storage
                Eigen::Map<Matrix> _lin_int_duals_matrix(scratch.data(), n_duals, n_int);
                Eigen::Map<Matrix> _lin_ext_eqs_matrix(scratch.data() + n_duals * n_int, n_eqs,
                                                       n_ext);
                Eigen::Map<Matrix> _lin_int_eqs_matrix(_lin_ext_eqs_matrix.data() + n_eqs * n_ext,
                                                       n_eqs, n_int);

                // View into given target
                Eigen::Map<Matrix> _linearization_matrix(linearization, n_duals, n_ext);

                // Fill fields
                _lin_int_duals_matrix.setZero();
                _lin_ext_eqs_matrix.setZero();
                _lin_int_eqs_matrix.setZero();
                _lin_ext_duals_func(linearization, coefficients, constants);
                _lin_int_duals_func(_lin_int_duals_matrix.data(), coefficients, constants);
                _lin_ext_eqs_func(_lin_ext_eqs_matrix.data(), coefficients, constants);
                _lin_int_eqs_func(_lin_int_eqs_matrix.data(), coefficients, constants);

                // Complete Schur complement
                _linearization_matrix -= _lin_int_duals_matrix *
                                         _lin_int_eqs_matrix.fullPivLu().solve(_lin_ext_eqs_matrix);
            };

            const auto code_data = create_consistent_linearization_code_data(
                lin_ext_duals, lin_int_duals, lin_ext_eqs, lin_int_eqs);

            return std::make_shared<Function<scalar_t>>(
                consistent_linearization, _lin_ext_duals->result_data, all_coefficients,
                active_coefficient_mask, all_constants, code_data);
        }

        extern template FunctionPtr<double> create_consistent_linearization<double>(
            FunctionPtr<double> lin_ext_duals, FunctionPtr<double> lin_int_duals,
            FunctionPtr<double> lin_ext_eqs, FunctionPtr<double> lin_int_eqs);

        extern template FunctionPtr<std::complex<double>>
        create_consistent_linearization<std::complex<double>>(
            FunctionPtr<std::complex<double>> lin_ext_duals,
            FunctionPtr<std::complex<double>> lin_int_duals,
            FunctionPtr<std::complex<double>> lin_ext_eqs,
            FunctionPtr<std::complex<double>> lin_int_eqs);
#endif // DOXYGEN_SHOULD_SKIP_THIS
    }  // namespace evolution
} // namespace materiaux
