# Copyright (C) 2019-2020 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


from materiaux import FieldData, CodeData
from collections import namedtuple
import typing


def derivative_index(derivative_state: typing.Iterable[int], num_coefficients: int) -> int:
    """Determine the "index", that is the position of a certain derivative within a flattened array of all coefficient
    derivatives of an Expression under consideration.

    :param derivative_state: indices (positions of coefficients) of coefficients representing the partial derivative
    :param num_coefficients: number of coefficients of the expression of which a derivative is taken
    :return: the determined index
    """
    index = 0

    derivative_state = list(derivative_state)
    degree = len(derivative_state)
    ncoeffs = num_coefficients

    for ii, coeff in enumerate(derivative_state):
        index += ncoeffs ** (ii) + ncoeffs ** (degree - ii - 1) * coeff

    return index


data_required_attributes = ("expression_name",
                            "handler_name",
                            "scalar_type",
                            "result",
                            "coefficients",
                            "constants",
                            "calling_code")
"""Attributes that must be provided to 'DataAggregate'"""


_default_code_data = CodeData(
    "", "", set(), set(), set(), set(), set()
)


data_optional_attributes = (
    ("namespace", None),
    ("active_coefficients", None),
    ("base_obj", None),
    ("derivative_state", None),
    ("header", {}),
    ("sources", {}),
    ("additional_includes", []),
    ("include_directories", []),
    ("link_libraries", []),
    ("link_directories", []),
    ("compile_flags", []),
    ("code_data", _default_code_data)
)
"""Attributes that can be provided to 'DataAggregate'"""


DataAggregate = namedtuple("DataAggregate",
                           list(data_required_attributes) + [item[0] for item in data_optional_attributes])
r"""
Data structure that gathers all information that is required for interface generation

Note that the actual code for the evaluation of the expression is not strictly required. 
It might exists already. Details on required and optional fields are provided below.

*Required fields*

expression_name (str) 
    The name given to the expression/function
    
handler_name (str) 
    The name of the handler that generated/provided the data to be processed further

scalar_type (str) 
    The scalar type (e.g. "double" or "complex") which the expression works with

result (materiaux.FieldData) 
    Result metadata

coefficients (Union[List[materiaux.FieldData], Tuple[materiaux.FieldData]]) 
    Coefficient metadata

constants (typing.Union[list[materiaux.FieldData], tuple[materiaux.FieldData]]) 
    Constant metadata

calling_code (str) 
    Defines the C function that implements or points to generated expression code. This must be a string containing the
    expression '${func_name}', which is will be replaced by some name during interface generation.  
    Example: "inline static void(\*${func_name})(scalar_t \*res, const scalar_t \*active_coefficients, const scalar_t \*constants) = $fptr_name;"
    where "$fptr_name" is the name of a C function that implements the expression under consideration. 
    Data concerning the implementation can be provided via optional fields (see below).

        
*Optional fields*

namespace (List[str]) 
    The namespace(s) to which the expression/function belongs to. If this is left empty by a handler, it might be set 
    set to an appropriate value at a later stage. 

active_coefficients (Iterable[bool]) 
    An iterable of the same length as 'coefficients'. 
    The value 'True' marks an active coefficient, that is a coefficient that has been considered in the code generated
    for the expression. Conversely, 'False' indicates that the coefficient is actually unused. A reason for that case could be
    that a group of expressions or functions should have the same set of coefficients for a uniform interface. Note that there 
    are facitilities provided by the C++ interface library that allow render all coefficients 'active' such that a uniform 
    interface is achieved.
    
base_obj (str) 
    If the expression under consideration is the derivative of another one, its name can be provided here. 
    This only makes sense if a set of DataAggregate instances has been provided. This is the direct predecessor in 
    a chain of derivations.
    
derivative_state (Iterable[int]) 
    Encodes the chain of derivatives to arrive at this expression.
    A [0] corresponds to the 'root' expressions, [0, 0] to the first derivative wrt. the first coefficient,
    [0, 1, 0] to the mixed derivative obtained by first deriving wrt. the second and then wrt. the first coefficient.
    
header (Dict[str, str]) 
    A map from filenames to header code. If given, the file will be populated with the given header code.

sources (Dict[str, str]) 
    A map from filenames to implementation. If given, the file will be populated with the given implementation code.
    The file paths will be given to CMake's 'add_library'.
    
additional_includes (Iterable[str]) 
    Additional include directives or header file paths (no spaces allowed in header paths)

include_directories (Iterable[str]) 
    Additional include directories. Will be given to CMake's 'target_include_directories'

link_libraries (Iterable[str]) 
    Additional libraries to link against. Will be given to CMake's 'target_link_libraries'

link_directories (Iterable[str]) 
    Additional directories to search during linking. Will be given to CMake's 'target_link_directories'

code_data (materiaux.CodeData)
    Code generation data for the object to be created (will be attached to a materiaux::Function).

*Below is the auto-generated list of tuple fields*
"""


def make_data_aggregate(**kwargs) -> DataAggregate:
    """
    A factory building a named tuple DataAggregate from kwargs
    """
    _kwargs = dict(**kwargs)
    for attr in data_required_attributes:
        if attr not in _kwargs.keys():
            raise ValueError("Required attribute \"{:s}\" not provided.".format(attr))
    for attr, default in data_optional_attributes:
        _kwargs[attr] = _kwargs.get(attr, default)
    return DataAggregate(**_kwargs)


def _find_aggregate_by_name(name: str, data: typing.Iterable[DataAggregate]) -> typing.Optional[DataAggregate]:
    aggregates = list(filter(lambda item: item.expression_name == name, data))
    if len(aggregates) == 0:
        return None
    if len(aggregates) > 1:
        raise ValueError("Expression name {:s} not unique!".format(name))
    return aggregates[0]


def _find_base(obj: str, data: typing.Iterable[DataAggregate]) -> typing.Optional[str]:
    obj_data = _find_aggregate_by_name(obj, data)
    if obj_data is None:
        return None
        # raise ValueError("Base object of derived expression was not found in provided objects.")
    if obj_data.base_obj is None:
        return obj
    else:
        _base_aggregate = _find_aggregate_by_name(obj_data.base_obj, data)
        if _base_aggregate is None:
            return None
        else:
            return _find_base(_base_aggregate.expression_name, data)


def find_derivative_families(data: typing.Iterable[DataAggregate]) -> typing.Dict[str, typing.Tuple[str, int]]:
    """
    Collects Functions based on the base Functions (an Functions not obtained as the derivative of another one).
    Each family is represented by an array, where the base Expression has index 0, the other positions are determined
    via "derivative_index".
    """
    families = {}
    for aggregate in data:
        if aggregate.base_obj is not None and aggregate.derivative_state is not None:
            found_base = _find_base(aggregate.base_obj, data)
            if found_base is None:
                continue
            families[found_base] = families.get(found_base, []) + [
                (derivative_index(aggregate.derivative_state, len(aggregate.coefficients)), aggregate.expression_name)
            ]
        else:
            families[aggregate.expression_name] = [(0, aggregate.expression_name)]
    return families


def coefficients_without_duplicates(data: typing.Iterable[DataAggregate]) -> typing.List[FieldData]:
    """
    Return unique coefficients of the given DataAggregate
    """
    coefficients_wo_dups = []
    for aggregate in data:
        for coefficient in aggregate.coefficients:
            if coefficient not in coefficients_wo_dups:
                coefficients_wo_dups.append(coefficient)
    return coefficients_wo_dups


def constants_without_duplicates(data: typing.Iterable[DataAggregate]) -> typing.List[FieldData]:
    """
    Return unique constants of the given DataAggregate
    """
    constants_wo_dups = []
    for aggregate in data:
        for constant in aggregate.constants:
            if constant not in constants_wo_dups:
                constants_wo_dups.append(constant)
    return constants_wo_dups

