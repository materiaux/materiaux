# Copyright (C) 2019-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


import string
import typing
import materiaux
import materiaux.common as common


class CMakeTemplate(string.Template):
    delimiter = "%"


project_template = CMakeTemplate(
    """
    cmake_minimum_required(VERSION 3.14.0)
    
    PROJECT(%projectname)
    
    set(REQUIRED_GCC_VERSION "8.1.0")
    set(REQUIRED_CLANG_VERSION "7")
    set(CMAKE_CXX_STANDARD 17)
    set(CMAKE_EXPORT_COMPILE_COMMANDS TRUE)
    
    find_package(Eigen3 3.3.7 REQUIRED NO_MODULE)
    
    # materiaux include and library directories
    if (NOT DEFINED MATERIAUX_INCLUDE_DIR)
        if (NOT DEFINED ENV{MATERIAUX_INCLUDE_DIR})
            execute_process(COMMAND python3 -c "import materiaux; print(materiaux.include_directory, end=\\\"\\\")"
                            RESULT_VARIABLE result
                            OUTPUT_VARIABLE MATERIAUX_INCLUDE_DIR)
            if (NOT ${result} EQUAL "0")
                unset(MATERIAUX_INCLUDE_DIR)
                message(FATAL_ERROR "Failed to detect materiaux include directory. Make sure that the materiaux package 'materiaux' is installed.")   
            endif ()
        else ()
            set(MATERIAUX_INCLUDE_DIR $ENV{MATERIAUX_INCLUDE_DIR})
        endif ()
    endif ()
    
    if (NOT DEFINED MATERIAUX_LIBRARY_DIR)
        if (DEFINED ENV{MATERIAUX_LIBRARY_DIR})
            set(MATERIAUX_LIBRARY_DIR $ENV{MATERIAUX_INCLUDE_DIR})
        else()
            set(MATERIAUX_LIBRARY_DIR ${MATERIAUX_INCLUDE_DIR})
        endif()
    endif()
    
    set(MATERIAUX_LIBRARY materiaux)
    
    if (WIN32 OR NOT UNIX)
        set(DL_LIBRARY "")
    else ()
        set(DL_LIBRARY dl)
    endif ()
    """
)

"""Template for cmake project data"""

lib_template = CMakeTemplate(
    """
    add_library(%modulename SHARED %modulename.cpp %lib_sources)
    target_include_directories(%modulename PRIVATE ${MATERIAUX_INCLUDE_DIR} ${CMAKE_INSTALL_PREFIX}/include %include_dirs)
    target_link_libraries(%modulename PRIVATE ${MATERIAUX_LIBRARY} ${DL_LIBRARY} %link_libraries)
    target_link_directories(%modulename PRIVATE ${MATERIAUX_LIBRARY_DIR} ${CMAKE_INSTALL_PREFIX}/lib %link_directories)
    target_compile_definitions(%modulename PRIVATE
        -DMATERIAUX_MODULE_%{modulename}_BINARY_NAME="${CMAKE_SHARED_LIBRARY_PREFIX}%{modulename}${CMAKE_SHARED_LIBRARY_SUFFIX}"
    )
    target_compile_options(%{modulename} PRIVATE "-fvisibility=hidden")
    
    install(TARGETS %modulename LIBRARY DESTINATION %{lib_dest})
    install(FILES %modulename.h DESTINATION %{header_dest})
    """
)
"""Template for the library of expression evaluation code"""


def _sanitize_libs(libs: typing.Iterable[str]) -> typing.Set[str]:
    def _sanitize(lib: str) -> str:
        if lib.startswith("-"):
            return lib
        elif (lib.endswith(common.shared_lib_suffix) or lib.endswith(common.static_lib_suffix)) and not lib.startswith(
                common.shared_lib_prefix):
            return "-l:" + lib
        elif lib.startswith("lib"):
            return lib[3:]
        else:
            return lib

    return set([_sanitize(lib) for lib in libs])


def create_cmake(modulename: str, data_aggregates) -> str:
    """
    Generates the cmake module for building the shared library.

    :param modulename: name of the module
    :param data_aggregates: data for the expression contained in the library
    :return: "code" for CMakeLists
    """

    modulename = modulename.replace("::", "__")
    parts = [project_template.substitute(
        projectname=modulename,
        materiaux_version=materiaux.__version__,
    )]
    _lib_sources = " ".join(set(sum([list(aggregate.sources.keys()) for aggregate in data_aggregates], [])))
    _link_libraries = " ".join(
        _sanitize_libs(set(sum([list(aggregate.link_libraries) for aggregate in data_aggregates], []))))
    _link_directories = " ".join(set(sum([list(aggregate.link_directories) for aggregate in data_aggregates], [])))
    _include_dirs = " ".join(set(sum([list(aggregate.include_directories) for aggregate in data_aggregates], [])))
    _compile_flags = " ".join(set(sum([list(aggregate.compile_flags) for aggregate in data_aggregates], [])))
    parts.append(lib_template.substitute(
        modulename=modulename,
        lib_sources=_lib_sources,
        lib_dest=str(common.module_lib_subdirectory),
        header_dest=str(common.module_include_subdirectory),
        link_libraries=_link_libraries,
        link_directories=_link_directories,
        include_dirs=_include_dirs,
        compile_flags=_compile_flags
    ))
    return "\n\n".join(parts)
