# Copyright (C) 2019-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


import typing
import hashlib
import pathlib
import materiaux
import materiaux.codegeneration.module_generation as module_generation
from materiaux import set_working_directory, get_working_directory, reset_working_directory
from materiaux.cpp import LibHandle, FieldData, CodeData, double, double_complex, load_library, call


def _get_expression_name(expression: typing.Any) -> str:
    expression_name = "function"
    if hasattr(expression, "name"):
        try:
            expression_name = expression.name()
        except TypeError:
            expression_name = expression.name
    return expression_name


def _create_expression_signature(expression: typing.Any) -> str:
    if hasattr(expression, "signature"):
        str_rep = expression.signature()
    elif hasattr(expression, "code_data"):
        str_rep = ":".join(list(expression.code_data.link_libraries) +
                           list(expression.code_data.link_directories) +
                           list(expression.code_data.include_directives) +
                           list(expression.code_data.include_directories) +
                           list(expression.code_data.symbol_name) +
                           list(expression.code_data.code) +
                           list(expression.code_data.compile_flags))
    else:
        str_rep = str(expression)

    str_rep = ":".join([_get_expression_name(expression), str_rep])
    return str(type(expression)) + str_rep


def jit_module(data: materiaux.Namespace, module_name="",
               working_directory: typing.Union[pathlib.Path, str] = None, override: bool = False,
               cmake_options: dict = None,
               parameters: dict = None) -> materiaux.Namespace:
    """
    Create a module, compile and load it.

    :param data: the symbolic expressions that should be compiled, if data has no name, a unique name will be generated
    :param[optional] module_name: name of the module, if not given, data.name will be tried
    :param[optional] working_directory: where the generated code should land
    :param[optional] override: whether existing code/modules should be overwritten
    :param[optional] cmake_options: options one would pass to cmake, e.g. -DCMAKE_BUILD_TYPE=Debug is given as
        {"CMAKE_BUILD_TYPE": "Debug"}
    :param[optional] parameters: passed on to module_generation.generate_module
    :return: the generated (loaded) materiaux module as namespace
    """
    parameters = {} if parameters is None else parameters
    cmake_options = {} if cmake_options is None else cmake_options
    module_name = data.name if module_name == "" else module_name
    if module_name == "":
        str_rep = ":".join([_create_expression_signature(data.get_item(item_name))
                            for item_name in sorted(data.contents()) if
                            not isinstance(data.get_item(item_name), materiaux.Namespace)])
        str_rep += str(materiaux.__version__) + str(sorted(parameters.items())) + str(sorted(cmake_options.items()))
        module_name = "jit_materiaux_" + hashlib.blake2b(str_rep.encode()).hexdigest()

    working_directory = pathlib.Path(working_directory) if working_directory is not None else get_working_directory()

    if not override:
        try:
            module_path_name = materiaux.common.module_name_to_path(module_name)
            lib_name = materiaux.common.lib_name(str(module_path_name))
            if materiaux.common.get_module_source_directory(working_directory, module_name).exists():
                return load_library(lib_name, working_directory=working_directory)
        except FileNotFoundError:
            pass

    def _build() -> dict:
        _, meta_data = module_generation.generate_module(
            data, module_name=module_name, working_directory=working_directory, override=override, cmake_lib=True,
            parameters=parameters
        )
        if not meta_data["success"]:
            raise meta_data["exception"]
        meta_data["built_lib"] = module_generation.build_module(working_directory,
                                                                pathlib.Path(meta_data["target_dir"]),
                                                                cmake_options=cmake_options, clean=override)
        return meta_data

    meta_data = _build()
    namespace = load_library(meta_data["built_lib"].name, working_directory=working_directory)
    return namespace


def jit_function(expression: typing.Any, **kwargs) -> materiaux.cpp.function_types:
    """
    Directly compile a symbolic expression and return a corresponding callable Function object

    :param expression: the symbolic expression to be "jitted"
    :param kwargs: forwarded to :py:func:`jit_module`
    :return: The jitted materiaux function
    """
    expression_name = _get_expression_name(expression)
    _module = jit_module(materiaux.Namespace(**{expression_name: expression}), **kwargs)
    return _module.get_item(expression_name)


def jit(data: typing.Any, **kwargs) -> typing.Any:
    """
    Create JIT-compiled data. If data is a Namespace, :py:func:`jit_module` will be called, otherwise
    :py:func:`jit_function`.
    """
    if isinstance(data, materiaux.Namespace):
        return jit_module(data, **kwargs)
    else:
        return jit_function(data, **kwargs)
