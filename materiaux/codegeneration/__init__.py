# Copyright (C) 2019-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


"""
This package is responsible for code (mainly the interface and build system parts) generation.


User modules
------------

The main user modules are :py:mod:`materiaux.codegeneration.jit` and :py:mod:`materiaux.codegeneration.module_generation`. The former handles
as the name suggests JIT compilation of some expression to a materiaux.Function while the latter
explicitly generates the interface, build system data (CMakeLists.txt,...) and -- if provided -- the expression code.


How to extend `materiaux.codegeneration`
----------------------------------------

On the developer side, the most important "data structure" is :py:class:`materiaux.codegeneration.analysis.DataAggregate`
in :py:mod:`materiaux.codegeneration.analysis`. :py:class:`~.materiaux.codegeneration.analysis.DataAggregate` collects
all information that is necessary to generate the code (must be provided by some `handler`),
the interface and everything else that is needed under the hood.
:py:class:`~.materiaux.codegeneration.analysis.DataAggregate`'s members are either of built-in type
or of some type provided by :py:mod:`materiaux`. Thus, in order to add a new symbolic backend, a new `handler` 
(see :py:mod:`materiaux.codegeneration.handlers`) has to be provided. It takes a symbolic expression of the new backend
based on which it returns a :py:class:`~.materiaux.codegeneration.analysis.DataAggregate`, as demonstrated by
:py:mod:`materiaux.codegeneration.fenics_handler`.


Some notes on the generated modules
-----------------------------------

The generated modules are dynamically linked to the materiaux library and thus depend on it being installed.
They are in a special format that can be dynamically loaded at runtime by :py:func:`materiaux.cpp.load_library`, which
returns as :py:class:`materiaux.namespace.Namespace` with the module contents.
The modules contain information about the materiaux version with which they have been created,
:cpp:class:`materiaux::Function` objects and a container that exposes them wrapped into a ``std::any`` to the python
interface. Corresponding unwrapping facilities are implemented in :py:mod:`materiaux.materiauXcpp` which are employed in
:py:func:`materiaux.cpp.load_library`.
Plain C wrappers of the callable "func" member of :cpp:class:`materiaux::Function` are generated as well. All metadata
is contained in the ``materiaux.json`` file contained in each module source directory.

The directory structure is as follows:
The ``working_directory`` as defined in :py:mod:`materiaux.common` is the root directory of a set of modules. It can be
retrieved via :py:func:`materiaux.common.get_working_directory`. The module sources are contained in
``working_directory/modules``. Compiled modules can be found in ``working_directory/lib`` and the corresponding headers
in ``working_directory/include``.
"""
