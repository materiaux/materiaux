# Copyright (C) 2019-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


import typing
import materiaux.modeling.core as core
import materiaux.cpp as cpp
import materiaux.codegeneration.fenics_handler as fenics_handler
import materiaux.codegeneration.function_handler as function_handler
from materiaux.codegeneration.analysis import DataAggregate

handlers = {}
handlers.update({_type: function_handler for _type in (cpp.double.Function, cpp.double_complex.Function)})


def register_handler(handled_type: type,
                     handler: typing.Callable[[typing.List[str], typing.Any, dict], DataAggregate]) -> None:
    """
    Add a handler for the given type to the dictionary of known handlers.

    :param handled_type: the type to which this handler is applied
    :param handler: A callable taking a list of "nested namespace" names, an object of the handled type and a dict
        holding parameters. A handler has to return a DataAggregate.
    """
    handlers[handled_type] = handler


register_handler(core.Function, fenics_handler.generate_data)
for _type in cpp.function_types.__args__:
    register_handler(_type, function_handler.generate_data)
