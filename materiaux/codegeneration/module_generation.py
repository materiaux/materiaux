# Copyright (C) 2019-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


import pathlib
import typing
import materiaux.version
from materiaux import FieldData, CodeData, field_data_to_dict, field_data_from_dict, get_working_directory, set_working_directory, reset_working_directory
from materiaux.namespace import Namespace
import materiaux.codegeneration.interface_generation as interface_generation
from materiaux.codegeneration.analysis import DataAggregate, make_data_aggregate
import materiaux.codegeneration.cmake_generation as cmake_generation
from materiaux.codegeneration.handlers import handlers, register_handler
import materiaux.common
import os
import json
import copy


def _filenames(filebasename):
    return [fmt.format(filebasename) for fmt in ("{:s}.h", "{:s}.c")]


def _write_code(filename: pathlib.Path, code: str):
    if isinstance(filename, str):
        filename = pathlib.Path(filename)
    parent = filename.parent.resolve()
    parent.mkdir(parents=True, exist_ok=True)
    with open(filename, "w") as f:
        print(code, file=f)


def _check_for_unique_scoped_names(data_aggregates: typing.Iterable):
    names = [data_aggregate.expression_name for data_aggregate in data_aggregates]
    for name in names:
        if names.count(name) > 1:
            raise RuntimeError("Name '{:s}' not unique.".format(name))


def _data_aggregate_to_dict(data: DataAggregate) -> dict:
    _dict = {attr: getattr(data, attr) for attr in data._fields if attr[0] != "_"}
    if isinstance(_dict["result"], FieldData):
        _dict["result"] = field_data_to_dict(_dict["result"])
    _dict["coefficients"] = [field_data_to_dict(item) if isinstance(item, FieldData) else item for item in
                             _dict["coefficients"]]
    _dict["constants"] = [field_data_to_dict(item) if isinstance(item, FieldData) else item for item in
                          _dict["constants"]]
    _dict["code_data"] = {attr_name: getattr(_dict["code_data"], attr_name)
                          for attr_name in ("symbol_name", "code", "include_directives", "include_directories",
                                            "link_libraries", "link_directories", "compile_flags")}
    return _dict


def _data_aggregate_from_dict(data: dict) -> DataAggregate:
    new_data = copy.deepcopy(data)
    if isinstance(data["result"], dict):
        new_data["result"] = field_data_from_dict(new_data["result"])
    new_data["coefficients"] = [field_data_from_dict(item) if isinstance(item, dict) else item
                                for item in new_data["coefficients"]]
    new_data["constants"] = [field_data_from_dict(item) if isinstance(item, dict) else item
                             for item in new_data["constants"]]

    _cd = new_data["code_data"]
    new_data["code_data"] = CodeData(_cd["symbol_name"], _cd["code"], set(_cd["include_directives"]),
                                     set(_cd["include_directories"]), set(_cd["link_libraries"]),
                                     set(_cd["link_directories"]),
                                     set(_cd["compile_flags"]))
    return DataAggregate(**new_data)


def _set_scope(aggregate: DataAggregate, scope: typing.List[str]):
    if aggregate.namespace is None:
        _dict = {attr: getattr(aggregate, attr) for attr in aggregate._fields if attr[0] != "_"}
        _dict["namespace"] = scope
        return make_data_aggregate(**_dict)
    else:
        return aggregate


def _analyze_expression(scope: typing.List[str], name: str, expression: typing.Iterable[typing.Any], parameters) -> \
        DataAggregate:
    return _set_scope(handlers[type(expression)](scope, name, expression, parameters), scope)


def generate_module(expressions: Namespace, module_name: str = None,
                    working_directory: pathlib.Path = None, override=False,
                    parameters: typing.Dict = None, cmake_lib: bool = False) -> typing.Tuple[Namespace, dict]:
    """
    Generate a module that is actual expression code and interfaces, but do not build.

    :param expressions: Namespace containing sub-namespaces and/or expressions Note that expression can be of any type
        as long there is an appropriate handler (see materiaux.codegeneration.handlers).
    :param working_directory: location where the module shall land
    :param override: whether existing data should be overwritten
    :param parameters: parameters passed down to code generation
    :param cmake_lib: add instructions for building a shared library to CMakeLists.txt
    :return: the given expressions and metadata providing information about the generation process and failures
    """
    module_name = expressions.name if module_name is None else module_name
    if module_name == "":
        raise ValueError("No module name provided. Either provide a namespace with a name use the kwarg 'module_name'")

    working_directory = materiaux.common.get_working_directory() if working_directory is None else working_directory
    target_dir = materiaux.common.get_module_source_directory(working_directory, module_name).resolve()

    _data = {"module_name": module_name, "parameters": parameters,
             "target_dir": target_dir.relative_to(working_directory.resolve()), "generated": list()}

    json_file = pathlib.Path(target_dir, 'materiaux.json')
    update_json = False

    if target_dir.exists() and target_dir.is_dir():
        try:
            with open(json_file, "r") as mdf:
                _data = json.load(mdf)
                override = True if not _data["success"] else override
                if not materiaux.version.check_compatibility(_data["materiaux_version"]):
                    raise RuntimeError("Existing module data has been generated with an incompatible version "
                                       "({:s}) of materiaux".format(_data["materiaux_version"]))
        except FileNotFoundError:
            override = True

    _data["materiaux_version"] = materiaux.version.__version__

    def _flatten_data(data_dict: typing.Dict[str, list]) -> list:
        return sum([data for data in data_dict.values()], [])

    if override or not target_dir.exists():
        update_json = True
        expressions = Namespace(name=module_name, **{
            item_name: expressions.get_item(item_name)
            for item_name in expressions.contents()
            if type(expressions.get_item(item_name)) in handlers.keys()
        })
        if len(expressions.contents(0)) == 0:
            raise RuntimeError("No supported expressions found in given namespace")

        target_dir.mkdir(parents=True, exist_ok=override)

        data_aggregate_dict = {
            "::".join(expressions.scope(namespace, join=False) + [namespace]): [
                _analyze_expression(expressions.get_item(namespace).scope(expression_name, join=False),
                                    expressions.get_item(namespace).unscoped_name(expression_name),
                                    expressions.get_item(namespace).get_item(expression_name),
                                    parameters)
                for expression_name in expressions.get_item(namespace).contents(0)
                if not isinstance(expressions.get_item(namespace).get_item(expression_name), Namespace)
            ]
            for namespace in expressions.contents() if isinstance(expressions.get_item(namespace), Namespace)
        }

        data_aggregate_dict[module_name] = [
            _analyze_expression(expressions.scope(expression_name, join=False),
                                expressions.unscoped_name(expression_name),
                                expressions.get_item(expression_name),
                                parameters)
            for expression_name in expressions.contents(0)
            if not isinstance(expressions.get_item(expression_name), Namespace)
        ]

        header_name = pathlib.Path(target_dir, "{!s}.h".format(module_name))
        source_name = pathlib.Path(target_dir, "{!s}.cpp".format(module_name))
        interface_header, interface_implementation = interface_generation.generate_interface(
            data_aggregate_dict, toplevel_namespace=module_name
        )

        data_aggregates = _flatten_data(data_aggregate_dict)

        _write_code(header_name, interface_header)
        _write_code(source_name, interface_implementation)

        _data["generated"].append("interface")
        for data_aggregate in data_aggregates:
            for name, code in data_aggregate.header.items():
                _write_code(pathlib.Path(target_dir, name), code)
            for name, code in data_aggregate.sources.items():
                _write_code(pathlib.Path(target_dir, name), code)
        _data["generated"].append("code")

        _data["data_aggregates"] = {
            kk: [_data_aggregate_to_dict(vvi) for vvi in vv] for kk, vv in data_aggregate_dict.items()
        }
    else:
        data_aggregate_dict = {kk: [_data_aggregate_from_dict(vvi) for vvi in vv]
                               for kk, vv in _data["data_aggregates"].items()}
        data_aggregates = _flatten_data(data_aggregate_dict)

    if cmake_lib:
        fpath = pathlib.Path(target_dir, "CMakeLists.txt")
        if override or not fpath.exists():
            update_json = True
            with open(fpath, "w") as f:
                print(cmake_generation.create_cmake(module_name, data_aggregates), file=f)
            if "cmake" not in _data["generated"]:
                _data["generated"].append("cmake")

    _data["generated"] = list(set(_data["generated"]))
    _data["success"] = True

    if update_json:
        with open(json_file, "w") as mdf:
            json.dump(_data, mdf, default=str)

    return expressions, _data


def build_module(working_dir: pathlib.Path, target_dir: pathlib.Path, cmake_options: dict = None, clean: bool = False) -> pathlib.Path:
    """
    Build an already generated module

    :param working_dir: the root of the module collection
    :param target_dir: the path to the source. If relative, it will be appended to working dir.
    :param cmake_options: dict with cmake options, e.g. {"CMAKE_BUILD_TYPE": "Debug"} -> -DCMAKE_BUILD_TYPE=Debug
    :param clean: whether "ninja clean" should be run an the cmake cache should be removed
    :return: the path where to complied module can be found
    """
    cwd = pathlib.Path.cwd()
    working_dir = working_dir.resolve()
    target_dir = target_dir.relative_to(working_dir) if target_dir.is_absolute() else target_dir
    source_dir = pathlib.Path(working_dir / target_dir)
    build_dir = source_dir / "build"
    build_dir.mkdir(exist_ok=True)

    cmake_options = cmake_options if cmake_options is not None else {}

    cmake_options["MATERIAUX_INCLUDE_DIR"] = materiaux.include_directory
    cmake_options["CMAKE_INSTALL_PREFIX"] = working_dir
    cmake_options_string = " ".join(["-D{:s}={!s}".format(str(kk), str(vv)) for kk, vv in cmake_options.items()])

    os.chdir(build_dir)
    if clean:
        os.system("ninja -t clean")
        os.system("rm CMakeCache.txt")

    success = os.system("cmake -G Ninja {:s} {!s} && ninja install".format(cmake_options_string, source_dir))
    os.chdir(cwd)

    if success != 0:
        raise RuntimeError("Build failed. See directory {!s}.".format(build_dir))

    built_lib = build_dir / materiaux.common.lib_name(source_dir.name)
    if not (built_lib.exists() and built_lib.is_file()):
        raise RuntimeError("The expectedly built file '{!s}' does not exist in {!s}".format(built_lib, build_dir))
    return built_lib.relative_to(source_dir)
