# Copyright (C) 2019-2020 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


import os
import tempfile


_clang_style = "BasedOnStyle: llvm, IndentWidth: 4, MaxEmptyLinesToKeep: 2, NamespaceIndentation: All, " \
               "FixNamespaceComments: true, ColumnLimit: 100"


def clang_format(code: str) -> str:
    """
    Format the given code with clang format
    """
    if os.system("clang-format --version &>/dev/null") != 0:
        return code

    _name = None
    with tempfile.NamedTemporaryFile(mode="w", delete=False) as f:
        _name = f.name
        f.write(code)

    os.system("clang-format -i -style=\"{{{:s}}}\" {:s}".format(_clang_style, _name))
    with open(_name, "r") as f:
        return f.read()
