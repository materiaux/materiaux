# Copyright (C) 2019-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


from string import Template


field_data_template = Template(
    """
    static const auto $name{materiaux::create_field_data(
        "$name" /* name */,
        $field_type /* field type */,
        $description /* description */,
        int{0} /* symbol position (will be adjusted) */,
        size_t{$size} /* total data size */,
        std::vector<size_t>{$rank_sizes} /* rank sizes */,
        std::vector<size_t>{$shape} /* shape */,
        std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>>{$symmetries} /* symmetries */
    )};
    """
)
"""Template for materiaux::FieldData"""

code_data_template = Template(
    """
    static const auto ${function_name}{materiaux::create_code_data(
        ${symbol_name} /* symbol name */,
        ${code} /* code */,
        std::vector<const char*>{${include_directives}} /* include directives */,
        std::vector<const char*>{${include_directories}} /*include directories */,
        std::vector<const char*>{${link_libraries}} /* link libraries */,
        std::vector<const char*>{${link_directories}} /* link directories */,
        std::vector<const char*>{${compile_flags}} /* compile flags */
    )};
    """
)
"""Template for materiaux::CodeData"""


generic_data_template = Template(
    """
        
        namespace field_data {

        // coefficients
        // ------------
    ${coefficient_data}


        // constants
        // ---------
    ${constant_data}
        }
       
        namespace result_data {
    ${result_data}
        }
        
        namespace code_data {
    ${code_data}
        }
        
        namespace implementation_data {
    ${implementation_data}
        }
        
    """
)
"""Template for coefficient, constant data and code_data"""


result_data_template = Template(
    """
    static const auto ${function_name}{materiaux::create_field_data(
        ${qualified_function_name} /* qualified function name */,
        ${field_type} /* field type */,
        ${description} /* description */,
        int{0} /* field position */,
        size_t{$size} /* size */,
        std::vector<size_t>{$rank_sizes} /* rank sizes */,
        std::vector<size_t>{$shape} /* shape */,
        std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>>{$symmetries} /* symmetries */
    )};
    """
)
"""Template for C++ compile time `result field` information"""


header_template = Template(
    """
    
    #once

    #include <materiaux.h>
    
    extern "C" {
        ${export_declarations_c}
    }
    """
)
"""Template for materiaux::Function declaration"""


function_impl_template = Template(
    """
        const materiaux::FunctionPtr<${scalar_type}> ${function_name}{materiaux::create_function<${scalar_type}>(
                implementation_data::${function_name}::$function_impl_name,
                result_data::${function_name},
                {$coefficient_names},
                {$active_coefficients},
                {$constant_names},
                code_data::${function_name}
        )};
    """
)
"""Template for materiaux::Function definition"""

function_export = Template(
    """
    extern "C" {
    
    ${exports_implementations_c}
        
    }
    """
)
"""Template for materiaux::Function export"""


implementation_template = Template(
    """
    $implementation
    """
)
"""Template for the C function implementation (or an equivalent)"""


namespace_template_pre = Template(
    """    
    
    namespace $namespace {
    """
)
"""Template for opening a namespace"""

namespace_template_post = Template(
    """
    }    
    """
)
"""Template for closing a namespace"""


header_template_pre = Template(
    """
    
$includes
    
    """
)
"""Template for include directives at the very top of the interface file"""

header_template_post = Template(
    """ 
    """
)
"""Template for code at the very bottom of the interface file [currently empty]"""


interface_header_template_cpp = Template(
    """
#pragma once

#ifdef __cplusplus

#include <materiaux_module.h>
${additional_header}

namespace ${toplevel_namespace} {
    static constexpr auto materiaux_version = ${version};
}

extern "C" {
    MATERIAUX_DLL_EXPORT extern const char* ${toplevel_namespace}__materiaux_version;
    MATERIAUX_DLL_EXPORT extern const std::vector<std::pair<const char*, std::any>> ${toplevel_namespace}__contents;
}
"""
)

interface_footer_template_cpp = "#endif // __cplusplus"

implementation_content_template = Template(
    """  
extern "C" {
    const char* ${toplevel_namespace}__materiaux_version{${toplevel_namespace}::materiaux_version};
    const std::vector<std::pair<const char*, std::any>> ${toplevel_namespace}__contents{
        ${content_pairs}
    };
}
    """
)
