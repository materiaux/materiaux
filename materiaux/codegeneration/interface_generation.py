# Copyright (C) 2019-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later

import typing
import string
import materiaux
from materiaux import FieldData
import materiaux.codegeneration.interface_templates as templates
from materiaux.codegeneration.formatting import clang_format
import materiaux.codegeneration.analysis as analysis
from materiaux.codegeneration.analysis import DataAggregate

interface_header_fmt = "{:s}.h"

function_implementation_name = "func"

coeff_data_fmt = " " * 16 + "FieldData({:s})"
coeff_index_fmt = " " * 8 + "static constexpr std::size_t {0:s}{{{1:d}}};"
const_signature_fmt = "const {0:s}_t &{0:s}"
nonconst_signature_fmt = "{0:s}_t &{0:s}"
template_type_fmt = "typename {:s}_t"
coeff_size_assert_fmt = " " * 8 + "assert(size({0:s}) == Info::{0:s}_data::size);"
initializer_fmt = "{0:s}{{{0:s}}}"


def _cpp_scalar_type(scalar_type: str) -> str:
    # detect C-style complex definition, e.g "double complex"
    if scalar_type.lower().count("complex") > 0 and scalar_type.count("std::complex") == 0 and \
            scalar_type.count("<") == 0:
        basic_type = scalar_type.replace("complex", "").strip()
        return "std::complex<{:s}>".format(basic_type)
    else:
        return scalar_type


def _str_join(join_str: str, str_gen: typing.Callable[[typing.Union[FieldData, str]], str],
              items: typing.Iterable[typing.Union[FieldData, str]]) -> str:
    return join_str.join([str_gen(item) for item in items])


def _cstr_def(arg_str: str) -> str:
    return "\"{!s}\"".format(arg_str)


def _symmetry_str(symmetries: typing.Iterable[dict]) -> str:
    str_list = ["[{:s}]".format(", ".join(["[{:s}, {:s}]".format(str(kk), str(vv)) for (kk, vv) in symmetry]))
                for symmetry in symmetries]
    for ii, item in enumerate(str_list):
        if item == "[]":
            str_list[ii] = "[[[], []]]"
    return ", ".join(str_list).replace("[", "{").replace("]", "}")


def _data_struct(field: FieldData, substitution=templates.field_data_template.substitute):
    return substitution(
        name=field.name,
        field_type=_cstr_def(field.type),
        description=_cstr_def(field.description),
        size=field.size,
        rank=len(field.rank_sizes),
        rank_sizes=", ".join([str(dim) for dim in field.rank_sizes]),
        shape=", ".join([str(dim) for dim in field.shape]),
        symmetries=_symmetry_str(field.symmetry)
    )


def _qualified_function_name(aggregate: DataAggregate) -> str:
    return "::".join(aggregate.namespace + [aggregate.expression_name])


def _result_struct(aggregate: DataAggregate) -> str:
    return templates.Template(_data_struct(aggregate.result, templates.result_data_template.safe_substitute)) \
        .substitute(function_name=aggregate.expression_name,
                    qualified_function_name=_cstr_def(_qualified_function_name(aggregate)),
                    module_name=aggregate.namespace[0])


def _code_struct(aggregate: DataAggregate) -> str:
    _cd = aggregate.code_data
    if _cd.symbol_name != "":
        _symbol_name = _cd.symbol_name
    else:
        _symbol_name = aggregate.expression_name

    return templates.code_data_template.substitute(
        function_name=aggregate.expression_name,
        symbol_name="\"{:s}\"".format(_symbol_name),
        code="\"static const auto {:s} = {:s};\"".format(_symbol_name, _qualified_function_name(aggregate)),
        include_directives=_cstr_def(_cd.include_directives
                                     if len(_cd.include_directives) > 0
                                     else "{:s}.h".format(aggregate.namespace[0])),
        include_directories=_cstr_def(_cd.include_directories) if len(_cd.include_directories) > 0 else "",
        link_libraries=_cstr_def(_cd.link_libraries
                                 if len(_cd.link_libraries) > 0
                                 else "\" MATERIAUX_MODULE_{:s}_BINARY_NAME \"".format(aggregate.namespace[0])),
        link_directories=_cstr_def(_cd.link_directories) if len(_cd.link_libraries) > 0 else "",
        compile_flags=_str_join(", ", lambda item: _cstr_def(item), _cd.compile_flags),
    )


def _create_header_pre(includes: typing.Iterable[str]) -> str:
    substitutes = {
        "includes": "\n".join(includes),
    }
    return templates.header_template_pre.substitute(**substitutes)


def _create_header_post() -> str:
    substitutes = {
    }
    return templates.header_template_post.substitute(**substitutes)


def _include_fmt(include: str):
    include.replace("#include", "").replace(" ", "")
    if include.count("\"") == include.count("<") == 0:
        include = "\"{:s}\"".format(include)
    return "#include {:s}".format(include)


c_func_fmt = "void {fname:s}({scalar_type:s}* result, const {scalar_type:s}* coefficients, const {scalar_type:s}* constants)"


def _export_decl_c(aggregate: DataAggregate) -> str:
    c_function_name = "::".join(aggregate.namespace + [aggregate.expression_name]).replace("::", "__")
    return "MATERIAUX_DLL_EXPORT extern {cfunc:s};".format(
        cfunc=c_func_fmt.format(fname=c_function_name, scalar_type=aggregate.scalar_type),
    )


def _export_decl_cpp(aggregate: DataAggregate) -> str:
    return "MATERIAUX_DLL_EXPORT extern const materiaux::FunctionPtr<{scalar_type:s}> {function_name:s};".format(
        scalar_type=_cpp_scalar_type(aggregate.scalar_type),
        function_name=aggregate.expression_name
    )


def _export_impl_c(aggregate: DataAggregate) -> str:
    namespace = "::".join(aggregate.namespace)
    function_name = aggregate.expression_name
    c_function_name = "::".join([namespace, function_name]).replace("::", "__")

    return "{cfunc:s}{{{namespace:s}::implementation_data::{function_name:s}::{impl_name:s}(result, coefficients, constants);}}".format(
        cfunc=c_func_fmt.format(fname=c_function_name, scalar_type=_cpp_scalar_type(aggregate.scalar_type)),
        function_name=function_name,
        namespace=namespace,
        impl_name=function_implementation_name
    )


def _wrap_by_namespace(code: str, namespace) -> str:
    return "namespace {namespace:s} {{\n\t{code:s}\n}}".format(namespace=namespace, code=code)


def _process_namespace(data: typing.Iterable[DataAggregate]):
    _data = list(data)
    if len(_data) == 0:
        return

    includes = sum([[_include_fmt(include) for include in aggregate.additional_includes] for aggregate in _data], [])
    if any([_cpp_scalar_type(aggregate.scalar_type).count("complex") > 0 for aggregate in _data]):
        includes.append("#include <complex>")
    header = sum([["#include \"{:s}\"".format(kk) for kk in aggregate.header.keys()] for aggregate in _data], [])
    header += ["#include \"{:s}.h\"".format(aggregate.namespace[0]) for aggregate in _data]

    # TODO: what to do with derivative families?
    # families = analysis.find_derivative_families(_data)
    coefficients_wo_dups = analysis.coefficients_without_duplicates(_data)
    constants_wo_dups = analysis.constants_without_duplicates(_data)
    fields = coefficients_wo_dups + constants_wo_dups
    _names = set([item.name for item in fields])
    if len(_names) != len(fields):
        raise RuntimeError("There is a name clash between a coefficient and a constant."
                           "\ncoefficient names: {!s}\nconstant names: {!s}".format(
            [item.name for item in coefficients_wo_dups],
            [item.name for item in constants_wo_dups]
        ))

    generic_substitutes = {
        "result_data": _str_join("", lambda item: _result_struct(item), _data),
        "coefficient_data": _str_join("", lambda item: _data_struct(item), coefficients_wo_dups),
        "constant_data": _str_join("", lambda item: _data_struct(item), constants_wo_dups),
        "code_data": _str_join("", lambda item: _code_struct(item), _data),
        "implementation_data": _str_join("",
                                         lambda item: _wrap_by_namespace(
                                             string.Template(item.calling_code)
                                                 .substitute(func_name=function_implementation_name),
                                             item.expression_name
                                         ),
                                         _data)
    }
    impl_code = templates.generic_data_template.substitute(**generic_substitutes)

    export_declarations_c = ""
    export_declarations_cpp = ""
    export_implementations_c = ""

    for aggregate in data:
        if aggregate.active_coefficients is not None:
            _fmt = "{active_coeff_mask:s}"
            active_coefficients = _fmt.format(
                active_coeff_mask=", ".join(["true" if active else "false" for active in aggregate.active_coefficients])
            )
        else:
            active_coefficients = ", ".join(
                ["true"] * len(aggregate.coefficients)
            )
        if len(list(aggregate.active_coefficients)) != len(aggregate.coefficients):
            raise RuntimeError("len(active_coefficients) != len(aggregate.coefficients): The mask "
                               "'aggregate.active_coefficients' must have as many elements as 'aggregate.coefficients'")
        func_data_substitutes = {
            "function_name": aggregate.expression_name,
            "function_impl_name": function_implementation_name,
            "scalar_type": _cpp_scalar_type(aggregate.scalar_type),
            # "handler_name": aggregate.handler_name,
            "coefficient_names": _str_join(", ", lambda item: "field_data::{:s}".format(item.name),
                                           aggregate.coefficients),
            "active_coefficients": active_coefficients,
            "constant_names": _str_join(", ", lambda item: "field_data::{:s}".format(item.name),
                                        aggregate.constants),
        }
        impl_code += templates.function_impl_template.substitute(**func_data_substitutes)

        export_declarations_c = _str_join("", lambda item: _export_decl_c(item), _data)
        export_declarations_cpp = _str_join("", lambda item: _export_decl_cpp(item), _data)
        export_implementations_c = _str_join("", lambda item: _export_impl_c(item), _data)

    return includes, header, export_declarations_cpp, export_declarations_c, impl_code, export_implementations_c


def generate_interface(
        expression_data: typing.Dict[str, typing.Iterable[DataAggregate]],
        toplevel_namespace="materiaux"):
    """
    Generate the interface for the given expressions. Dict keys, if present translate to namespaces.

    :return: interface code (C++)
    """
    namespaces = {}
    contents = []
    for namespace, data in expression_data.items():
        if len(data) > 0:
            namespaces[namespace] = _process_namespace(data)
            contents += [("::".join([namespace, item.expression_name]), _cpp_scalar_type(item.scalar_type))
                         for item in data]

    includes = []
    headers = []
    interfaces = []
    implementations = []
    c_decls = []

    for namespace, data in namespaces.items():
        includes += data[0]
        headers += data[1]

        interfaces.append(templates.namespace_template_pre.substitute(namespace=namespace))
        interfaces.append(data[2])  # cpp declaration
        interfaces.append(templates.namespace_template_post.substitute())

        c_decls += [data[3]]

        implementations.append(templates.namespace_template_pre.substitute(namespace=namespace))
        implementations.append(data[4])  # cpp implementation
        implementations.append(templates.namespace_template_post.substitute())

        if len(data[5]) > 0:
            implementations.append("extern \"C\" {")
            implementations.append(data[5])  # extern C implementation
            implementations.append("}")

    includes_wo_duplicates = []
    for include in includes + headers:
        if include not in includes_wo_duplicates:
            includes_wo_duplicates.append(include)

    additional_header_cpp = []
    additional_header_c = []
    try:
        cindex = includes.index("#include <complex>")
        additional_header_cpp.append(includes.pop(cindex))
        additional_header_c.append("#include <complex.h>")
    except ValueError:
        pass
    interfaces.insert(0, templates.interface_header_template_cpp.substitute(
        toplevel_namespace=toplevel_namespace, additional_header="\n".join(additional_header_cpp),
        version=_cstr_def(materiaux.__version__)))
    interfaces.append(templates.interface_footer_template_cpp)
    if len(c_decls) > 0:
        interfaces.append("#ifndef __cplusplus")
        interfaces += additional_header_c
        interfaces += c_decls
        interfaces.append("#endif // __cplusplus")

    interface = clang_format("\n".join(interfaces))

    mk_pair = lambda key, val: string.Template("{\"$key\", std::any{$val}}").substitute(key=key, val=val)
    implementations.insert(0, _create_header_pre(includes_wo_duplicates))
    implementations.append(_create_header_post())
    implementations.append(templates.implementation_content_template.substitute(
        toplevel_namespace=toplevel_namespace,
        content_pairs=", ".join([mk_pair(item[0], item[0]) for item in contents])
    ))
    implementation = clang_format("\n".join(implementations))
    return interface, implementation
