# Copyright (C) 2020-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


import typing
from string import Template
from materiaux.cpp import function_types
from materiaux.codegeneration.analysis import DataAggregate, make_data_aggregate


calling_code_template = Template(
    """
    namespace function_object {
        ${function_code}
    }
    inline static const std::function<void(${scalar_type} *res, const ${scalar_type} *active_coefficients, const ${scalar_type} *constants)>${func_name}{function_object::${symbol_name}->func};
    """
)


def generate_data(scope: typing.List[str], name: str, function: function_types, parameters: dict) \
        -> DataAggregate:
    if function.code_data.code == "":
        raise ValueError("Given materiaux cpp Function does not provide any code.")

    return make_data_aggregate(
        handler_name="function",
        expression_name=name,
        namespace=scope,
        scalar_type=function.scalar_type,
        result=function.result_data,
        coefficients=function.coefficient_data,
        active_coefficients=function.active_coefficient_mask,
        constants=function.constant_data,
        calling_code=Template(calling_code_template.safe_substitute(
            symbol_name=function.code_data.symbol_name,
            function_code=function.code_data.code,
        )).safe_substitute(scalar_type=function.scalar_type),
        header={},
        sources={},
        additional_includes=function.code_data.include_directives,
        include_directories=function.code_data.include_directories,
        link_libraries=function.code_data.link_libraries,
        link_directories=function.code_data.link_directories,
        compile_flags=function.code_data.compile_flags,
    )
