// Copyright (C) 2019-2021 Matthias Rambausek
//
// This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
//
// SPDX-License-Identifier:    AGPL-3.0-or-later

#include "materiaux.h"

namespace materiaux {

    bool operator==(const FieldData &lhs, const FieldData &rhs) noexcept {
        // do not compare "position"
        return lhs.name == rhs.name && lhs.type == rhs.type && lhs.description == rhs.description &&
               lhs.size == rhs.size && lhs.rank_sizes == rhs.rank_sizes && lhs.shape == rhs.shape &&
               lhs.symmetry == rhs.symmetry;
    }

    FieldDataPtr create_scalar_field_data(std::string name, std::string type,
                                          std::string description, int position) {
        return std::make_shared<FieldData>(
            name, type, description, position, 1, std::vector<size_t>{}, std::vector<size_t>{},
            std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>>{
                {{{}, {}}}});
    }

    FieldDataPtr create_vector_field_data(std::string name, std::string type,
                                          std::string description, int position, size_t size) {
        return std::make_shared<FieldData>(
            name, type, description, position, size, std::vector<size_t>{size},
            std::vector<size_t>{size},
            std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>>{
                {{{}, {}}}});
    }

    FieldDataPtr create_vector_field_data(std::string name, std::string type,
                                          std::string description, size_t size) {
        return create_vector_field_data(name, type, description, 0, size);
    }

    bool operator==(const FieldDataPtr &lhs, const FieldDataPtr &rhs) noexcept {
        return (lhs.get() == rhs.get()) || (lhs && rhs && *lhs == *rhs);
    }

    LibHandle::LibHandle(const std::filesystem::path &shared_library)
        : shared_library{shared_library} {
        char *error;
        dlerror();

        handle = dlopen(this->shared_library.c_str(), RTLD_NOW);

        error = dlerror();
        if (error == nullptr) {
            std::cout << "Successfully loaded library \"" << this->shared_library.string() << "\"."
                      << std::endl;
        } else {
            std::string msg = "An error occured when trying to load library \"" +
                              this->shared_library.string() + "\":\n\t" + std::string(error);
            throw std::runtime_error(msg);
        }

        const auto content_vec = *load_symbol<std::vector<std::pair<const char *, std::any>>>(
            handle, module_name() + "__contents");
        lib_contents.insert(begin(content_vec), end(content_vec));

        const auto version_info =
            *load_symbol<const char *>(handle, module_name() + "__materiaux_version");
        lib_contents["materiaux_version"] = std::any{version_info};
    }

    LibHandle::~LibHandle() {
        lib_contents.clear();
        dlclose(handle);
    }

    const std::map<std::string, std::any> &LibHandle::contents() const { return lib_contents; }

    std::string LibHandle::module_name() const {
        std::string _module_name = shared_library.stem().string();
        _module_name = _module_name.substr(0, _module_name.find_first_of('.', 1));
        if (_module_name.find_first_of("lib") == 0)
            return _module_name.substr(3);
        else
            return _module_name;
    }

    std::filesystem::path LibHandle::shared_library_path() const { return shared_library; }

    template struct Function<double>;
    template struct Function<std::complex<double>>;

    template int result_size<double>(const Function<double> &function);
    template int result_size<std::complex<double>>(const Function<std::complex<double>> &function);

    template int active_coefficients_size<double>(const Function<double> &function);
    template int active_coefficients_size<std::complex<double>>(const Function<std::complex<double>> &function);

    template int constants_size<double>(const Function<double> &function);
    template int constants_size<std::complex<double>>(const Function<std::complex<double>> &function);

    template void call(const Function<double> &function, double *res, const double *coefficients,
                       const double *constants);

    template void call(const Function<std::complex<double>> &function, std::complex<double> *res,
                       const std::complex<double> *coefficients,
                       const std::complex<double> *constants);

    template FunctionPtr<double> wrap_function<double>(FunctionPtr<double> function,
                                                       std::vector<FieldDataPtr> new_coefficients,
                                                       std::vector<FieldDataPtr> new_constants);

    template FunctionPtr<std::complex<double>>
    wrap_function<std::complex<double>>(FunctionPtr<std::complex<double>> function,
                                        std::vector<FieldDataPtr> new_coefficients,
                                        std::vector<FieldDataPtr> new_constants);

    FieldDataPtr create_field_data(
        const char *name, const char *type, const char *description, int position, size_t size,
        std::vector<size_t> rank_sizes, std::vector<size_t> shape,
        std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>> symmetry) {
        return std::make_shared<materiaux::FieldData>(name, type, description, position, size,
                                                      rank_sizes, shape, symmetry);
    }

    std::set<std::string> convert(std::vector<const char *> input) {
        return std::set<std::string>{begin(input), end(input)};
    }

    CodeDataPtr create_code_data(const char *symbol_name, const char *code,
                                 std::vector<const char *> include_directives,
                                 std::vector<const char *> include_directories,
                                 std::vector<const char *> link_libraries,
                                 std::vector<const char *> link_directories,
                                 std::vector<const char *> compile_flags) {
        return std::make_shared<materiaux::CodeData>(materiaux::CodeData{
            symbol_name, code, convert(include_directives), convert(include_directories),
            convert(link_libraries), convert(link_directories), convert(compile_flags)});
    }

    template FunctionPtr<double> create_function(typename Function<double>::func_t func,
                                                 FieldDataPtr result,
                                                 std::vector<FieldDataPtr> coefficients,
                                                 std::vector<bool> active_coefficient_mask,
                                                 std::vector<FieldDataPtr> constants,
                                                 CodeDataPtr code_data);

    template FunctionPtr<std::complex<double>>
    create_function(typename Function<std::complex<double>>::func_t func, FieldDataPtr result,
                    std::vector<FieldDataPtr> coefficients,
                    std::vector<bool> active_coefficient_mask, std::vector<FieldDataPtr> constants,
                    CodeDataPtr code_data);
} // namespace materiaux

namespace materiaux::evolution {
    template struct NonlinearSolver<double>;
    template struct NonlinearSolver<std::complex<double>>;

    template struct NewtonRaphson<double>;
    template struct NewtonRaphson<std::complex<double>>;

    template NonlinearSolverPtr<double> create_newton_raphson<double>();
    template NonlinearSolverPtr<std::complex<double>> create_newton_raphson<std::complex<double>>();

    template struct ExplicitTSAlgo<double>;
    template struct ExplicitTSAlgo<std::complex<double>>;

    template struct RungeKutta5Stable<double>;
    template struct RungeKutta5Stable<std::complex<double>>;

    template ExplicitTSAlgoPtr<double> create_runge_kutta_5_stable<double>();
    template ExplicitTSAlgoPtr<std::complex<double>>
    create_runge_kutta_5_stable<std::complex<double>>();

    template FunctionPtr<double> create_evolution<double>(FunctionPtr<double> eqs,
                                                          FunctionPtr<double> lin_eqs,
                                                          std::vector<FieldDataPtr> internal_states,
                                                          NonlinearSolverPtr<double> algo,
                                                          bool throw_on_error);

    template FunctionPtr<std::complex<double>> create_evolution<std::complex<double>>(
        FunctionPtr<std::complex<double>> eqs, FunctionPtr<std::complex<double>> lin_eqs,
        std::vector<FieldDataPtr> internal_states, NonlinearSolverPtr<std::complex<double>> algo,
        bool throw_on_error);

    template FunctionPtr<double> create_evolution<double>(FunctionPtr<double> eqs,
                                                          FunctionPtr<double> lin_eqs,
                                                          NonlinearSolverPtr<double> algo,
                                                          bool throw_on_error);

    template FunctionPtr<std::complex<double>> create_evolution<std::complex<double>>(
        FunctionPtr<std::complex<double>> eqs, FunctionPtr<std::complex<double>> lin_eqs,
        NonlinearSolverPtr<std::complex<double>> algo, bool throw_on_error);

    template FunctionPtr<double>
    create_evolution<double, const char *>(FunctionPtr<double> eqs, FunctionPtr<double> lin_eqs,
                                           std::vector<const char *> internal_states,
                                           NonlinearSolverPtr<double> algo, bool throw_on_error);

    template FunctionPtr<std::complex<double>> create_evolution<std::complex<double>, const char *>(
        FunctionPtr<std::complex<double>> eqs, FunctionPtr<std::complex<double>> lin_eqs,
        std::vector<const char *> internal_states, NonlinearSolverPtr<std::complex<double>> algo,
        bool throw_on_error);

    template FunctionPtr<double>
    create_evolution<double, std::string>(FunctionPtr<double> eqs, FunctionPtr<double> lin_eqs,
                                          std::vector<std::string> internal_states,
                                          NonlinearSolverPtr<double> algo, bool throw_on_error);

    template FunctionPtr<std::complex<double>> create_evolution<std::complex<double>, std::string>(
        FunctionPtr<std::complex<double>> eqs, FunctionPtr<std::complex<double>> lin_eqs,
        std::vector<std::string> internal_states, NonlinearSolverPtr<std::complex<double>> algo,
        bool throw_on_error);

    template FunctionPtr<double> create_evolution<double>(FunctionPtr<double> rate,
                                                          std::vector<FieldDataPtr> internal_states,
                                                          FieldDataPtr time_increment,
                                                          ExplicitTSAlgoPtr<double> algo,
                                                          bool throw_on_error);

    template FunctionPtr<std::complex<double>> create_evolution<std::complex<double>>(
        FunctionPtr<std::complex<double>> rate, std::vector<FieldDataPtr> internal_states,
        FieldDataPtr time_increment, ExplicitTSAlgoPtr<std::complex<double>> algo,
        bool throw_on_error);

    template FunctionPtr<double> create_evolution<double>(FunctionPtr<double> rate,
                                                          ExplicitTSAlgoPtr<double> algo,
                                                          bool throw_on_error);

    template FunctionPtr<std::complex<double>>
    create_evolution<std::complex<double>>(FunctionPtr<std::complex<double>> rate,
                                           ExplicitTSAlgoPtr<std::complex<double>> algo,
                                           bool throw_on_error);

    template FunctionPtr<double> create_evolution<double, const char *>(
        FunctionPtr<double> rate, std::vector<const char *> internal_states,
        const char *time_increment, ExplicitTSAlgoPtr<double> algo, bool throw_on_error);

    template FunctionPtr<std::complex<double>> create_evolution<std::complex<double>, const char *>(
        FunctionPtr<std::complex<double>> rate, std::vector<const char *> internal_states,
        const char *time_increment, ExplicitTSAlgoPtr<std::complex<double>> algo,
        bool throw_on_error);

    template FunctionPtr<double>
    create_evolution<double, std::string>(FunctionPtr<double> rate,
                                          std::vector<std::string> internal_states,
                                          std::string time_increment,
                                          ExplicitTSAlgoPtr<double> algo, bool throw_on_error);

    template FunctionPtr<std::complex<double>> create_evolution<std::complex<double>, std::string>(
        FunctionPtr<std::complex<double>> rate, std::vector<std::string> internal_states,
        std::string time_increment, ExplicitTSAlgoPtr<std::complex<double>> algo,
        bool throw_on_error);

    template FunctionPtr<double> create_consistent_linearization<double>(
        FunctionPtr<double> lin_ext_duals, FunctionPtr<double> lin_int_duals,
        FunctionPtr<double> lin_ext_eqs, FunctionPtr<double> lin_int_eqs);

    template FunctionPtr<std::complex<double>>
    create_consistent_linearization<std::complex<double>>(
        FunctionPtr<std::complex<double>> lin_ext_duals,
        FunctionPtr<std::complex<double>> lin_int_duals,
        FunctionPtr<std::complex<double>> lin_ext_eqs,
        FunctionPtr<std::complex<double>> lin_int_eqs);

} // namespace materiaux::evolution

namespace materiaux::utils {
    std::vector<FieldDataPtr> active_fields(const std::vector<FieldDataPtr> &fields,
                                            const std::vector<bool> &mask) {
        assert(size(fields) == size(mask));
        std::vector<FieldDataPtr> active_coefficients;
        active_coefficients.reserve(size(mask));
        for (std::size_t ii = 0; ii < fields.size(); ++ii) {
            if (mask[ii])
                active_coefficients.push_back(fields[ii]);
        }
        return active_coefficients;
    }

    std::vector<size_t> field_offsets(const std::vector<FieldDataPtr> &fields) {
        std::vector<size_t> offsets{{0}};
        assert(offsets.size() == 1);
        for (auto &data : fields) {
            offsets.push_back(offsets.back() + data->size);
        }
        return offsets;
    }

    size_t field_size(const FieldData &field) {
        return field.size;
    }

    size_t field_size(FieldDataPtr field) {
        return field_size(*field);
    }

    size_t fields_size(const std::vector<FieldDataPtr> &fields) {
        return fields_size(begin(fields), end(fields));
    }

    size_t field_offset(const FieldDataPtr &field, const std::vector<FieldDataPtr> &fields) {
        auto it = std::find_if(begin(fields), end(fields),
                               [&](const auto &item) { return item == field; });
        return fields_size(begin(fields), it);
    }

    size_t field_offset(const FieldData &field, const std::vector<FieldDataPtr> &fields) {
        auto it = std::find_if(begin(fields), end(fields),
                               [&](const auto &item) { return *item == field; });
        return fields_size(begin(fields), it);
    }

    void append_unique(std::vector<FieldDataPtr> &target, const std::vector<FieldDataPtr> &source) {
        for (const auto &fdp : source)
            if (std::find(begin(target), end(target), fdp) == end(target))
                target.push_back(fdp);
    }

    std::vector<FieldDataPtr> merge(const std::vector<std::vector<FieldDataPtr>> &sources) {
        return std::accumulate(begin(sources), end(sources), std::vector<FieldDataPtr>{},
                               [&](std::vector<FieldDataPtr> target, const auto &source) {
                                   append_unique(target, source);
                                   return std::move(target);
                               });
    }

    FieldData renumber_field(const FieldData &other, int new_position) {
        return FieldData{other.name, other.type,       other.description, new_position,
                         other.size, other.rank_sizes, other.shape,       other.symmetry};
    }

    std::vector<FieldDataPtr> renumber_fields(const std::vector<FieldDataPtr> &original) {
        int pos = 0;
        return std::accumulate(begin(original), end(original), std::vector<FieldDataPtr>{},
                               [&](std::vector<FieldDataPtr> data, const FieldDataPtr &ptr) {
                                   data.push_back(
                                       std::make_shared<FieldData>(renumber_field(*ptr, pos)));
                                   pos += 1;
                                   return std::move(data);
                               });
    }

    std::string substitute(const std::string &template_string, const std::string &key,
                           const std::string &new_value) {
        std::string result = template_string;
        const std::string replace = std::string("${") + key + "}";
        auto pos = result.find(replace);
        while (pos != std::string::npos) {
            result.replace(pos, replace.size(), new_value);
            pos = result.find(replace);
        }
        return result;
    }

    std::string substitute(const std::string &template_string,
                           std::map<std::string, std::string> substitutes) {
        return std::accumulate(begin(substitutes), end(substitutes), template_string,
                               [](std::string _template, const auto &_subs_pair) {
                                   return substitute(_template, _subs_pair.first,
                                                     _subs_pair.second);
                               });
    }
} // namespace materiaux::utils
