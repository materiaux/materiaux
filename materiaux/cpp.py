# Copyright (C) 2019-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


import typing
import pathlib
import hashlib
import os
import numpy as np
import ufl
import materiaux.version
from materiaux.common import get_working_directory, set_working_directory, reset_working_directory, lib_name
import materiaux.common as common
from .materiauXcpp import FieldData, double, double_complex, CodeData, LibHandle, create_evolution, create_consistent_linearization
from .materiauXcpp import create_newton_raphson as _create_newton_raphson
from .materiauXcpp import create_runge_kutta_5_stable as _create_runge_kutta_5_stable
from .materiauXcpp import create_function as _create_function
from .materiauXcpp import scalar_type_names
from .materiauXcpp.any import get_symbol as _get_symbol
from materiaux.namespace import Namespace


DOUBLE = "double"
DOUBLE_COMPLEX = "std::complex<double>"

_nptypes = {"double": np.float_, "double complex": np.complex_, "std::complex<double>": np.complex_}
function_types = typing.Union[double.Function, double_complex.Function]
nonlinear_solver_types = typing.Union[double.NonlinearSolver, double_complex.NonlinearSolver]
explicit_time_stepping_types = typing.Union[double.ExplicitTSAlgo, double_complex.ExplicitTSAlgo]

_LD_LIBRARY_PATH_on_load = os.environ.get("LD_LIBRARY_PATH", None)


# keep alive the lib handles
_loaded_cpp_libs = {}


class LibraryLoaderError(RuntimeError):
    """Class representing an error when loading a dynamic library module."""
    def __init__(self, msg):
        super().__init__(msg)


def _get_dyn_linker_paths() -> str:
    return os.environ.get("LD_LIBRARY_PATH", "")


def _set_dyn_linker_paths(paths: str) -> None:
    os.environ["LD_LIBRARY_PATH"] = str(paths)


def _reset_dyn_linker_paths() -> None:
    if _LD_LIBRARY_PATH_on_load is None:
        os.environ.pop("LD_LIBRARY_PATH")
    else:
        os.environ["LD_LIBRARY_PATH"] = _LD_LIBRARY_PATH_on_load


def _append_dyn_linker_paths(paths: typing.Iterable[pathlib.Path]) -> None:
    paths = list(paths)
    if len(paths) > 0:
        _set_dyn_linker_paths(":".join([_get_dyn_linker_paths()] + [str(path) for path in paths]))


def _prepend_dyn_linker_paths(paths: typing.Iterable[pathlib.Path]) -> None:
    paths = list(paths)
    if len(paths) > 0:
        _set_dyn_linker_paths(":".join([str(path) for path in paths] + [_get_dyn_linker_paths()]))


def get_file_hash(path: pathlib.Path) -> str:
    """Compute the hash of the given file based on its contents."""
    with open(path, "rb") as lf:
        return hashlib.blake2b(lf.read()).hexdigest()


def get_library_content(lib: LibHandle):
    """Crates a namespace filled with all (materiaux module) items provided by the lib."""
    separator = lib.contents.get("separator", "::")
    namespace = Namespace(name=lib.module_name, separator=separator)
    namespace.set_item("lib", lib)

    def _remove_toplevel(_symbol_name: str):
        names = _symbol_name.split(separator)
        if len(names) > 1 and names[0] == namespace.name:
            names.pop(0)
        return separator.join(names)

    for symbol_name, type_name in lib.contents.items():
        namespace.set_item(_remove_toplevel(symbol_name), _get_symbol[type_name](lib, symbol_name))

    if not materiaux.version.check_compatibility(namespace.materiaux_version):
        raise LibraryLoaderError("Loaded library has incompatible version {:s}".format(namespace.materiaux_version))
    return namespace


def load_library(lib: typing.Union[str, pathlib.Path], working_directory: pathlib.Path = None,
                 dyn_linker_directories: typing.Iterable[pathlib.Path] = None) -> Namespace:
    """Load the materiaux module-type shared library.

    :param lib: name or path of the shared library. If lib path is relative, it will be first searched in the current
        path and then in the module/working paths defined in :py:mod:`materiaux.common`.
    :param working_directory[optional]: Defaults to the value returned by
        :py:func:`materiaux.codegeneration.module_generation.get_working_directory`
    :param dyn_linker_directories[optional]: will temporarily be added to dynamic linker search path
    :return: a namespace object representing the library
    """
    working_directory = get_working_directory() if working_directory is None else pathlib.Path(working_directory)
    dyn_linker_directories = [] if dyn_linker_directories is None else dyn_linker_directories

    lib = pathlib.Path(lib)
    if lib.is_absolute():
        abs_path = lib
    else:
        lib = lib.parent / common.lib_name(lib.name)
        abs_path = pathlib.Path.cwd() / lib
        if not abs_path.exists():
            abs_path = working_directory / common.module_lib_subdirectory / lib.name
    if not abs_path.exists():
        raise FileNotFoundError("Was not able to find {!s}".format(lib))

    lib_hash = get_file_hash(abs_path)
    if abs_path in _loaded_cpp_libs.keys() and _loaded_cpp_libs[abs_path][1] != lib_hash:
        raise LibraryLoaderError("Library located at {!s} already loaded but file hashes differ")
    else:
        if lib_hash != get_file_hash(abs_path):
            raise LibraryLoaderError("Library hashes before and after loading differ from each other")
        _old_env = _get_dyn_linker_paths()
        try:
            _append_dyn_linker_paths(dyn_linker_directories)
            handle = LibHandle(str(abs_path))
            _loaded_cpp_libs[abs_path] = (handle, lib_hash)
        finally:
            _set_dyn_linker_paths(_old_env)
    content = get_library_content(_loaded_cpp_libs[abs_path][0])
    content.set_item("hash", lib_hash)
    return content


def field_data_to_dict(field_data: FieldData) -> typing.Dict:
    """Create a dict representing FieldData for, e.g., pickling."""
    return {attr: getattr(field_data, attr) for attr in dir(field_data) if attr[0] != "_"}


def field_data_from_dict(field_data_dict: typing.Dict) -> FieldData:
    """Create a FieldData object from a dictionary (from serialization...)"""
    return FieldData(field_data_dict["name"], field_data_dict["type"], field_data_dict["description"],
                     field_data_dict["position"], field_data_dict["size"], field_data_dict["rank_sizes"],
                     field_data_dict["shape"], field_data_dict["symmetry"])


def field_element(field_data: typing.Union[FieldData, typing.Iterable[FieldData]], cell_type: ufl.Cell,
                  quadrature_degree: int = 0, quad_scheme: str = "default") -> ufl.FiniteElementBase:
    """
    Creates a ufl element representing the field data. By default, the resulting element will correspond to a single
    evaluation point.
    """
    if not isinstance(field_data, FieldData):
        elements = [field_element(fd, cell_type, quadrature_degree=quadrature_degree, quad_scheme=quad_scheme) for fd in
                    field_data]
        return ufl.MixedElement(*elements)
    else:
        symmetry = field_data.symmetry
        symmetry_maps = [dict([(tuple(v1), tuple(v2)) for v1, v2 in symmetry_vec if len(v1) > 0]) for symmetry_vec in
                         symmetry]

        if len(field_data.shape) == 0:
            return ufl.FiniteElement("Quadrature", cell_type, quadrature_degree, quad_scheme=quad_scheme)
        elif len(field_data.shape) == 1 and len(symmetry_maps[0]) == 0:
            return ufl.VectorElement("Quadrature", cell_type, quadrature_degree, dim=field_data.size,
                                     quad_scheme=quad_scheme)
        else:
            if len(field_data.rank_sizes) > 1 and any([len(symm) > 0 for symm in symmetry_maps]):
                raise NotImplementedError(
                    "Handling the symmetry of objects of rank 2 (in form sense) has not been implemented yet.")
            symmetry_map = symmetry_maps[0]
            return ufl.TensorElement("Quadrature", cell_type, quadrature_degree, shape=tuple(field_data.shape),
                                     quad_scheme=quad_scheme, symmetry=symmetry_map if len(symmetry) > 0 else None)


def get_scalar_t(func: function_types):
    """
    Determine the scalar type (double or complex) for the given Function object
    """
    return func.scalar_type


def _element(item: typing.Union[ufl.Coefficient, ufl.Argument, FieldData]) -> ufl.FiniteElementBase:
    return item.ufl_element() if isinstance(item, (ufl.Coefficient, ufl.Argument)) \
        else field_element(item, ufl.interval)


def _mk_shape(shape: typing.Union[typing.Tuple, typing.List]):
    return (1,) if len(shape) == 0 else shape


def _shape(item: typing.Union[ufl.Coefficient, ufl.Constant, ufl.Argument, FieldData]) -> tuple:
    shape = _element(item).reference_value_shape() if isinstance(item, (
        ufl.Coefficient, ufl.Argument, FieldData)) else item.ufl_shape
    return _mk_shape(shape)


def _prep_array(scalar_type: str, item):
    try:
        return item.flatten().astype(_nptypes[scalar_type])
    except AttributeError as e:
        return np.array([item]).astype(_nptypes[scalar_type])


def create_function_result(scalar_type: str, arguments: typing.Iterable[typing.Union[ufl.Argument, FieldData]]) -> np.ndarray:
    """
    Creates an array of appropriate type and shape corresponding to the given FieldData or ufl.Argument(s)
    """
    if isinstance(arguments, (ufl.Argument, FieldData)):
        arguments = (arguments,)
    shape = sum([list(_shape(arg) if isinstance(arg, ufl.Argument) else arg.rank_sizes) for arg in arguments], [])
    return np.zeros(shape if len(shape) > 0 else 1, dtype=_nptypes[scalar_type])


def create_field_arrays(scalar_type: str, fields: typing.Iterable[typing.Union[ufl.Coefficient, ufl.Constant, FieldData]]) -> typing.Dict[str, np.ndarray]:
    """
    Creates a dictionary of arrays that may hold the numerical data for the given fields.
    """
    return dict([(field.name, np.zeros(_shape(field), dtype=_nptypes[scalar_type])) for field in fields])


def active_coefficients(function: function_types) -> \
        typing.Tuple[FieldData]:
    """
    Extract the active coefficient for the given Function
    """
    return tuple([coeff for ii, coeff in enumerate(function.coefficient_data) if function.active_coefficient_mask[ii]])


def active_coefficients_dict(function: function_types) -> \
        typing.Dict[str, FieldData]:
    """
    Extract the active coefficient for the given Function
    """
    return {coeff.name: coeff for ii, coeff in enumerate(function.coefficient_data) if
            function.active_coefficient_mask[ii]}


def create_function_coefficients(function: function_types,
                                 coefficients: typing.Dict[str, np.ndarray] = None) -> np.ndarray:
    """
    Creates a single flat array to collect the values of all coefficients of the Function
    """
    scalar_t = get_scalar_t(function)
    coefficients = coefficients if coefficients is not None else create_field_arrays(scalar_t, function.coefficient_data)
    if not any(function.active_coefficient_mask):
        return np.empty(0, dtype=_nptypes[scalar_t])
    try:
        return np.concatenate(
            [_prep_array(scalar_t, coefficients[coeff.name]) for ii, coeff in enumerate(function.coefficient_data)
             if function.active_coefficient_mask[ii]])
    except KeyError as e:
        raise ValueError("Not all coefficients have been provided: {:s} is missing.".format(e.args[0]))


def function_coefficient_offsets(function: function_types) \
        -> typing.Tuple[int]:
    """
    Returns the offsets of the Function's coefficients when places in a single flat array:
    """
    offsets = [0]
    for ii, coeff in enumerate(function.coefficient_data):
        if not function.active_coefficient_mask[ii]:
            continue
        offsets.append(offsets[-1] + coeff.size)
    return tuple(offsets)


def create_function_constants(function: function_types,
                              constants: typing.Dict[str, np.ndarray] = None) -> np.ndarray:
    """
    Creates a single flat array to collect the values of all constants of the Function
    """
    scalar_t = get_scalar_t(function)
    constants = constants if constants is not None else create_field_arrays(scalar_t, function.constant_data)
    if len(function.constant_data) == 0:
        return np.empty(0, dtype=_nptypes[scalar_t])
    try:
        return np.concatenate(
            [_prep_array(scalar_t, constants[constant.name]) for ii, constant in enumerate(function.constant_data)]
        )
    except KeyError as e:
        raise ValueError("Not all constants have been provided: {:s} is missing.".format(e.args[0]))


def function_constant_offsets(function: function_types) \
        -> typing.Tuple[int]:
    """
    Returns the offsets of the Function's constants when places in a single flat array:
    """
    offsets = [0]
    for ii, constant in enumerate(function.constant_data):
        offsets.append(offsets[-1] + constant.size)
    return tuple(offsets)


def call(func: function_types, result: np.ndarray = None, **function_params):
    """
    Call the compiled Function

    :param func: the function
    :param result: if "None" then an array will be created and returned; otherwise the given will hold the result
    :param function_params: the keywords are the name of parameters (coefficients or constants of the form), the
        corresponding values have to be numpy arrays of appropriate shape or scalars
    :return: the result array
    """
    scalar_t = get_scalar_t(func)
    if result is None:
        _result = create_function_result(scalar_t, func.result_data)
    else:
        assert result.size == func.result_data.size
        _result = result
    func(_result.reshape(_result.size, 1),
         create_function_coefficients(func, function_params),
         create_function_constants(func, function_params))
    return _result


def create_newton_raphson(scalar_type: str) -> nonlinear_solver_types:
    """
    Create a Newton-Raphson evolution solver for the given scalar type.

    :param scalar_type: The name of the scalar type -- must be in scalar_type_names
    :return: A nonlinear solver (:cpp:class:`materiaux::evolution::NonlinearSolver`) instance encapsulating a
        Newton-Raphson algorithm
    """
    return _create_newton_raphson[scalar_type]()


def create_runge_kutta_5_stable(scalar_type: str) -> explicit_time_stepping_types:
    """
    Create a Newton-Raphson evolution solver for the given scalar type.

    :param scalar_type: The name of the scalar type -- must be in scalar_type_names
    :return: An explicit time integration/stepping algorithm (:cpp:class:`materiaux::evolution::ExplicitTSAlgo`)
        instance encapsulating a 5th order Runge-Kutta scheme with extended region of stability.
    """
    return _create_runge_kutta_5_stable[scalar_type]()


def create_function(scalar_type: str, func_ptr: int, coefficients: typing.Iterable[FieldData],
                    active_coefficients: typing.Iterable[bool], constants: typing.Iterable[FieldData],
                    code_data: CodeData) -> function_types:
    """
    Create a :cpp:class:`materiaux::Function` with a plain c function pointer and metadata.

    :param scalar_type: The name of the scalar type -- must be in scalar_type_names
    :param func_ptr: Plain C pointer (uintptr_t)
    :param coefficients: Coefficient data of the the function
    :param active_coefficients: Flags indicating active/inactive coefficients
    :param constants: Constant data of the the function
    :param code_data: Code data object for code generation
    :return: The function created
    """
    return _create_function[scalar_type](func_ptr, coefficients, active_coefficients, constants, code_data)
