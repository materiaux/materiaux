// Copyright (C) 2019-2021 Matthias Rambausek
//
// This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
//
// SPDX-License-Identifier:    AGPL-3.0-or-later

#pragma once

// Based on https://gcc.gnu.org/wiki/Visibility

/// Macro for explicitly exporting symbols (when default visibility is hidden)
#if defined _WIN32 || defined __CYGWIN__
#ifdef __GNUC__
#define MATERIAUX_DLL_EXPORT __attribute__((dllexport))
#else
#define MATERIAUX_DLL_EXPORT __declspec(dllexport)
#endif
#else
#define MATERIAUX_DLL_EXPORT __attribute__((visibility("default")))
#endif

#include <any>
#include <functional>
#include <memory>
#include <vector>


namespace materiaux {

    struct FieldData;
    using FieldDataPtr = std::shared_ptr<FieldData>;

    struct CodeData;
    using CodeDataPtr = std::shared_ptr<CodeData>;

    /// Class representing a numerical function

    /// Computes a result (array) from two input arrays, coefficients and constants,
    /// all provided as raw C pointers. Metadata is provided in terms of vectors of
    /// FieldDataPtr and a mask indicating actually active coefficients (inactive coefficients may
    /// result from derivation). Note that the character of coefficients and constants is
    /// effectively defined by external context, i.e. the domain of application, the code generation
    /// implementation and the user. It is suggested that coefficients represent temporarily
    /// variable or spatial fields (as in physics) while constants are not. As a guideline, one
    /// might ask whether a quantity is subject to evolution or undergoes change *during* solver
    /// iterations. Thus, material parameters a good example for constants. Time-like quantities
    /// could "fit" into either category, depending on the actual context.
    ///
    /// The class has an optional plain C function members. In any case, there
    /// will be a member of std::function-type. It is strongly recommended that
    /// a given std::function-type callable, does not have any side effects.
    ///
    /// Note that there exist functions materiaux::call which provide some more
    /// convenient but sometimes also slower ways for calling a
    /// materiaux::Function. Also, there are materiaux::wrap_function and
    /// materiaux::make_all_coeffs_active which allow modifications of a
    /// materiaux::Function's interface.
    ///
    /// @tparam scalar_type The scalar type of result, coefficients and constants
    template <typename scalar_type> struct MATERIAUX_DLL_EXPORT Function {

        using scalar_t = scalar_type;

        /// Type alias for an appropriate underlying function pointer
        using func_ptr_t = void (*)(scalar_t *res, const scalar_t *coefficients,
                                    const scalar_t *constants);

        /// Type alias for an appropriate underlying std.:function
        using func_t = decltype(std::function{std::declval<func_ptr_t>()});

        /// Constructor using a Function::func_t (std::function)
        ///
        /// @param func The callable doing the actual computation. Throughout
        /// this library it is assumed that this function has no side effects.
        /// @param result Information about the result
        /// @param coefficients Information about the coefficients
        /// @param active_coefficient_mask A vector of same length as coefficients.
        /// A "true" entry indicates that the respective coefficients is
        /// considered as input to the working function. Thus, if "false", the
        /// respective coefficient *must not* be omitted in calls.
        /// @param constants Information about the constants
        /// @param code_data Information for code_generation
        Function(func_t func, FieldDataPtr result, std::vector<FieldDataPtr> coefficients,
                 std::vector<bool> active_coefficient_mask, std::vector<FieldDataPtr> constants,
                 CodeDataPtr code_data);

        /// std::function object holding the actual function
        const func_t func;

        /// Metadata for the result of the function
        const FieldDataPtr result_data;

        /// Metadata for the coefficients (by convention spatially and temporarily variable)
        const std::vector<FieldDataPtr> coefficient_data;

        /// Mask indicating the position of required coefficients. Some might be dropped as a
        /// result of expression optimization or derivations.
        const std::vector<bool> active_coefficient_mask;

        /// Metadata for the constants (by convention spatially and temporarily constant)
        const std::vector<FieldDataPtr> constant_data;

        /// Code generation data
        const CodeDataPtr code_data;

        /// Call operator. See materiaux::call for some more convenient interfaces.
        ///
        /// @param[out] res Result array
        /// @param[in] coefficients Numerical values of all *active* coefficients
        /// @param[in] constants Numerical values of all constants
        void operator()(scalar_t *res, const scalar_t *coefficients,
                        const scalar_t *constants) const {
            func(res, coefficients, constants);
        }
    };

    /// Type alias
    template <typename scalar_type> using FunctionPtr = std::shared_ptr<Function<scalar_type>>;


    /// Factory for field data.
    /// @param[in] name Name of the field
    /// @param[in] type Preferably one materiaux::FieldTypes, though any string is possible in
    /// principle.
    /// @param[in] description Longer description of the Field. Useful, if 'name' is just a
    /// letter/symbol.
    /// @param[in] position Position of the field in some argument list (group of fields)
    /// @param[in] size Number of numerical entries (size of a container) holding data
    /// representing the field
    /// @param[in] rank_sizes In case that it makes sense to distinguish an additional internal
    /// structure as it may
    ///     be the case of fields representing a second derivatives.
    /// @param[in] shape The shape of the array representing the numerical field data
    /// @param[in] symmetry A symmetry map with pairs of governing and governed indices. One
    /// such map for each rank.
    /// @return
    MATERIAUX_DLL_EXPORT FieldDataPtr create_field_data(
        const char *name, const char *type, const char *description, int position, size_t size,
        std::vector<size_t> rank_sizes, std::vector<size_t> shape,
        std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>> symmetry);

    /// Factory for code data
    ///
    /// @param[in] symbol_name Name of the symbol in the generated code (see `code`)
    /// @param[in] code Code that generates a `const std::shared_ptr<Function>` named symbol_name
    /// @param[in] include_directives Include directives
    /// @param[in] include_directories Include directories
    /// @param[in] link_libraries Link libraries (could be linker flags as well)
    /// @param[in] link_directories Link directories
    /// @param[in] compile_flags Additional compile flags
    /// @return
    MATERIAUX_DLL_EXPORT CodeDataPtr create_code_data(const char *symbol_name, const char *code,
                                            std::vector<const char *> include_directives,
                                            std::vector<const char *> include_directories,
                                            std::vector<const char *> link_libraries,
                                            std::vector<const char *> link_directories,
                                            std::vector<const char *> compile_flags);

    template <typename scalar_type>
    MATERIAUX_DLL_EXPORT FunctionPtr<scalar_type>
    create_function(typename Function<scalar_type>::func_t func, FieldDataPtr result,
                    std::vector<FieldDataPtr> coefficients,
                    std::vector<bool> active_coefficient_mask, std::vector<FieldDataPtr> constants,
                    CodeDataPtr code_data);

    namespace utils {
        /// Determine the total size of the common array gathering a set of fields from a vector
        MATERIAUX_DLL_EXPORT size_t fields_size(const std::vector<FieldDataPtr> &fields);

        /// Return the total (array-)size of a field
        MATERIAUX_DLL_EXPORT size_t field_size(FieldDataPtr field);

        /// Return the total (array-)size of a field
        MATERIAUX_DLL_EXPORT size_t field_size(const FieldData& field);
    }


    /// Total size (length) required to store the numeric data of a function's result
    template <typename scalar_t> MATERIAUX_DLL_EXPORT int result_size(const Function<scalar_t> &function);

    /// Total size (length) required to store the numeric data of a function's (active) coefficients
    template <typename scalar_t> MATERIAUX_DLL_EXPORT int active_coefficients_size(const Function<scalar_t> &function);

    /// Total size (length) required to store the numeric data of a function's constants
    template <typename scalar_t> MATERIAUX_DLL_EXPORT int constants_size(const Function<scalar_t> &function);

    namespace evolution {
        template <typename scalar_t> struct NonlinearSolver;
        template <typename scalar_t>
        using NonlinearSolverPtr = std::shared_ptr<evolution::NonlinearSolver<scalar_t>>;

        template <typename scalar_t> struct ExplicitTSAlgo;
        template <typename scalar_t>
        using ExplicitTSAlgoPtr = std::shared_ptr<evolution::ExplicitTSAlgo<scalar_t>>;

        /// Factory for a materiaux::evolution::NewtonRaphson solver
        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT NonlinearSolverPtr<scalar_t> create_newton_raphson();

        /// Factory for a materiaux::evolution::RungeKutta5Stable solver
        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT ExplicitTSAlgoPtr<scalar_t> create_runge_kutta_5_stable();


        /// Create a materiaux::Function that returns the internal state that solves the evolution
        /// equations.
        ///
        /// @tparam scalar_t Scalar type; derives from the scalar type of equations
        /// @param[in] eqs Function that represents the residual of the evolution equations
        /// @param[in] lin_eqs Function that represents the linearization of the residual with
        /// respect to the internal
        ///     state
        /// @param[in] internal_states Fields (a subset of the coefficients of 'eqs') that refer to
        /// the internal states which are to be computed.
        /// @param[in] algo Solver for the evolution equations. See
        /// materiaux::evolution::NewtonRaphson for an example.
        /// @param[in] throw_on_error Whether an exception should be thrown on failure. If not,
        /// "nan"s are returned.
        /// @return The materiaux::Function that returns the internal state that solves the
        /// evolution equations
        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t>
        create_evolution(FunctionPtr<scalar_t> eqs, FunctionPtr<scalar_t> lin_eqs,
                         std::vector<FieldDataPtr> internal_states,
                         NonlinearSolverPtr<scalar_t> algo, bool throw_on_error);


        /// Create a materiaux::Function that returns the internal state that solves the evolution
        /// equations. For this version of the factory, internal states are extracted from the
        /// equations based on the names provided.
        ///
        /// @tparam scalar_t Scalar type; derives from the scalar type of equations
        /// @param[in] eqs Function that represents the residual of the evolution equations
        /// @param[in] lin_eqs Function that represents the linearization of the residual with
        /// respect to the internal state
        /// @param[in] internal_states The names of the internal states
        /// @param[in] algo Solver for the evolution equations. See
        /// materiaux::evolution::NewtonRaphson for an example.
        /// @param[in] throw_on_error Whether an exception should be thrown on failure. If not,
        /// "nan"s are returned.
        /// @return The materiaux::Function that returns the internal state that solves the
        /// evolution equations
        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t>
        create_evolution(FunctionPtr<scalar_t> eqs, FunctionPtr<scalar_t> lin_eqs,
                         std::vector<const char*> internal_states,
                         NonlinearSolverPtr<scalar_t> algo, bool throw_on_error = false);


        /// Create a materiaux::Function that returns the internal state that solves the evolution
        /// equations. For this version of the factory, internal states are all fields of type
        /// materiaux::FieldTypes::InternalState.
        ///
        /// @tparam scalar_t Scalar type; derives from the scalar type of equations
        /// @param[in] eqs Function that represents the residual of the evolution equations
        /// @param[in] lin_eqs Function that represents the linearization of the residual with
        /// respect to the internal state
        /// @param[in] algo Solver for the evolution equations. See
        /// materiaux::evolution::NewtonRaphson for an example.
        /// @param[in] throw_on_error Whether an exception should be thrown on failure. If not,
        /// "nan"s are returned.
        /// @return The materiaux::Function that returns the internal state that solves the
        /// evolution equations
        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t>
        create_evolution(FunctionPtr<scalar_t> eqs, FunctionPtr<scalar_t> lin_eqs,
                         NonlinearSolverPtr<scalar_t> algo, bool throw_on_error = false);

        /// Create a materiaux::Function that returns the updated internal state computed by an
        /// explicit time integration algorithm.
        ///
        /// @tparam scalar_t Scalar type; derives from the scalar type of equations
        /// @param[in] rate Function that represents the rate of the internal states
        /// @param[in] internal_states Fields (a subset of the coefficients of 'rate') that refer to
        /// the internal states which are to be computed.
        /// @param[in] time_increment Field that refers to a general time (sub-)increment. It is
        /// intended to compute t = t_n + Delta_t where Delta_t is not necessarily t_(n+1) - t_n and
        /// thus encodes a time-dependence of the rate function. If a null pointer is given a dummy
        /// will be created.
        /// @param[in] algo Explicit time stepping algorithm. See
        /// materiaux::evolution::RungeKutta5stable for an example.
        /// @param[in] throw_on_error Whether an exception should be thrown on failure. If not,
        /// "nan"s are returned.
        /// @return The materiaux::Function that returns the internal state that solves the
        /// evolution equations
        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t>
        create_evolution(FunctionPtr<scalar_t> rate, std::vector<FieldDataPtr> internal_states,
                         FieldDataPtr time_increment, ExplicitTSAlgoPtr<scalar_t> algo,
                         bool throw_on_error = false);

        /// Create a materiaux::Function that returns the updated internal state computed by an
        /// explicit time integration algorithm.
        ///
        /// @tparam scalar_t Scalar type; derives from the scalar type of equations
        /// @param[in] rate Function that represents the rate of the internal states
        /// @param[in] internal_states Name of fields (a subset of the coefficients of 'eqs') that
        /// refer to the internal states which are to be computed.
        /// @param[in] time_increment Name that refers to a general time (sub-)increment. It is
        /// intended to compute t = t_n + Delta_t where Delta_t is not necessarily t_(n+1) - t_n and
        /// thus encodes a time-dependence of the rate function. If name is "" or null pointer, a
        /// dummy will be created.
        /// @param[in] algo Explicit time stepping algorithm. See
        /// materiaux::evolution::RungeKutta5stable for an example.
        /// @param[in] throw_on_error Whether an exception should be thrown on failure. If not,
        /// "nan"s are returned.
        /// @return The materiaux::Function that returns the internal state that solves the
        /// evolution equations
        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t>
        create_evolution(FunctionPtr<scalar_t> rate, std::vector<const char*> internal_states,
                         const char* time_increment, ExplicitTSAlgoPtr<scalar_t> algo,
                         bool throw_on_error = false);

        /// Create a materiaux::Function that returns the updated internal state computed by an
        /// explicit time integration algorithm. For this version of the factory, internal states
        /// are all fields of type materiaux::FieldTypes::InternalState and the time increment is
        /// of type materiaux::FieldTypes::TimeIncrement. If no time increment is found, a dummy
        /// will be created.
        ///
        /// @tparam scalar_t Scalar type; derives from the scalar type of equations
        /// @param[in] rate Function that represents the rate of the internal states
        /// @param[in] algo Explicit time stepping algorithm. See
        /// materiaux::evolution::RungeKutta5stable for an example.
        /// @param[in] throw_on_error Whether an exception should be thrown on failure. If not,
        /// "nan"s are returned.
        /// @return The materiaux::Function that returns the internal state that solves the
        /// evolution equations
        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t>
        create_evolution(FunctionPtr<scalar_t> rate, ExplicitTSAlgoPtr<scalar_t> algo,
                         bool throw_on_error = false);

        /// Create a Function that forms the consistent linearization of a material response
        /// ('dual'), e.g. stress, with respect the corresponding primal field(s), e.g. strain, in
        /// presence of internal states. In this context, 'duals' refers to the materials response,
        /// 'eqs' to the evolutions equations, 'ext' to primal external states and 'int' to (primal)
        /// internal states.
        ///
        /// @tparam scalar_t scalar type: derived from the given Functions
        /// @param[in] lin_ext_duals Linearization of the (external) material response with respect
        /// to external states
        /// @param[in] lin_int_duals Linearization of the (external) material response with respect
        /// to internal states
        /// @param[in] lin_ext_eqs Linearization of the evolution equations with respect to external
        /// states
        /// @param[in] lin_int_eqs Linearization of the evolution equations with respect to internal
        /// states
        /// @return Function that forms the consistent linearization of a material response
        template <typename scalar_t>
        MATERIAUX_DLL_EXPORT FunctionPtr<scalar_t> create_consistent_linearization(
            FunctionPtr<scalar_t> lin_ext_duals, FunctionPtr<scalar_t> lin_int_duals,
            FunctionPtr<scalar_t> lin_ext_eqs, FunctionPtr<scalar_t> lin_int_eqs);
    } // namespace evolution

} // namespace materiaux

