# Copyright (C) 2019-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


"""
This module collects common data for code generation and library loading, i.e. working directories and
library/module name patterns.

The working directory defaults to the environment variable 'MATERIAUX_MODEL_DIR' if this is variable is set and falls
back to the local directory ".materiaux_jit" otherwise.
"""


import os
import typing
import pathlib
import distutils.ccompiler


shared_lib_prefix = "lib"
shared_lib_suffix = distutils.ccompiler.new_compiler().shared_lib_extension
static_lib_suffix = distutils.ccompiler.new_compiler().static_lib_extension


def lib_name(module_dir_name: str) -> str:
    """Helper providing the name of a shared library for a given module directory name."""
    lib_prefix = shared_lib_prefix
    lib_suffix = shared_lib_suffix
    prefix = "" if module_dir_name.startswith(lib_prefix) else lib_prefix
    suffix = "" if module_dir_name.endswith(lib_suffix) else lib_suffix
    return prefix + module_dir_name + suffix


module_source_subdirectory = pathlib.Path("modules")
module_lib_subdirectory = pathlib.Path("lib")
module_include_subdirectory = pathlib.Path("include")


_default_working_directory = pathlib.Path(".materiaux_jit")
_working_directory_on_load = os.environ.get("MATERIAUX_MODEL_DIR", None)


def set_working_directory(directory: typing.Union[str, pathlib.Path]) -> None:
    """Set working directory for code generation/models."""
    _dir = pathlib.Path(directory)
    if _dir.exists() and not _dir.is_dir():
        raise ValueError("The given path {!s} exists but does not refer to a directory".format(_dir))
    os.environ["MATERIAUX_MODEL_DIR"] = str(pathlib.Path(directory))


def reset_working_directory() -> None:
    """Reset working directory for code generation/models to the value detected during loading the module."""
    if _working_directory_on_load is None:
        os.environ.pop("MATERIAUX_MODEL_DIR")
    else:
        os.environ["MATERIAUX_MODEL_DIR"] = str(_working_directory_on_load)


def get_working_directory() -> pathlib.Path:
    """Get the directory for code generation and modules."""
    return pathlib.Path(os.environ.get("MATERIAUX_MODEL_DIR", _default_working_directory))


def module_name_to_path(module_name: str) -> pathlib.Path:
    """Expand a scoped module name (scopes are indicated with "::") to a path where each scope is a directory."""
    return pathlib.Path(*module_name.split("::"))


def get_module_source_directory(working_directory: pathlib.Path, module_name: str) -> pathlib.Path:
    """Get the module source directory based on the working directory and the module name."""
    return pathlib.Path(working_directory, module_source_subdirectory, module_name_to_path(module_name))
