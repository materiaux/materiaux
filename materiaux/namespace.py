# Copyright (C) 2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiaux)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


import typing


class Namespace:
    """
    Python class representing namespaces. The structure is somewhat similar to nested dicts. In comparison to such an
    implementation, the present class support separators similar to "::" in C++.
    """

    NOTHING = 0
    THROW = 1
    OVERRIDE = 2

    def __init__(self, name: str = "", parent=None, separator: str = "::", init_dict: dict=None, **kwargs):
        """
        Constructor

        :param name: Name of the instance
        :param parent: Parent (of type Namespace)
        :param separator: String used to indicated nested (sub-)namespaces
        :param init_dict: Dict containing initial data
        :param kwargs: Initial data
        """
        names = name.split(separator)
        self._separator = separator
        self._name = names.pop(0)
        self._parent = parent
        self._contents = set()

        _obj = self
        init_dict = init_dict if init_dict is not None else {}
        init_dict.update(**kwargs)

        if len(names) > 0:
            _obj = self.get_sub_namespace(self._separator.join(names))

        for key, val in init_dict.items():
            _obj.set_item(key, val)

    def is_toplevel(self) -> bool:
        return self._parent is None

    @property
    def parent(self):
        return self._parent

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, new_name):
        self._name = new_name

    def set_item(self, name: str, value: typing.Any, on_exist=THROW) -> None:
        """
        Add an item to the namespace.

        :param name: Name (key) of the item added
        :param value: The value
        :param on_exist: The action to be triggered if an item with the given name already exists. Supported values are
            Namespace.THROW, Namespace.NOTHING and Namespace.OVERRIDE.
        """
        names = name.split(self._separator)
        _name = names.pop(0)
        if len(names) > 0:
            self.get_sub_namespace(_name).set_item(self._separator.join(names), value, on_exist=on_exist)
        elif not hasattr(self, name) or on_exist == self.OVERRIDE:
            setattr(self, name, value)
            if isinstance(value, Namespace):
                value._parent = self
            self._contents.update({name})
        elif on_exist == self.THROW:
            raise AttributeError("Attribute {:s} already exits".format(name))
        elif on_exist == self.NOTHING:
            pass
        else:
            raise RuntimeError("This point should not be reachable")

    def _gather_contents(self, levels, contents=None, prefix=""):
        contents = set() if contents is None else contents
        prefix = prefix + self._separator if prefix != "" else prefix
        contents.update({prefix + content for content in self._contents})
        if levels != 0:
            for item_name in filter(lambda item_name: isinstance(self.get_item(item_name), Namespace), self._contents):
                self.get_item(item_name)._gather_contents(levels-1, contents=contents, prefix=prefix+item_name)
        return contents

    def contents(self, levels=-1) -> typing.Set[str]:
        """
        Gather all item names/key for the specified number of levels (downwards).
        :param levels: The number of levels to go down in the hierarchy. If levels is -1, all levels will be visited.
        :return: Names/keys gathered
        """
        return self._gather_contents(levels)

    def get_item(self, name: str) -> typing.Any:
        """Get the item with the given name/key."""
        names = name.split(self._separator)
        _name = names.pop(0)
        attr = getattr(self, _name)
        return attr.get_item(self._separator.join(names)) if len(names) > 0 else attr

    def _scope(self) -> typing.List[typing.Tuple[str, str]]:
        parent_scope = [] if self.is_toplevel() else self.parent._scope()
        return parent_scope + [(self.name, self._separator)]

    def unscoped_name(self, name) -> str:
        """Remove all scopes from the given name.
        This is only trivial if all nested namespaces use the same separator."""
        names = name.split(self._separator)
        _name = names.pop(0)
        item = self.get_item(_name)
        if isinstance(item, Namespace):
            return item.unscoped_name(self._separator.join(names))
        else:
            return name

    def scope(self, name: str=None, join=True) -> typing.Union[str, list]:
        """Get the scope of the item with given name."""
        if name is None:
            scope = self._scope()
            if join:
                return "".join([_name + _sep for _name, _sep in scope[:-1]]) + scope[-1][0]
            else:
                return [_name for _name, _ in scope]

        names = name.split(self._separator)
        _name = names.pop(0)
        item = self.get_item(_name)
        if len(names) > 0:
            return item.scope(self._separator.join(names), join=join)
        else:
            return self.scope(join=join)

    def get_sub_namespace(self, name):
        """Get the sub-namespace with name/key "name" and create a new one if no item with that name exists."""
        if hasattr(self, name):
            attr = getattr(self, name)
            if isinstance(attr, Namespace):
                return attr
            else:
                raise AttributeError("{:s} exists but is not of type Namespace".format(name))
        else:
            self.set_item(name.split(self._separator)[0],
                          Namespace(name=name, parent=self, separator=self._separator), on_exist=self.NOTHING)
            return getattr(self, name)
