Examples
========

.. toctree::
   :maxdepth: 1

   examples/1-hyperelasticity
   examples/2-viscoelasticity
   examples/3-ferromagnetism
