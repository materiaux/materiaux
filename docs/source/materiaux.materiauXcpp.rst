materiaux.materiauXcpp package
==============================

This package is the Python interface to the C++ backend library. We provide a minimalistic
`Python interface documentation <materiaux.html#module-materiaux.materiauXcpp>`_ alongside the full
`C++ library documentation <materiaux_cpp_header.html>`_.
