## Build instructions
This package can be built with the usual python `setuptools` toolchain. However, the C++ bindings 
require [Eigen](http://eigen.tuxfamily.org) and [pybind11](https://github.com/pybind/pybind11/) 
at build time.


### Supported platforms

*materiaux* is currently developed only on linux installations. However, it might (be possible to make it) 
work on other platforms (Windows, Mac OS) as well.


### Required dependencies

 * [Eigen](http://eigen.tuxfamily.org)
 * [cffi](http://github.com/cffi/cffi)
 * [pybind11](https://github.com/pybind/pybind11/)
 * [numpy](https://numpy.org/)
 * [UFL](https://github.com/FEniCS/ufl)
 * [FFCX](https://github.com/FEniCS/ffcx)

 
### Installation

#### General procedure

First ensure that the Eigen3 and pybind11 C++ libraries are installed at standard system location or 
can be found through some additional flags (`CMAKE_PREFIX_PATH`) or environment variables, 
i.e. `Eigen3_ROOT` and `pybind11_ROOT`. The preferred way to achieve this for `pybind11` is to
"pip-install" `pybind11-global` which installs not only the python parts of `pybind11` but also the 
necessary `cmake` scripts such that no separate installation of the C++ part is required.

When at least Eigen3 is there, install python requirements:
```
python3 -m pip install --upgrade pybind11-global
python3 -m pip install --upgrade cffi
python3 -m pip install --upgrade numpy
```
Since materiaux requires *specific* development versions of UFL, FIAT and FFCX, it is recommended
to install snapshots against which materiaux has been tested.
```
python3 -m pip install --upgrade git+https://github.com/mrambausek/ufl.git#egg=fenics-ufl-2019.2.0.dev0
python3 -m pip install --upgrade git+https://github.com/mrambausek/fiat.git#egg=fenics-fiat-2019.2.0.dev0
python3 -m pip install --upgrade git+https://github.com/mrambausek/ffcx.git#egg=fenics-ffcx-2019.2.0.dev0
```
Note that this will change once these packages see their next stable release.
Finally, to install materiaux,
```
python3 -m pip install --upgrade git+https://gitlab.com/materiaux/materiaux.git
```

Specific releases, once ready, can be installed via
```
python3 -m pip install --upgrade git+https://gitlab.com/materiaux/materiaux.git@$VERSION_TAG
```
where `$VERSION_TAG` is the "tag" of the version to be installed.

The file `requirements.txt` in the project root directory contains further optional dependencies. Run
```
pip install -r requirements.txt
```
in materiaux's root directory to install all required and optional dependencies.


#### Building the documentation

HTML documentation can be created locally by running `make html` in the subdirectory `docs`.
This requires `pandoc`, `doxygen` and `sphinx` and a number of `sphinx` extensions. 
The necessary python packages can be installed with
```
python3 -m pip install -U Sphinx==3.3.1 sphinx_rtd_theme==0.5.0 breathe==4.24.1 nbsphinx==0.8.0 sphinxcontrib-svg2pdfconverter==1.1.0 sphinxcontrib-bibtex==1.0.0 recommonmark==0.6.0
```


#### OCI (docker) containers

The OCI image that contains all dependencies including those for building the documentation but not `materiaux` itself
can be obtained via
```
docker pull registry.gitlab.com/materiaux/materiaux/materiaux:ci_img
```
It is the same image that is used for integration tests. Its build recipe is the file [buildah.sh](https://gitlab.com/materiaux/materiaux/-/blob/master/buildah.sh)
(requiring podman/buildah) that is also locally available in the repository root. The image is based on Fedora (version 32).
After pulling the image, start the container with something like (you may need a different port mapping)
```
docker run -ti -p 8888:8888 materiaux:ci_img
```
To install *materiaux* enter the container and run
```
cd ~
git clone https://gitlab.com/materiaux/materiaux.git
cd materiaux
pip3 install -U -e .
```
After that, you may install the Jupyter notebook server with
```
pip3 install notebook
```
and start a jupyter session by running
```
jupyter-notebook --no-browser --allow-root &
```
which will launch the jupyter notebook server and show you the corresponding link.

OCI containers with full-stack materiaux installations (including FEniCS/dolfin and 
materiauXdolfin) are provided under [https://gitlab.com/materiaux/containers/container_registry/](https://gitlab.com/materiaux/containers/container_registry/).
This is the container registry of the "Materiaux Containers" subproject. 
Please note that these are currently not built automatically and thus might be out-of-date. However, 
the build files contained in that repository provide recipies that are likely to work.


### Generate HTML docs

To build the HTML documentation run `make html` in the subdirectory `docs`.
This requires "doxygen" and "sphinx" and a number of sphinx extensions.


### Highly recommended packages and software

 * [matplotlib](https://matplotlib.org) for plotting
 * [jupyter](https://jupyter.org) for working with notebooks
 * [scipy](https://scipy.org) for general scientific computing
 * [ccache](https://ccache.dev/) to speed-up recompilation
 * LaTeX (incl. siunitx) for typesetting of, e.g., plot labels
 
### Plotting in Jupyter notebooks

Plotting with `matplotlib` in plain Jupyter notebooks work out-of-the-box with the `%matplotlib nbagg` special 
directive. 

For Jupyer Lab, it is recommended to use [ipympl](https://github.com/matplotlib/ipympl). For this purpose run
```
pip install ipympl
jupyter labextension install @jupyter-widgets/jupyterlab-manager
jupyter lab build
``` 
and use the `Jupyter` magic `%matplotlib widget` instead of `%matplotlib nbagg`.
Please note that this may require to install `npm` on your system to actually work.

[Disclaimer: Due to the fast changing Jupyter API this information might be outdated at some point.]
 
