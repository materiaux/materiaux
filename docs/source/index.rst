.. materiaux documentation master file


*materiaux* - an extensible framework for the modeling of materials
===================================================================

.. contents::
    :depth: 1
    :local:

Introduction
------------

This project has been created for material modeling in the sense of continuum mechanics.
Mathematically speaking, it deals with local coefficients of partial differential equations.
The motivation for the creation of this package was the frustration with the tedious and
error prone implementation of complex material models. It is this huge effort that causes
accurate and powerful models to remain widely ignored as probably no practitioner
will take the effort of implementation just for the purpose of evaluation.
In this regard, this package is intended to close the gap between material modelers and
their actual audience which are not only other material modelers building more complex models
on top but also other researchers as well as engineers in industry that want to solve a
problem for which the model was developed.


Features and Design
-------------------

This package allows material modelers to develop their models
in a convenient environment and automatically obtain performant C code. In fact, all
evaluations in python directly use the (just-in-time) generated C and C++ code such that
the model does not have to be re-implemented in a native language before it can be used in
numerical simulations.

The general design of materiaux is modular in the sense that the symbolic facilities are separate from the
code generation infrastructure, whereby the latter defines a generic interface that allows for new symbolic constructs
or "languages". The generated code comes along a good deal of human-readable metadata in JSON format. Moreover, the
generated material models can be compiled into dynamically loadable shared libraries (modules).

The two main types of which actual instances are generated -- besides the actual code doing the computations are the
C++ types

 * :cpp:class:`materiaux::Function` which holds the "working code" as well as information about the arguments to that
   code. This is also the central type of objects contained in modules.
 * :cpp:class:`materiaux::FieldData` which provides information about individual arguments.

A :cpp:class:`materiaux::Function` instance can be called with plain C-style arrays or corresponding pointers.
In addition, the set of :cpp:func:`materiaux::call` provides further ways for calling.

Both of :cpp:class:`materiaux::Function` and :cpp:class:`materiaux::FieldData` have corresponding python types and
facilities around them in :py:mod:`materiaux`, :py:mod:`materiaux.cpp` and :py:mod:`materiaux.materiauXcpp`.

Emphasis is put on these two types because materiaux strives to make generated code independent from the framework
that was used for code-generation. This allows, on the one hand, to exchange symbolic frameworks and, on the other hand,
stable exports of generated models as well as their integration into numerical simulation frameworks. At the same time,
just-in-time (JIT) compilation is supported as well. The latter feature is great for developing material models in a
Python environment (see `examples <examples.html>`_).

The default symbolic facilities of materiaux are built upon
`UFL, the Unified Form Language <https://github.com/FEniCS/ufl>`_ which allows compilation with
`FFCX, the next generation FEniCS Form Compiler <https://github.com/FEniCS/ffcx>`_.
UFL provides mathematical operators and symbolic calculus (mainly derivatives) for scalars
and tensors. FFCX is used to generate C code for which materiaux libraries create custom
bindings to C++ and python. What this package adds to UFL/FFCX semantics is

* the focus on *local* functions not bound to any notion of a cell or a finite element,
* symbolic and numerical tools for models that require the computation of internal variables
  such required for dissipative materials but also other models lacking a closed form
  representation,
* straight-forward evaluation of models with only minimal boilerplate code in python,
* addtional constructs such a time-discrete evolution equations and consistent linearizations of models with internal
  variables and, finally,
* the convenient export of all code and metadata into standalone packages.

It is emphasized again that the modular structure of materiaux allows for the implementation of alternative symbolic
"backends". Also, code generation is not strictly required at all. It is, indeed, prefectly possible to wrap handwritten
material routines in a materiaux C++ module.


Project structure
-----------------

1. `materiaux <materiaux.html>`_: The top-level package contains a only few general definitions as well as essential
   facilities for importing and calling compiled models. The default user interface mainly consists of the public
   interfaces of :py:mod:`materiaux.modeling` and :py:mod:`materiaux.codegeneration.jit`.
2. `materiaux.codegeneration <materiaux.codegeneration.html>`_: a modular framework for code generation featuring an implementation based on
   UFL and FFCX that plays together with the "in-house" modeling package :py:mod:`materiaux.modeling` but with the possibility
   for plugins for other symbolic math and CAS frameworks.
   Such extensions are enabled through a fairly abstract interface (:py:class:`~.materiaux.codegeneration.analysis.DataAggregate`)
   that connects symbolic modeling and code generation and is also used by :py:mod:`materiaux.modeling`. Another
   important part of the :py:mod:`~.materiaux.codegeneration` sub-package is :py:mod:`.materiaux.codegeneration.jit`,
   which provides a just-in-time (JIT) compilation front-end to the codegeneration facilities.
3. `materiaux.modeling <materiaux.modeling.html>`_: serves the purpose of the description of material models with a
   symbolic language. Besides that, it also contains helpers for preparing model descriptions for code generation.
   The user interface is represented by the module :py:mod:`~.materiaux.modeling.gsm`, whereby "gsm" stands for
   "generalized standard materials".
4. `materiaux.materiauXcpp <materiaux.materiauXcpp.html>`_: C++ extension exposing parts of ``materiaux.h`` and
   ``materiaux_module.h``. This module has submodules for ``double`` and ``complex`` variants that contain wrapped
   templated types, all factories are however collected in the corresponding "user module" :py:mod:`materiaux.cpp`. The
   latter wraps :py:mod:`materiaux.materiauXcpp` and adds a number of additional utilities. One of the interesting features
   provided is a generic Newton-Raphson solver intended for non-linear evolution equations and a function computing the
   consistent linearization of a model with internal variables.

Extending *materiaux* and interfacing external libraries
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

There are several directions in which materiaux can be extended. First of all, the code generation infrastructure
is rather generic and has a simple public interface. This can be used, for example, to generate interface code for
existing libraries. For details, have a look the the documentation of
`materiaux.codegeneration <materiaux.codegeneration.html>`_. If this is too complicated for your needs, you may as well
consider to write an interface to your favourite library by hand. If you prefer the C++ interface, then the two main
types of interest for this task are :cpp:class:`materiaux::Function` and :cpp:class:`materiaux::FieldData`. Just run some
of the provided examples and have a look at the generated computation and interface code as well as build files.
Note that these two types are the main user types around which the whole Materiaux Project has been created.
Alternative to the C++ interface, a :cpp:class:`materiaux::Function`
(or a :py:class:`materiaux.materiauXcpp.double.Function` respectively)
can be constructed from Python with :py:class:`~materiaux.materiauXcpp.FieldData`,
:py:class:`~materiaux.materiauXcpp.CodeData` (can be empty) and an integer
representing a pointer to a C function. The latter can be obtained, e.g., via the library
`CFFI <https://github.com/cffi/cffi>`_.


Relation to other packages provided by the `Materiaux <https://gitlab.com/materiaux>`_ project
----------------------------------------------------------------------------------------------

materiauXdolfin and materiauXdolfinx
''''''''''''''''''''''''''''''''''''
The packages `materiauXdolfin <https://gitlab.com/materiaux/materiauXdolfin>`_ and
`materiauXdolfinx <https://gitlab.com/materiaux/materiauXdolfinx>`_ provide integration with
`dolfin <https://bitbucket.org/fenics-project/dolfin>`_ and
`dolfinx <https://github.com/FEniCS/dolfinx>`_, both part of the
`FEniCS project <https://fenicsproject.org>`_. These integration bits are work in progress
and will improve over time. Contributions on integration or bindings for other simulations
tools or libraries are welcome. By design, the framework can also be extended towards
alternative symbolic engines and new language bindings with reasonable effort.

dolfincoefficients and dolfinxcoefficients
''''''''''''''''''''''''''''''''''''''''''
materiauXdolfin and materiauXdolfinx depend on the more generic packages
`dolfincoefficients <https://gitlab.com/materiaux/dolfincoefficients>`_ and
`dolfinxcoefficients <https://gitlab.com/materiaux/dolfinxcoefficients>`_ respectively. The latter two provide the actual
handling of external compiled "coefficients" for dolfin/dolfinx. They are standalone packages
since they might be useful on their own also for other projects with a similar scope as
materiaux. Note that since there is currenly no stable version of dolfinx, dolfinxcoefficients is not stable either and
currently also not actively maintained. This will change once a dolfinx will see a stable release.


Materiaux Containers
''''''''''''''''''''
`"Materiaux Containers" <https://gitlab.com/materiaux/containers>`_ is a subproject that provides build
recipies and container images.


Projects with related or similar targets
----------------------------------------

The list below is for sure incomplete. If you know a project of which you think that it should
be mentioned, please let me know.

- `MFront <http://tfel.sourceforge.net/>`_:
  Compared with materiaux, MFront provides more ready to
  use building blocks for certain classes of materials. On the other hand, it does not seem to offer such a powerful
  symbolic language to the user. Its companion `MGIS <https://thelfer.github.io/mgis/web/index.html>`_ exposes MFront
  models in a generic way.
- `ACEGEN <https://www.wolfram.com/products/applications/acegen/>`_: This commercial tool
  features strong symbolic capabilities also for the modeling of materials, but has a much
  wider scope than materiaux, reaching far into code generation for finite elements and numerical
  algorithms in general.
- `NGSolve <https://ngsolve.org>`_, Firedrake <https://www.firedrakeproject.org>`_ and `FEniCS <https://fenicsproject.org>`_:
  All of these three finite element softeare projects currently work towards capabilities provided by materiaux. 
  By contrast to these efforts, materiaux strives to not only serve a particular numerics framework.


.. toctree::
   :maxdepth: 2
   :caption: Usage

   installation
   examples

.. toctree::
   :maxdepth: 1
   :caption: Reference

   materiaux
   materiaux_cpp_header


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
