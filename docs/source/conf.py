# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# ================
# -- Path setup --
# ================

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import subprocess
import pathlib
import shutil
from shlex import quote

sys.path.insert(0, os.path.abspath('../../'))

# =========================
# -- Project information --
# =========================

project = 'materiaux'
copyright = '2020-2021, Matthias Rambausek'
author = 'Matthias Rambausek'

# The full version, including alpha/beta/rc tags
release = None

if release is None:
    import materiaux.version as _version
    release = _version.__version__


# ===========================
# -- General configuration --
# ===========================

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.mathjax',
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
#    'breathe',
    'nbsphinx',
    'sphinxcontrib.bibtex',
    'sphinxcontrib.inkscapeconverter',
    'recommonmark',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['**.ipynb_checkpoints']

# =============================
# -- Options for HTML output --
# =============================

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = 'alabaster'
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# ======================
# -- My own additions --
# ======================

doxygen_output_dir = pathlib.Path.cwd().parent / "build" / "doxygen"
sphinx_output_dir = pathlib.Path.cwd().parent / "build" / "sphinx"
# call api doc gens
subprocess.call("sphinx-apidoc -M -T -o {source!s} {target!s}".format(source=pathlib.Path.cwd(), target=pathlib.Path.cwd().parent.parent / "materiaux"), shell=True, cwd=os.curdir)
subprocess.call("doxygen", shell=True, cwd=os.curdir) # uses doxyfile in present directory
#subprocess.call("breathe-apidoc -f -o cpp-api {!s}".format(doxygen_output_dir / "xml"), shell=True, cwd=os.curdir)
shutil.copytree(doxygen_output_dir / "html", sphinx_output_dir / "html" / "doxygen", dirs_exist_ok=True)


# nbsphinx
# ========

# check nbsphinx's conf.py fore some example setting!

if "CI" not in os.environ.keys():
    print("Variable ${CI} not detected. Thus, execution of notebooks is disabled.")
    nbsphinx_execute = "never"
    
nbsphinx_timeout = 120

# List of arguments to be passed to the kernel that executes the notebooks:
nbsphinx_execute_arguments = [
    "--InlineBackend.figure_formats={'svg', 'pdf'}",
    "--InlineBackend.rc={'figure.dpi': 96}",
]


# breathe
# =======
#breathe_projects = {"materiaux": str(doxygen_output_dir / "xml")}
#breathe_default_project = "materiaux"


# misc
# ====

# Default language for syntax highlighting in reST and Markdown cells:
highlight_language = 'none'

# Don't add .txt suffix to source files:
html_sourcelink_suffix = ''

bibtex_bibfiles = []
