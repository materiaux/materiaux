# *materiaux* - an extensible framework for the modeling of materials

This project has been created for material modeling in the sense of continuum mechanics. 
Mathematically speaking, it deals with local coefficients of partial differential equations.
The motivation for the creation of this package was the frustration with the tedious and 
error prone implementation of complex material models. It is this huge effort that causes 
accurate and powerful models to remain widely ignored as probably no practitioner 
will take the effort of implementation just for the purpose of evaluation. 
In this regard, this package is intended to close the gap between material modelers and
their actual audience which are not only other material modelers building more complex models 
on top but also other researchers as well as engineers in industry that want to solve a 
problem for which the model was developed.


## Features and Design

This package allows material modelers to develop their models
in a convenient environment and automatically obtain performant C code. In fact, all 
evaluations in python directly use the (just-in-time) generated C and C++ code such that 
the model does not have to be re-implemented in a native language before it can be used in 
numerical simulations. 

The symbolic and code generation facilities of *materiaux* are built upon 
[UFL, the Unified Form Language](https://github.com/FEniCS/ufl) and
[FFCX, the Next generation FEniCS Form Compiler](https://github.com/FEniCS/ffcx).
UFL provides mathematical operators and symbolic calculus (mainly derivatives) for scalars
and tensors. FFCX is used to generate C code for which *materiaux* libraries create custom 
bindings to C++ and python. What this package adds to UFL/FFCX semantics is
 * the focus on *local* functions not bound to any notion of a cell or a finite element,
 * symbolic and numerical tools for models that require the computation of internal variables
 such required for dissipative materials but also other models lacking a closed form 
 representation and
 * straight-forward evaluation of models with only minimal boilerplate code in python
 * the convenient export of all code and metadata into standalone packages.
 

## Build and installation

Instructions concerning native installs as well as container images can be found [here](docs/source/installation.md).
 

## Documentation

In general, the documentation of the high-level user interface is fairly complete. 
When something is undocumented, this is a strong indication that it is not intended for
general users.

While the `Python` interface is fairly complete, it is pointed out that many symbolic
operations are borrowed from `UFL` such that a certain knowledge of the latter is advantageous.

The library furthermore comes along with some Jupyter notebooks in the [examples](./examples) directory.

The documentation for the master branch is built automatically and hosted on [https://materiaux.gitlab.io/materiaux/](https://materiaux.gitlab.io/materiaux/).


## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.


## Acknowledgements

The initial development (2018-2020) of the Materiaux suite happened in the group of 
Konstantinos Danas at the Solid Mechanics Lab (LMS) at École Polytechnique 
in Palaiseau/France under the ERC Starting Grant MAGNETO.

From April 2020 on, work on the library continued in the group of Joachim Schöberl at 
the Institute of Analysis and Scientific Computing at the TU Vienna and has been 
supported by the Austrian Science Fund (FWF) project F65 --
[Taming Complexity in Partial Differential Systems](https://www.univie.ac.at/sfb65/).

