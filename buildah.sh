#!/usr/bin/env bash

set -o errexit

if [ -z "${BASE_IMG}" ]
then
    export BASE_IMG=fedora:34
fi

if [ -z "${TARGET_IMG}" ]
then
    export TARGET_IMG="ci:main"
fi


container=$(buildah from ${BASE_IMG})
echo $container > current_container
buildah config --label maintainer="Matthias Rambausek <matthias.rambausek@gmail.com>" $container

buildah run $container -- /bin/bash -c '
    dnf -y update
    dnf -y install \
        bison \
        make \
        ninja-build \
        cmake \
        which \
        diffutils \
        flex \
        gcc-c++ \
        git \
        libzip \
        lbzip2 \
        hostname \
        graphviz \
        doxygen \
        boost-devel \
        nano \
        pkgconfig \
        redhat-rpm-config \
        wget \
        ccache \
        google-benchmark-devel \
        openblas-devel \
        libpng-devel \
        eigen3-devel \
        python3-devel \
        python3-pip \
        python3-scipy \
        python3-matplotlib \
        python3-cffi \
        python3-pytest \
        python3-pytest-xdist \
        python3-nbconvert \
        python3-ipykernel \
        freetype-devel \
        texlive-updmap-* \
        texlive-cm-super \
        texlive-type1cm \
        texlive-stix \
        texlive-siunitx \
        doxygen \
        pandoc
    dnf clean all
'

# note that ipython and sphinx packages are needed from pip to enable proper rendering of ipython/notebook code in examples
buildah run $container -- /bin/bash -c '
    pip3 install -U ipython pybind11-global Sphinx==3.3.1 sphinx_rtd_theme==0.5.0 breathe==4.24.1 nbsphinx==0.8.0 sphinxcontrib-svg2pdfconverter==1.1.0 sphinxcontrib-bibtex==1.0.0 recommonmark==0.6.0
    pip3 install -U git+https://github.com/mrambausek/fiat.git
    pip3 install -U git+https://github.com/mrambausek/ufl.git
    pip3 install -U git+https://github.com/mrambausek/ffcx.git
'

buildah commit $container materiaux-${TARGET_IMG}

# buildah push materiaux-${TARGET_IMG} registry.gitlab.com/materiaux/materiaux/${TARGET_IMG}
