"""
Test on real world material models of reasonable complexity to cover a number of features.

The models at hand are the viscosity model of Kumar & Lopez-Pamies (2016) and a model along the lines of
Le Tallec et al. (1993). For the latter we also refer to Rambausek, Mukherjee & Danas (in progress, 2021).
"""

import typing
import pathlib
import numpy as np
from materiaux.modeling import gsm
from materiaux.modeling.gsm import Expr, Operator
import materiaux.cpp
from materiaux.cpp import create_function_result, create_function_coefficients, create_function_constants
from materiaux.codegeneration import jit
import pytest


double = materiaux.cpp.DOUBLE


diag = tuple(([0, 1, 2], [0, 1, 2]))
fdiag = tuple(([0, 4, 8],))
sdiag = tuple(([0, 3, 5],))


def true_stress(_S, F):
    p = _S[0, 0] * F[0, 0]
    _Sc = _S.copy()
    _Sc[diag] -= p / F[diag]
    return _Sc


def set_F(F, stretch: float):
    F[0, 0] = 1 / np.sqrt(stretch)
    F[1, 1] = 1 / np.sqrt(stretch)
    F[2, 2] = stretch


def set_F_simple_shear(F, shear: float):
    F[0, 0] = 1.0
    F[0, 1] = 1 - shear
    F[1, 1] = 1.0
    F[2, 2] = 1.0


def b(F: Expr) -> Operator:
    return F * gsm.transpose(F)


def C(F: Expr) -> Operator:
    return gsm.transpose(F) * F


def I_1(C_or_b: Expr) -> Operator:
    return gsm.tr(C_or_b)


def I_2(C_or_b: Expr) -> Operator:
    C = C_or_b
    return 1 / 2 * (gsm.tr(C) ** 2 - gsm.tr(C * C))


def I_3(C_or_b: Expr) -> Operator:
    C = C_or_b
    return gsm.det(C)


def deviator(T: Expr) -> Operator:
    return T - 1 / 3 * gsm.tr(T) * gsm.Identity(3)


def J_2(T: Expr) -> Operator:
    i, j = gsm.indices(2)
    return 1 / 2 * T[i, j] * T[i, j]


def J_2_dev(T: Expr) -> Operator:
    return J_2(deviator(T))


def be(F, Cv):
    return F * gsm.inv(Cv) * gsm.transpose(F)


def be_inv(F, Cv):
    return gsm.inv(F) * Cv * gsm.transpose(gsm.inv(F))


def psi_full_integration(I_1: Expr, I_3: Expr, mu_list: typing.Iterable[gsm.Parameter],
                         alpha_list: typing.Iterable[gsm.Parameter]):
    return sum([
        3 ** (1 - alpha) / (2 * alpha) * mu * ((I_1 - gsm.ln(I_3)) ** alpha - 3 ** alpha) if alpha != 1 else
        1 / 2 * mu * ((I_1 - gsm.ln(I_3)) - 3)
        for mu, alpha in zip(mu_list, alpha_list)
    ])


def psi_full_integration_of_C_or_b(C_or_b: Expr, mu_list: typing.Iterable[gsm.Parameter],
                                   alpha_list: typing.Iterable[gsm.Parameter]):
    return psi_full_integration(I_1(C_or_b), I_3(C_or_b), mu_list, alpha_list)


def psi_reduced_integration(I_3: Expr, mu_prime: gsm.Parameter):
    return mu_prime * (gsm.sqrt(I_3) - 1) ** 2


def psi_reduced_integration_of_C_or_b(C_or_b: Expr, mu_prime: gsm.Parameter):
    return psi_reduced_integration(I_3(C_or_b), mu_prime)


def direct_evolution_eqs(F: gsm.State, F_t: typing.Union[gsm.State, gsm.HistoryState], Cv_dot: gsm.TimeRate,
                         Cv: gsm.InternalState,
                         Cv_t: typing.Union[gsm.InternalState, gsm.HistoryState], Cv_n: gsm.HistoryState,
                         m_list: typing.Iterable[gsm.Parameter], a_list: typing.Iterable[gsm.Parameter],
                         m_prime: gsm.Parameter,
                         K_1: gsm.Parameter, K_2: gsm.Parameter, eta_0: gsm.Parameter, eta_infty: gsm.Parameter,
                         beta_1: gsm.Parameter, beta_2: gsm.Parameter):
    _C = C(F)
    _C_n = C(F_t)
    _b = b(F)
    Iv_1_n = I_1(Cv_n)
    _be = be(F, Cv)
    _be_inv = be_inv(F, Cv)
    Ie_1 = I_1(_be)
    Ie_1_n = gsm.replace(Ie_1, {Cv: Cv_n, F: F_t})
    Ie_1 = gsm.variable(Ie_1)
    Ie_3 = I_3(_be)
    Ie_3 = gsm.variable(Ie_3)
    psi_neq = psi_full_integration(Ie_1, Ie_3, m_list, a_list) + psi_reduced_integration(Ie_3, m_prime)
    dpsi_dIe1 = gsm.diff(psi_neq, Ie_1)
    Cv_n_inv = gsm.inv(Cv_n)
    Ie_2_n = 1 / 2 * (Ie_1_n ** 2 - gsm.inner(Cv_n_inv * _C_n, _C_n * Cv_n_inv))
    _A_n = 2 * gsm.replace(dpsi_dIe1, {Cv: Cv_n, F: F_t})
    J_2_neq = _A_n ** 2 * (Ie_1_n ** 2 / 3 - Ie_2_n)
    eta_k = eta_infty + (eta_0 - eta_infty + K_1 * (Iv_1_n ** beta_1 - 3 ** beta_1)) / (1 + (K_2 * J_2_neq) ** beta_2)
    vCv = gsm.variable(Cv)
    psi_neq_of_be = psi_full_integration_of_C_or_b(be(F, vCv), m_list, a_list) + \
                    psi_reduced_integration_of_C_or_b(be(F, vCv), m_prime)
    dpsi_dCv = gsm.diff(psi_neq_of_be, vCv)
    R = 2 * Cv * gsm.replace(dpsi_dCv, {Cv: Cv_t}) * Cv
    return gsm.Function("direct_evolution_equations",
                        gsm.equations({Cv: Cv_dot.discretization() + 1 / eta_k * R}))


# external states
F = gsm.State("F", (3, 3))

# internal states
Cv = gsm.InternalState("Cv", (3, 3), symmetry=True)

# history states
F_n = gsm.HistoryState(F)
Cv_n = gsm.HistoryState(Cv)

# time
t = gsm.Time("t")
t_n = gsm.Time("t_n")

# time rates
Cv_dot = gsm.TimeRate(Cv, (Cv - Cv_n) / (t - t_n))

# elastic parameters
mu_1 = gsm.Parameter("mu_1")
alpha_1 = gsm.Parameter("alpha_1")
mu_2 = gsm.Parameter("mu_2")
alpha_2 = gsm.Parameter("alpha_2")
mu_prime = gsm.Parameter("mu_prime")

# viscosity model parameters
m_1 = gsm.Parameter("m_1")
a_1 = gsm.Parameter("a_1")
m_2 = gsm.Parameter("m_2")
a_2 = gsm.Parameter("a_2")
m_prime = gsm.Parameter("m_prime")
K_1 = gsm.Parameter("K_1")
K_2 = gsm.Parameter("K_2")
eta_0 = gsm.Parameter("eta_0")
eta_infty = gsm.Parameter("eta_infty")
beta_1 = gsm.Parameter("beta_1")
beta_2 = gsm.Parameter("beta_2")

response = gsm.Function("energy", psi_full_integration_of_C_or_b(C(F), [mu_1, mu_2], [alpha_1, alpha_2])
                        + psi_full_integration_of_C_or_b(be(F, Cv), [m_1, m_2], [a_1, a_2]))
eqs = direct_evolution_eqs(F, F_n, Cv_dot, Cv, Cv, Cv_n, [m_1, m_2], [a_1, a_2], m_prime, K_1, K_2, eta_0,
                           eta_infty, beta_1, beta_2)
viscoelasticity_klp = gsm.GSMBase(response, eqs)

# elastic parameters
mu = gsm.Parameter("mu")
alpha = 1
mu_prime = gsm.Parameter("mu_prime")

# viscosity model parameters
m = gsm.Parameter("m")
a = 1
eta = gsm.Parameter("eta")

Cv_t_inv = gsm.inv(Cv)

viscoelasticity_simple = gsm.GeneralizedStandardMaterial(
    gsm.Function("energy",
                 psi_full_integration_of_C_or_b(C(F), [mu], [alpha])
                 + psi_full_integration_of_C_or_b(be(F, Cv), [m], [a])
                 ),
    gsm.Function("dissipation_potential", eta / 4 * (gsm.tr(Cv_dot * Cv_t_inv * Cv_dot * Cv_t_inv))),
    internal_constraints=[I_3(C(F)) - I_3(Cv)]
)


def mk_integrator(t, t_n):
    return lambda f: f * (t - t_n)


Cv_t_inv = gsm.inv(Cv_n)

viscoelasticity_simple_incr = gsm.IncrementalMaterialPotential(
    gsm.Function("energy",
                 psi_full_integration_of_C_or_b(C(F), [mu], [alpha])
                 + psi_full_integration_of_C_or_b(be(F, Cv), [m], [a])
                 ),
    gsm.Function("dissipation_potential", eta / 4 * (gsm.tr(Cv_dot * Cv_t_inv * Cv_dot * Cv_t_inv))),
    mk_integrator(t, t_n),
    internal_constraints=[I_3(C(F)) - I_3(Cv)]
)

cmake_options = {"CMAKE_BUILD_TYPE": "Debug"}
wdir = pathlib.Path("generated") / "visco"

compiled_viscoelasticity_klp = jit.jit(
    gsm.create_module_data(viscoelasticity_klp), override=False, cmake_options=cmake_options,
    working_directory=wdir
)

compiled_viscoelasticity_simple = jit.jit(
    gsm.create_module_data(viscoelasticity_simple), override=False,
    cmake_options=cmake_options,
    working_directory=wdir
)

compiled_viscoelasticity_simple_incr = jit.jit(
    gsm.create_module_data(viscoelasticity_simple_incr), override=False,
    cmake_options=cmake_options,
    working_directory=wdir
)

all_data = sum([model.material_response.stored_energy.coefficient_data +
                model.material_response.stored_energy.constant_data +
                model.evolution_equations.eqs.coefficient_data +
                model.evolution_equations.eqs.coefficient_data
                for model in (compiled_viscoelasticity_klp,
                              compiled_viscoelasticity_simple)], []) + \
           compiled_viscoelasticity_simple_incr.incremental_potential.coefficient_data + \
           compiled_viscoelasticity_simple_incr.incremental_potential.constant_data

field_arrays = materiaux.cpp.create_field_arrays(double, all_data)

field_arrays["F"][diag] = 1
field_arrays["Cv"][sdiag] = 1
field_arrays["pv"] = np.array([0])
field_arrays["F_n"] = field_arrays["F"].copy()
field_arrays["Cv_n"] = field_arrays["Cv"].copy()
field_arrays["internal_multipliers"][:] = 0

field_arrays["t"][:] = 0
field_arrays["t_n"][:] = 0
field_arrays["mu_1"][:] = 1.08
field_arrays["alpha_1"][:] = 0.26
field_arrays["mu_2"][:] = 0.017
field_arrays["alpha_2"][:] = 7.68
field_arrays["m_1"][:] = 1.57
field_arrays["a_1"][:] = -10
field_arrays["m_2"][:] = 0.59
field_arrays["a_2"][:] = 7.53
field_arrays["m_prime"][:] = 500 * field_arrays["m_1"][:]
field_arrays["K_1"][:] = 442
field_arrays["K_2"][:] = 1289.49
field_arrays["eta_0"][:] = 2.11
field_arrays["eta_infty"][:] = 0.1
field_arrays["beta_1"][:] = 3,
field_arrays["beta_2"][:] = 1.929

# # param set 2
# units: base is [F] = MN, [L] = m
_MN = _MPa = 1
_kPa = _kPas = 1e-3
_Pa = _Pas = 1e-6

field_arrays["mu"][:] = 5 * _kPa
field_arrays["m"][:] = 20 * _kPa
field_arrays["eta"][:] = 20 * _Pas

expected = {
    compiled_viscoelasticity_klp: {
        "F": np.array([1.05409255, 1.05409255, 0.9]),
        "Cv": np.array([1.10041635, 1.10041635, 0.82582131]),
        "S": np.array([0.13477392, 0.13477392, -0.27601221]),
        "true_stress": np.array([-0.4338613])
    },
    compiled_viscoelasticity_simple: {
        "F": np.array([1.05409255, 1.05409255, 0.9]),
        "Cv": np.array([1.10987792, 1.10987792, 0.81180099]),
        "S": np.array([0.00054813, 0.00054813, -0.00110486]),
        "true_stress": np.array([-0.00174683])
    },
    compiled_viscoelasticity_simple_incr: {
        "F": np.array([1.05409255, 1.05409255, 0.9]),
        "Cv": np.array([1.10988872, 1.10988872, 0.81178519]),
        "S": np.array([0.0005482, 0.0005482, -0.00110413]),
        "true_stress": np.array([-0.00174618])
    }
}


@pytest.mark.parametrize("model,stretch_rate,set_F", ((compiled_viscoelasticity_klp, 0.00023, set_F),
                                                      (compiled_viscoelasticity_simple, 1.0, set_F),
                                                      (compiled_viscoelasticity_simple_incr, 1.0, set_F)))
def test_gsm_forms(model, stretch_rate, set_F):
    if hasattr(model, "evolution_equations"):
        duals = model.material_response.duals
        evolution = materiaux.cpp.create_evolution(model.evolution_equations.eqs,
                                                   model.evolution_equations.d_int_eqs,
                                                   materiaux.cpp.create_newton_raphson(double),
                                                   True)
    else:
        duals = model.d_ext_incremental_potential
        evolution = materiaux.cpp.create_evolution(model.d_int_incremental_potential,
                                                   model.d_int_d_int_incremental_potential,
                                                   materiaux.cpp.create_newton_raphson(double),
                                                   True)

    target_stretch = 0.9
    n_steps = 25
    stretch = 1.0
    direction = 1 if target_stretch > 1 else -1
    dt = np.abs(target_stretch - 1) / stretch_rate / n_steps

    field_arrays["t"][:] = 0
    field_arrays["t_n"][0] = 0

    field_arrays["F_n"][:] = np.array([1, 0, 0, 0, 1, 0, 0, 0, 1], dtype=np.float_).reshape((3, 3))
    field_arrays["Cv"][:] = np.array([1, 0, 0, 1, 0, 1], dtype=np.float_)
    field_arrays["Cv_n"][:] = np.array([1, 0, 0, 1, 0, 1], dtype=np.float_)

    field_arrays["newton_tol"] = 1e-6
    field_arrays["newton_max_iter"] = 10

    _pv = None
    coeff_names = [coeff.name for coeff in evolution.coefficient_data]
    if "pv" in coeff_names:
        _pv = "pv"
    elif "internal_multipliers" in coeff_names:
        _pv = "internal_multipliers"
    if _pv is not None:
        field_arrays[_pv][:] = 0

    res_evolution = create_function_result(double, evolution.result_data).flatten()
    res_duals = create_function_result(double, duals.result_data).flatten()

    F_data = np.zeros((3, n_steps * 2))
    Cv_data = np.zeros((3, n_steps * 2))
    S_data = np.zeros((3, n_steps * 2))
    true_stress_data = np.zeros((1, n_steps * 2))

    for step in range(n_steps * 2):
        if step >= n_steps and stretch_rate != 0:
            stretch_rate = 0
        stretch += direction * stretch_rate * dt
        set_F(field_arrays["F"], stretch)
        field_arrays["t"][0] += dt
        coeffs_evolution = create_function_coefficients(evolution, coefficients=field_arrays)
        constants_evolution = create_function_constants(evolution, constants=field_arrays)
        res_evolution[:] = 0
        try:
            evolution(res_evolution, coeffs_evolution, constants_evolution)
        except Exception as e:
            print(e)
            break

        field_arrays["Cv"][:] = res_evolution[:6]
        if _pv is not None:
            field_arrays[_pv][:] = res_evolution[6]

        res_duals[:] = 0
        coeffs_duals = create_function_coefficients(duals, coefficients=field_arrays)
        constants_duals = create_function_constants(duals, constants=field_arrays)
        duals(res_duals, coeffs_duals, constants_duals)

        field_arrays["F_n"][:] = field_arrays["F"]
        field_arrays["Cv_n"][:] = field_arrays["Cv"]
        field_arrays["t_n"][0] = field_arrays["t"][0]

        F_data[:, step] = field_arrays["F"][diag]
        Cv_data[:, step] = field_arrays["Cv"][sdiag]
        S_data[:, step] = res_duals[fdiag]
        true_stress_data[:, step] = true_stress(res_duals.reshape((3, 3)), field_arrays["F"])[2, 2]

    if __name__ == "__main__":
        return F_data, Cv_data, S_data, true_stress_data
    assert np.allclose(F_data[:, n_steps - 1], expected[model]["F"], atol=1e-8, rtol=1e-8)
    assert np.allclose(Cv_data[:, n_steps - 1], expected[model]["Cv"], atol=1e-8, rtol=1e-8)
    assert np.allclose(S_data[:, n_steps - 1], expected[model]["S"], atol=1e-8, rtol=1e-8)
    assert np.allclose(true_stress_data[:, n_steps - 1], expected[model]["true_stress"], atol=1e-8, rtol=1e-8)


if __name__ == "__main__":
    data_klp = test_gsm_forms(compiled_viscoelasticity_klp, 0.00023, set_F)
    data_simple = test_gsm_forms(compiled_viscoelasticity_simple, 1.0, set_F)
    data_simple_incr = test_gsm_forms(compiled_viscoelasticity_simple_incr, 1.0, set_F)
