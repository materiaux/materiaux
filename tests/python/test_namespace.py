from materiaux.namespace import Namespace


def test_namespace():
    toplevel = Namespace("toplevel")
    assert toplevel.name == "toplevel"
    assert toplevel.is_toplevel()

    toplevel.set_item("level", 0)
    assert toplevel.level == 0

    toplevel.set_item("sub::level", 1)
    assert isinstance(toplevel.sub, Namespace)
    assert toplevel.sub.level == 1

    try:
        toplevel.set_item("sub::level", 1, on_exist=Namespace.THROW)
        raise RuntimeError("This should not be reached")
    except AttributeError:
        pass

    toplevel.set_item("sub::level", 2, on_exist=Namespace.NOTHING)
    assert toplevel.sub.level == 1

    toplevel.set_item("sub::level", 2, on_exist=Namespace.OVERRIDE)
    assert toplevel.sub.level == 2

    assert toplevel.get_item("sub::level") == 2

    assert toplevel.contents(levels=0) == {"sub", "level"}
    assert toplevel.contents(levels=1) == {"sub", "level", "sub::level"}
    assert toplevel.contents(levels=-1) == {"sub", "level", "sub::level"}

    toplevel.sub.set_item("text", "a")
    assert toplevel.contents(levels=-1) == {"sub::text", "sub::level", "level", "sub"}
    assert toplevel.sub.contents() == {"text", "level"}

    assert toplevel.scope("sub::level") == "toplevel::sub"

    assert toplevel.get_item("sub").scope() == "toplevel::sub"

    assert toplevel.get_item("sub").scope("level") == "toplevel::sub"

    toplevel2 = Namespace("toplevel::sub", level=1, init_dict={"test": 0})
    toplevel3 = Namespace("toplevel", level=1, init_dict={"sub::test": 0})
    assert toplevel2.sub.level == toplevel3.level
    assert toplevel2.get_item("sub::test") == toplevel3.get_item("sub::test")
