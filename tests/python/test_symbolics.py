"""
Very basic tests on extensions of UFL
"""

from materiaux.modeling.core import create_mixed, Coefficient, Constant
from materiaux.modeling.gsm import Function, derivative, derivatives, split, dot, equations

u = Coefficient("u", description="u")
gu = Coefficient("gu", shape=(3,), description="gradient of u")
k = Constant("k", description="first param")
kk = Constant("kk", shape=(3, 3))
ga = Coefficient("ga", shape=(3,), description="gradient of a")

kkk = create_mixed("mixed_param", [k, kk], description="{k, kk}")
gugu = create_mixed("mixed_coeff", [gu, gu], description="{gu, gu}")

kks = Coefficient("kks", shape=(3, 3), symmetry=True)


def test_coeffs():
    assert gu.ufl_shape == (3,)
    assert gu.field_data().name == "gu"
    assert gu.field_data().size == 3
    assert gu.field_data().shape == [3, ]
    assert gu.field_data().type == Coefficient.__name__

    assert k.ufl_shape == ()
    assert k.field_data().name == "k"
    assert k.field_data().size == 1
    assert k.field_data().shape == []
    assert k.field_data().type == Constant.__name__

    assert kk.ufl_shape == (3, 3)
    assert kk.field_data().name == "kk"
    assert kk.field_data().size == 9
    assert kk.field_data().shape == [3, 3]
    assert kk.field_data().type == Constant.__name__

    assert kkk.ufl_shape == (10,)
    assert kkk.field_data().name == "mixed_param"
    assert kkk.field_data().size == 10
    assert kkk.field_data().shape == [10, ]
    assert kkk.field_data().type == Constant.__name__

    _gu1, _gu2 = split(gugu)
    assert _gu1.ufl_shape == gu.ufl_shape
    assert _gu2.ufl_shape == gu.ufl_shape


def test_symmetry():
    assert list([[(list(kk), list(vv)) for kk, vv in symm.items()] for symm in
                 (kks.ufl_element().symmetry(),)]) == kks.field_data().symmetry


def test_merge():
    psi = Function("psi", k / 2 * dot(gu, dot(kk, gu)))
    d_psi = derivative(psi, gu)
    assert equations({gu: d_psi, u: u})


def test_integrand():
    psi = Function("psi", k / 2 * dot(gu, dot(kk, gu)))
    d_psi = derivative(psi, gu)
    Dd_psi = derivative(d_psi, gu)

    for fn in (psi, d_psi, Dd_psi):
        assert fn.coefficients().index(gu) == 0
        assert fn.constants().index(k) == 0
        assert fn.constants().index(kk) == 1

    assert psi.name() == "psi"
    assert d_psi.name() == "d0_psi"
    assert Dd_psi.name() == "d0_d0_psi"


def test_integrand_two_field():
    psi = Function("psi", k / 2 * dot(gu, dot(kk, gu)) + dot(ga, ga))
    _derivatives = derivatives(psi, max_degree=3)

    assert len(_derivatives) == 15
