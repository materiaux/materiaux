"""
Test generation of modules and interface header. Does not check the correctness of the generated files. This is handled
in a second step by the cpp tests which rely on the modules generated from this set of tests.
"""

import pathlib
import os
import json
import typing
from materiaux.modeling.core import Function, derivative
from materiaux.modeling.gsm import State, Parameter, dot, tr, det, inv
import materiaux.codegeneration.module_generation as module_generation
from materiaux import Namespace
import pytest

interface_header_fmt = "{:s}.h"

_dir = pathlib.Path(pathlib.Path.cwd())
_wdir = pathlib.Path(_dir).parent
_generated = _wdir / "modules"


def basename(scope: typing.List[str], expression_name):
    return "__".join(scope) + "_" + expression_name


source_suffix = ".c"


def test_module_generation():
    os.chdir(_dir)
    module_name = "gsm__test"
    os.system("rm -rf {:s}".format(str(pathlib.Path(_generated, module_name))))

    gu = State("gu", shape=(3,), description="gradient of u")
    gu2 = State("gu2", shape=(3,), description="gradient of u2")
    gus = State("gus", shape=(3, 3), description="symmetric gradient of u", symmetry=True)

    k = Parameter("k", description="first param")
    kk = Parameter("kk", shape=(3, 3))
    kk3 = Parameter("kk3", shape=(3, 3, 3))

    psi = Function("psi", k / 2 * dot(gu, dot(kk, gu)))
    d_psi = derivative(psi, gu)
    Dd_psi = derivative(d_psi, gu)

    psis = Function("psis", k / 2 * (tr(gus) + det(gus)))
    d_psis = derivative(psis, gus)
    Dd_psis = derivative(d_psis, gus)

    psis2 = Function("psis", k / 2 * (tr(gus) + det(gus) + det(gus * kk) + dot(gu, dot(inv(dot(kk3, gu2)), gu))**3))
    d_psis2 = derivative(psis, gus)
    Dd_psis2 = derivative(d_psis, gus)

    integrands_psi = [psi, d_psi, Dd_psi]
    integrands_psis = [psis, d_psis, Dd_psis]
    integrands_psis2 = [psis2, d_psis2, Dd_psis2]

    omega = Function("psi", k ** 2 / 4 * dot(gu, dot(kk, gu)))
    d_omega = derivative(omega, gu)
    Dd_omega = derivative(d_omega, gu)

    integrands_omega = [omega, d_omega, Dd_omega]

    kk2 = Parameter("kk", shape=(3, 3))
    gu2 = State("gu", shape=(3,), description="gradient of u")
    k2 = Parameter("k", description="first param (now not really)")

    psi2 = Function("psi2", k2 / 2 * dot(gu2, dot(kk2, gu2)))
    d_psi2 = derivative(psi2, gu2)
    Dd_psi2 = derivative(d_psi2, gu2)

    integrands_psi2 = [psi2, d_psi2, Dd_psi2]

    gu32 = State("gu2", shape=(3,), description="another gradient of u")
    gu33 = State("gu3", shape=(3,), description="one more gradient of u")
    psi3 = Function("psi3", k / 2 * dot(gu33 + gu, dot(kk2, gu33 + gu)), additional_coefficients=(gu, gu32, gu33))

    # # handle missing base objects gracefully (just do not create the family)!
    module_name = "gsm__test_malformed"
    os.system("rm -rf {:s}".format(str(pathlib.Path(_generated, module_name))))
    integrands = Namespace(module_name,
                           **{"psi::{:s}".format(exp.name()): exp for exp in integrands_psi},
                           **{"omega::{:s}".format(exp.name()): exp for exp in [omega, Dd_omega]})
    module_generation.generate_module(integrands, override=True, working_directory=_wdir, cmake_lib=True)
    assert pathlib.Path(_generated, module_name).exists()
    with open(pathlib.Path(_generated, module_name, "materiaux.json"), "r") as mdf:
        md = json.load(mdf)
        for item_name in integrands.contents():
            integrand = integrands.get_item(item_name)
            if isinstance(integrand, Namespace):
                continue
            aggregates = md["data_aggregates"][integrands.scope(item_name)]
            aggregate = list(filter(lambda item: item["expression_name"] == integrand.name(), aggregates))[0]
            for fname in list(aggregate["header"].keys()) + list(aggregate["sources"].keys()):
                assert pathlib.Path(_generated, module_name, fname).is_file()
    assert pathlib.Path(_generated, module_name, interface_header_fmt.format(module_name)).is_file()

    # nested integrands
    module_name = "gsm__test"
    os.system("rm -rf {:s}".format(str(pathlib.Path(_generated, module_name))))
    integrands = Namespace(module_name,
                           **{"psi::{:s}".format(exp.name()): exp for exp in integrands_psi},
                           **{"psi2::{:s}".format(exp.name()): exp for exp in integrands_psi2},
                           **{"psi3::{:s}".format(exp.name()): exp for exp in [psi3]},
                           **{"omega::{:s}".format(exp.name()): exp for exp in integrands_omega})
    module_generation.generate_module(integrands, override=True, working_directory=_wdir, cmake_lib=True)
    assert pathlib.Path(_generated, module_name).exists()
    with open(pathlib.Path(_generated, module_name, "materiaux.json"), "r") as mdf:
        md = json.load(mdf)
        for item_name in integrands.contents():
            integrand = integrands.get_item(item_name)
            if isinstance(integrand, Namespace):
                continue
            aggregates = md["data_aggregates"][integrands.scope(item_name)]
            aggregate = list(filter(lambda item: item["expression_name"] == integrand.name(), aggregates))[0]
            for fname in list(aggregate["header"].keys()) + list(aggregate["sources"].keys()):
                assert pathlib.Path(_generated, module_name, fname).is_file()
    assert pathlib.Path(_generated, module_name, interface_header_fmt.format(module_name)).is_file()

    # non-nested integrands
    module_name = "gsm__test_psi"
    os.system("rm -rf {:s}".format(str(pathlib.Path(_generated, module_name))))
    integrands = Namespace(module_name,
                           **{"{:s}".format(exp.name()): exp for exp in integrands_psi})
    module_generation.generate_module(integrands, override=True, working_directory=_wdir, cmake_lib=True)
    assert pathlib.Path(_generated, module_name).exists()
    with open(pathlib.Path(_generated, module_name, "materiaux.json"), "r") as mdf:
        md = json.load(mdf)
        for item_name in integrands.contents():
            integrand = integrands.get_item(item_name)
            if isinstance(integrand, Namespace):
                continue
            aggregates = md["data_aggregates"][integrands.scope(item_name)]
            aggregate = list(filter(lambda item: item["expression_name"] == integrand.name(), aggregates))[0]
            for fname in list(aggregate["header"].keys()) + list(aggregate["sources"].keys()):
                assert pathlib.Path(_generated, module_name, fname).is_file()
    assert pathlib.Path(_generated, module_name, interface_header_fmt.format(module_name)).is_file()

    # non-nested integrands (symmetry)
    module_name = "gsm__test_psis"
    os.system("rm -rf {:s}".format(str(pathlib.Path(_generated, module_name))))
    integrands = Namespace(module_name,
                           **{"{:s}".format(exp.name()): exp for exp in integrands_psis})
    module_generation.generate_module(integrands, override=True, working_directory=_wdir, cmake_lib=True)
    assert pathlib.Path(_generated, module_name).exists()
    with open(pathlib.Path(_generated, module_name, "materiaux.json"), "r") as mdf:
        md = json.load(mdf)
        for item_name in integrands.contents():
            integrand = integrands.get_item(item_name)
            if isinstance(integrand, Namespace):
                continue
            aggregates = md["data_aggregates"][integrands.scope(item_name)]
            aggregate = list(filter(lambda item: item["expression_name"] == integrand.name(), aggregates))[0]
            for fname in list(aggregate["header"].keys()) + list(aggregate["sources"].keys()):
                assert pathlib.Path(_generated, module_name, fname).is_file()
    assert pathlib.Path(_generated, module_name, interface_header_fmt.format(module_name)).is_file()


    # non-nested integrands (symmetry); more complicated for some performance testing
    module_name = "gsm__test_psis2"
    os.system("rm -rf {:s}".format(str(pathlib.Path(_generated, module_name))))
    integrands = Namespace(module_name,
                           **{"{:s}".format(exp.name()): exp for exp in integrands_psis2})
    module_generation.generate_module(integrands, override=True, working_directory=_wdir, cmake_lib=True)
    assert pathlib.Path(_generated, module_name).exists()
    with open(pathlib.Path(_generated, module_name, "materiaux.json"), "r") as mdf:
        md = json.load(mdf)
        for item_name in integrands.contents():
            integrand = integrands.get_item(item_name)
            if isinstance(integrand, Namespace):
                continue
            aggregates = md["data_aggregates"][integrands.scope(item_name)]
            aggregate = list(filter(lambda item: item["expression_name"] == integrand.name(), aggregates))[0]
            for fname in list(aggregate["header"].keys()) + list(aggregate["sources"].keys()):
                assert pathlib.Path(_generated, module_name, fname).is_file()
    assert pathlib.Path(_generated, module_name, interface_header_fmt.format(module_name)).is_file()
