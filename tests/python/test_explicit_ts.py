import typing
import pathlib

import materiaux
import numpy as np
import materiaux.codegeneration.jit as jit
import materiaux.cpp as cpp
import materiaux.modeling.gsm as gsm
import pytest

wdir = pathlib.Path("generated") / "explicit_TS"
jit.set_working_directory(wdir)
override = False
double = cpp.DOUBLE

x = gsm.State("x")
x_n = gsm.HistoryState(x)

t = gsm.Time("t")
t_n = gsm.HistoryTime(t)

Delta_t = gsm.TimeIncrement()


@pytest.fixture
def jitted():
    return materiaux.Namespace("jitted",
                               sol=jit.jit_function(gsm.Function("sine_of_t", gsm.sin(t)), override=override),
                               # d(sin(t)) / dt = cos(t) = sqrt(1 - sin^2(x))
                               rate=jit.jit_function(gsm.Function("dx_dt", gsm.sqrt(1 - x ** 2)), override=override),
                               rate_t=jit.jit_function(gsm.Function("dx_dt", gsm.cos(t)), override=override),
                               rate_dt=jit.jit_function(gsm.Function("dx_dt", gsm.cos(t_n + Delta_t)),
                                                        override=override),
                               )


class Result:
    def __init__(self, t, y):
        self.t = t
        self.y = y


def RK5S(rate_func, t_space, y0):
    res_y = np.zeros_like(t_space)
    res_y[0] = y0
    for n, (tau, tau_n) in enumerate(zip(t_space[1:], t_space[:-1])):
        Dt = tau - tau_n
        k1 = float(rate_func(tau_n,
                             res_y[n]))
        k2 = float(rate_func(tau_n + Dt / 2,
                             res_y[n] + Dt / 2 * k1))
        k3 = float(rate_func(tau_n + Dt / 4,
                             res_y[n] + Dt / 16 * (3 * k1 + k2)))
        k4 = float(rate_func(tau_n + Dt / 2,
                             res_y[n] + Dt / 2 * k3))
        k5 = float(rate_func(tau_n + 3 * Dt / 4,
                             res_y[n] + Dt / 16 * 3 * (-k2 + 2 * k3 + 3 * k4)))
        k6 = float(rate_func(tau_n + Dt,
                             res_y[n] + Dt / 7 * (k1 + 4 * k2 + 6 * k3 - 12 * k4 + 8 * k5)))
        res_y[n + 1] = res_y[n] + Dt / 90 * (7 * (k1 + k6) + 32 * (k3 + k5) + 12 * k4)
    return Result(t_space, res_y)


def RK5S_cpp(rate_jit_func, t_space, y0):
    rk5 = cpp.create_runge_kutta_5_stable("double")
    evol = cpp.create_evolution(rate_jit_func, [x.field_data()], Delta_t.field_data(), rk5)
    res_y = np.zeros_like(t_space)
    res_y[0] = y0
    for n, (tau, tau_n) in enumerate(zip(t_space[1:], t_space[:-1])):
        res_y[n + 1] = float(cpp.call(evol, x=res_y[n], t_n=tau_n, Delta_t=(tau - tau_n)))
    return Result(t_space, res_y)


expected_error_log = {"rate_dt": -24, "rate": -13}
expected_error_decrease = {"rate_dt": 4.1, "rate": 3.3}


@pytest.mark.parametrize("rate_funcs", tuple([("rate_t", "rate_dt"), ("rate", "rate")]))
def test_explicit_TS(jitted, rate_funcs: typing.Tuple[cpp.double.Function]):
    y0 = float(cpp.call(jitted.sol, t=0))
    rate_for_python, rate_for_cpp = [jitted.get_item(rf) for rf in rate_funcs]

    def pyrate(t, y):
        return cpp.call(rate_for_python, x=y, t=t)

    res_1 = RK5S(pyrate, np.linspace(0, 1, 6), y0)
    res_1_cpp = RK5S_cpp(rate_for_cpp, np.linspace(0, 1, 6), y0)

    assert np.all(np.isclose(res_1.y, res_1_cpp.y, atol=1e-14, rtol=1e-15))

    res_2 = RK5S(pyrate, np.linspace(0, 1, 11), y0)
    res_2_cpp = RK5S_cpp(rate_for_cpp, np.linspace(0, 1, 11), y0)

    assert np.all(np.isclose(res_2.y, res_2_cpp.y, atol=1e-14, rtol=1e-15))

    err_1_cpp = float(np.abs(cpp.call(jitted.sol, t=1) - res_1_cpp.y[-1]))
    err_2_cpp = float(np.abs(cpp.call(jitted.sol, t=1) - res_2_cpp.y[-1]))

    assert np.log(err_1_cpp) < expected_error_log[rate_funcs[1]]
    assert np.log(err_1_cpp) - np.log(err_2_cpp) > expected_error_decrease[rate_funcs[1]]


def test_compiled_evolution_ts(jitted):
    rk5 = cpp.create_runge_kutta_5_stable("double")
    evol = cpp.create_evolution(jitted.rate, [x.field_data()], Delta_t.field_data(), rk5)
    evol_jit = jit.jit_module(materiaux.Namespace("explicit_ts_rk5s", evolution=evol))

    t_space = np.linspace(0, 1, 6)

    y0 = float(cpp.call(jitted.sol, t=t_space[0]))

    res_expected = RK5S_cpp(jitted.rate, t_space, y0).y

    res_jit = np.zeros_like(t_space)
    res_jit[0] = y0
    for n, (tau, tau_n) in enumerate(zip(t_space[1:], t_space[:-1])):
        res_jit[n + 1] = float(cpp.call(evol_jit.evolution, x=res_jit[n], t_n=tau_n, Delta_t=(tau - tau_n)))

    assert np.all(np.allclose(res_jit, res_expected, atol=1e-14, rtol=1e-15))
