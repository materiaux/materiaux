"""
Tests for jit mechanisms. Comparison is against generated modules. Correctness of generated code is tested elsewhere.
"""

import pathlib
import os
import numpy as np
import shutil
import pytest

import materiaux
from materiaux.modeling.gsm import State, Parameter, Function, derivative, tr, dot, det, inner
import materiaux.codegeneration.jit as jit
import materiaux.cpp as cpp
from materiaux import Namespace


_dir = pathlib.Path(pathlib.Path.cwd())
override = False

gu = State("gu", shape=(3,), description="gradient of u")
gus = State("gus", shape=(3, 3), description="symmetric gradient of u", symmetry=True)

k = Parameter("k", description="first param")
kk = Parameter("kk", shape=(3, 3))

_gu = np.array([1.0, 2.0, 1.0])
_gus = np.array([1.0, 3.0, 2.0, 3.0, 1.0, 4.0])
_k = 3.0
_kk = np.eye(3)
_kk[0, 0] = 2.0


psi = Function("psi", k / 2 * dot(gu, dot(kk, gu)))
d_psi = derivative(psi, gu)
Dd_psi = derivative(d_psi, gu)


def test_jit_variants():
    os.chdir(_dir)

    integrands_psi = [psi, d_psi, Dd_psi]
    module_name = "jit_test_psi"
    module_psi = \
        jit.jit(Namespace(name=module_name, **{integrand.name(): integrand for integrand in integrands_psi}),
                override=override)
    d0_psi = module_psi.d0_psi
    d0_d0_psi = module_psi.d0_d0_psi

    assert module_psi.materiaux_version == materiaux.__version__
    assert d0_psi.result_data.name == "::".join([module_name, "d0_psi"])
    assert d0_psi.code_data.include_directives == {"jit_test_psi.h"}
    assert d0_psi.code_data.include_directories == set()
    assert d0_psi.code_data.link_directories == set()
    assert d0_psi.code_data.link_libraries == {"libjit_test_psi.so"}
    assert d0_d0_psi.scalar_type == "double"

    func_d0_psi = jit.jit(d0_psi)
    cpp.load_library(jit.get_working_directory().resolve() / "lib" / "libjit_test_psi.so")
    module_psi_export = jit.jit(module_psi, module_name="exported_d0_psi")

    assert np.allclose(jit.call(func_d0_psi, gu=_gu, k=_k, kk=_kk), np.array([6.0, 6.0, 3.0]),
                       rtol=1e-12, atol=1e-16)
    assert np.allclose(jit.call(func_d0_psi, gu=_gu, k=_k, kk=_kk),
                       jit.call(d0_psi, gu=_gu, k=_k, kk=_kk),
                       rtol=1e-12, atol=1e-16)
    assert np.allclose(jit.call(module_psi_export.d0_psi, gu=_gu, k=_k, kk=_kk),
                       jit.call(d0_psi, gu=_gu, k=_k, kk=_kk),
                       rtol=1e-12, atol=1e-16)

    psis = Function("psis", k / 2 * (tr(gus) + det(gus)))
    d_psis = derivative(psis, gus)
    func_d0_psis = jit.jit(d_psis)
    assert func_d0_psis.code_data.symbol_name == "d0_psis"
    assert np.allclose(jit.call(func_d0_psis, k=_k, gus=_gus), np.array([18., -30.,-9., 1.5, 15., -7.5]),
                       rtol=1e-12, atol=1e-16)

    # relocation
    jit2_dir = jit.get_working_directory().resolve().parent / ".jit2"
    if not jit2_dir.exists():
        shutil.copytree(jit.get_working_directory().resolve(), jit2_dir)
    lib2 = cpp.load_library("libjit_test_psi.so", working_directory=jit2_dir)
    d0_psi2 = lib2.get_item("d0_psi")
    assert np.allclose(jit.call(d0_psi2, gu=_gu, k=_k, kk=_kk),
                       jit.call(d0_psi, gu=_gu, k=_k, kk=_kk),
                       rtol=1e-12, atol=1e-16)


def test_complex():
    # Note that there seems to be an issue with the pinned versions of ufl/ffcx, where d_psi cannot be compiled for
    # complex numbers if one inner is replaced by a dot
    func_d0_psi = jit.jit(d_psi, parameters={"scalar_type": "double complex"})
    assert np.allclose(jit.call(func_d0_psi, gu=_gu, k=_k, kk=_kk), np.array([6.0, 6.0, 3.0]),
                       rtol=1e-12, atol=1e-16)