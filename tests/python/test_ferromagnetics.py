"""
Test on a real world material model of reasonable complexity to cover a number of features.

The model at hand is the plasticity-inspired model for ferromagnetism by Mukherjee & Danas (2019).
See also Rambausek, Mukherjee & Danas (in progress, 2021).
"""

import typing
import os
import pathlib
import numpy as np
from materiaux.modeling import gsm
from materiaux.modeling.gsm import Expr, Operator
import materiaux.cpp
from materiaux.codegeneration import jit
import pytest

double = materiaux.cpp.DOUBLE

wdir = pathlib.Path("generated") / "ferro"
override = False

_epsilon = 1e-4
_pi = np.pi

diag = tuple(([0, 1, 2], [0, 1, 2]))
fdiag = tuple(([0, 4, 8],))
sdiag = tuple(([0, 3, 5],))


def b(F: Expr) -> Operator:
    return F * gsm.transpose(F)


def C(F: Expr) -> Operator:
    return gsm.transpose(F) * F


def I_1(C_or_b: Expr) -> Operator:
    return gsm.tr(C_or_b)


def I_2(C_or_b: Expr) -> Operator:
    C = C_or_b
    return 1 / 2 * (gsm.tr(C) ** 2 - gsm.tr(C * C))


def I_3(C_or_b: Expr) -> Operator:
    C = C_or_b
    return gsm.det(C)


def H_t(F: Expr, H: Expr) -> Operator:
    return gsm.dot(gsm.transpose(gsm.inv(F)), H)


def inv_sigmoid_log(x: Expr) -> Operator:
    """inverse hypergeometric sigmoid"""
    return -(gsm.ln(1 - x) + x)


def inv_sigmoid_tan(x: Expr) -> Operator:
    """inverse tan sigmoid"""
    return -(4.0 / _pi ** 2.0) * gsm.ln(gsm.cos(_pi * x / 2.0))


def inv_sigmoid_atanh(x: Expr) -> Operator:
    """inverse artanh sigmoid"""
    return 1 / 2 * gsm.ln((1 + x) / (1 - x))


def psi_vac(I_3: Expr, I_5: Expr, mu_0: gsm.Parameter) -> Operator:
    """magnetic vacuum energy density"""
    return -1 / 2 * mu_0 * gsm.sqrt(I_3) * I_5


def phi(I_b_r: Expr, b_c: Expr) -> Operator:
    """switching surface"""
    return I_b_r - b_c ** 2


def psi_mag(I_3: Expr, I_5: Expr, I_5_er: Expr, I_5_r: Expr, mu_0: gsm.Parameter, chi: gsm.Parameter,
            chi_r: gsm.Parameter, h_r_s: gsm.Parameter, sat_func: typing.Callable[[Expr], Operator]) -> Operator:
    """magnetic energy"""
    _I_5_er = I_5_er / gsm.sqrt(I_3)
    _I_5_r = I_5_r / I_3
    x2 = _I_5_r / h_r_s ** 2
    x = gsm.sqrt(x2)
    return gsm.sqrt(I_3) * (
            - mu_0 / 2 * (1 + chi) * I_5
            + mu_0 * (1 + chi) * _I_5_er
            + mu_0 * (h_r_s ** 2.0 / chi_r) * gsm.conditional(x > 1e-8, sat_func(x), x2 / 2)
    )


def magnetic_energy_density(F: gsm.State, H: gsm.State, H_r: gsm.InternalState, H_r_map: typing.Callable,
                            sat_func: typing.Callable, H_r_s: gsm.Parameter, chi: gsm.Parameter, chi_r: gsm.Parameter,
                            mu_0: gsm.Parameter):
    _I_3 = I_3(C(F))
    _I_5 = gsm.inner(H, gsm.inv(C(F)) * H)
    _I_5_er = gsm.inner(H, gsm.inv(F) * H_r_map(H_r))
    _I_5_r = gsm.inner(H_r, H_r)
    return psi_mag(_I_3, _I_5, _I_5_er, _I_5_r, mu_0, chi, chi_r, H_r_s, sat_func)


def dual_dissipation_potential(H_r_dot: gsm.TimeRate, B_r: gsm.InternalState, B_r_n: gsm.HistoryState,
                               Lambda: gsm.InternalState, B_c: gsm.Parameter, evol_tol: gsm.Parameter):
    evolve = phi(gsm.inner(B_r_n, B_r_n), B_c) > evol_tol
    return -gsm.inner(B_r, H_r_dot) + gsm.conditional(evolve, Lambda * phi(gsm.inner(B_r, B_r), B_c), Lambda ** 2)


def direct_evolution_equations(psi_mag: Operator, H_r_dot: gsm.TimeRate, H_r: gsm.InternalState,
                               H_r_t: typing.Union[gsm.InternalState, gsm.HistoryState],
                               H_r_n: gsm.HistoryState, lambda_: gsm.InternalState,
                               B_c: gsm.Parameter, evol_tol: gsm.Parameter) -> gsm.Function:
    _b_r = -gsm.diff(psi_mag, H_r)
    _b_r = gsm.variable(_b_r)
    _b_c = B_c
    _b_r_n = gsm.replace(_b_r, {H_r: H_r_n})
    _b_c_n = B_c
    evolve = phi(gsm.inner(_b_r_n, _b_r_n), _b_c_n) > evol_tol

    def _L(_b_r):
        return -gsm.inner(_b_r, H_r_dot) + lambda_ * phi(gsm.inner(_b_r, _b_r), _b_c)

    L = _L(_b_r)
    _b_r_t = gsm.replace(_b_r, {H_r: H_r_t})
    L_t = _L(_b_r_t)
    eq_h_r_dot = gsm.conditional(evolve, gsm.diff(L_t, _b_r_t), H_r_dot.discretization())
    eq_lambda = gsm.conditional(evolve, gsm.diff(L, lambda_), lambda_)
    return gsm.Function("direct_evolution_equations_H_r__lambda",
                        gsm.equations({H_r: eq_h_r_dot, lambda_: eq_lambda}))


def ferromagnetic_material(F: gsm.State, H: gsm.State, H_r_dot: gsm.TimeRate, H_r: gsm.InternalState,
                           H_r_t: typing.Union[gsm.InternalState, gsm.HistoryState], H_r_map: typing.Callable,
                           H_r_n: gsm.HistoryState, lambda_: gsm.InternalState, chi: gsm.Parameter,
                           chi_r: gsm.Parameter, sat_func: typing.Callable, H_r_s: gsm.Parameter,
                           B_c: gsm.Parameter, evol_tol: gsm.Parameter,
                           mu_0: gsm.Parameter) -> gsm.GSMBase:
    psi_mag = magnetic_energy_density(F, H, H_r, H_r_map, sat_func, H_r_s, chi, chi_r, mu_0)
    response = gsm.Function("energy", psi_mag)
    evolution = direct_evolution_equations(psi_mag, H_r_dot, H_r, H_r_t, H_r_n, lambda_, B_c, evol_tol)
    return gsm.GSMBase(response, evolution)


def ferromagnetic_material_gsm(F: gsm.State, H: gsm.State, H_r_dot: gsm.TimeRate, H_r: gsm.InternalState,
                               H_r_map: typing.Callable, B_r: gsm.InternalState, B_r_n: gsm.HistoryState,
                               Lambda: gsm.InternalState, chi: gsm.Parameter, chi_r: gsm.Parameter,
                               sat_func: typing.Callable, H_r_s: gsm.Parameter, B_c: gsm.Parameter,
                               evol_tol: gsm.Parameter, mu_0: gsm.Parameter) -> gsm.GSMBase:
    return gsm.GeneralizedStandardMaterial(
        gsm.Function("energy",
                     magnetic_energy_density(F, H, H_r, H_r_map, sat_func, H_r_s, chi, chi_r, mu_0)),
        dissipation_potential=gsm.Function("dual_dissipation_potential",
                                           dual_dissipation_potential(H_r_dot, B_r, B_r_n, Lambda, B_c, evol_tol))
    )


F = gsm.State("F", (3, 3))
H = gsm.State("H", (3,))
H_r = gsm.InternalState("H_r", (3,))
B_r = gsm.InternalState("B_r", (3,))
lambda_ = gsm.InternalState("lambda_")

t = gsm.Time("t")
t_n = gsm.Time("t_n")

H_r_n = gsm.HistoryState(H_r)
B_r_n = gsm.HistoryState(B_r)
H_r_dot = gsm.TimeRate(H_r, (H_r - H_r_n) / (t - t_n))
chi = gsm.Parameter("chi")
chi_r = gsm.Parameter("chi_r")
H_r_s = gsm.Parameter("H_r_s")
B_c = gsm.Parameter("B_c")
evol_tol = gsm.Parameter("evol_tol")
mu_0 = gsm.Parameter("mu_0")


def H_r_map(H_r):
    return H_t(F, H_r)


ferro = gsm.create_module_data(ferromagnetic_material(F, H, H_r_dot, H_r, H_r, H_r_map, H_r_n, lambda_, chi, chi_r,
                                                      inv_sigmoid_log, H_r_s, B_c, evol_tol, mu_0))

ferro_gsm = gsm.create_module_data(ferromagnetic_material_gsm(F, H, H_r_dot, H_r, H_r_map, B_r, B_r_n, lambda_, chi,
                                                              chi_r, inv_sigmoid_log, H_r_s, B_c, evol_tol, mu_0))

id_map = [id(ferro), id(ferro_gsm)]

expected_H_r = {
    id(ferro): np.array([0.65667307, 0.65667459, 0.65667611]),
    id(ferro_gsm): np.array([0.65667307, 0.65667307, 0.65667611]),
}

expected_lin = {
    id(ferro): np.array([[-1.3812374, 0., 0.],
                         [0., -1.37602133, 0.],
                         [0., 0., -1.37602133]]),
    id(ferro_gsm): np.array([[-1.3812374, 0., 0.],
                             [0., -1.37602508, 0.],
                             [0., 0., -1.37602508]]),
}


@pytest.fixture
def jitted():
    return {
        id(model_data): jit.jit_module(model_data, override=override, cmake_options={"CMAKE_BUILD_TYPE": "Debug"},
                                       working_directory=wdir)
        for model_data in (ferro, ferro_gsm)
    }


def create_evolution(model):
    solver = materiaux.cpp.create_newton_raphson(double)
    return materiaux.cpp.create_evolution(
        model.evolution_equations.eqs, model.evolution_equations.d_int_eqs, solver, True
    )


def create_linearization(model):
    return materiaux.cpp.create_consistent_linearization(
        model.material_response.d_ext_duals,
        model.material_response.d_int_duals,
        model.evolution_equations.d_ext_eqs,
        model.evolution_equations.d_int_eqs
    )


def init_fields(field_arrays: dict):
    newton_tol = 1e-6
    field_arrays["t"][:] = 0
    field_arrays["t_n"][:] = 0
    field_arrays["F"][diag] = 1
    field_arrays["H"][:] = 0
    field_arrays["H_r"][:] = 0
    field_arrays["H_r_n"][:] = 0
    if "B_r" in field_arrays.keys():
        field_arrays["B_r"][:] = 0
        field_arrays["B_r_n"] = field_arrays["B_r"].copy()
    field_arrays["mu_0"][:] = 4 * np.pi * 1e-1
    field_arrays["chi"][0] = 0.095
    field_arrays["chi_r"][0] = 8.76
    field_arrays["H_r_s"][0] = 0.67
    field_arrays["B_c"][0] = 0.766
    field_arrays["evol_tol"][0] = newton_tol * 1e1
    field_arrays["newton_tol"] = newton_tol
    field_arrays["newton_max_iter"] = 10


def _run_evolution(model):
    if not hasattr(model.evolution_equations, "evolution"):
        model.evolution_equations.set_item("evolution", create_evolution(model))

    if not hasattr(model.material_response, "linearization"):
        model.material_response.set_item("linearization", create_linearization(model))

    evolution = model.evolution_equations.evolution
    linearization = model.material_response.linearization

    internal_states = [coeff for coeff in evolution.coefficient_data if coeff.type == "InternalState"]

    field_arrays = \
        materiaux.cpp.create_field_arrays(double,
                                          model.material_response.stored_energy.coefficient_data
                                          + model.material_response.stored_energy.constant_data)

    init_fields(field_arrays)

    H_rate = 0.01
    H_0 = 0.0
    target_H = 4
    n_steps = 30000
    direction = 1 if target_H > H_0 else -1
    dt = np.abs(target_H - H_0) / H_rate / (n_steps / 3)

    res_duals = materiaux.cpp.create_function_result(double, model.material_response.duals.result_data).flatten()
    res_evolution = materiaux.cpp.create_function_result(double, evolution.result_data).flatten()
    H_r_data = np.zeros((3, n_steps))
    H_data = np.zeros((3, n_steps))
    S_data = np.zeros((3, 3, n_steps))
    B_data = np.zeros((3, n_steps))
    for step in range(n_steps):
        if direction > 0 and step > n_steps / 3:
            direction = -1
        field_arrays["H"][0] += direction * H_rate * dt
        field_arrays["t"][0] += dt
        coeffs_evolution = materiaux.cpp.create_function_coefficients(evolution, coefficients=field_arrays)
        constants_evolution = materiaux.cpp.create_function_constants(evolution, constants=field_arrays)
        res_evolution[:] = 0
        try:
            evolution(res_evolution, coeffs_evolution, constants_evolution)
        except Exception as e:
            print(e)
            res_eqs = materiaux.cpp.create_function_result(double,
                                                           model.evolution_equations.eqs.result_data).flatten()
            coeffs_eqs = materiaux.cpp.create_function_coefficients(model.evolution_equations.eqs,
                                                                    coefficients=field_arrays)
            constants_eqs = materiaux.cpp.create_function_constants(model.evolution_equations.eqs,
                                                                    constants=field_arrays)
            res_eqs_lin = materiaux.cpp.create_function_result(double,
                                                               model.evolution_equations.d_int_eqs.result_data).flatten()
            coeffs_eqs_lin = materiaux.cpp.create_function_coefficients(
                model.evolution_equations.d_int_eqs,
                coefficients=field_arrays)
            constants_eqs_lin = materiaux.cpp.create_function_constants(
                model.evolution_equations.d_int_eqs,
                constants=field_arrays)
            model.evolution_equations.eqs(res_eqs, coeffs_eqs, constants_eqs)
            model.evolution_equations.d_int_eqs(res_eqs_lin, coeffs_eqs_lin, constants_eqs_lin)
            print(res_eqs)
            print(res_eqs_lin.reshape((res_eqs.size, res_eqs.size)))
            if direction > 0:
                direction = -1
                H_data[:, step] = H_data[:, step - 1]
                H_r_data[:, step] = H_r_data[:, step - 1]
                S_data[:, :, step] = S_data[:, :, step - 1]
                B_data[:, step] = B_data[:, step - 1]
                continue
            else:
                print(step)
                break

        offset = 0
        for istate in internal_states:
            field_arrays[istate.name][:] = res_evolution[offset:offset + istate.size]
            offset += istate.size

        res_duals[:] = 0
        coeffs_duals = materiaux.cpp.create_function_coefficients(model.material_response.duals,
                                                                  coefficients=field_arrays)
        constants_duals = materiaux.cpp.create_function_constants(model.material_response.duals,
                                                                  constants=field_arrays)
        model.material_response.duals(res_duals, coeffs_duals, constants_duals)

        dB__dH = jit.call(linearization, **field_arrays)[9:, 9:]

        field_arrays["H_r_n"][:] = field_arrays["H_r"]
        if "B_r" in field_arrays.keys():
            field_arrays["B_r_n"][:] = field_arrays["B_r"]
        field_arrays["t_n"][0] = field_arrays["t"][0]
        H_data[:, step] = field_arrays["H"][:]
        H_r_data[:, step] = field_arrays["H_r"][:]
        S_data[:, :, step] = res_duals[:9].reshape((3, 3))
        B_data[:, step] = res_duals[9:]

    state = [field_arrays[name] for name in [coeff.name for coeff in internal_states]] + [res_duals]
    print(np.array2string(np.concatenate(state), precision=14, separator=", ", ))
    return H_data, H_r_data, S_data, B_data, dB__dH


@pytest.mark.parametrize("model_data", tuple([ferro, ferro_gsm]))
def test_evolution(jitted, model_data):
    H_data, H_r_data, S_data, B_data, dB__dH = _run_evolution(jitted[id(model_data)])
    assert np.allclose(H_r_data[0, -3:], expected_H_r[id(model_data)], rtol=1e-8, atol=1e-8)
    assert np.allclose(dB__dH, expected_lin[id(model_data)], rtol=1e-8, atol=1e-8)


def test_compiled_evolution_and_linearization(jitted):
    model = jitted[id(ferro)]
    re_jit = materiaux.Namespace("ferro_with_evolution",
                                 **{key: model.get_item(key) for key in model.contents()
                                    if not (isinstance(model.get_item(key), materiaux.Namespace)
                                            or key in ("evolution_equations::evolution",
                                                       "material_response::consistent_linearization"))})
    re_jit.evolution_equations.set_item("evolution", create_evolution(re_jit))
    re_jit.material_response.set_item("consistent_linearization", create_linearization(re_jit))
    re_jitted = jit.jit_module(re_jit, working_directory=wdir)
    H_data, H_r_data, S_data, B_data, dB__dH = _run_evolution(re_jitted)
    assert np.allclose(H_r_data[0, -3:], expected_H_r[id(ferro)], rtol=1e-8, atol=1e-8)
    assert np.allclose(dB__dH, expected_lin[id(ferro)], rtol=1e-8, atol=1e-8)


if __name__ == "__main__":
    plot_data = _run_evolution(jitted[id(ferro)])
    plot_data_gsm = _run_evolution(jitted[id(ferro_gsm)])
    print("ready...")
