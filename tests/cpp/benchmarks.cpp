//
// Created by mrambausek on 2/1/21.
//

#include "modules/gsm__test_psis2/gsm__test_psis2.h"
#include <materiaux.h>

#include <Eigen/Core>
#include <array>
#include <benchmark/benchmark.h>
#include <chrono>

// This file contains some benchmark playground for argument handling, i.e. how much does it cost
// to tear apart a vector or form one out of multiple.

using span = Eigen::Map<Eigen::ArrayXd>;
using cspan = Eigen::Map<const Eigen::ArrayXd>;

struct DSpan {
    double *ptr;
    std::size_t N;
};

struct DCSpan {
    const double *ptr;
    std::size_t N;
};

using namespace gsm__test_psis2;
const auto &f = *d0_psis;
const auto &grad = *d0_psis;
const auto &hess = *d0_d0_psis;

using namespace materiaux;
using utils::begin;
using utils::end;

std::vector<double> kk3(27);
Eigen::ArrayXd Ek(1);
std::vector<double> gus{1.0, 1.0, 1.0, 2.0, 0.0, 2.0};
Eigen::ArrayXd Ekk(9);
std::vector<double> kk{{1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0}};
Eigen::ArrayXd Ekk3(27);
Eigen::ArrayXd Egu(3);
std::vector<double> gu2{0.0, 1.0, 2.0};
Eigen::ArrayXd Egu2(3);
std::vector<double> k = {1};
std::vector<double> gu{1.0, 1.0, 1.0};
Eigen::ArrayXd Egus(6);

span mk_span(std::vector<double> &vec) { return span(vec.data(), vec.size()); }
cspan mk_cspan(const std::vector<double> &vec) { return cspan(vec.data(), vec.size()); }

DSpan mk_Span(std::vector<double> &vec) { return DSpan{vec.data(), vec.size()}; }
DCSpan mk_CSpan(const std::vector<double> &vec) { return DCSpan{vec.data(), vec.size()}; }

auto arg_map = std::map<std::string, const std::vector<double> *>{
    {"gu", &gu}, {"gu2", &gu2}, {"gus", &gus}, {"k", &k}, {"kk", &kk}, {"kk3", &kk3}};

const std::vector<std::size_t> cf_default_sizes{gu.size(), gu2.size(), gus.size()};
const std::vector<std::size_t> cs_default_sizes{k.size(), kk.size(), kk3.size()};

const std::vector<std::size_t> cf_default_offsets{0, gu.size(), gu.size() + gu2.size(),
                                                  gu.size() + gu2.size() + gus.size()};
const std::vector<std::size_t> cs_default_offsets{0, k.size(), k.size() + kk.size(),
                                                  k.size() + kk.size() + kk3.size()};

const auto sgu = mk_cspan(gu);
const auto sgu2 = mk_cspan(gu2);
const auto sgus = mk_cspan(gus);

const auto sk = mk_cspan(k);
const auto skk = mk_cspan(kk);
const auto skk3 = mk_cspan(kk3);

auto coeff_vecs = std::vector<cspan>{sgu, sgu2, sgus};
auto const_vecs = std::vector<cspan>{sk, skk, skk3};

std::vector<const double *> cf_ptrs{gu.data(), gu2.data(), gus.data()};
std::vector<const double *> cs_ptrs{k.data(), kk.data(), kk3.data()};

std::vector<DCSpan> cf_Spans{mk_CSpan(gu), mk_CSpan(gu2), mk_CSpan(gus)};
std::vector<DCSpan> cs_Spans{mk_CSpan(k), mk_CSpan(kk), mk_CSpan(kk3)};

auto coeffs_flat = std::vector<double>(12);
auto consts_flat = std::vector<double>(37);
auto all_flat = std::vector<double>(49);

std::vector<double> res(f.result_data->size, 0.0);
Eigen::ArrayXd Ed0_res(grad.result_data->size);
std::vector<double> d0_d0_res(hess.result_data->size, 0.0);
Eigen::ArrayXd Eres(f.result_data->size);
std::vector<double> d0_res(grad.result_data->size, 0.0);
Eigen::ArrayXd Ed0_d0_res(hess.result_data->size);

std::vector<cspan> get_cspans(const double *vec, const std::vector<std::size_t> &offsets) {
    std::vector<cspan> svec;
    const int l = static_cast<int>(offsets.size() - 1);
    svec.reserve(l);
    for (int ii = 0; ii < l; ++ii) {
        svec.emplace_back(cspan(vec + offsets[ii], offsets[ii + 1] - offsets[ii]));
    }
    return svec;
}

std::vector<cspan> get_cspans(const double *vec, const std::vector<std::size_t> &offsets,
                              const std::vector<int> &perm) {
    std::vector<cspan> svec;
    svec.reserve(perm.size());
    for (const auto pos : perm) {
        svec.emplace_back(cspan(vec + offsets[pos], offsets[pos + 1] - offsets[pos]));
    }
    return svec;
}

std::vector<DCSpan> get_CSpans(const double *vec, const std::vector<std::size_t> &offsets) {
    std::vector<DCSpan> svec;
    const auto l = offsets.size() - 1;
    svec.reserve(l);
    for (std::remove_cv_t<decltype(l)> ii = 0; ii < l; ++ii) {
        svec.emplace_back(DCSpan{vec + offsets[ii], offsets[ii + 1] - offsets[ii]});
    }
    return svec;
}

std::vector<DSpan> get_Spans(double *vec, const std::vector<std::size_t> &offsets) {
    std::vector<DSpan> svec;
    const auto l = offsets.size() - 1;
    svec.reserve(l);
    for (std::remove_cv_t<decltype(l)> ii = 0; ii < l; ++ii) {
        svec.emplace_back(DSpan{vec + offsets[ii], offsets[ii + 1] - offsets[ii]});
    }
    return svec;
}

void update_CSpans(std::vector<DCSpan> &svec, const double *vec,
                   const std::vector<std::size_t> &offsets) {
    const auto l = offsets.size() - 1;
    svec.reserve(l);
    for (std::remove_cv_t<decltype(l)> ii = 0; ii < l; ++ii) {
        svec[ii].ptr = vec + offsets[ii];
    }
}

void update_CSpans(DCSpan *svec, const double *vec, const std::vector<std::size_t> &offsets) {
    const auto l = offsets.size() - 1;
    for (std::remove_cv_t<decltype(l)> ii = 0; ii < l; ++ii) {
        svec[ii].ptr = vec + offsets[ii];
        svec[ii].N = offsets[ii + 1] - offsets[ii];
    }
}

std::vector<cspan> reorder(const std::vector<cspan> &ovec, const std::vector<int> &perm) {
    std::vector<cspan> nvec;
    nvec.reserve(ovec.size());
    for (const auto pos : perm)
        nvec.emplace_back(ovec[pos]);
    return nvec;
}

void copy_array_of_pointers_into_one(double *dest, const std::vector<cspan> &field_ptrs,
                                     std::size_t offset = 0) {
    for (const auto &coefficient_data : field_ptrs) {
        std::copy(begin(coefficient_data), end(coefficient_data), dest + offset);
        offset += coefficient_data.size();
    }
}

void copy_array_of_pointers_into_one(double *dest, const std::vector<DCSpan> &field_ptrs,
                                     std::size_t offset = 0) {
    for (const auto &coefficient_data : field_ptrs) {
        std::copy(coefficient_data.ptr, coefficient_data.ptr + coefficient_data.N, dest + offset);
        offset += coefficient_data.N;
    }
}

void copy_array_of_pointers_into_one(double *dest, const std::vector<DCSpan> &field_ptrs,
                                     const std::vector<int> &perm, std::size_t offset = 0) {
    for (const auto pos : perm) {
        std::copy(field_ptrs[pos].ptr, field_ptrs[pos].ptr + field_ptrs[pos].N, dest + offset);
        offset += field_ptrs[pos].N;
    }
}

void copy_array_of_pointers_into_one(double *dest, const DCSpan *field_ptrs,
                                     const std::vector<int> &perm, std::size_t offset = 0) {
    for (const auto pos : perm) {
        std::copy(field_ptrs[pos].ptr, field_ptrs[pos].ptr + field_ptrs[pos].N, dest + offset);
        offset += field_ptrs[pos].N;
    }
}

void copy_array_of_pointers_into_one(double *dest, const double *const *field_ptrs,
                                     const std::vector<size_t> &sizes, const std::vector<int> &perm,
                                     std::size_t offset = 0) {
    for (const auto pos : perm) {
        std::copy(field_ptrs[pos], field_ptrs[pos] + sizes[pos], dest + offset);
        offset += sizes[pos];
        //        = utils::size(field_ptrs[pos]);
        //        const auto _src = field_ptrs[pos].data();
        //        std::copy(_src, _src + _size, dest + offset);
        //        offset += _size;
    }
}

void copy_array_of_pointers_into_one(double *dest, const std::vector<cspan> &field_ptrs,
                                     const std::vector<int> &perm, std::size_t offset = 0) {
    for (const auto pos : perm) {
        std::copy(begin(field_ptrs[pos]), end(field_ptrs[pos]), dest + offset);
        offset += utils::size(field_ptrs[pos]);
        //        = utils::size(field_ptrs[pos]);
        //        const auto _src = field_ptrs[pos].data();
        //        std::copy(_src, _src + _size, dest + offset);
        //        offset += _size;
    }
}

void copy_array_of_pointers_into_one(double *dest, const double *src,
                                     const std::vector<size_t> &s_offsets,
                                     const std::vector<int> &perm, std::size_t offset = 0) {
    for (const auto pos : perm) {
        std::copy(src + s_offsets[pos], src + s_offsets[pos + 1], dest + offset);
        offset += s_offsets[pos + 1] - s_offsets[pos];
    }
}

void copy_array_of_pointers_into_one(double *dest, const double *src, std::size_t len,
                                     std::size_t offset = 0) {
    std::copy(src, src + len, dest + offset);
}

void setup();

class Setup : public benchmark::Fixture {
  public:
    // add members as needed
    Setup() {
        std::cout << "Setup" << std::endl;
        setup();
    }
};

void setup() {
    kk3[0] = 2.0;
    kk3[3] = 3.0;
    kk3[10] = 1.0;
    kk3[15] = 0.3;
    kk3[22] = 0.5;
    kk3[26] = 4.0;

    std::copy(begin(k), end(k), Ek.data());
    std::copy(begin(kk), end(kk), Ekk.data());
    std::copy(begin(kk3), end(kk3), Ekk3.data());
    std::copy(begin(gu), end(gu), Egu.data());
    std::copy(begin(gu2), end(gu2), Egu2.data());
    std::copy(begin(gus), end(gus), Egus.data());

    std::copy(begin(k), end(k), begin(consts_flat));
    std::copy(begin(kk), end(kk), begin(consts_flat) + 1);
    std::copy(begin(kk3), end(kk3), begin(consts_flat) + 10);
    std::copy(begin(gu), end(gu), begin(coeffs_flat));
    std::copy(begin(gu2), end(gu2), begin(coeffs_flat) + 3);
    std::copy(begin(gus), end(gus), begin(coeffs_flat) + 6);

    std::copy(begin(coeffs_flat), end(coeffs_flat), begin(all_flat));
    std::copy(begin(consts_flat), end(consts_flat), begin(all_flat) + 12);

    Eres.setZero();
    Ed0_res.setZero();
    Ed0_d0_res.setZero();
}

constexpr int N = 100000;

BENCHMARK_F(Setup, empty)(benchmark::State &state) {
    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            for (auto &e : coeffs_flat)
                e -= 1;
            benchmark::DoNotOptimize(coeffs_flat);
        }
    }
}

const std::function<void(double *, const std::vector<cspan> &, const std::vector<cspan> &)>
    arg_vecs_split{
        [cfsize = gu.size() + gu2.size() + gus.size(), cssize = k.size() + kk.size() + kk3.size(),
         cforder = std::vector<int>{2, 1, 0}, csorder = std::vector<int>{2, 0, 1}](
            double *res, const std::vector<cspan> &coeffs, const std::vector<cspan> &consts) {
            Eigen::ArrayXd _ecoeffs(cfsize);
            Eigen::ArrayXd _econsts(cssize);
            auto _coeffs = _ecoeffs.data();
            auto _consts = _econsts.data();
            copy_array_of_pointers_into_one(_coeffs, coeffs);
            copy_array_of_pointers_into_one(_consts, consts);
            hess(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, reorder(coeffs, cforder));
            copy_array_of_pointers_into_one(_consts, reorder(consts, csorder));
            f(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, reorder(coeffs, cforder));
            copy_array_of_pointers_into_one(_consts, reorder(consts, csorder));
            grad(res, _coeffs, _consts);
        }};

// BENCHMARK_F(Setup, bm_arg_vecs_split)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            arg_vecs_split(d0_d0_res.data(), get_cspans(coeffs_flat.data(), cf_default_offsets),
//                           get_cspans(consts_flat.data(), cs_default_offsets));
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}

const std::function<void(double *, const decltype(coeffs_flat) &, const decltype(consts_flat) &)>
    flat_Eigen_split{
        [cfsize = coeffs_flat.size(), cssize = consts_flat.size(),
         cforder = std::vector<int>{2, 1, 0}, csorder = std::vector<int>{2, 0, 1},
         cfoffsets = cf_default_offsets, csoffsets = cs_default_offsets](
            double *res, const decltype(coeffs_flat) &coeffs, const decltype(consts_flat) &consts) {
            Eigen::ArrayXd _ecoeffs{cfsize};
            Eigen::ArrayXd _econsts{cssize};
            auto _coeffs = _ecoeffs.data();
            auto _consts = _econsts.data();
            std::copy(begin(coeffs), end(coeffs), _coeffs);
            std::copy(begin(consts), end(consts), _consts);
            hess(res, _coeffs, _consts);

            const auto scoeffs = get_cspans(coeffs.data(), cfoffsets);
            const auto sconsts = get_cspans(consts.data(), csoffsets);
            copy_array_of_pointers_into_one(_coeffs, reorder(scoeffs, cforder));
            copy_array_of_pointers_into_one(_consts, reorder(sconsts, csorder));
            f(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, reorder(scoeffs, cforder));
            copy_array_of_pointers_into_one(_consts, reorder(sconsts, csorder));
            grad(res, _coeffs, _consts);
        }};

// BENCHMARK_F(Setup, bm_flat_Eigen_split)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            flat_Eigen_split(d0_d0_res.data(), coeffs_flat, consts_flat);
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}

const std::function<void(double *, const decltype(coeffs_flat) &, const decltype(consts_flat) &)>
    flat_Eigen_split2{
        [cfsize = coeffs_flat.size(), cssize = consts_flat.size(),
         cforder = std::vector<int>{2, 1, 0}, csorder = std::vector<int>{2, 0, 1},
         cfoffsets = cf_default_offsets, csoffsets = cs_default_offsets](
            double *res, const decltype(coeffs_flat) &coeffs, const decltype(consts_flat) &consts) {
            Eigen::ArrayXd _ecoeffs{cfsize};
            Eigen::ArrayXd _econsts{cssize};
            auto _coeffs = _ecoeffs.data();
            auto _consts = _econsts.data();
            std::copy(begin(coeffs), end(coeffs), _coeffs);
            std::copy(begin(consts), end(consts), _consts);
            hess(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, get_cspans(coeffs.data(), cfoffsets, cforder));
            copy_array_of_pointers_into_one(_consts, get_cspans(consts.data(), csoffsets, csorder));
            f(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, get_cspans(coeffs.data(), cfoffsets, cforder));
            copy_array_of_pointers_into_one(_consts, get_cspans(consts.data(), csoffsets, csorder));
            grad(res, _coeffs, _consts);
        }};

// BENCHMARK_F(Setup, bm_flat_Eigen_split2)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            flat_Eigen_split2(d0_d0_res.data(), coeffs_flat, consts_flat);
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}

const std::function<void(double *, const decltype(coeffs_flat) &, const decltype(consts_flat) &)>
    flat_Eigen_static_split{
        [cfsize = coeffs_flat.size(), cssize = consts_flat.size(),
         cforder = std::vector<int>{2, 1, 0}, csorder = std::vector<int>{2, 0, 1},
         cfoffsets = cf_default_offsets, csoffsets = cs_default_offsets](
            double *res, const decltype(coeffs_flat) &coeffs, const decltype(consts_flat) &consts) {
            Eigen::Array<double, 200, 1> _ecoeffs;
            Eigen::Array<double, 200, 1> _econsts;
            auto _coeffs = _ecoeffs.data();
            auto _consts = _econsts.data();
            std::copy(begin(coeffs), end(coeffs), _coeffs);
            std::copy(begin(consts), end(consts), _consts);
            hess(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, get_cspans(coeffs.data(), cfoffsets, cforder));
            copy_array_of_pointers_into_one(_consts, get_cspans(consts.data(), csoffsets, csorder));
            f(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, get_cspans(coeffs.data(), cfoffsets, cforder));
            copy_array_of_pointers_into_one(_consts, get_cspans(consts.data(), csoffsets, csorder));
            grad(res, _coeffs, _consts);
        }};

// BENCHMARK_F(Setup, bm_flat_Eigen_static_split)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            flat_Eigen_static_split(d0_d0_res.data(), coeffs_flat, consts_flat);
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}

const std::function<void(double *, const decltype(coeffs_flat) &, const decltype(consts_flat) &)>
    flat_Eigen_SMO_200{
        [cfsize = coeffs_flat.size(), cssize = consts_flat.size(),
         cforder = std::vector<int>{2, 1, 0}, csorder = std::vector<int>{2, 0, 1},
         cfoffsets = cf_default_offsets, csoffsets = cs_default_offsets](
            double *res, const decltype(coeffs_flat) &coeffs, const decltype(consts_flat) &consts) {
            Eigen::ArrayXd _ecoeffs(cfsize > 200 ? cfsize : 0);
            Eigen::ArrayXd _econsts(cssize > 200 ? cssize : 0);
            double _vcoeffs[200];
            double _vconsts[200];
            double *_coeffs = cfsize > 200 ? _ecoeffs.data() : _vcoeffs;
            double *_consts = cssize > 200 ? _econsts.data() : _vconsts;
            std::copy(begin(coeffs), end(coeffs), _coeffs);
            std::copy(begin(consts), end(consts), _consts);
            hess(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, get_cspans(coeffs.data(), cfoffsets, cforder));
            copy_array_of_pointers_into_one(_consts, get_cspans(consts.data(), csoffsets, csorder));
            f(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, get_cspans(coeffs.data(), cfoffsets, cforder));
            copy_array_of_pointers_into_one(_consts, get_cspans(consts.data(), csoffsets, csorder));
            grad(res, _coeffs, _consts);
        }};

// BENCHMARK_F(Setup, bm_flat_Eigen_SMO_200)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            flat_Eigen_SMO_200(d0_d0_res.data(), coeffs_flat, consts_flat);
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}

const std::function<void(double *, const decltype(coeffs_flat) &, const decltype(consts_flat) &)>
    flat_Eigen_SMO_10{
        [cfsize = coeffs_flat.size(), cssize = consts_flat.size(),
         cforder = std::vector<int>{2, 1, 0}, csorder = std::vector<int>{2, 0, 1},
         cfoffsets = cf_default_offsets, csoffsets = cs_default_offsets](
            double *res, const decltype(coeffs_flat) &coeffs, const decltype(consts_flat) &consts) {
            Eigen::ArrayXd _ecoeffs(cfsize > 10 ? cfsize : 0);
            Eigen::ArrayXd _econsts(cssize > 10 ? cssize : 0);
            double _vcoeffs[10];
            double _vconsts[10];
            double *_coeffs = cfsize > 10 ? _ecoeffs.data() : _vcoeffs;
            double *_consts = cssize > 10 ? _econsts.data() : _vconsts;
            std::copy(begin(coeffs), end(coeffs), _coeffs);
            std::copy(begin(consts), end(consts), _consts);
            hess(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, get_cspans(coeffs.data(), cfoffsets, cforder));
            copy_array_of_pointers_into_one(_consts, get_cspans(consts.data(), csoffsets, csorder));
            f(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, get_cspans(coeffs.data(), cfoffsets, cforder));
            copy_array_of_pointers_into_one(_consts, get_cspans(consts.data(), csoffsets, csorder));
            grad(res, _coeffs, _consts);
        }};

// BENCHMARK_F(Setup, bm_flat_Eigen_SMO_10)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            flat_Eigen_SMO_10(d0_d0_res.data(), coeffs_flat, consts_flat);
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}

const std::function<void(double *, const decltype(coeffs_flat) &, const decltype(consts_flat) &)>
    flat_vla_split{
        [cfsize = coeffs_flat.size(), cssize = consts_flat.size(),
         cforder = std::vector<int>{2, 1, 0}, csorder = std::vector<int>{2, 0, 1},
         cfoffsets = cf_default_offsets, csoffsets = cs_default_offsets](
            double *res, const decltype(coeffs_flat) &coeffs, const decltype(consts_flat) &consts) {
            double _coeffs[cfsize];
            double _consts[cssize];
            std::copy(begin(coeffs), end(coeffs), _coeffs);
            std::copy(begin(consts), end(consts), _consts);
            hess(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, get_cspans(coeffs.data(), cfoffsets, cforder));
            copy_array_of_pointers_into_one(_consts, get_cspans(consts.data(), csoffsets, csorder));
            f(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, get_cspans(coeffs.data(), cfoffsets, cforder));
            copy_array_of_pointers_into_one(_consts, get_cspans(consts.data(), csoffsets, csorder));
            grad(res, _coeffs, _consts);
        }};
BENCHMARK_F(Setup, bm_flat_vla_split)(benchmark::State &state) {
    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            flat_vla_split(d0_d0_res.data(), coeffs_flat, consts_flat);
            benchmark::DoNotOptimize(d0_d0_res);
        }
    }
}

const std::function<void(double *, const decltype(coeffs_flat) &, const decltype(consts_flat) &)>
    flat_vla_split_ro1{
        [cfsize = coeffs_flat.size(), cssize = consts_flat.size(),
         cforder = std::vector<int>{2, 1, 0}, csorder = std::vector<int>{2, 0, 1},
         cfoffsets = cf_default_offsets, csoffsets = cs_default_offsets](
            double *res, const decltype(coeffs_flat) &coeffs, const decltype(consts_flat) &consts) {
            double _coeffs[cfsize];
            double _consts[cssize];
            std::copy(begin(coeffs), end(coeffs), _coeffs);
            std::copy(begin(consts), end(consts), _consts);
            hess(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, get_cspans(coeffs.data(), cfoffsets, cforder));
            copy_array_of_pointers_into_one(_consts, get_cspans(consts.data(), csoffsets, csorder));
            f(res, _coeffs, _consts);
            grad(res, _coeffs, _consts);
        }};
BENCHMARK_F(Setup, bm_flat_vla_split_ro1)(benchmark::State &state) {
    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            flat_vla_split_ro1(d0_d0_res.data(), coeffs_flat, consts_flat);
            benchmark::DoNotOptimize(d0_d0_res);
        }
    }
}

const std::function<void(double *, const decltype(coeffs_flat) &, const decltype(consts_flat) &)>
    flat_vla_split_ro_fast{
        [cfsize = coeffs_flat.size(), cssize = consts_flat.size(),
         cforder1 = std::vector<int>{0, 1, 2}, csorder1 = std::vector<int>{0, 1, 2},
         cforder2 = std::vector<int>{1, 0, 2}, csorder2 = std::vector<int>{0, 2, 1},
         cforder = std::vector<int>{2, 1, 0}, csorder = std::vector<int>{2, 0, 1},
         cfoffsets = cf_default_offsets, csoffsets = cs_default_offsets](
            double *res, const decltype(coeffs_flat) &coeffs, const decltype(consts_flat) &consts) {
            alignas(32) double _coeffs[cfsize];
            alignas(32) double _consts[cssize];
            //            Eigen::ArrayXd _ecoeffs(cfsize > 200 ? cfsize : 0);
            //            Eigen::ArrayXd _econsts(cssize > 200 ? cssize : 0);
            //            double _vcoeffs[cfsize > 200 ? 0 : cfsize];
            //            double _vconsts[cssize > 200 ? 0 : cssize];
            //            double *_coeffs = cfsize > 200 ? _ecoeffs.data() : _vcoeffs;
            //            double *_consts = cssize > 200 ? _econsts.data() : _vconsts;
            const double *cfp = coeffs.data();
            const double *csp = consts.data();
            copy_array_of_pointers_into_one(_coeffs, cfp, cfoffsets, cforder1);
            copy_array_of_pointers_into_one(_consts, csp, csoffsets, csorder1);
            hess(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, cfp, cfoffsets, cforder);
            copy_array_of_pointers_into_one(_consts, csp, csoffsets, csorder);
            f(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, cfp, cfoffsets, cforder);
            copy_array_of_pointers_into_one(_consts, csp, csoffsets, csorder);
            grad(res, _coeffs, _consts);
        }};
BENCHMARK_F(Setup, bm_flat_vla_split_ro_fast)(benchmark::State &state) {
    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            flat_vla_split_ro_fast(d0_d0_res.data(), coeffs_flat, consts_flat);
            benchmark::DoNotOptimize(d0_d0_res);
        }
    }
}

const std::function<void(double *, const decltype(coeffs_flat) &, const decltype(consts_flat) &)>
    flat_vla_split_ro_SMO{
        [cfsize = coeffs_flat.size(), cssize = consts_flat.size(),
         cforder1 = std::vector<int>{0, 1, 2}, csorder1 = std::vector<int>{0, 1, 2},
         cforder2 = std::vector<int>{1, 0, 2}, csorder2 = std::vector<int>{0, 2, 1},
         cforder = std::vector<int>{2, 1, 0}, csorder = std::vector<int>{2, 0, 1},
         cfoffsets = cf_default_offsets, csoffsets = cs_default_offsets](
            double *res, const decltype(coeffs_flat) &coeffs, const decltype(consts_flat) &consts) {
            //      alignas(32) double _coeffs[cfsize];
            //      alignas(32) double _consts[cssize];
            Eigen::ArrayXd _ecoeffs(cfsize > 200 ? cfsize : 0);
            Eigen::ArrayXd _econsts(cssize > 200 ? cssize : 0);
            double _vcoeffs[cfsize > 200 ? 0 : cfsize];
            double _vconsts[cssize > 200 ? 0 : cssize];
            double *_coeffs = cfsize > 200 ? _ecoeffs.data() : _vcoeffs;
            double *_consts = cssize > 200 ? _econsts.data() : _vconsts;
            const double *cfp = coeffs.data();
            const double *csp = consts.data();
            copy_array_of_pointers_into_one(_coeffs, cfp, cfoffsets, cforder1);
            copy_array_of_pointers_into_one(_consts, csp, csoffsets, csorder1);
            hess(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, cfp, cfoffsets, cforder);
            copy_array_of_pointers_into_one(_consts, csp, csoffsets, csorder);
            f(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, cfp, cfoffsets, cforder);
            copy_array_of_pointers_into_one(_consts, csp, csoffsets, csorder);
            grad(res, _coeffs, _consts);
        }};
BENCHMARK_F(Setup, bm_flat_vla_split_ro_SMO)(benchmark::State &state) {
    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            flat_vla_split_ro_SMO(d0_d0_res.data(), coeffs_flat, consts_flat);
            benchmark::DoNotOptimize(d0_d0_res);
        }
    }
}

const std::function<void(double *, const std::vector<cspan> &, const std::vector<cspan> &)>
    arg_vecs_split_vla{
        [cfsize = gu.size() + gu2.size() + gus.size(), cssize = k.size() + kk.size() + kk3.size(),
         cforder1 = std::vector<int>{2, 0, 1}, csorder1 = std::vector<int>{0, 2, 1},
         cforder = std::vector<int>{2, 1, 0}, csorder = std::vector<int>{2, 0, 1}](
            double *res, const std::vector<cspan> &coeffs, const std::vector<cspan> &consts) {
            double _coeffs[cfsize];
            double _consts[cssize];
            copy_array_of_pointers_into_one(_coeffs, coeffs, cforder1);
            copy_array_of_pointers_into_one(_consts, consts, csorder1);
            hess(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, coeffs, cforder);
            copy_array_of_pointers_into_one(_consts, consts, csorder);
            f(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, coeffs, cforder);
            copy_array_of_pointers_into_one(_consts, consts, csorder);
            grad(res, _coeffs, _consts);
        }};

BENCHMARK_F(Setup, bm_arg_vecs_split_vla)(benchmark::State &state) {
    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            arg_vecs_split_vla(d0_d0_res.data(), get_cspans(coeffs_flat.data(), cf_default_offsets),
                               get_cspans(consts_flat.data(), cs_default_offsets));
            benchmark::DoNotOptimize(d0_d0_res);
        }
    }
}

const std::function<void(double *, const double *const *, const double *const *)>
    arg_vecs_split_vla2{[cfsize = gu.size() + gu2.size() + gus.size(),
                         cssize = k.size() + kk.size() + kk3.size(), cf_sizes = cf_default_sizes,
                         cs_sizes = cs_default_sizes, cforder1 = std::vector<int>{2, 0, 1},
                         csorder1 = std::vector<int>{0, 2, 1}, cforder = std::vector<int>{2, 1, 0},
                         csorder = std::vector<int>{2, 0, 1}](
                            double *res, const double *const *coeffs, const double *const *consts) {
        double _coeffs[cfsize];
        double _consts[cssize];
        copy_array_of_pointers_into_one(_coeffs, coeffs, cf_sizes, cforder1);
        copy_array_of_pointers_into_one(_consts, consts, cs_sizes, csorder1);
        hess(res, _coeffs, _consts);

        copy_array_of_pointers_into_one(_coeffs, coeffs, cf_sizes, cforder);
        copy_array_of_pointers_into_one(_consts, consts, cs_sizes, csorder);
        f(res, _coeffs, _consts);

        copy_array_of_pointers_into_one(_coeffs, coeffs, cf_sizes, cforder);
        copy_array_of_pointers_into_one(_consts, consts, cs_sizes, csorder);
        grad(res, _coeffs, _consts);
    }};

BENCHMARK_F(Setup, bm_arg_vecs_split_vla2)(benchmark::State &state) {
    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            arg_vecs_split_vla2(d0_d0_res.data(), cf_ptrs.data(), cs_ptrs.data());
            benchmark::DoNotOptimize(d0_d0_res);
        }
    }
}

const std::function<void(double *, const DCSpan *, const DCSpan *)> arg_vecs_split_vla4{
    [cfsize = gu.size() + gu2.size() + gus.size(), cssize = k.size() + kk.size() + kk3.size(),
     cf_sizes = cf_default_sizes, cs_sizes = cs_default_sizes, cforder1 = std::vector<int>{2, 0, 1},
     csorder1 = std::vector<int>{0, 2, 1}, cforder = std::vector<int>{2, 1, 0},
     csorder = std::vector<int>{2, 0, 1}](double *res, const DCSpan *coeffs, const DCSpan *consts) {
        double _coeffs[cfsize];
        double _consts[cssize];
        copy_array_of_pointers_into_one(_coeffs, coeffs, cforder1);
        copy_array_of_pointers_into_one(_consts, consts, csorder1);
        hess(res, _coeffs, _consts);

        copy_array_of_pointers_into_one(_coeffs, coeffs, cforder);
        copy_array_of_pointers_into_one(_consts, consts, csorder);
        f(res, _coeffs, _consts);

        copy_array_of_pointers_into_one(_coeffs, coeffs, cforder);
        copy_array_of_pointers_into_one(_consts, consts, csorder);
        grad(res, _coeffs, _consts);
    }};

BENCHMARK_F(Setup, bm_arg_vecs_split_vla4)(benchmark::State &state) {
    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            arg_vecs_split_vla4(d0_d0_res.data(), cf_Spans.data(), cs_Spans.data());
            benchmark::DoNotOptimize(d0_d0_res);
        }
    }
}

BENCHMARK_F(Setup, bm_arg_vecs_split_vla5)(benchmark::State &state) {
    const auto cfsize = gu.size() + gu2.size() + gus.size();
    const auto cssize = k.size() + kk.size() + kk3.size();
    const auto cforder1 = std::vector<int>{2, 0, 1};
    const auto csorder1 = std::vector<int>{0, 2, 1};
    const auto cforder2 = std::vector<int>{2, 1, 0};
    const auto csorder2 = std::vector<int>{2, 0, 1};
    auto coeffs1 = [=](double *dest, const DCSpan *cf) {
        copy_array_of_pointers_into_one(dest, cf, cforder1);
    };
    auto coeffs2 = [=](double *dest, const DCSpan *cf) {
        copy_array_of_pointers_into_one(dest, cf, cforder2);
    };
    auto consts1 = [=](double *dest, const DCSpan *cs) {
        copy_array_of_pointers_into_one(dest, cs, csorder1);
    };
    auto consts2 = [=](double *dest, const DCSpan *cs) {
        copy_array_of_pointers_into_one(dest, cs, csorder2);
    };
    std::function<void(double *, const DCSpan *, const DCSpan *)> func{
        [=](double *res, const DCSpan *cf, const DCSpan *cs) {
            double _coeffs[cfsize];
            double _consts[cssize];
            coeffs1(_coeffs, cf);
            consts1(_consts, cs);
            hess(res, _coeffs, _consts);

            coeffs2(_coeffs, cf);
            consts2(_consts, cs);
            f(res, _coeffs, _consts);

            coeffs1(_coeffs, cf);
            consts1(_consts, cs);
            grad(res, _coeffs, _consts);
        }};

    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            DCSpan cf[3];
            DCSpan cs[3];
            update_CSpans(cf, coeffs_flat.data(), cf_default_offsets);
            update_CSpans(cs, consts_flat.data(), cs_default_offsets);
            func(d0_d0_res.data(), cf, cs);
            benchmark::DoNotOptimize(cf);
            benchmark::DoNotOptimize(cs);
            benchmark::DoNotOptimize(d0_d0_res);
        }
    }
}

const std::function<void(double *, const std::vector<DCSpan> &, const std::vector<DCSpan> &)>
    arg_vecs_split_vla3{
        [cfsize = gu.size() + gu2.size() + gus.size(), cssize = k.size() + kk.size() + kk3.size(),
         cf_sizes = cf_default_sizes, cs_sizes = cs_default_sizes,
         cforder1 = std::vector<int>{2, 0, 1}, csorder1 = std::vector<int>{0, 2, 1},
         cforder = std::vector<int>{2, 1, 0}, csorder = std::vector<int>{2, 0, 1}](
            double *res, const std::vector<DCSpan> &coeffs, const std::vector<DCSpan> &consts) {
            double _coeffs[cfsize];
            double _consts[cssize];
            copy_array_of_pointers_into_one(_coeffs, coeffs, cforder1);
            copy_array_of_pointers_into_one(_consts, consts, csorder1);
            hess(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, coeffs, cforder);
            copy_array_of_pointers_into_one(_consts, consts, csorder);
            f(res, _coeffs, _consts);

            copy_array_of_pointers_into_one(_coeffs, coeffs, cforder);
            copy_array_of_pointers_into_one(_consts, consts, csorder);
            grad(res, _coeffs, _consts);
        }};

BENCHMARK_F(Setup, bm_arg_vecs_split_vla3)(benchmark::State &state) {
    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            arg_vecs_split_vla3(d0_d0_res.data(), cf_Spans, cs_Spans);
            benchmark::DoNotOptimize(d0_d0_res);
        }
    }
}

const std::function<void(Eigen::Map<Eigen::Array<double, Eigen::Dynamic, 1>>,
                         const Eigen::Map<const Eigen::Array<double, Eigen::Dynamic, 1>> &,
                         const Eigen::Map<const Eigen::Array<double, Eigen::Dynamic, 1>> &)>
    flat_no_op2{[_hess = hess.func, _grad = grad.func, _f = f.func](
                    Eigen::Map<Eigen::Array<double, Eigen::Dynamic, 1>> res,
                    const Eigen::Map<const Eigen::Array<double, Eigen::Dynamic, 1>> &coeffs,
                    const Eigen::Map<const Eigen::Array<double, Eigen::Dynamic, 1>> &consts) {
        _hess(res.data(), coeffs.data(), consts.data());
        _grad(res.data(), coeffs.data(), consts.data());
        _f(res.data(), coeffs.data(), consts.data());
    }};

BENCHMARK_F(Setup, bm_flat_no_op2)(benchmark::State &state) {
    Eigen::Map<Eigen::Array<double, Eigen::Dynamic, 1>> _res(d0_d0_res.data(), d0_d0_res.size());
    const Eigen::Map<const Eigen::Array<double, Eigen::Dynamic, 1>> _coeffs(coeffs_flat.data(),
                                                                            d0_d0_res.size());
    const Eigen::Map<const Eigen::Array<double, Eigen::Dynamic, 1>> _consts(consts_flat.data(),
                                                                            d0_d0_res.size());
    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            flat_no_op2(_res, _coeffs, _consts);
            benchmark::DoNotOptimize(d0_d0_res);
        }
    }
}

const std::function<void(DSpan &, const DCSpan &, const DCSpan &)> flat_no_op3{
    [_hess = hess.func, _grad = grad.func, _f = f.func](DSpan &res, const DCSpan &coeffs,
                                                        const DCSpan &consts) {
        _hess(res.ptr, coeffs.ptr, consts.ptr);
        _grad(res.ptr, coeffs.ptr, consts.ptr);
        _f(res.ptr, coeffs.ptr, consts.ptr);
    }};

BENCHMARK_F(Setup, bm_flat_no_op3)(benchmark::State &state) {
    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            DSpan _res{d0_d0_res.data(), d0_d0_res.size()};
            const DCSpan _coeffs{coeffs_flat.data(), coeffs_flat.size()};
            const DCSpan _consts{consts_flat.data(), consts_flat.size()};
            flat_no_op3(_res, _coeffs, _consts);
            benchmark::DoNotOptimize(d0_d0_res);
            benchmark::DoNotOptimize(_res);
            benchmark::DoNotOptimize(_coeffs);
            benchmark::DoNotOptimize(_consts);
        }
    }
}

using VMap = Eigen::Map<Eigen::Array<double, Eigen::Dynamic, 1>>;
using VCMap = Eigen::Map<const Eigen::Array<double, Eigen::Dynamic, 1>>;

const std::function<void(VMap &, const VMap &, const VMap &)> flat_no_op4{
    [_hess = hess.func, _grad = grad.func, _f = f.func](VMap &res, const VMap &coeffs,
                                                        const VMap &consts) {
        _hess(res.data(), coeffs.data(), consts.data());
        _grad(res.data(), coeffs.data(), consts.data());
        _f(res.data(), coeffs.data(), consts.data());
    }};

BENCHMARK_F(Setup, bm_flat_no_op4)(benchmark::State &state) {
    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            VMap _res(d0_d0_res.data(), d0_d0_res.size());
            const VMap _coeffs(coeffs_flat.data(), coeffs_flat.size());
            const VMap _consts(consts_flat.data(), consts_flat.size());
            flat_no_op4(_res, _coeffs, _consts);
            benchmark::DoNotOptimize(d0_d0_res);
            benchmark::DoNotOptimize(_res);
            benchmark::DoNotOptimize(_coeffs);
            benchmark::DoNotOptimize(_consts);
        }
    }
}

const std::function<void(double *, const decltype(coeffs_flat) &, const decltype(consts_flat) &)>
    flat_no_op{
        [](double *res, const decltype(coeffs_flat) &coeffs, const decltype(consts_flat) &consts) {
            hess(res, coeffs.data(), consts.data());
            grad(res, coeffs.data(), consts.data());
            f(res, coeffs.data(), consts.data());
        }};
BENCHMARK_F(Setup, bm_flat_no_op)(benchmark::State &state) {
    for (auto _ : state) {
        for (int i = 0; i < N; ++i) {
            flat_no_op(d0_d0_res.data(), coeffs_flat, consts_flat);
            benchmark::DoNotOptimize(d0_d0_res);
        }
    }
}

//
// BENCHMARK_F(Setup, bm_arg_vecs_short)(benchmark::State &state) {
//    auto _coeff_vecs = std::vector<const std::vector<double> *>{&gu, &gu2, &gus};
//    auto _const_vecs = std::vector<const std::vector<double> *>{&k, &kk, &kk3};
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : gu) {
//                e -= 1;
//            }
//            for (auto &e : gu2) {
//                e -= 1;
//            }
//            for (auto &e : gus) {
//                e -= 1;
//            }
//
//            _coeff_vecs[N % 2] = &gu;
//            _coeff_vecs[N % 2 + 1] = &gu2;
//            _coeff_vecs[2] = &gus;
//            Eigen::ArrayXd coeffs(gu.size() + gu2.size() + gus.size());
//            Eigen::ArrayXd constants(k.size() + kk.size() + kk3.size());
//            utils::copy_array_of_pointers_into_one(coeffs, _coeff_vecs);
//            utils::copy_array_of_pointers_into_one(constants, _const_vecs);
//            hess(d0_d0_res.data(), coeffs.data(), constants.data());
//            benchmark::DoNotOptimize(d0_d0_res);
//
//            // no rearrangement
//            utils::copy_array_of_pointers_into_one(coeffs, _coeff_vecs);
//            utils::copy_array_of_pointers_into_one(constants, _const_vecs);
//            f(res.data(), coeffs.data(), constants.data());
//            benchmark::DoNotOptimize(res);
//        }
//    }
//}
//
// BENCHMARK_F(Setup, bm_arg_vecs)(benchmark::State &state) {
//    auto _coeff_vecs = std::vector<const std::vector<double> *>{&gu, &gu2, &gus};
//    auto _const_vecs = std::vector<const std::vector<double> *>{&k, &kk, &kk3};
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : gu)
//                e -= 1;
//            for (auto &e : gu2)
//                e -= 1;
//            for (auto &e : gus)
//                e -= 1;
//
//            _coeff_vecs[0] = &gu;
//            _coeff_vecs[1] = &gu2;
//            _coeff_vecs[2] = &gus;
//            Eigen::ArrayXd coeffs(gu.size() + gu2.size() + gus.size());
//            Eigen::ArrayXd constants(k.size() + kk.size() + kk3.size());
//            utils::copy_array_of_pointers_into_one(coeffs, _coeff_vecs);
//            utils::copy_array_of_pointers_into_one(constants, _const_vecs);
//            hess(d0_d0_res.data(), coeffs.data(), constants.data());
//            benchmark::DoNotOptimize(d0_d0_res);
//
//            _coeff_vecs[1] = &gu;
//            _coeff_vecs[0] = &gu2;
//            _coeff_vecs[2] = &gus;
//            utils::copy_array_of_pointers_into_one(coeffs, _coeff_vecs);
//            utils::copy_array_of_pointers_into_one(constants, _const_vecs);
//            f(res.data(), coeffs.data(), constants.data());
//            benchmark::DoNotOptimize(res);
//        }
//    }
//}
//// BENCHMARK(bm_arg_vecs)->Unit(benchmark::kMillisecond);
//
// BENCHMARK_F(Setup, bm_ptr_to_ptr_val_split)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : gu)
//                e += 1;
//            for (auto &e : gu2)
//                e += 1;
//            for (auto &e : gus)
//                e += 1;
//
//            double coeffs[gu.size() + gu2.size() + gus.size()];
//            double constants[k.size() + kk.size() + kk3.size()];
//            std::copy(begin(gu), end(gu), &coeffs[0]);
//            std::copy(begin(gu2), end(gu2), &coeffs[3]);
//            std::copy(begin(gus), end(gus), &coeffs[6]);
//            std::copy(begin(k), end(k), &constants[0]);
//            std::copy(begin(kk), end(kk), &constants[1]);
//            std::copy(begin(kk3), end(kk3), &constants[10]);
//            hess(d0_d0_res.data(), coeffs, constants);
//            benchmark::DoNotOptimize(d0_d0_res);
//
//            std::copy(begin(gu2), end(gu2), &coeffs[0]);
//            std::copy(begin(gu), end(gu), &coeffs[3]);
//            std::copy(begin(gus), end(gus), &coeffs[6]);
//            std::copy(begin(kk), end(kk), &constants[0]);
//            std::copy(begin(k), end(k), &constants[1]);
//            std::copy(begin(kk3), end(kk3), &constants[10]);
//            f(res.data(), coeffs, constants);
//            benchmark::DoNotOptimize(d0_res);
//        }
//    }
//}
//// BENCHMARK(bm_ptr_to_ptr_val_split)->Unit(benchmark::kMillisecond);
//
// BENCHMARK_F(Setup, bm_ptr_to_ptr_Eigen)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : gu)
//                e += 1;
//            for (auto &e : gu2)
//                e += 1;
//            for (auto &e : gus)
//                e += 1;
//
//            Eigen::ArrayXd coeffs_and_constants(49);
//            std::copy(begin(gu), end(gu), begin(coeffs_and_constants));
//            std::copy(begin(gu2), end(gu2), begin(coeffs_and_constants) + 3);
//            std::copy(begin(gus), end(gus), begin(coeffs_and_constants) + 6);
//            std::copy(begin(k), end(k), begin(coeffs_and_constants) + 12);
//            std::copy(begin(kk), end(kk), begin(coeffs_and_constants) + 13);
//            std::copy(begin(kk3), end(kk3), begin(coeffs_and_constants) + 22);
//            hess(d0_d0_res.data(), coeffs_and_constants.data(), coeffs_and_constants.data() + 12);
//            benchmark::DoNotOptimize(d0_d0_res);
//
//            std::copy(begin(gus), end(gus), begin(coeffs_and_constants));
//            std::copy(begin(gu2), end(gu2), begin(coeffs_and_constants) + 3);
//            std::copy(begin(gu), end(gu), begin(coeffs_and_constants) + 6);
//            std::copy(begin(kk), end(kk), begin(coeffs_and_constants) + 12);
//            std::copy(begin(k), end(k), begin(coeffs_and_constants) + 13);
//            std::copy(begin(kk3), end(kk3), begin(coeffs_and_constants) + 22);
//            f(res.data(), coeffs_and_constants.data(), coeffs_and_constants.data() + 12);
//            benchmark::DoNotOptimize(res);
//        }
//    }
//}
//// BENCHMARK(bm_ptr_to_ptr_Eigen)->Unit(benchmark::kMillisecond);
//
// BENCHMARK_F(Setup, bm_ptr_to_ptr_vla)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : gu)
//                e += 1;
//            for (auto &e : gu2)
//                e += 1;
//            for (auto &e : gus)
//                e += 1;
//
//            double coeffs_and_constants[gu.size() + gu2.size() + gus.size() + k.size() + kk.size()
//            +
//                                        kk3.size()];
//            std::copy(begin(all_flat), begin(all_flat) + 3, &coeffs_and_constants[0]);
//            std::copy(begin(all_flat) + 6, begin(all_flat) + 12, &coeffs_and_constants[6]);
//            std::copy(begin(all_flat) + 3, begin(all_flat) + 6, &coeffs_and_constants[3]);
//            std::copy(begin(all_flat) + 12, begin(all_flat) + 13, &coeffs_and_constants[12]);
//            std::copy(begin(all_flat) + 40, begin(all_flat) + 49, &coeffs_and_constants[13]);
//            std::copy(begin(all_flat) + 13, begin(all_flat) + 40, &coeffs_and_constants[22]);
//            hess(d0_d0_res.data(), &coeffs_and_constants[0], &coeffs_and_constants[12]);
//            benchmark::DoNotOptimize(d0_d0_res);
//
//            std::copy(begin(all_flat) + 3, begin(all_flat) + 6, &coeffs_and_constants[0]);
//            std::copy(begin(all_flat) + 6, begin(all_flat) + 12, &coeffs_and_constants[6]);
//            std::copy(begin(all_flat) + 0, begin(all_flat) + 3, &coeffs_and_constants[3]);
//            std::copy(begin(all_flat) + 48, begin(all_flat) + 49, &coeffs_and_constants[12]);
//            std::copy(begin(all_flat) + 12, begin(all_flat) + 21, &coeffs_and_constants[13]);
//            std::copy(begin(all_flat) + 21, begin(all_flat) + 48, &coeffs_and_constants[22]);
//            f(res.data(), &coeffs_and_constants[0], &coeffs_and_constants[12]);
//            benchmark::DoNotOptimize(res);
//        }
//    }
//}
//
// BENCHMARK_F(Setup, bm_ptr_to_ptr_vla_short)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : gu)
//                e += 1;
//            for (auto &e : gu2)
//                e += 1;
//            for (auto &e : gus)
//                e += 1;
//
//            double coeffs_and_constants[gu.size() + gu2.size() + gus.size() + k.size() + kk.size()
//            +
//                                        kk3.size()];
//            std::copy(begin(all_flat), begin(all_flat) + 3, &coeffs_and_constants[0]);
//            std::copy(begin(all_flat) + 6, begin(all_flat) + 12, &coeffs_and_constants[6]);
//            std::copy(begin(all_flat) + 3, begin(all_flat) + 6, &coeffs_and_constants[3]);
//            std::copy(begin(all_flat) + 12, begin(all_flat) + 13, &coeffs_and_constants[12]);
//            std::copy(begin(all_flat) + 40, begin(all_flat) + 49, &coeffs_and_constants[13]);
//            std::copy(begin(all_flat) + 13, begin(all_flat) + 40, &coeffs_and_constants[22]);
//            hess(d0_d0_res.data(), &coeffs_and_constants[0], &coeffs_and_constants[12]);
//            benchmark::DoNotOptimize(d0_d0_res);
//
//            f(res.data(), &coeffs_and_constants[0], &coeffs_and_constants[12]);
//            benchmark::DoNotOptimize(res);
//        }
//    }
//}
//// BENCHMARK(bm_ptr_to_ptr_vla)->Unit(benchmark::kMillisecond);
//
// BENCHMARK_F(Setup, bm_flat)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : coeffs_flat)
//                e += 1;
//            hess(d0_d0_res.data(), coeffs_flat.data(), consts_flat.data());
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}
//// BENCHMARK(bm_flat)->Unit(benchmark::kMillisecond);
//
// BENCHMARK_F(Setup, bm_flat_Eigen)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : coeffs_flat)
//                e += 1;
//            Eigen::ArrayXd _all_flat{coeffs_flat.size() + consts_flat.size()};
//            std::copy(begin(all_flat), begin(all_flat), begin(_all_flat));
//            hess(d0_d0_res.data(), _all_flat.data(), _all_flat.data() + coeffs_flat.size());
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}
//// BENCHMARK(bm_flat_Eigen)->Unit(benchmark::kMillisecond);
//
// BENCHMARK_F(Setup, bm_flat_Eigen_split)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : coeffs_flat)
//                e += 1;
//            Eigen::ArrayXd _coeffs_flat{coeffs_flat.size()};
//            Eigen::ArrayXd _consts_flat{consts_flat.size()};
//            std::copy(begin(all_flat), begin(all_flat) + 12, begin(_coeffs_flat));
//            std::copy(begin(all_flat) + 12, end(all_flat), begin(_consts_flat));
//            hess(d0_d0_res.data(), _coeffs_flat.data(), _consts_flat.data());
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}
//
// BENCHMARK_F(Setup, bm_flat_Eigen_fixed_split)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : coeffs_flat)
//                e += 1;
//            Eigen::Array<double, 12, 1> _coeffs_flat{};
//            Eigen::Array<double, 37, 1> _consts_flat{};
//            std::copy(begin(all_flat), begin(all_flat) + 12, begin(_coeffs_flat));
//            std::copy(begin(all_flat) + 12, end(all_flat), begin(_consts_flat));
//            hess(d0_d0_res.data(), _coeffs_flat.data(), _consts_flat.data());
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}
//
// BENCHMARK_F(Setup, bm_flat_stdarray_fixed_split)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : coeffs_flat)
//                e += 1;
//            std::array<double, 12> _coeffs_flat{};
//            std::array<double, 37> _consts_flat{};
//            std::copy(begin(all_flat), begin(all_flat) + 12, begin(_coeffs_flat));
//            std::copy(begin(all_flat) + 12, end(all_flat), begin(_consts_flat));
//            hess(d0_d0_res.data(), _coeffs_flat.data(), _consts_flat.data());
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}
//
// BENCHMARK_F(Setup, bm_flat_Eigen_map_split)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : coeffs_flat)
//                e += 1;
//            std::array<double, 12> _coeffs_flat{};
//            std::array<double, 37> _consts_flat{};
//            std::copy(begin(all_flat), begin(all_flat) + 12, begin(_coeffs_flat));
//            std::copy(begin(all_flat) + 12, end(all_flat), begin(_consts_flat));
//            hess(d0_d0_res.data(), _coeffs_flat.data(), _consts_flat.data());
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}
//
// BENCHMARK_F(Setup, bm_flat_vla)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : coeffs_flat)
//                e += 1;
//            double _all_flat[all_flat.size()];
//            std::copy(begin(all_flat), end(all_flat), &_all_flat[0]);
//            hess(d0_d0_res.data(), _all_flat, &_all_flat[coeffs_flat.size()]);
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}
//
// BENCHMARK_F(Setup, bm_flat_vla_split)(benchmark::State &state) {
//    for (auto _ : state) {
//        for (int i = 0; i < N; ++i) {
//            for (auto &e : coeffs_flat)
//                e += 1;
//            double _coeffs_flat[coeffs_flat.size()];
//            double _consts_flat[consts_flat.size()];
//            std::copy(begin(coeffs_flat), end(coeffs_flat), &_coeffs_flat[0]);
//            std::copy(begin(consts_flat), end(consts_flat), &_consts_flat[0]);
//            hess(d0_d0_res.data(), _coeffs_flat, _consts_flat);
//            benchmark::DoNotOptimize(d0_d0_res);
//        }
//    }
//}
// BENCHMARK(bm_flat_vla)->Unit(benchmark::kMillisecond);

BENCHMARK_MAIN();
