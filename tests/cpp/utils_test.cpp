//
// Created by rambausek on 16.07.19.
//

#include <materiaux.h>

#include "catch.hpp"

#include <iostream>

using namespace materiaux;

constexpr double tol = 1e-14;

// template<typename array_t, typename offsets_t>
// std::vector<const double *> make_pointers(const array_t &array, const
// offsets_t &offsets) {
//    std::vector<const double *> pointers;
//    pointers.reserve(std::tuple_size_v<offsets_t>);
//    materiaux::calling::set_pointers(array.data(), pointers, offsets);
//    return pointers;
//}

template <int n> struct constN { static constexpr int N = n; };

/// These tests check various array manipulations connected to splitting and
/// merging coefficient vectors/arrays. Such facilities are essential for the
/// internals of materiaux::calling::Function evaluations to work.
TEST_CASE("UtilsTest") {

  SECTION("arrays") {
    Eigen::VectorXd vec1(10);
    vec1 << 0, 1, 2, 3, 4, 5, 6, 7, 8, 9;
    Eigen::VectorXd vec2(6);
    vec2.setZero();
    Eigen::VectorXd vec3(6);
    vec3.setZero();

    auto source_offsets = std::vector<size_t>{0, 5, 10};
    auto target_offsets = std::vector<size_t>{0, 2, 6};
    auto sizes = std::vector<size_t>{{2, 4}};
    std::copy(vec1.data() + source_offsets[0],
              vec1.data() + source_offsets[0] + sizes[0],
              vec2.data() + target_offsets[0]);
    std::copy(vec1.data() + source_offsets[1],
              vec1.data() + source_offsets[1] + sizes[1],
              vec2.data() + target_offsets[1]);
    std::cout << std::endl << vec2.transpose();
    auto vec_update =
        materiaux::utils::create_field_update<Eigen::VectorXd, Eigen::VectorXd>(
            target_offsets, source_offsets);
    vec_update(vec3, vec1);
    std::cout << std::endl << vec3.transpose();
    CHECK(vec3.isApprox(vec2));

    std::vector<double> arr1{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::vector<double> arr2{0, 0, 0, 0, 0, 0};
    std::vector<double> arr3{0, 0, 0, 0, 0, 0};

    std::copy(arr1.data() + source_offsets[0],
              arr1.data() + source_offsets[0] + sizes[0],
              arr2.data() + target_offsets[0]);
    std::copy(arr1.data() + source_offsets[1],
              arr1.data() + source_offsets[1] + sizes[1],
              arr2.data() + target_offsets[1]);
    std::cout << std::endl;
    for (auto item : arr2)
      std::cout << item;
    auto arr_update =
        materiaux::utils::create_field_update<std::vector<double>,
                                              std::vector<double>>(
            target_offsets, source_offsets);
    arr_update(arr3, arr1);
    std::cout << std::endl;
    for (auto item : arr3)
      std::cout << item;
    for (std::size_t ii = 0; ii < arr2.size(); ++ii)
      CHECK(arr2[ii] == arr3[ii]);

    const std::vector<std::vector<double>*> ptrs1{&arr1, &arr2, &arr3};
    std::vector<std::vector<double>*> ptrs2(2);
    const std::vector<int> extract{2, 0};
    auto ptr_extraction = materiaux::utils::create_extraction<std::vector<std::vector<double>*>>(extract);
    ptr_extraction(ptrs2, ptrs1);
    for (size_t ii = 0; ii < size(extract); ++ii)
        CHECK(ptrs2[ii] == ptrs1[extract[ii]]);
  }

  SECTION("string_handling") {

    const std::string string_template{"${a_a}_${b_b}_${c_c}"};
    const std::vector<std::string> template_parts{"${a_a}", "${b_b}", "${c_c}"};

    std::string result = materiaux::utils::join("_", template_parts);
    CHECK(result == string_template);

    result = materiaux::utils::substitute(string_template, "a_a", "a");
    CHECK(result == "a_${b_b}_${c_c}");

    result = materiaux::utils::substitute(
        string_template, {{"a_a", "a"}, {"b_b", "b"}, {"c_c", "c"}});
    CHECK(result == "a_b_c");

    std::cout << "\n";
    auto vec = Vector<int>(result.size());
    vec.setZero();
    const std::function<void(int)> f1 = [counter = vec](int n) mutable -> void {counter(0) += n; std::cout << counter(0) << " ";};
    const std::function<void(int)> f2{f1};
    f1(1);
    f2(2);

  }
}