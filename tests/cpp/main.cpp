//
// Created by mrambausek on 9/16/20.
//

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

int main(int argc, char *argv[]) {
    return Catch::Session().run(argc, argv);
}