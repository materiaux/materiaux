//
// Created by rambausek on 15.07.19.
//

#include "catch.hpp"

#include "modules/gsm__test/gsm__test.h"
#include "modules/gsm__test_psi/gsm__test_psi.h"
#include "modules/gsm__test_psis/gsm__test_psis.h"

#include <materiaux.h>

#include <iostream>

constexpr double tol = 1e-14;

/// These tests check correctness of generated code and evaluation of
/// derivatives represented via templates. More complicated cases are covered by
/// test_ferromagnetics.py and test_viscoelasticity.py.
TEST_CASE("PsiTest") {

    SECTION("psi: nested evaluation") {

        using namespace gsm__test;

        std::vector<double> k{{1}};
        double k_d = 1;

        std::vector<double> kk{{1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0}};

        std::vector<double> gu{{1.0, 1.0, 1.0}};
        std::vector<double> gu2{{2.0, 2.0, 2.0}};
        std::vector<double> gu3{{3.0, 3.0, 3.0}};

        //        using potential =
        //        typename materiaux::derivations::DerivativeBase<typename
        //        gsm__test::psi::derivatives_psi>; using potential2 = typename
        //        materiaux::derivations::DerivativeBase<typename
        //        gsm__test::psi2::derivatives_psi2>;

        //        auto D0 =
        //        materiaux::derivations::create_function(materiaux::derivations::d<0>(potential{}));
        //        auto D0_2 =
        //        materiaux::derivations::create_function<materiaux::derivations::D<potential,
        //        0>>(); auto D00 =
        //        materiaux::derivations::create_function(materiaux::derivations::d<0,
        //        0>(potential{})); auto D00_2 =
        //        materiaux::derivations::create_function(materiaux::derivations::d<potential,
        //        0, 0>()); auto D00_4 =
        //        materiaux::derivations::create_function<materiaux::derivations::D<potential,
        //        0, 0>>();

        const auto psi = *psi::psi;

        CHECK(psi.code_data->code == "static const auto psi = gsm__test::psi::psi;");

        auto arg_map = std::map<std::string, const std::vector<double> *>{
            {"gu", &gu}, {"gu2", &gu2}, {"gu3", &gu3}, {"k", &k}, {"kk", &kk}};

        using array_t = Eigen::Array<typename decltype(psi)::scalar_t, Eigen::Dynamic, 1>;
        array_t res{psi.result_data->size};
        array_t expected{psi.result_data->size};
        expected << 3.0 / 2;
        res.setZero();
        array_t coefficients{active_coefficients_size(psi)};
        materiaux::utils::copy_array_of_pointers_into_one(
            coefficients, std::vector<const std::vector<double> *>{{&gu}},
            active_coefficients(psi));
        array_t constants{constants_size(psi)};
        materiaux::utils::copy_array_of_pointers_into_one(
            constants, std::vector<const std::vector<double> *>{{&k, &kk}});
        materiaux::call<double>(psi, res, coefficients, constants);
        CHECK(std::abs(res[0] - expected[0]) < tol);

        res.setZero();
        call(psi, res, arg_map);
        CHECK(std::abs(res[0] - expected[0]) < tol);

        const auto psi2 = *psi2::psi2;
        res.setZero();
        array_t coefficients2{active_coefficients_size(psi2)};
        materiaux::utils::copy_array_of_pointers_into_one(
            coefficients, std::vector<const std::vector<double> *>{{&gu}},
            active_coefficients(psi2));
        array_t constants2{constants_size(psi2)};
        materiaux::utils::copy_array_of_pointers_into_one(
            constants, std::vector<const std::vector<double> *>{{&kk, &k}});
        materiaux::call<double>(psi2, res, coefficients, constants);
        CHECK(std::abs(res[0] - expected[0]) < tol);

        res.setZero();
        auto safe_call = materiaux::safe_call<double>;
        safe_call(psi2, res, coefficients, constants);
        CHECK(std::abs(res[0] - expected[0]) < tol);
        CHECK_THROWS_AS(safe_call(psi2, coefficients, res, constants), std::invalid_argument);

        res.setZero();
        call(psi2, res, arg_map);
        CHECK(std::abs(res[0] - expected[0]) < tol);

        const auto duals = *psi::d0_psi;
        Eigen::ArrayXd d0_res(duals.result_data->size);
        d0_res.setZero();
        call(duals, d0_res, arg_map);
        CHECK(std::abs(d0_res[0] - k_d * kk[0] * gu[0]) < tol);
        CHECK(std::abs(d0_res[1] - k_d * kk[4] * gu[1]) < tol);
        CHECK(std::abs(d0_res[2] - k_d * kk[8] * gu[2]) < tol);

        const auto tangent = *psi::d0_d0_psi;
        Eigen::ArrayXd d0_d0_res(tangent.result_data->size);
        d0_d0_res.setZero();
        call(tangent, d0_d0_res, arg_map);
        for (std::size_t  ii = 0; ii < size(kk); ++ii)
            CHECK(std::abs(d0_d0_res[ii] - k_d * kk[ii]) < tol);

        const auto psi3 = *psi3::psi3;
        Eigen::ArrayXd res3(psi3.result_data->size);
        res3.setZero();
        call(psi3, res3, arg_map);
        CHECK(std::abs(res3[0] - 24) < tol);
    }

    SECTION("psi: symmetries") {
        std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>>
            no_symmetry_expected{{{{{}, {}}}}};
        CHECK(gsm__test_psi::psi->coefficient_data[0]->symmetry == no_symmetry_expected);

        using namespace gsm__test_psis;

        std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>> symmetry_expected{
            {{{{{1, 0}}, {{0, 1}}}, {{{2, 0}}, {{0, 2}}}, {{{2, 1}}, {{1, 2}}}}}};
        CHECK(gsm__test_psis::psis->coefficient_data[0]->symmetry == symmetry_expected);

        CHECK(gsm__test_psis::d0_psis->result_data->symmetry == symmetry_expected);

        std::vector<std::vector<std::pair<std::vector<size_t>, std::vector<size_t>>>> symmetry_expected_2{
            {{{{{1, 0}}, {{0, 1}}}, {{{2, 0}}, {{0, 2}}}, {{{2, 1}}, {{1, 2}}}},
             {{{{1, 0}}, {{0, 1}}}, {{{2, 0}}, {{0, 2}}}, {{{2, 1}}, {{1, 2}}}}}};

        CHECK(gsm__test_psis::d0_d0_psis->result_data->symmetry == symmetry_expected_2);
    }

    SECTION("field data manipulations") {
        auto psi = gsm__test_psi::psi;
        auto psis = gsm__test_psis::psis;
        const auto all_coefficients =
            materiaux::utils::merge(active_coefficients(psi), active_coefficients(psis));

        const auto selected = materiaux::utils::extract_fields(psis->coefficient_data,
                                                               std::vector<std::string>{"gus"});

        CHECK(all_coefficients.front() == active_coefficients(psi).front());
        CHECK(all_coefficients.back() == active_coefficients(psis).back());

        const auto all_coefficients_2 =
            materiaux::utils::merge({active_coefficients(psi), active_coefficients(psis)});

        CHECK(all_coefficients_2.front() == active_coefficients(psi).front());
        CHECK(all_coefficients_2.back() == active_coefficients(psis).back());

        //    std::cout << "all coefficients: ";
        //    for(const auto& fdp: all_coefficients)
        //        std::cout << fdp->name + "; ";
        //    std::cout << std::endl;
    }

    SECTION("lib handling") {
        std::vector<double> k{{1}};
        double k_d = 1;

        std::vector<double> kk{{1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0}};

        std::vector<double> gu{{1.0, 1.0, 1.0}};
        std::vector<double> gu2{{2.0, 2.0, 2.0}};
        std::vector<double> gu3{{3.0, 3.0, 3.0}};

        auto arg_map = std::map<std::string, const std::vector<double> *>{
            {"gu", &gu}, {"gu2", &gu2}, {"gu3", &gu3}, {"k", &k}, {"kk", &kk}};

        const materiaux::LibHandle lh(
            std::filesystem::path("../modules/gsm__test_psi/libgsm__test_psi.so"));
        auto contents = lh.contents();
        for (auto content : contents)
            std::cout << "(" << content.first << ", " << content.second.type().name() << "),\n";

        auto tangent = std::any_cast<materiaux::FunctionPtr<double>>(contents["gsm__test_psi::d0_d0_psi"]);
        Eigen::ArrayXd d0_d0_res(tangent->result_data->size);
        d0_d0_res.setZero();
        call(*tangent, d0_d0_res, arg_map);
        for (std::size_t ii = 0; ii < size(kk); ++ii)
            CHECK(std::abs(d0_d0_res[ii] - k_d * kk[ii]) < tol);

        const auto lh2 = std::make_shared<materiaux::LibHandle>(
            std::filesystem::path("../modules/gsm__test_psis/libgsm__test_psis.so"));
        contents = lh2->contents();
        for (auto content : contents)
            std::cout << "(" << content.first << ", " << content.second.type().name() << "),\n";
    }
}